trigger AutoCreateAssets on Request__c (after update) {
//private String[] splitField(String value)
//{
private String[] splitField(String value)
{
   //value = value.replaceAll('[]','');
   return value.split(';');
}
Set<String> allAssetsNames = new Set<String>();
List<List<String>> AssetsNamesByOverview = new List<List<String>>();

if(TriggerHandler.firstRun) { 
for (Request__c newReq : Trigger.New) 
{
 Request__c oldReq = Trigger.oldMap.get(newReq.ID);  
 if (newreq.status__c == 'Open' && oldreq.status__c != 'Open') {
   String[] AssetsNames = splitField(newReq.Software_Products__c);
   allAssetsNames.addAll(AssetsNames);
   AssetsNamesByOverview.add(AssetsNames);


   List<Asset> subs = new List<Asset>();
   for (Integer i = 0; i < Trigger.New.size(); i++) 
   {
    Request__c newReqract = Trigger.New[i];
    Contact newContact = new Contact();
    Account newAccount = new Account();
    String[] AssetsNames2 = AssetsNamesByOverview[i];

    //String ewhoid = newReqract.Requested_For__c;
    //List<contact> ccount = new List<contact>();
    //ccount  = [select name from contact where id = :ewhoid  limit 1];     
        
    for(String subAssetsNames : AssetsNames2)
    {    
        //Id prodId = newProdIdsByName.get(subAssetsNames);
        String ewhoid = newReqract.Requested_For__c;

   List<Product2> newProdIdsByName = new List<Product2>();
   List<User> newsubmitter = new List<User>();
   List<User> newManager = new List<User>();
   newProdIdsByName = [select id, Name, TMU_Manager_Approval__c from product2 where Name = :subAssetsNames ]; 
   
   id appid = null; 
   id subid = null;
   
   if (newProdIdsByName[0].TMU_Manager_Approval__c) {
      newManager = [select id from User where email = 'rajiv_prabhakar@calpers.ca.gov']; 
      appid = newManager[0].Id;
      newsubmitter = [select id from User where email = 'carson_farnsworth@calpers.ca.gov'];
      subid = newsubmitter[0].Id;    
   }   
   
   string subprodname = null;
   string subprodlist = null;
   
   if (subAssetsNames == 'Matlab') {
       if (newReqract.MatLab_Sub_Product_List__c != null) {
         subprodname = 'MatLab';
         subprodlist = NewReqract.MatLab_Sub_Product_List__c;
       }
    
   }
   
   if (subAssetsNames == 'Factset') {
       if (newReqract.Factset_Sub_Product_List__c != null) {
         subprodname = 'Factset';
         subprodlist = NewReqract.Factset_Sub_Product_List__c;
       }
    
   }     
   
   if (subAssetsNames == 'Aladdin') {
       if (newReqract.Aladdin__c != null) {
         subprodname = 'Aladdin';
         subprodlist = NewReqract.Aladdin__c;
       }
    
   }
   
   if (subAssetsNames == 'CRIMS v9') {
       if (newReqract.crims_roles__c != null) {
         subprodname = 'CRIMS';
         subprodlist = NewReqract.crims_roles__c;
       }
    
   }         
   
            Asset ssoc = new Asset(
            Name = subAssetsNames,
            Request__c = newReqract.id,
            Product2id = newProdIdsByName[0].Id,
            Contactid = ewhoid,
            Approver__c = appid,
            Submitter__c = subid,
            Sub_Product_Name__c = subprodname,
            Sub_Products__c = subprodlist, 
            Status = 'Pending Install');

        subs.add(ssoc);
    }
   }
System.debug(subs) ;

   insert subs;
}
}
   TriggerHandler.firstRun = false;
   }

}