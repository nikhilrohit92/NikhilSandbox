trigger AddCampaignMemtoCld on CampaignMember (before insert) {
  //nikhil did this  
    Set<ID> CampIDs=new  Set<ID>();
    List<CampaignMember> CMemberToInsert=new List<CampaignMember>();
    for(CampaignMember cm: Trigger.New)
    {
    if(cm.campaignID !=null)   
        CampIDs.add(cm.campaignID);   
    }
    Map<ID, Campaign> ParentCampaignsWithChilds=new  Map<ID, Campaign>([Select name, (Select id, name, parentID from ChildCampaigns) from Campaign where ID IN : CampIDs AND  parentID =null]);

    for(CampaignMember cm: Trigger.New)
    {
        if(ParentCampaignsWithChilds.containsKey(cm.CampaignId))
        {
            
            for(Campaign CCamp: ParentCampaignsWithChilds.get(cm.CampaignId).childCampaigns)
            {
                CampaignMember camMem=new CampaignMember();
                CamMem.campaignID=CCamp.ID;
                if(cm.Type=='Lead')
                {
                   CamMem.LeadID= cm.LeadID;
                }
                else  if(cm.Type=='Contact')
                {
                    CamMem.ContactID=cm.ContactID;
                    
                }
                CMemberToInsert.add(CamMem);
                
            }
        }
    }
            
            if(!CMemberToInsert.isEmpty()) insert CMemberToInsert;
            
        }