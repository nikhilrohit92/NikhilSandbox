trigger Opportunity on Opportunity (after insert, after update) {

	MKP011.Dynamic_Sharing d = new MKP011.Dynamic_Sharing();
	d.addSharing(trigger.new,'Opportunity','OpportunityShare');

	
	if ( trigger.isAfter && trigger.isUpdate ){
		opportunity_Methods.afterUpdate(trigger.new, trigger.old);
	}
}