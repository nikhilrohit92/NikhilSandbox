Trigger CCRequestStatusNotify on Change_control_request__c (after update) {

  List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>(); 
 //List<user> Usr=new List<user> ([SELECT email, name FROM user WHERE Id = : UserInfo.getUserId()]);
   //Usr = [SELECT email, name FROM User WHERE Id = : UserInfo.getUserId()]; 
   String loginuserEmail=UserInfo.getUseremail();
    Map<ID, ProcessInstance> ApprovalMap=new Map<ID, ProcessInstance>();
    for(ProcessInstance PI: [SELECT Status,TargetObjectID, (SELECT Id, StepStatus, Comments,processInstance.lastActor.Name FROM Steps where processInstance.status='approved' OR processInstance.status='Rejected') FROM ProcessInstance where TargetObjectId IN :Trigger.NewMap.keyset()])    
    {
        ApprovalMap.put(PI.TargetObjectID, PI);
        
        }
  for (Change_control_request__c ccRequest : Trigger.new) {
    if(ccRequest.Status__c!=trigger.oldmap.get(ccRequest.id).Status__c &&(ccRequest.Status__c=='Approved' || ccRequest.Status__c=='Denied'))
      {
           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
          List<String> sendTo = new List<String>();
          sendTo.add(ccRequest.Initiator_E_mail__c );
          mail.setToAddresses(sendTo);    
          mail.setReplyTo(loginuserEmail);
         // mail.setSenderDisplayName(UserInfo.getUserfullname());
        
          List<String> ccTo = new List<String>();
          if(ccRequest.CC_Email__c !=null && ccRequest.CC_Email__c !='') {ccTo.addAll(ccRequest.CC_Email__c.split(','));}
          if(!ccTo.isempty()) {    mail.setCcAddresses(ccTo); }

          mail.setSubject('Change control request Status');
       string body='<HTML>';
        if(ccRequest.Status__c=='Approved') {
        mail.setSubject('Change Control      '+    ccRequest.Name  + '--' + ccRequest.Request_Title__c  +'     Status is Now  '+  ccRequest.Status__c);
      //  body +=         'Title: Change Control      '+    ccRequest.Name  + '--' + ccRequest.Request_Title__c  +'     Status is Now  '+  ccRequest.Status__c + '</br> </br>';
            body += 'Request: ';
           body+= ccRequest.Name +' - ';
            body+=ccRequest.Request_Title__c +'<br/>';
            body += ' Status: Changed to  ';
           body+=ccRequest.Status__c + '<br/>';
            //body += '  By:  ';
           // body+=ccRequest.LastModifiedBy__c +'<br/>';
            body += ' Date: ';
             body+=ccRequest.LastModifiedDate__c +'<br/>';
                       if(ApprovalMap.containskey(ccRequest.Id))
             {
               if(ApprovalMap.get(ccRequest.Id).steps.size()>0)
                 {
                        body += ' Approver Name: ';
                        body+=ApprovalMap.get(ccRequest.Id).steps[0].processInstance.lastActor.Name +'<br/>';  
                     
                            body += ' Approver Comments: ';
                        body+=ApprovalMap.get(ccRequest.Id).steps[0].comments +'<br/>';                     
                
                 }
                 
                 
             }
           // body+='<a href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/'+ccRequest.id+'">'+ccRequest.Name+'</a>';
            body+='</HTML>';}
         else if(ccRequest.Status__c=='Denied') {
            mail.setSubject('Change Control      '+    ccRequest.Name  + '--' + ccRequest.Request_Title__c  +'     Status is Now   '+  ccRequest.Status__c);
           // body +=         'Title: Change Control      '+    ccRequest.Name  + '--' + ccRequest.Request_Title__c  +'      Status is Now  '+  ccRequest.Status__c + '</br> </br>';
            body += 'Request: ';
           body+= ccRequest.Name +' - ';
            body+=ccRequest.Request_Title__c +'<br/>';
            body += ' Status: Changed to  ';
           body+=ccRequest.Status__c + '<br/>';
           // body += '  By:  ';
           // body+=ccRequest.LastModifiedBy__c +'<br/>';
            body += ' Date: ';
             body+=ccRequest.LastModifiedDate__c +'<br/>';
           if(ApprovalMap.containskey(ccRequest.Id))
             {
            if(ApprovalMap.get(ccRequest.Id).steps.size()>0)
                 {
                        body += ' Approver Name: ';
                        body+=ApprovalMap.get(ccRequest.Id).steps[0].processInstance.lastActor.Name +'<br/>';  
                     
                            body += ' Approver Comments: ';
                        body+=ApprovalMap.get(ccRequest.Id).steps[0].comments +'<br/>';                     
                
                 }
                 
                 
             }
          //  body+='<a href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/'+ccRequest.id+'">'+ccRequest.Name+'</a>';
            body+='</HTML>';}
          mail.setHtmlBody(body);
      //  PageReference pagePdf = new PageReference('/apex/changecontrolv3?id='+ccRequest.id); 
   // Blob pdfPageBlob; pdfPageBlob = pagePdf.getContentAsPDF(); 
   // Attachment a = new Attachment(); 
  //  a.Body = pdfPageBlob;
   // a.Name = 'CCRequestForm.pdf'; 
    
    //List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
  //Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
 // efa.setFileName(a.Name);
  //efa.setBody(a.Body);
  //fileAttachments.add(efa);
 //mail.setFileAttachments(fileAttachments);
         mails.add(mail);
        }
    
  }
  Messaging.sendEmail(mails);
}