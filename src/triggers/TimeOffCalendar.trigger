trigger TimeOffCalendar on Time_Off_Request__c (after update) {

  List<Event> lstNewEvents = new List<Event>(); 


       // if(acc.AccountNumber != oldAccount.AccountNumber) {


  for (Time_Off_Request__c eve : Trigger.new) 
  {
    Time_Off_Request__c oldeve = Trigger.oldMap.get(eve.ID);  
    if (eve.status__c == 'Approved' && oldeve.status__c != 'Approved') {
        Event e = new Event();

        //e.StartDateTime = eve.Start_Date__c;
        //e.EndDateTime = eve.End_Date__c;

		String ewhoid = eve.requestor__c;
		List<contact> ccount = new List<contact>();
		ccount  = [select name from contact where id = :ewhoid  limit 1];     
		e.Subject = ccount[0].name + ': ' + eve.Leave_hours__c + ' Hours ' + eve.Leave_Type__c ;
        
        //e.Subject = eve.Leave_Type__c + ' ' +eve.Leave_hours__c + ' Hours';
        //e.whoid = eve.requestor__c;
        e.OwnerId = '00580000005uV8S';
        e.description = eve.Status__c;
        e.whatid = eve.id;      
        
        //date t_startdate = datevalue(eve.Start_Date__c);
        //date t_enddate = eve.End_Date__c;
        //&& datevalue(eve.Start_Date__c) == datevalue(eve.Start_Date__c)
        
        if (eve.Leave_hours__c != 8 )  {
        	e.StartDateTime = eve.Start_Date__c;
         	e.EndDateTime = eve.End_Date__c;
         	if (eve.Leave_hours__c > 8 ) {
         	e.ShowAs = 'Free';	
         	}
        }        
        if (eve.Leave_hours__c == 8 )  {
        	e.StartDateTime = eve.Start_Date__c;
         	e.durationinminutes = 1440;
        	e.IsAllDayEvent = true;
        	e.ShowAs = 'Free';
        }        
        //if (datevalue(eve.Start_Date__c) != datevalue(eve.Start_Date__c))  {
        	//e.StartDateTime = eve.Start_Date__c;
        	//e.EndDateTime = eve.End_Date__c;
      		//e.ShowAs = 'Free';
        //}
        
        lstNewEvents.add(e);
  }

        insert lstNewEvents;
    }  
 
  for (Time_Off_Request__c eve : Trigger.new) 
  {
    Time_Off_Request__c oldeve = Trigger.oldMap.get(eve.ID);  
    if (eve.status__c == 'Recalled/Dismissed' && oldeve.status__c == 'Approved') {
        
String ewhatid = eve.id;

List<event> lcount = new List<event>();

Lcount  = [select id from event  where whatid = :ewhatid  limit 1];

if (lcount.size() == 1) 
{
        Event e = new Event();        
        e.id = Lcount[0].id ;      
        lstNewEvents.add(e);
}         
        delete lstNewEvents;
    }    
}   
    
}