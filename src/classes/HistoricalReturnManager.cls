//Helper class to manage historical returns
public with sharing class HistoricalReturnManager {
    public List<HistoricalSummaryWrapper> historicalReturns {get; set;}
  //  public Integer pointer {get; set;}
    public HistoricalSummaryWrapper historicalReturn {get; set;}
    public ErrorHandler ehandler {get; set;}
    public String startDateOfTrackRecord {get; set;}
    public String asOfDate {get; set;}
    public String context {get; set;}
    public static final String LISTCONTEXT = 'LISTCONTEXT';
    public static final String ADDCONTEXT = 'ADDCONTEXT';
    public static final String EDITCONTEXT = 'EDITCONTEXT';
    public List<SelectOption> investmentVehicleOptions {get; set;}
    public List<SelectOption> propTypeOptions {get; set;}
    public List<CheckboxWrapper> multiselectPropertyTypeOptions {get; set;}
    public List<CheckboxWrapper> multiselectInvestmentVehicleOptions {get; set;}
    
    public class HistoricalSummaryWrapper {
    	
        public Historical_Summary__c  areturn {get; set;}
        public Integer listPosition {get; set;}
        
        public HistoricalSummaryWrapper() { 
            areturn = new Historical_Summary__c();
            listPosition = -1;
        }
        
        public String getGrossMultiple() {
            return (areturn.Gross_Multiple__c != null) ? String.valueOf(areturn.Gross_Multiple__c) : Constants.NO_RESPONSE_PROVIDED;
        }
        
        public String getNetMultiple() {
            return (areturn.Net_Multiple__c != null) ? String.valueOf(areturn.Net_Multiple__c) : Constants.NO_RESPONSE_PROVIDED;
        }
        
        public String getGrossIRR() {
            return (areturn.Gross_IRR__c != null) ? String.valueOf(areturn.Gross_IRR__c) : Constants.NO_RESPONSE_PROVIDED;
        }
        
        public String getNetIRR() {
            return (areturn.Net_IRR__c != null) ? String.valueOf(areturn.Net_IRR__c) : Constants.NO_RESPONSE_PROVIDED;
        }
        public String getNetDPI() {
            return (areturn.Net_DPI__c != null) ? String.valueOf(areturn.Net_DPI__c) : Constants.NO_RESPONSE_PROVIDED;
        }
        
        public string wrapperAsOfDate{
    		
    	get{
    		
    		String returnThis='';
    		
    		if(areturn.as_of_date__c!=null)
    		returnThis = utilities.toDateString(areturn.as_of_date__c);
    		return returnThis;
    		}
    	set{
    		
    		wrapperAsOfDate = value;
    		
    		areturn.as_of_date__c = date.parse(value);
    	//	system.debug('***** areturn as of date' + areturn.as_of_date__c + '*****' );
    	}
    	}
    }
    
    public HistoricalReturnManager() {
     //   pointer = -1; //EOF
        historicalReturns = new List<HistoricalSummaryWrapper>();
        historicalReturn = new HistoricalSummaryWrapper();
        AsOfDate = null;
        startDateOfTrackRecord = null;
        context = LISTCONTEXT;
        ehandler = new ErrorHandler();

        propTypeOptions = PicklistManager.getRealEstatePropertyType();
        multiselectPropertyTypeOptions = CheckboxWrapper.convertOptions(propTypeOptions);

        investmentVehicleOptions = PicklistManager.getRealEstateInvestmentVehicle();
        multiselectInvestmentVehicleOptions = CheckboxWrapper.convertOptions(investmentVehicleOptions);
        
        trackRecordReturnsOptions = PicklistManager.gettrackRecordReturnsOptions();
    }

  

    public PageReference saveAIMHistoricalReturn() {
        ehandler.clearMessages();
        if (!HistoricalReturnValidator.isValidAIMPartnershipHistoricalReturn(historicalReturn.areturn, Utilities.toDateString(historicalReturn.areturn.as_Of_Date__c), ehandler))
            return null;
        if (context == ADDCONTEXT) {
          //  ++pointer;
          //  historicalReturn.listPosition = pointer;
             historicalReturn.listPosition = historicalReturns.size();
            historicalReturns.add(historicalReturn);
            newHistoricalReturn();
        }
        context = LISTCONTEXT;
        return null;
    }
    
    public PageReference addMoreAIMHistoricalReturn() {
        saveAIMHistoricalReturn();
        context = ADDCONTEXT;
        return null;
    }


    private void setInvestmentVehicle() {
        String investmentVehicleString = Utilities.getSelectedString(multiselectInvestmentVehicleOptions);
        if (investmentVehicleString != null && investmentVehicleString.length() > 0)
            historicalReturn.areturn.Investment_Vehicle__c = investmentVehicleString;
    }

    private void setPropertyType() {
        String propertyTypeString = Utilities.getSelectedString(multiselectPropertyTypeOptions);
        if (propertyTypeString != null && propertyTypeString.length() > 0)
            historicalReturn.areturn.Property_Type__c = propertyTypeString;
    }


    public PageReference saveRealEstateHistoricalReturn() {
        ehandler.clearMessages();
        setInvestmentVehicle();
        setPropertyType();
        if (!HistoricalReturnValidator.isValidRealEstateHistoricalReturn(historicalReturn.areturn, startDateOfTrackRecord, Utilities.toDateString(historicalReturn.areturn.as_Of_Date__c), ehandler))
            return null;
        if (context == ADDCONTEXT) {
         //   ++pointer;
          
          //  historicalReturn.listPosition = pointer;
              historicalReturn.listPosition = historicalReturns.size();
            historicalReturns.add(historicalReturn);
            newHistoricalReturn();
        }
        context = LISTCONTEXT;
        return null;
    }

    public PageReference addMoreRealEstateHistoricalReturn() {
        saveRealEstateHistoricalReturn();
        context = ADDCONTEXT;
        return null;
    }

    public PageReference saveInfrastructurePartnershipHistoricalReturn() {
        ehandler.clearMessages();
        if (!HistoricalReturnValidator.isValidInfrastructurePartnershipHistoricalReturn(historicalReturn.areturn, Utilities.toDateString(historicalReturn.areturn.as_Of_Date__c), ehandler))
            return null;
        if (context == ADDCONTEXT) {
          //  ++pointer;
            //historicalReturn.listPosition = pointer;
               historicalReturn.listPosition = historicalReturns.size();
            historicalReturns.add(historicalReturn);
            newHistoricalReturn();
        }
        context = LISTCONTEXT;
        return null;
    }
    
    public PageReference addMoreInfrastructureHistoricalReturn() {
        saveInfrastructurePartnershipHistoricalReturn();
        context = ADDCONTEXT;
        return null;
    }
    
    public PageReference addHistoricalReturn() {
        if (context == ADDCONTEXT && HistoricalReturnValidator.isValidHistoricalReturn(historicalReturn.areturn)) {
           // ++pointer;
           // historicalReturn.listPosition = pointer;
              historicalReturn.listPosition = historicalReturns.size();
            historicalReturns.add(historicalReturn);
            newHistoricalReturn();
        }
        context = LISTCONTEXT;
        return null;
    }
    
    public PageReference addMoreHistoricalReturn() {
        addHistoricalReturn();
        context = ADDCONTEXT;
        return null;
    }
    
    public PageReference cancelHistoricalReturn() {
        newHistoricalReturn();
        context = LISTCONTEXT;
        return null;
    }
    
    public PageReference newHistoricalReturn() {
        ehandler.clearMessages();
        historicalReturn = new HistoricalSummaryWrapper();
        AsOfDate = null;
        startDateOfTrackRecord = null;
        context = ADDCONTEXT;
        return null;
    }
    
    public PageReference editHistoricalReturn() {
        ehandler.clearMessages();
        String ndx = ApexPages.currentPage().getParameters().get('return.index');
        System.debug('**** line item ****' + ndx);
        Integer location = Integer.valueOf(ndx);
        for (HistoricalSummaryWrapper w : historicalReturns) {
            if (w.listPosition == location) {
                historicalReturn = w;
             //   system.debug('*****historicalReturn*****' + historicalReturn );
                break;
            }
        }
        context = EDITCONTEXT;
        return null;
    }
    
    public PageReference removeHistoricalReturn() {
        ehandler.clearMessages();
        String ndx = ApexPages.currentPage().getParameters().get('return.index');
        system.debug('*** parameter ***' + ndx);
        Integer location = Integer.valueOf(ndx);
        Integer toDeleteIndex = -1;
        for (Integer i=0; i<historicalReturns.size(); i++) {
            HistoricalSummaryWrapper w = historicalReturns.get(i);
            if (w.listPosition == location) {
                toDeleteIndex = i;
                break;
            }
        }
        if (toDeleteIndex != -1){
 			
 			
 			HistoricalSummaryWrapper temp = historicalReturns[toDeleteIndex];           
         //   system.debug('***Historical Returns ***' + historicalReturns[toDeleteIndex].areturn );
            historicalReturns.remove(toDeleteIndex);
      		
      		if(temp.areturn != null){
      		
      			List<Historical_Summary__c> tempRecord = [Select id from Historical_Summary__c where id = :temp.areturn.id limit 1];
      			if(tempRecord.size()>0){
		      		delete tempRecord;
		      	}
		      }
		      for (Integer i=0; i<historicalReturns.size(); i++) {
		           HistoricalSummaryWrapper w = historicalReturns.get(i);
			       w.listPosition = i;
        		}
               //historicalReturn.listPosition = historicalReturns.size();
            }
        context = LISTCONTEXT;
        return null;
    }
    
    public Boolean getHasReturns() {
        return (!historicalReturns.isEmpty()) ? true : false;
    }
    
    public Boolean getIsListContext() {
        return (context == LISTCONTEXT) ? true : false;
    }
    
    public Boolean getIsAddContext() {
        return (context == ADDCONTEXT) ? true : false;
    }
    
    public Boolean getIsEditContext() {
        return (context == EDITCONTEXT) ? true : false;
    }

    public List<SelectOption> trackRecordReturnsOptions {get;set;}
}