public with sharing class PrintAsps {

    public String tempCampaignName { get; set; }
    public List<proposal_question__c> ASPcustomrec { get; set; }
    public List<ASP_Component__c> ASPcomprecAll { get; set; }
    public id     tempCampaignId  {get;set;}
    public Opportunity tempproposal ;
    public Opportunity tproposal ;
    public ASP_Component__c ASPcompliance;
    public PrintAsps() {
    
    
    }
    
    public PageReference asp_getid(){

        //String idParam = apexPages.currentPage().getParameters().get('id');
        tempCampaignId = apexPages.currentPage().getParameters().get('id'); //'7015600000091S3';
        tempCampaignName = [select name from Campaign where id = :tempCampaignId].Name;
        //Opportunity tempproposal = utilities.queryOpportunityById('00656000002ABpQ'); 
        //tproposal = [select id, name from opportunity where id = '00656000002ABpQ'];     
        ASPcomprecAll = [Select Id, Title__c, TextValue__c, type__c, Page_No__c, Proposal_Name_Page__c from ASP_Component__c where ASP_ID__c = :tempCampaignId and Active__c = true
        and (type__c = :Constants.FREEFORM or type__c = :Constants.CUSTOM or type__c = :Constants.CUSTOM_ACK or type__c = :Constants.PROPOSAL or type__c = :Constants.FREEFORM_ACK or 
        type__c = :Constants.COMPLIANCE) order by Page_No__c, Order__c asc];        
        
        return Page.PrintASP;
    }
        
    public Component.Apex.OutputText getspace() {
        Component.Apex.OutputText osp = new Component.Apex.OutputText ();
        osp.value = '';//'-------------------------------------------------------------------------------------------------------------------';
        osp.style='display:block';
        return osp;
    }         

    public Component.Apex.OutputText getack() {
        Component.Apex.OutputText ack = new Component.Apex.OutputText ();
        ack.value = 'Acknowledgement';
        ack.style='display:block';
        return ack;
    }         

    public Component.Apex.Image getimage() {
        Component.Apex.Image img = new Component.Apex.Image ();
        img.value = '/img/calpers-logo.png';
       // img.style='display:block';
       img.width='220';
       img.height='55';
        return img ;
    }  
    
    public Component.Apex.Form FormInfo() {
        Component.Apex.Form fmInfo = new Component.Apex.Form ();
        fmInfo.id = 'theform'; //hname;
        fmInfo.style='border:0px;background-color: #dddddd;height: 2px;color: #dddddd; ';
     
        return fmInfo;
    }      

    public Component.Apex.OutputText getUInfo3(string hname) {
        Component.Apex.OutputText hInfo = new Component.Apex.OutputText ();
        hInfo.value = '<h4><u>'+hname+'</u></h4>'; //hname;
        hInfo.style='display:block;margin-bottom:25px';
        hinfo.escape = false;
        
        return hinfo;
    } 
          
    public Component.Apex.OutputText getInfoU(string hname) {
        Component.Apex.OutputText hInfo = new Component.Apex.OutputText ();
        hInfo.value = '<u>'+hname+'</u>'; //hname;
        hInfo.style='display:block;margin-bottom:25px';
        hinfo.escape = false;
        
        return hinfo;
    } 

    public Component.Apex.OutputText getHeaderInfo3(string hname) {
        Component.Apex.OutputText hInfo = new Component.Apex.OutputText ();
        hInfo.value = '<h3>'+hname+'</h3>'; //hname;
        hInfo.style='display:block;margin-bottom:25px';
        hinfo.escape = false;
        
        return hinfo;
    } 
        public Component.Apex.OutputText getInfo(string fname) {
        Component.Apex.OutputText firmInfo = new Component.Apex.OutputText ();
        firmInfo.value = fname;
        firmInfo.style='display:block;margin-bottom:25px';
        
        return firmInfo;
    }    

        public Component.Apex.OutputText getInfoB(string fname) {
        Component.Apex.OutputText firmInfo = new Component.Apex.OutputText ();
        firmInfo.value = '<b>'+fname+'</b>';
        firmInfo.style='display:block;margin-bottom:25px';
        firmInfo.escape = false;
        
        return firmInfo;
    }   
    
    public Component.Apex.OutputPanel JointVenture() {
        Component.Apex.OutputPanel fsubpanel = new Component.Apex.OutputPanel ();
        //fsubpanel.childComponents.add(getHeaderInfo3('Joint Ventures'));
        //fsubpanel.childComponents.add(getInfo (tproposal.name));
        Component.Apex.OutputText ftext = new Component.Apex.OutputText ();
        ASPcompliance = [Select Id, name, TextValue__c, type__c, Page_No__c, Proposal_Name_Page__c from ASP_Component__c where ASP_ID__r.name = 'System' and name ='JointVenturePage' limit 1];
        ftext.escape =false;
        fsubpanel.childComponents.add(ftext);
        ftext.value = ASPcompliance.TextValue__c;        
        
        /*fsubpanel.childComponents.add(getInfo ('Are you submitting a proposal as a Joint Venturer?'));
        fsubpanel.childComponents.add(getInfo ('If Yes:'));
        fsubpanel.childComponents.add(getInfo ('Please download the "Joint Venture Policy"  and follow the instructions. Please upload the required attachments on the Attachments page at the end of your submission.'));
        fsubpanel.childComponents.add(getspace());*/
        fsubpanel.layout='block';
        fsubpanel.style='page-break-after:always'; 
        return fsubpanel ;
    }    

    public Component.Apex.OutputPanel SubAdvisors() {
        Component.Apex.OutputPanel fsubpanel = new Component.Apex.OutputPanel ();
        fsubpanel.childComponents.add(getHeaderInfo3('Sub-Advisors'));
        //fsubpanel.childComponents.add(getInfo (tproposal.name));
        fsubpanel.childComponents.add(getInfo ('Please advise if your firm anticipates in utilizing Sub-Advisors in order to provide the services CalPERS is requesting.'));
        fsubpanel.childComponents.add(getInfo ('If Yes:'));
        fsubpanel.childComponents.add(getInfoU ('Sub-Advisor No. 1'));
        fsubpanel.childComponents.add(getInfo ('Company Name'));
        fsubpanel.childComponents.add(getInfo ('Contact Name'));
        fsubpanel.childComponents.add(getInfo ('Contact Telephone Number'));
        fsubpanel.childComponents.add(getInfo ('Street - 255 character limit'));
        fsubpanel.childComponents.add(getInfo ('City'));
        fsubpanel.childComponents.add(getInfo ('State'));
        fsubpanel.childComponents.add(getInfo ('Postal Code'));
        fsubpanel.childComponents.add(getInfo ('Description of Work to be Performed by Sub-Advisor'));
        fsubpanel.childComponents.add(getInfo ('Percentage of Total Agreement Amount'));
        fsubpanel.childComponents.add(getInfoU ('Sub-Advisor No. 2'));
        fsubpanel.childComponents.add(getInfo ('Company Name'));
        fsubpanel.childComponents.add(getInfo ('Contact Name'));
        fsubpanel.childComponents.add(getInfo ('Contact Telephone Number'));
        fsubpanel.childComponents.add(getInfo ('Street - 255 character limit'));
        fsubpanel.childComponents.add(getInfo ('City'));
        fsubpanel.childComponents.add(getInfo ('State'));
        fsubpanel.childComponents.add(getInfo ('Postal Code'));
        fsubpanel.childComponents.add(getInfo ('Description of Work to be Performed by Sub-Advisor'));
        fsubpanel.childComponents.add(getInfo ('Percentage of Total Agreement Amount'));        
        
        fsubpanel.childComponents.add(getspace());
        fsubpanel.layout='block';
        fsubpanel.style='page-break-after:always'; 
        return fsubpanel ;
    }    

    public Component.Apex.OutputPanel PAgent() {
        Component.Apex.OutputPanel fsubpanel = new Component.Apex.OutputPanel ();
        //fsubpanel.childComponents.add(getHeaderInfo3('Placement Agent Regulation and Disclosure'));
        //fsubpanel.childComponents.add(getInfo (tproposal.name));
        Component.Apex.OutputText ftext = new Component.Apex.OutputText ();
        ASPcompliance = [Select Id, name, TextValue__c, type__c, Page_No__c, Proposal_Name_Page__c from ASP_Component__c where ASP_ID__r.name = 'System' and name ='PlacementAgentPage' limit 1];
        ftext.escape =false;
        fsubpanel.childComponents.add(ftext);
        ftext.value = ASPcompliance.TextValue__c;
                
        /*fsubpanel.childComponents.add(getInfo ('Respondents must comply with Placement Agent Regulation and Disclosure, Section 559, Article 2 of Chapter 2 of Division 1 of Title 2 of the California Code of Regulations. Questions regarding Placement Agent Regulation may be directed to: PlacementAgentDisclosure@CalPERS.CA.GOV. Respondents must provide a completed "Placement Agent Disclosure" to PlacementAgentDisclosure@CalPERS.CA.GOV. The completed disclosure should be received prior to the Proposal Submission deadline.'));
        fsubpanel.childComponents.add(getspace());*/
        fsubpanel.layout='block';
        fsubpanel.style='page-break-after:always'; 
        return fsubpanel ;
    }  
    public Component.Apex.OutputPanel ContractAct() {
        Component.Apex.OutputPanel fsubpanel = new Component.Apex.OutputPanel ();
        fsubpanel.childComponents.add(getHeaderInfo3('Contract Act Declarations'));
        //fsubpanel.childComponents.add(getInfo (tproposal.name));
        fsubpanel.childComponents.add(getInfo ('CalPERS is prohibited from contracting with individuals or firms who do business subject to the following statutes. By acknowledging below, you are certifying that you or your firm is not prohibited from submitting a bid or proposal for a contract with CalPERS, a state agency.'));
        fsubpanel.childComponents.add(getUInfo3 ('SUDAN CONTRACTING DECLARATION:'));
        fsubpanel.childComponents.add(getInfo ('Pursuant to Public Contract Code section 10477, a bidder or proposer who is a “scrutinized company” due to specific activities in Sudan (as defined in Public Contract Code section 10476), is ineligible to, and shall not, bid on or submit a proposal for a contract with a state agency for goods or services.'));
        fsubpanel.childComponents.add(getInfo ('This firm is not prohibited from submitting a bid or proposal for a contract with CalPERS, because (Choose one of the following):'));
        fsubpanel.childComponents.add(getInfo ('1) This firm does not currently have, and has not had within the previous three years, business activities or other operations outside of the United States, and in accordance with Public Contract Code section 10478, this firm hereby certifies it is not a “scrutinized company” (as defined in Public Contract Code section 10476).'));
        fsubpanel.childComponents.add(getInfo ('2) This firm is a “scrutinized company” (as defined in Public Contract Code section 10476), but has received written permission from the Department of General Services (DGS) to submit a bid or proposal pursuant to Public Contract Code section 10477(b).  A copy of the written permission from DGS is included with the firm’s bid or proposal.'));
        fsubpanel.childComponents.add(getInfo ('3) This firm currently has, or has had within the previous three years, business activities or other operations outside of the United States, but hereby certifies that is not a “scrutinized company” due to specific activities in Sudan (as defined in Public Contract Code section 10476).'));
        fsubpanel.childComponents.add(getUInfo3 ('IRAN CONTRACTING DECLARATION:'));
        fsubpanel.childComponents.add(getInfo ('Pursuant to the Iran Contracting Act of 2010 (Public Contract Code section 2201, et. seq.), if a bidder, at the time of the bid or proposal for a new contract, or renewal of an existing contract, has engaged in investment activities in Iran (as described in Public Contract Code section 2202.5) is ineligible, and shall not, bid on, submit a proposal for, or enter into or renew a contract with a public entity for goods or services for one million dollars ($1,000,000) or more. (Public Contract Code section 2203(a)).'));
        fsubpanel.childComponents.add(getInfo ('This firm is not prohibited from submitting a bid or proposal for a contract with CalPERS, because (Choose one of the following):'));
        fsubpanel.childComponents.add(getInfo ('1) This firm, or person, hereby certifies that it (or he or she) is not identified on the DGS list created pursuant to Public Contract Code section 2203(b) as a firm or person engaging in “investment activities” in Iran described in Public Contract Code section 2202.5(a) or 2202.5(b).'));
        fsubpanel.childComponents.add(getInfo ('2) The firm hereby certifies it is permitted to submit a bid or proposal to a public agency pursuant to Public Contract Code section 2203(c) because its investment activities in Iran were (1) made before July 1, 2010, (2) were not expanded or renewed after July 1, 2010, and (3) the firm has adopted, publicized, and is implementing a formal plan to cease the investment activities in Iran and to refrain from engaging in any new investments in Iran.'));
        fsubpanel.childComponents.add(getInfo ('3) This firm hereby certifies that although it is a financial institution that extends twenty million dollars ($20,000,000) or more in credit to another person, for 45 days or more, the person who will use the credit to provide goods or services in the energy sector in Iran is a person permitted to submit a bid or proposal to the public agency pursuant to Public Contract Code section 2203(c).  (See Public Contract Code section 2202.3(d)).'));
        fsubpanel.childComponents.add(getUInfo3 ('DEMOCRATIC REPUBLIC OF THE CONGO CONTRACTING DECLARATION:'));
        fsubpanel.childComponents.add(getInfo ('Pursuant to Public Contract Code section 10490 and section 13 of the Securities Exchange Act of 1934 (15 U.S.C.A. § 78(m)), a scrutinized company (as defined in Public Contract Code section 10490(b)) is ineligible to, and shall not, bid on or submit a proposal for a contract with a state agency for goods or services related to products or services that are the reason the firm must comply with section 13(p) of the Securities Exchange Act of 1934.'));
        fsubpanel.childComponents.add(getInfo ('This firm is not prohibited from submitting a bid or proposal for a contract with CalPERS, because (Choose one of the following):'));
        fsubpanel.childComponents.add(getInfo ('1) This firm is not a “scrutinized company” or does not currently use any conflict mineral products that are necessary to the functionality or production of a product manufactured by the firm from the Democratic Public of the Congo or an “adjoining country” (see 15 U.S.C.A § 78(m)(p)), and section 1502 of the Dodd-Frank Wall Street Reform and Consumer Protection Act (Pub.L. No.111-203, (July 21, 2010) 124 Stat. 1376)).'));
        fsubpanel.childComponents.add(getInfo ('2) This firm has ceased to be regarded as a “scrutinized company” (as defined by Cal. Public Contract Code section 10490(b)) because (1) it is not in violation of section 13(p) of the Securities Exchange Act of 1934 (15 U.S.C.A. § 78(m)(p)); or (2) it has filed an amended or corrective filing under section 13(p) of the Securities Exchange Act of 1934, which filing corrected the violations; or (3) three years have passed since the date of the by final judgment or settlement entered in a civil or administrative action brought by the Securities and Exchange Commission (SEC).'));
        fsubpanel.childComponents.add(getInfo ('3) Disclosure requirements have been temporarily waived by the SEC because the President of the United States has transmitted a determination that the waiver or revision is in the national security interest of the United States, or the disclosure requirements have been terminated because the President has determined and certified to the appropriate congressional committees that no armed groups continue to be directly involved and benefitting from this firm’s commercial activity involving conflict minerals.'));
        
        fsubpanel.childComponents.add(getack());

        fsubpanel.childComponents.add(getspace());
        fsubpanel.layout='block';
        fsubpanel.style='page-break-after:always'; 
        return fsubpanel ;
    }    


    public Component.Apex.OutputPanel CaTaxPayer() {
        Component.Apex.OutputPanel fsubpanel = new Component.Apex.OutputPanel ();
        fsubpanel.childComponents.add(getHeaderInfo3('California Taxpayer and Shareholder Protection Act Declaration'));
        //fsubpanel.childComponents.add(getInfo (tproposal.name));
        fsubpanel.childComponents.add(getInfo ('Proposers must complete the below California Taxpayer and Shareholder Protection Act Declaration. This form must be completed by an individual who is authorized to make the declaration on behalf of the Proposer.  The Act prohibits a state agency from entering into any contract with an expatriate corporation or its subsidiaries, with specified exceptions.'));
        fsubpanel.childComponents.add(getInfo ('Firm\'s Name'));
        fsubpanel.childComponents.add(getInfo ('The firm named above certifies, that the firm is eligible to contract with the State of California and CalPERS, pursuant to the provisions of the California Taxpayer and Shareholder Protection Act of 2003 (Public Contract Code section 10286 et. seq.).'));
        fsubpanel.childComponents.add(getInfo ('I, the official named below, hereby declare that I am duly authorized to make this declaration on behalf of the above named firm.  I declare under penalty of perjury under the laws of the State of California that the foregoing is true and correct and that this declaration was executed on the date listed below.'));
        fsubpanel.childComponents.add(getInfo ('Authorizer First Name'));
        fsubpanel.childComponents.add(getInfo ('Authorizer Last Name'));
        fsubpanel.childComponents.add(getInfo ('Authorizer Title'));
        fsubpanel.childComponents.add(getInfo ('Date'));

        fsubpanel.childComponents.add(getack());                
        fsubpanel.childComponents.add(getspace());
        fsubpanel.layout='block';
        fsubpanel.style='page-break-after:always'; 
        return fsubpanel ;
    }    

    public Component.Apex.OutputPanel ASPAckFields() {
        Component.Apex.OutputPanel fsubpanel = new Component.Apex.OutputPanel ();
        //fsubpanel.childComponents.add(getHeaderInfo(tempCampaignName));
        //fsubpanel.childComponents.add(getInfo (tproposal.name));
        Component.Apex.OutputText ftext = new Component.Apex.OutputText ();
        ASPcompliance = [Select Id, name, TextValue__c, type__c, Page_No__c, Proposal_Name_Page__c from ASP_Component__c where ASP_ID__r.name = 'System' and name ='AcknowledgementPage' limit 1];
        ftext.escape =false;
        fsubpanel.childComponents.add(ftext);
        ftext.value = ASPcompliance.TextValue__c;
        
       /* fsubpanel.childComponents.add(getInfo ('CalPERS requires all new investment proposals be submitted via the Investment Proposal Submission. This process allows you to securely submit your proposals to our investment professionals for consideration and evaluation.'));
        fsubpanel.childComponents.add(getInfo ('Prior to submitting an investment proposal to CalPERS you are required to read the below information and acknowledge that you have read and understand the information.'));
        fsubpanel.childComponents.add(getHeaderInfo3('Public Records Act and Confidentiality'));
        fsubpanel.childComponents.add(getInfo ('As a public agency, CalPERS is subject to a myriad of laws mandating the disclosure of certain types of information to the public. For an introduction to the laws, regulations, and policies relating to these issues, please read "Notes on Confidentiality".  Please note that before submitting an investment proposal you will be asked to acknowledge that CalPERS is subject to laws, regulations, and policies that may require it to disclose to the public some or all of the information you submit through this website.'));
        fsubpanel.childComponents.add(getHeaderInfo3('Conflicts of Interests and Political Contributions'));
        fsubpanel.childComponents.add(getInfo ('If you are registered with the U.S. Securities and Exchange Commission (SEC), your firm’s and your firm’s employee’s ability to make political contributions to certain elected positions in California, may be limited. In particular, violations of SEC Rule 206(4)-50 of the Investment Advisers Act of 1940, could lead to a prohibition on accepting fees or other compensation from CalPERS.  Please carefully research and review this rule before deciding whether to submit an investment proposal to CalPERS.'));
        fsubpanel.childComponents.add(getHeaderInfo3('Placement Agent Rules'));
        fsubpanel.childComponents.add(getInfo ('Please review the "CalPERS Placement Agent Regulation".  By acknowledging the below, you are acknowledging CalPERS has provided you with the Placement Agent regulation and you understand the requirements of the regulation. A Placement Agent disclosure is not necessarily required to submit an investment proposal. If you have any questions on the Placement Agent Disclosure process emali us at PlacementAgentDisclosure@CalPERS.CA.GOV.'));
        fsubpanel.childComponents.add(getHeaderInfo3('Restricted Communication and Related Disclosures'));
        fsubpanel.childComponents.add(getInfo ('Government Code sections 20152.5 and 20153 list specific prohibitions and disclosure requirements on potential transactions by:'));
        fsubpanel.childComponents.add(getInfo ('   • limiting communication with CalPERS Board Members,)'));
        fsubpanel.childComponents.add(getInfo ('   • requiring  written disclosure of any communication with a CalPERS Board member'));
        fsubpanel.childComponents.add(getInfo ('   • requiring  written disclosure of gifts to CalPERS Board Member or CalPERS staff'));
        fsubpanel.childComponents.add(getHeaderInfo3('External Investment Resource Rules'));
        fsubpanel.childComponents.add(getInfo ('Please review the "CalPERS External Investment Resource Regulation".  By acknowledging the below, you are acknowledging that CalPERS has provided you with the External Investment Resource Regulation and you understand the requirements of the regulation. Please carefully review the regulation and specifically the requirements that apply to External Investment Resources.'));

        fsubpanel.childComponents.add(getack());                
        fsubpanel.childComponents.add(getspace()); */
        fsubpanel.layout='block';
        fsubpanel.style='page-break-after:always'; 
        return fsubpanel ;
    }    
                
    public Component.Apex.OutputPanel FirmInfoFields() {
        Component.Apex.OutputPanel fsubpanel = new Component.Apex.OutputPanel ();
        fsubpanel.childComponents.add(getHeaderInfo3('Firm Information'));
        //fsubpanel.childComponents.add(getInfo (tproposal.name));
        fsubpanel.childComponents.add(getInfo ('Partner or Proposing Entity Name'));
        fsubpanel.childComponents.add(getInfo ('Web site'));
        fsubpanel.childComponents.add(getInfo ('Fax'));
        fsubpanel.childComponents.add(getInfo ('Street - 255 character limit'));
        fsubpanel.childComponents.add(getInfo ('City'));
        fsubpanel.childComponents.add(getInfo ('State'));
        fsubpanel.childComponents.add(getInfo ('Postal Code'));
        fsubpanel.childComponents.add(getInfo ('Country'));
        fsubpanel.childComponents.add(getInfo ('Organization Type'));
        fsubpanel.childComponents.add(getInfo ('Federal Employer ID No. (FEIN) '));
        fsubpanel.childComponents.add(getInfo ('Submitter Contact Information (Individual completing the submission form)'));
        fsubpanel.childComponents.add(getInfo ('Title'));
        fsubpanel.childComponents.add(getInfo ('Phone'));
        fsubpanel.childComponents.add(getInfo ('Email'));
        fsubpanel.childComponents.add(getInfo ('Firm Primary Contact Information (Person who will receive all communication regarding the decisions on this opportunity)'));
        fsubpanel.childComponents.add(getInfo ('Title'));
        fsubpanel.childComponents.add(getInfo ('Phone'));
        fsubpanel.childComponents.add(getInfo ('Email'));
        fsubpanel.childComponents.add(getInfo ('Does your firm, or the firm you represent, have an existing relationship with CalPERS?'));
        fsubpanel.childComponents.add(getInfo ('Firm Inception Date'));
        fsubpanel.childComponents.add(getInfo ('Assets Under Management (USD Millions- One billion entered as 1000)'));
        fsubpanel.childComponents.add(getInfo ('Brief Firm History - 1000 character limit'));
        fsubpanel.childComponents.add(getInfo ('Strategy/Opportunity Name'));

                        
        fsubpanel.childComponents.add(getspace());
        fsubpanel.layout='block';
        fsubpanel.style='page-break-after:always'; 
        return fsubpanel ;
    }    
      
    public Component.Apex.OutputPanel DiversityFields() {
        Component.Apex.OutputPanel fsubpanel = new Component.Apex.OutputPanel ();
        fsubpanel.childComponents.add(getHeaderInfo3('Diversity Definitions and Questionnaire'));
        //fsubpanel.childComponents.add(getInfo (tproposal.name));
        fsubpanel.childComponents.add(getInfo ('CalPERS Investment Office defines a Diverse Investment Management Business as a for profit enterprise, regardless of size, physically headquartered in the United States or its trust territories, which is owned and operated by women and/or minority group members.'));
        fsubpanel.childComponents.add(getInfo ('Minority Group Members are United States citizens and permanent residents who are African American, Asian American, Hispanic American or Native American based on 2010 US Census classifications.'));
        fsubpanel.childComponents.add(getInfo ('CalPERS will report on Diverse Investment Management Businesses in the following categories at the time of CalPERS investment:'));
        fsubpanel.childComponents.add(getInfo ('• Substantially Diverse: A firm that is 25% to 49% women and/or minority group member owned'));
        fsubpanel.childComponents.add(getInfo ('• Majority Diverse: A firm that is 50% or more women and/or minority group member owned'));
        fsubpanel.childComponents.add(getInfo ('In private market asset classes such as private equity and real estate, Diverse Investment Management Businesses should determine employee ownership by women and/or minority group members based on carried interest participation in each of the funds managed by the business.'));
        fsubpanel.childComponents.add(getInfo ('In public market asset classes such as global equity, global fixed income and hedge funds/absolute return strategies, Diverse Investment Management Businesses should determine employee ownership by minority group members and/or women based on participation in the investment advisor for each of the funds managed by the business.'));
        fsubpanel.childComponents.add(getInfo ('Providing the following information relating to ethnicity, gender and race is for record keeping purposes, is voluntary, and will not be considered as a factor in any investment decision.'));
        
        fsubpanel.childComponents.add(getInfo ('Please indicate if your firm meets the definition referenced above of a Diverse Investment Management Business.'));
        fsubpanel.childComponents.add(getInfo ('Is your firm physically headquartered in the United States or its trust territories?'));
        fsubpanel.childComponents.add(getInfo ('Percent of the firm owned by women employees'));
        fsubpanel.childComponents.add(getInfo ('African American'));
        fsubpanel.childComponents.add(getInfo ('Asian American'));
        fsubpanel.childComponents.add(getInfo ('Hispanic American'));
        fsubpanel.childComponents.add(getInfo ('Native American'));
        fsubpanel.childComponents.add(getInfo ('Other'));
        fsubpanel.childComponents.add(getInfo ('Total percent owned combining gender, race, and ethnicity'));
                        
        fsubpanel.childComponents.add(getspace());
        fsubpanel.layout='block';
        fsubpanel.style='page-break-after:always'; 
        return fsubpanel ;
    }    
      
    public Component.Apex.OutputPanel AttachmentsFields() {
        Component.Apex.OutputPanel fsubpanel = new Component.Apex.OutputPanel ();
        fsubpanel.childComponents.add(getHeaderInfo3('Attachments'));

                        
        fsubpanel.childComponents.add(getspace());
        fsubpanel.layout='block';
        fsubpanel.style='page-break-after:always'; 
        return fsubpanel ;
    }    

 public Component.Apex.OutputPanel getHeader3() {return null;}
 
    public Component.Apex.OutputPanel getReport() {
        Component.Apex.OutputPanel outpanel = new Component.Apex.OutputPanel ();

    Component.Apex.OutputPanel formpanel = new Component.Apex.OutputPanel ();
    formpanel.childComponents.add(FormInfo());
    outpanel.childComponents.add(formpanel);

    for(Integer i = 0; i < ASPcomprecAll.size(); i++) {    
        if (ASPcomprecAll[i].Type__c == Constants.FREEFORM || ASPcomprecAll[i].Type__c == Constants.FREEFORM_ACK) {
            Component.Apex.OutputText otext = new Component.Apex.OutputText ();
            Component.Apex.OutputText otitle = new Component.Apex.OutputText ();
            Component.Apex.OutputPanel subpanel = new Component.Apex.OutputPanel ();
            otitle.style = 'display:block';
            otitle.value = ASPcomprecAll[i].title__c;
            subpanel.childComponents.add(otitle);
            otext.escape =false;
            subpanel.childComponents.add(otext);
            otext.value = ASPcomprecAll[i].TextValue__c;
            if (ASPcomprecAll[i].Type__c == Constants.FREEFORM_ACK) {
                otext.style = 'display:block;margin-bottom:50px';
                //subpanel.childComponents.add(getimage());
                subpanel.childComponents.add(getack());
            }    
               
            subpanel.childComponents.add(getspace());
            subpanel.layout='block';
            subpanel.style='page-break-after:always';
            outpanel.childComponents.add(subpanel);
            
        }
        else if (ASPcomprecAll[i].Type__c == Constants.CUSTOM || ASPcomprecAll[i].Type__c == Constants.CUSTOM_ACK) {
            Component.Apex.OutputText otext = new Component.Apex.OutputText ();
            Component.Apex.OutputText otitle = new Component.Apex.OutputText ();            
            Component.Apex.OutputPanel subpanel = new Component.Apex.OutputPanel ();
            otitle.style = 'display:block';
            otitle.value = ASPcomprecAll[i].title__c;
            subpanel.childComponents.add(otitle);            
            otext .escape =false;
            otext.value = ASPcomprecAll[i].TextValue__c;
            subpanel.childComponents.add(otext);  
            otext.style='display:block;'; 
                  
            outpanel.childComponents.add(getspace());
            ASPcustomrec = [SELECT id, field_title__c, order__c,  asp_component__r.textvalue__c  FROM Proposal_question__c 
            where asp_component__r.ASP_ID__c = :tempCampaignId and asp_component__r.Page_no__c = :ASPcomprecAll[i].Page_No__c and active__c = true order by Order__c asc];
            for(Integer a = 0; a < ASPcustomrec.size(); a++) {
                Component.Apex.OutputText ctext = new Component.Apex.OutputText ();
                ctext.style='display:block;margin-bottom:15px';
                ctext.value = ASPcustomrec[a].Field_Title__c;
                
                subpanel.childComponents.add(ctext);

                            
                subpanel.childComponents.add(getspace());
                subpanel.layout='block';
                subpanel.style='page-break-after:always';
                outpanel.childComponents.add(subpanel);
            }             
                            
                if (ASPcomprecAll[i].Type__c == Constants.CUSTOM_ACK) {
                    otext.style = 'display:block;margin-bottom:50px';
                    //subpanel.childComponents.add(getimage());
                    subpanel.childComponents.add(getack());
                }    
        }
        else if (ASPcomprecAll[i].Type__c == Constants.PROPOSAL || ASPcomprecAll[i].Type__c == Constants.COMPLIANCE) {
            Component.Apex.OutputText ptext = new Component.Apex.OutputText ();
            Component.Apex.OutputPanel subpanel = new Component.Apex.OutputPanel ();
            if (ASPcomprecAll[i].Proposal_Name_Page__c == constants.ASP_FirmInformation)  outpanel.childComponents.add(FirmInfoFields());       //ptext.value = 'ASP_FirmInformation';
            if (ASPcomprecAll[i].Proposal_Name_Page__c == constants.ASP_Diversity)        outpanel.childComponents.add(DiversityFields());       //ptext.value = 'ASP_Diversity';
            if (ASPcomprecAll[i].Proposal_Name_Page__c == constants.ASP_Acknowledgement)  outpanel.childComponents.add(ASPAckFields());
          //  if (ASPcomprecAll[i].Proposal_Name_Page__c == constants.ASP_Attachments)    outpanel.childComponents.add(AttachmentsFields());       //ptext.value = 'ASP_Attachments';
            if (ASPcomprecAll[i].Proposal_Name_Page__c == constants.ASP_California_Tax_Payer)  outpanel.childComponents.add(CaTaxPayer()); 
            if (ASPcomprecAll[i].Proposal_Name_Page__c == constants.ASP_Contract_Act_Declarations)  outpanel.childComponents.add(ContractAct());  
            if (ASPcomprecAll[i].Proposal_Name_Page__c == constants.ASP_Joint_Venture)  outpanel.childComponents.add(JointVenture());  
            if (ASPcomprecAll[i].Proposal_Name_Page__c == constants.ASP_Placement_Agent)  outpanel.childComponents.add(PAgent());  
            if (ASPcomprecAll[i].Proposal_Name_Page__c == constants.ASP_Sub_Advisors)  outpanel.childComponents.add(SubAdvisors()); 
            
            
      /*       ptext.style='display:block';       

           subpanel.style='page-break-after:always';  
            subpanel.childComponents.add(ptext); 
            subpanel.childComponents.add(getspace());
            subpanel.layout='block';
            subpanel.style='page-break-after:always'; 
            outpanel.childComponents.add(subpanel);       */  
        }
        else outpanel.childComponents.add(getspace());
        
    }    
//<br style="margin-top:240px;"/><br style="margin-top:240px;"/><br style="margin-top:240px;"/><br style="margin-top:240px;"/>

    return outpanel;        
                
    }       
}