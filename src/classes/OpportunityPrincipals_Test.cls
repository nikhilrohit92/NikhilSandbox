// *********************************************************************************
// Copyright 2010 MK Partners, Inc. - www.mkpartners.com
// Version 2010-09-28
// *********************************************************************************
@isTest
private class OpportunityPrincipals_Test {
		
	static testMethod void test_OpportunityPrincipals_Scheduler() {
		test.startTest();
			OpportunityPrincipals_Scheduler m = new  OpportunityPrincipals_Scheduler();
			String sch  =  '0 0 0 3 9 ? 2022';
			String jobId = system.schedule('OpportunityPrincipals Job Schedule', sch, m);
			CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
			System.assertEquals('0 0 0 3 9 ? 2022', ct.CronExpression);
			System.assertEquals(0, ct.TimesTriggered);
			System.assertEquals('2022-09-03 00:00:00', String.valueOf(ct.NextFireTime));
		test.stopTest();
	}


	static testMethod void test_OpportunityPrincipals_Batch_Success() {
		Account a = new Account();
		a.Name = 'test';
		insert a;
		
		Contact c = new Contact();
		c.LastName = 'test';
		c.AccountId = a.Id;
		insert c;
		
		Opportunity o = new Opportunity();
		o.Name = 'test';
		o.Amount = 0;
		o.StageName = 'Prospecting';
		o.CloseDate = system.today();
		insert o;
		
		OpportunityContactRole ocr = new OpportunityContactRole();
		ocr.OpportunityId = o.Id;
		ocr.ContactId = c.Id;
		ocr.Role = 'Principal';
		insert ocr;
		
		Test.StartTest();
			String queryString = 'Select Id, Principals__c from Opportunity where Id = \''+ o.Id + '\'';
			OpportunityPrincipals_Batch job = new OpportunityPrincipals_Batch(queryString);
			Id processId = Database.executeBatch(job);
		Test.stopTest();
		
    }

	static testMethod void test_OpportunityPrincipals_Batch_Failure() {
		Test.StartTest();
			OpportunityPrincipals_Batch job = new OpportunityPrincipals_Batch('null');
			Id processId = Database.executeBatch(job);
		Test.stopTest();
		
		AsyncApexJob j = [select ApexClassId, CompletedDate, CreatedById, CreatedDate, 
							Id, JobItemsProcessed, JobType, MethodName, NumberOfErrors, Status, 
							TotalJobItems from AsyncApexJob where Id =:processId];
		System.assertEquals('Failed', j.Status);

		try {
			OpportunityPrincipals_Batch job1 = new OpportunityPrincipals_Batch(null);
			Id processId1 = Database.executeBatch(job1);
			System.assert(false);
		} catch (Exception exp) {
			System.assert(true);
		}
	}

}