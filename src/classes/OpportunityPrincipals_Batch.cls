// *********************************************************************************
// Copyright 2010 MK Partners, Inc. - www.mkpartners.com
// Version 2010-10-06
// *********************************************************************************
/*
	To Run:
			OpportunityPrincipals_Batch job = new OpportunityPrincipals_Batch();
			Id processId = Database.executeBatch(job);
	
	To Check Status:
			AsyncApexJob j = [select ApexClassId, CompletedDate, CreatedById, CreatedDate, 
								Id, JobItemsProcessed, JobType, MethodName, NumberOfErrors, Status, 
								TotalJobItems from AsyncApexJob where Id =:processId];
	
	Info about this Class
			ApexClass ac = [select ApiVersion, Body, BodyCrc, CreatedById, CreatedDate, Id, 
								IsValid, LastModifiedById, LastModifiedDate, LengthWithoutComments, Name, 
								NamespacePrefix, Status, SystemModstamp from ApexClass where Id = :j.ApexClassId];
*/
global with sharing class OpportunityPrincipals_Batch implements Database.Batchable<SObject> {

	// Queries to be used
	public static final String DATA_SET_QUERY = 'Select Id, Principals__c from Opportunity';

	// Query to fetch the data required by the job
	private String jquery; 

	global OpportunityPrincipals_Batch() {
		this(DATA_SET_QUERY);
	}

	public class BatchApexJobException extends Exception {}
	public class InvalidObjectInstanceException extends Exception {}

	global OpportunityPrincipals_Batch(String query) {
		try {
	    	// If no query set, throw exception
	    	if (query == null) {
	    		throw new BatchApexJobException('Query string is null.');
	    	}
	    	jquery = query;
	    	
	    	//Check if the job is running already
	        ApexClass ac = [select Name from ApexClass where Name = 'OpportunityPrincipals_Batch'];
	        List<AsyncApexJob> aajs = [select Status from AsyncApexJob where ApexClassId = :ac.Id and 
	        							JobType = 'batchApex' and Status in ('Queued', 'Processing')];
	        if (!aajs.isEmpty()) {
	        	throw new BatchApexJobException(aajs.size() + ' OpportunityPrincipals_Batch job(s) already running or processing.');
			}
    	}
    	catch (Exception e) {
            throw e;
        }
	}

    //Method required by the framework to start the job execution.
    global Database.QueryLocator start(Database.BatchableContext ctx) {
        Database.QueryLocator qLocator = null;
        try {
            qLocator = Database.getQueryLocator(jquery);
        }
        catch (Exception e) {
            throw e;
        }
        return qLocator;
    }


    //Method required by the framework for the actual job execution.
	global void execute(Database.BatchableContext ctx, List<SObject> sobjects) {
		try {
			Map<Id,String> opptyPrincipalMap = new Map<Id,String>();
			List<Opportunity> theQuery = toOpportunityCollection(sobjects);
			for ( Opportunity o : theQuery ){
				opptyPrincipalMap.put(o.Id,'');
			}
			for ( OpportunityContactRole ocr : [Select Id, ContactId, Contact.Name, OpportunityId from OpportunityContactRole where Role = 'Principal' and OpportunityId in :opptyPrincipalMap.keySet() order by Contact.Name]){
				String x = opptyPrincipalMap.get(ocr.OpportunityId);
				x += ocr.Contact.Name;
				x += '; ';
				opptyPrincipalMap.put(ocr.OpportunityId,x);
			}
			List<Opportunity> opptyUpdates = new List<Opportunity>();
			for ( Opportunity o : theQuery ){
				if ( opptyPrincipalMap.containsKey(o.Id) ){
					if ( o.Principals__c != opptyPrincipalMap.get(o.Id)){
						o.Principals__c = opptyPrincipalMap.get(o.Id);
						opptyUpdates.add(o);
					}
				}
			}
			if ( opptyUpdates.size() > 0 ){
				update opptyUpdates;
			}
		} catch (Exception e) {

			throw e;
		}
	}


	//Method required and invoked by the framework when a batch job is done.
	global void finish(Database.BatchableContext ctx) {

	}
	
	//Method to convert list of SObjects to a list of Contacts
	public List<Opportunity> toOpportunityCollection(SObject[] sobjects) {
		List<Opportunity> thisList = new List<Opportunity>();
		for (SObject obj : sobjects) {
			if (obj instanceof Opportunity) {
				thisList.add((Opportunity) obj);
			} else {
				throw new InvalidObjectInstanceException('Invalid object instance in cursor.');
			}
		}	
		return thisList;
    }

	//Method to convert list of SObjects to a list of Contacts
	public List<OpportunityContactRole> toCollection(SObject[] sobjects) {
		List<OpportunityContactRole> thisList = new List<OpportunityContactRole>();
		for (SObject obj : sobjects) {
			if (obj instanceof OpportunityContactRole) {
				thisList.add((OpportunityContactRole) obj);
			} else {
				throw new InvalidObjectInstanceException('Invalid object instance in cursor.');
			}
		}	
		return thisList;
    }

}