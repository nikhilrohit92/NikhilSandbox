public with sharing class Subscribe {

    public Lead regLead {get; set;}
    public String LeadEmail {get; set;}
    public String LeadEmail2 {get; set;}
    public String LeadEmailtemp {get; set;}
    public ErrorHandler ehandler {get; set;}
    public string tempCampaignName {get;set;}
    public id     tempCampaignId  {get;set;}
    public string tempCampaignAssetClass  {get;set;} 
    public CampaignMember regCampMem {get; set;}  
    public Boolean regBoolean{get;set;}
             
    public Boolean acBoolean {get; set;}
    public List <SelectOption> selAssetClass { get; set; }
    public List <SelectOption> allAssetClass { get; set; }
    public List<CheckboxWrapper> AssetClassOptions {get; set;}
    public Boolean AllOption {get; set;}
    
    public Boolean tipBoolean {get; set;}
    public List <SelectOption> selTip { get; set; }
    public List <SelectOption> allTip { get; set; }
    public List<CheckboxWrapper> TipOptions {get; set;}
        
    public Boolean isIe {get; set;}    
    
public Subscribe() {
    
    regLead = new Lead();  
      
    regBoolean = false;
    ehandler = new ErrorHandler();
    isIe = false;
    allAssetClass = new List<SelectOption>();  
    allTip = new List<SelectOption>();    
    AllOption = false;
         
    allAssetClass.add(new SelectOption('Commodities', 'Commodities'));       
    allAssetClass.add(new SelectOption('Forestland', 'Forestland'));
    allAssetClass.add(new SelectOption('Global Equities', 'Global Equities'));
    allAssetClass.add(new SelectOption('Global Fixed Income', 'Global Fixed Income'));
    allAssetClass.add(new SelectOption('Infrastructure', 'Infrastructure'));  
    allAssetClass.add(new SelectOption('Opportunistic', 'Opportunistic'));
    allAssetClass.add(new SelectOption('Private Equity', 'Private Equity'));  
    allAssetClass.add(new SelectOption('Real Estate', 'Real Estate'));
    
       
    AssetClassOptions = CheckboxWrapper.convertOptions(allAssetClass);
    
          
   
   allTip.add(new SelectOption('California Investments', 'California Investments')); 
   allTip.add(new SelectOption('Diverse', 'Diverse')); 
   allTip.add(new SelectOption('Emerging Managers', 'Emerging Managers'));
   allTip.add(new SelectOption('General', 'General')); 
   allTip.add(new SelectOption('Transition Managers', 'Transition Managers'));
   

   
   tipOptions = CheckboxWrapper.convertOptions(allTip);
       
       
    
    }    

    public PageReference setAllOption() {
       
       if (allOption) {
           for( integer i = 0; i < AssetClassOptions.size(); i++) {
               AssetClassOptions[i].selected = true; 
           }
           for( integer i = 0; i < TipOptions.size(); i++) {
               TipOptions[i].selected = true; 
           }           
       } else {
           for( integer i = 0; i < AssetClassOptions.size(); i++) {
               AssetClassOptions[i].selected = false; 
           }
           for( integer i = 0; i < TipOptions.size(); i++) {
               TipOptions[i].selected = false; 
           }           
       }        
        return null;    
    }
        
    public String getInputRequiredMessage() {
        return ErrorHandler.INPUT_MSG;
    }    
    
    public String getInvalidEmailMessage() {
        return ErrorHandler.INVALID_EMAIL_FORMAT;
    }
        
        public PageReference addLead(){

        ehandler.clearMessages();
        Boolean isValid = true;

        if (string.isBlank(regLead.FirstName)) {
            ehandler.addError('thisPage:thisForm:FirstName', 'First Name', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.clog.firstNameError = true;
        }
        
        if (string.isBlank(regLead.LastName)) {
            ehandler.addError('thisPage:thisForm:LastName', 'Last Name', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.clog.LastNameError = true;
        }
        
        if (string.isBlank(regLead.company)) {
            isValid = false;
            ehandler.addError('thisPage:thisForm:Company', 'Company', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            ehandler.clog.companyError = true;
        }                    
        
        if (LeadEmail != null && LeadEmail.trim().length() > 0){
            if ( Utilities.isValidEmailString(LeadEmail) ){
                regLead.Email = LeadEmail ;
            } else {
                ehandler.addError('thisPage:thisForm:LeadEmail', 'Email Address', ErrorHandler.INVALID_EMAIL_FORMAT, ErrorHandler.INVALID_EMAIL_FORMAT);
                ehandler.clog.emailFormatError = true;
                isValid = false;
            }
        } else {
            ehandler.addError('thisPage:thisForm:LeadEmail', 'Email Address', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.clog.emailError = true;
            
        }

        if (LeadEmail2 != null && LeadEmail2.trim().length() > 0){
            if ( Utilities.isValidEmailString(LeadEmail2) ){
                regLead.Email = LeadEmail ;
            } else {                
                ehandler.addError('thisPage:thisForm:LeadEmail2', 'Confirm Email Address', ErrorHandler.INVALID_EMAIL_FORMAT, ErrorHandler.INVALID_EMAIL_FORMAT);
                ehandler.clog.emailFormatError2 = true;
                isValid = false;
            }
        } else {
            ehandler.addError('thisPage:thisForm:LeadEmail2', 'Confirm Email Address', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.clog.emailError2 = true;
            
        }
        
        if (LeadEmail != LeadEmail2 ) {
            ehandler.addError('thisPage:thisForm:LeadEmail',  'Email Address', 'Email Addresses do not match', 'Email Addresses do not match');
            ehandler.clog.emailFormatError = true;
            
            ehandler.addError('thisPage:thisForm:LeadEmail2', 'Confirm Email Address', 'Email Addresses do not match', 'Email Addresses do not match');
            ehandler.clog.emailFormatError2 = true;
            isValid = false;
        }    
        LeadEmailtemp = LeadEmail;
        if ( isValid ){
         try {
            //
            List<Lead> templead = [Select id, LastName, FirstName, Phone, Email  from Lead where email = :LeadEmail and recordtype.name='Subscribe' limit 1];
                        
            if (templead.size() > 0) {
               reglead.id = templead[0].id;
               
               update regLead;
               List<CampaignMember> delcm = [Select id from CampaignMember where leadid = :templead[0].id];
               delete delcm;
               
               
           }
            else        
                insert regLead;
                
                
            List<CampaignMember> mem = new List<CampaignMember >();
            for( CheckboxWrapper w : AssetClassOptions) {
                if (w.selected) {
                 system.debug('** getvalue *** '+w.getValue());
                    Campaign camp = [Select id from Campaign where Subscription__c =  :w.getValue()]; 
                    regCampMem = new CampaignMember(); 
                    regcampmem.leadid = reglead.id;
                    regCampmem.campaignId = camp.id;
                    regCampmem.Status = 'Registered';
                    mem.add(regcampmem);
                }
            }            
            for( CheckboxWrapper w : TipOptions) {
                if (w.selected) {
                 system.debug('** getvalue *** '+w.getValue());
                    Campaign camp = [Select id from Campaign where Subscription__c =  :w.getValue()]; 
                    regCampMem = new CampaignMember(); 
                    regcampmem.leadid = reglead.id;
                    regCampmem.campaignId = camp.id;
                    regCampmem.Status = 'Registered';
                    mem.add(regcampmem);
                }
            }           
            upsert mem;   
            regLead.clear();
            
            AllOption = false;
           for( integer i = 0; i < AssetClassOptions.size(); i++) {
               AssetClassOptions[i].selected = false; 
           }
           for( integer i = 0; i < TipOptions.size(); i++) {
               TipOptions[i].selected = false; 
           } 
            LeadEmail = null;
            LeadEmail2 = null;            
            regBoolean = true;
            }//else what?
         catch ( exception e){

         }
        }
        return null;
    }
    
}