public with sharing class DisclosureFormUpload_Extension {

	public Attachment attachment {get;set;}
	public Disclosure_Form__c df {get;set;}
	
    public DisclosureFormUpload_Extension(ApexPages.StandardController stdController) {
        this.df = (Disclosure_Form__c)stdController.getRecord();
		attachment = new Attachment();
    }

	public PageReference uploadAttachment() {
		attachment.ParentId = df.Id;
		insert attachment;

		df.Uploaded_Signed_PDF__c = true;
		update df;
		return null;
	}
	
	public PageReference backToDisclosureForm() {
		PageReference p = new PageReference('/' + df.Id);
		p.setRedirect(true);
		return p;
	}

}