public class OpportunityReferAssetClass_Extension {

    public Opportunity opportunity {get;set;}
    public Opportunity newOpportunity {get;set;}
    public boolean AssetClassDisable  {get {return defvalue;} set;}
    public boolean defvalue = true;
    
    public OpportunityReferAssetClass_Extension(ApexPages.StandardController stdController) {
        this.opportunity = (Opportunity)stdController.getRecord();
        Id thisId = opportunity.Id;
        Map<String, Schema.SObjectField> cFieldMap = Schema.SObjectType.Opportunity.fields.getMap();
        string qryString = 'Select ';
        for ( String f : cFieldMap.keySet() ){
            qryString += f;
            qryString += ', ';
        }
        qryString = qryString.subString(0,qryString.length()-2);
        qryString += ' from Opportunity where Id =:thisId limit 1';
        opportunity = database.query(qryString);
        newOpportunity = opportunity.clone(false, true);


    }

    public void AssetClassChange() {
          if ( Opportunity.Admin_Asset_Class__c == newOpportunity.Admin_Asset_Class__c || 
          newOpportunity.Admin_Asset_Class__c == null ||
          Opportunity.Admin_Asset_Class__c == null ) {
            AssetClassDisable = true; defvalue = true;
        } else { AssetClassDisable = false; defvalue = false;
  }
                }
    
    
    public PageReference referInvestment(){
        List<User> users = [Select id from User where Id = '00580000003QwWE' limit 1];
        opportunity.StageName = 'Referred to CalPERS Unit';
        String x = newOpportunity.Admin_Asset_Class__c;
        if ( opportunity.Referred_To_CalPERS_Unit__c != null ){
            x += '; ';
            x += opportunity.Referred_To_CalPERS_Unit__c;
        }
        opportunity.Referred_To_CalPERS_Unit__c = x;
        opportunity.Last_CalPERS_Unit_Referred_To__c = newOpportunity.Admin_Asset_Class__c;
        opportunity.CanRefer__c = true;
        update opportunity;

        if ( users.size() > 0 ){
            newOpportunity.OwnerId = users[0].Id;
        }
        newOpportunity.stageName = 'Submitted';
        if (newOpportunity.Admin_Type__c == null ){
            newOpportunity.Admin_Type__c = 'Partnership';
        }
        insert newOpportunity;
        List<Opportunity> opps = new List<Opportunity>();
        opps.add(newOpportunity);
        MKP011.Dynamic_Sharing d = new MKP011.Dynamic_Sharing();
        d.addSharing(opps,'Opportunity','OpportunityShare');
        PageReference p = new PageReference('/' + opportunity.Id);
        return p;
    }
}