//Helper class to manage picklist values and dependencies
public with sharing class PicklistManager {

    public static List<SelectOption> getAIMPartnershipStrategy(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Buyout-Diversified', 'Buyout-Diversified'));
        options.add(new SelectOption('Buyout-Large', 'Buyout-Large'));
        options.add(new SelectOption('Buyout-Mega', 'Buyout-Mega'));
        options.add(new SelectOption('Buyout-Micro', 'Buyout-Micro'));
        options.add(new SelectOption('Buyout-Mid', 'Buyout-Mid'));
        options.add(new SelectOption('Buyout-Small', 'Buyout-Small'));
        options.add(new SelectOption('Distressed-Debt', 'Distressed-Debt'));
        options.add(new SelectOption('Distressed-Sub\' Debt', 'Distressed-Sub\' Debt'));
        options.add(new SelectOption('Fund-of-Funds', 'Fund-of-Funds'));
        options.add(new SelectOption('Growth Equity/Expansion', 'Growth Equity/Expansion'));
        options.add(new SelectOption('Mezzanine', 'Mezzanine'));
        options.add(new SelectOption('Secondary', 'Secondary'));
        options.add(new SelectOption('Special Situations', 'Special Situations'));
        options.add(new SelectOption('Venture Capital-Diversified', 'Venture Capital-Diversified'));
        options.add(new SelectOption('Venture Capital-Late', 'Venture Capital-Late'));
        options.add(new SelectOption('Venture Capital-Mid', 'Venture Capital-Mid'));
        options.add(new SelectOption('Venture Capital-Seed/Early', 'Venture Capital-Seed/Early'));
        options.add(new SelectOption('Other', 'Other'));
        return options;
    } 
    
    public static List<SelectOption> getAIMPartnershipGeoFocus()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Africa', 'Africa'));
        options.add(new SelectOption('Australia', 'Australia'));
        options.add(new SelectOption('Canada', 'Canada'));
        options.add(new SelectOption('Central and Eastern Europe', 'Central and Eastern Europe'));
        options.add(new SelectOption('China', 'China'));
        options.add(new SelectOption('Global', 'Global'));
        options.add(new SelectOption('India', 'India'));
        options.add(new SelectOption('Japan', 'Japan'));
        options.add(new SelectOption('LatAm', 'LatAm'));
        options.add(new SelectOption('MENA', 'MENA'));
        options.add(new SelectOption('Southeast Asia', 'Southeast Asia'));
        options.add(new SelectOption('U.S.', 'U.S.'));
        options.add(new SelectOption('Western Europe', 'Western Europe'));
        return options;
    } 

    public static List<SelectOption> getTrackRecordReturnsOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'Please select....'));
        options.add(new SelectOption('TWR', 'TWR'));
        options.add(new SelectOption('IRR', 'IRR'));
        return options;
    }


    public static List<SelectOption> getSubmittorRoleOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'Please select....'));
        options.add(new SelectOption('Broker', 'Broker'));
        options.add(new SelectOption('Principal', 'Principal'));
        options.add(new SelectOption('Other', 'Other'));
        return options;
    }

    public static List<SelectOption> getYesNoOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'Please select....'));
        options.add(new SelectOption('Yes', 'Yes'));
        options.add(new SelectOption('No', 'No'));
        return options;
    }
    
    static List<Schema.PicklistEntry> howDidYouHearAboutUs_Picklist = Opportunity.How_did_you_hear_about_us__c.getDescribe().getPicklistValues();
    public static List<SelectOption> gethowDidYouHearAboutUsOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'Please select....'));
        for ( Schema.PicklistEntry pe : howDidYouHearAboutUs_Picklist ){
            options.add( new SelectOption(pe.getValue(),pe.getLabel()));
        }
//        options.add(new SelectOption('Attended CalPERS Event', 'Attended CalPERS Event'));
//        options.add(new SelectOption('Attended Industry Event', 'Attended Industry Event'));
//        options.add(new SelectOption('Referred by CalPERS Staff &/or Board', 'Referred by CalPERS Staff &/or Board'));
//        options.add(new SelectOption('Current CalPERS business partner', 'Current CalPERS business partner'));
//        options.add(new SelectOption('Referred by a third party that does business with CalPERS', 'Referred by a third party that does business with CalPERS'));
//       options.add(new SelectOption('Other', 'Other'));
        return options;
    }

    public static List<SelectOption> getSBASBICOptions(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Yes', 'Yes'));
        options.add(new SelectOption('No', 'No'));
        return options;
    }
    
    public static List<SelectOption> getCAFocusOptions(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Yes', 'Yes'));
        options.add(new SelectOption('No', 'No'));
        return options;
    }

    public static List<SelectOption> getOwnershipPositionOptions()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'Please select....'));
        options.add(new SelectOption('Private Real Estate Equity', 'Private Real Estate Equity'));
        options.add(new SelectOption('Private Real Estate Debt', 'Private Real Estate Debt'));
        options.add(new SelectOption('Public Real Estate Equity', 'Public Real Estate Equity'));
        options.add(new SelectOption('Public Real Estate Debt', 'Public Real Estate Debt'));

        return options;
    }

    public static List<SelectOption> getBasisForFundManagementFeeOptions()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'Please select....'));
        options.add(new SelectOption('Equity', 'Equity'));
        options.add(new SelectOption('Gross Asset Value', 'Gross Asset Value'));
        options.add(new SelectOption('Other', 'Other'));
        return options;
    }
    
    public static List<SelectOption> getSalesForceStrategy()
    {
        Schema.DescribeFieldResult fieldResult = Opportunity.Strategy__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        List<SelectOption> types = new List<SelectOption>();
        for( Schema.PicklistEntry f : ple)
        {
            types.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return types;
    }
    
    public static List<SelectOption> getSalesForceGeoFocus()
    {
        Schema.DescribeFieldResult fieldResult = Opportunity.Geographic_Focus__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        List<SelectOption> types = new List<SelectOption>();
        for( Schema.PicklistEntry f : ple)
        {
            types.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return types;
    }
    
    public static List<SelectOption> getSalesForceExistingCalpersRelationship()
    {
        Schema.DescribeFieldResult fieldResult = Account.Existing_CalPERS_Relationships__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        List<SelectOption> types = new List<SelectOption>();
        for( Schema.PicklistEntry f : ple)
        {
            types.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return types;
    }
    
    public static List<SelectOption> getSalesForceDiversityOptions()
    {
        Schema.DescribeFieldResult fieldResult = Opportunity.Diversity_Details__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        List<SelectOption> types = new List<SelectOption>();
        for( Schema.PicklistEntry f : ple)
        {
            types.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return types;
    }
    
    
    public static List<SelectOption> getSalesForceDiverseBusinessOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'Please select....'));
        options.add(new SelectOption('Yes', 'Yes'));
        options.add(new SelectOption('No', 'No'));
        options.add(new SelectOption('Decline to Respond', 'Decline to Respond'));
        return options;
    }

    
    public static List<SelectOption> getSalesForceDiversityRanges()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'Please Select...'));
        options.add(new SelectOption('0-24%', '0-24%'));
        options.add(new SelectOption('25-49%', '25-49%'));
        options.add(new SelectOption('50-74%', '50-74%'));
        options.add(new SelectOption('75-100%', '75-100%'));
        return options;
    }
     
        public static List<SelectOption> getSalesForceIndFocus()
    {
        Schema.DescribeFieldResult fieldResult = Opportunity.Industry_Focus__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        List<SelectOption> types = new List<SelectOption>();
        for( Schema.PicklistEntry f : ple)
        {
            types.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return types;
    }
    
    public static List<SelectOption> getSalesForceFundSector()
    {
        Schema.DescribeFieldResult fieldResult = Opportunity.Fund_Sector2__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        List<SelectOption> types = new List<SelectOption>();
        for( Schema.PicklistEntry f : ple)
        {
            types.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return types;
    }
    
    public static List<SelectOption> getRealEstateCurrencyValues()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('USD', 'USD'));
        return options;
    }

    public static List<SelectOption> getRealEstateFundSectorValues()
    {
        Schema.DescribeFieldResult fieldResult = Opportunity.Fund_Sector2__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        List<SelectOption> types = new List<SelectOption>();
        for( Schema.PicklistEntry f : ple)
        {
            if ( f.getLabel() != 'Value Added SA' && f.getLabel() != 'Opportunistic'){
                types.add(new SelectOption(f.getLabel(), f.getValue()));
            }
        }
        return types;
    }

    public static List<SelectOption> getRealEstateInvestmentVehicle()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Commingled Funds', 'Commingled Funds'));
        options.add(new SelectOption('Separate Account - Core', 'Separate Account - Core'));
        options.add(new SelectOption('Joint Venture', 'Joint Venture'));
        options.add(new SelectOption('Operating Company', 'Operating Company'));
        return options;
    }

    public static List<SelectOption> getRealEstatePartnershipGeoFocus()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Asia', 'Asia'));
        options.add(new SelectOption('Africa', 'Africa'));
        options.add(new SelectOption('North America', 'North America'));
        options.add(new SelectOption('Europe', 'Europe'));
        options.add(new SelectOption('Antarctica', 'Antarctica'));
        options.add(new SelectOption('Australia', 'Australia'));
        options.add(new SelectOption('India', 'India'));
        options.add(new SelectOption('South/Central America', 'South/Central America'));
        options.add(new SelectOption('U.S. - West', 'U.S. - West'));
        options.add(new SelectOption('U.S. - Mid-West', 'U.S. - Mid-West'));
        options.add(new SelectOption('U.S. - East', 'U.S. - East'));
        options.add(new SelectOption('U.S. - South', 'U.S. - South'));
        options.add(new SelectOption('Other', 'Other'));
        return options;
    } 

    public static List<SelectOption> getRealEstatePropertyType()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Apartment', 'Apartment'));
        options.add(new SelectOption('Industrial', 'Industrial'));
        options.add(new SelectOption('Office', 'Office'));
        options.add(new SelectOption('Retail', 'Retail'));
        options.add(new SelectOption('Mixed Use', 'Mixed Use'));
        options.add(new SelectOption('Hotel', 'Hotel'));
        options.add(new SelectOption('Housing', 'Housing'));
        options.add(new SelectOption('Land', 'Land'));
        options.add(new SelectOption('Senior Housing', 'Senior Housing'));
        options.add(new SelectOption('Healthcare Facilities', 'Healthcare Facilities'));
        options.add(new SelectOption('Technology', 'Technology'));
        options.add(new SelectOption('Entertainment', 'Entertainment'));
        options.add(new SelectOption('Recreation', 'Recreation'));
        options.add(new SelectOption('Self Storage', 'Self Storage'));
        options.add(new SelectOption('Other', 'Other'));
        return options;
    } 

    
    public static List<SelectOption> getRealEstateStrategy()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Core', 'Core'));
        options.add(new SelectOption('Value Added', 'Value Added'));
        options.add(new SelectOption('Opportunistic', 'Opportunistic'));
        return options;
    }
    
    public static List<SelectOption> getRealEstateMarket()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Developed', 'Developed'));
        options.add(new SelectOption('Emerging', 'Emerging'));
        options.add(new SelectOption('Frontier', 'Frontier'));
        return options;
    }
    
    public static List<SelectOption> getForestlandInvestmentVehicle()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Commingled Funds', 'Commingled Funds'));
        options.add(new SelectOption('Separate Account - Core', 'Separate Account - Core'));
        options.add(new SelectOption('Direct Investment', 'Direct Investment'));
        options.add(new SelectOption('Joint Venture', 'Joint Venture'));
        options.add(new SelectOption('Operating Company', 'Operating Company'));
        options.add(new SelectOption('Separate Account - Other', 'Separate Account - Other'));
        return options;
    }
    
    public static List<SelectOption> getForestlandDevStage()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Pre-Development', 'Pre-Development'));
        options.add(new SelectOption('Development', 'Development'));
        options.add(new SelectOption('Expansion', 'Expansion'));
        options.add(new SelectOption('Mature with some growth', 'Mature with some growth'));
        options.add(new SelectOption('Mature', 'Mature'));
        return options;
    }
    
    public static List<SelectOption> getForestlandGeoFocus()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Africa', 'Africa'));
        options.add(new SelectOption('Australia', 'Australia'));
        options.add(new SelectOption('Canada', 'Canada'));
        options.add(new SelectOption('Central and Eastern Europe', 'Central and Eastern Europe'));
        options.add(new SelectOption('China', 'China'));
        options.add(new SelectOption('Global', 'Global'));
        options.add(new SelectOption('India', 'India'));
        options.add(new SelectOption('Japan', 'Japan'));
        options.add(new SelectOption('LatAm', 'LatAm'));
        options.add(new SelectOption('MENA', 'MENA'));
        options.add(new SelectOption('North America', 'North America'));
        options.add(new SelectOption('OECD', 'OECD'));
        options.add(new SelectOption('Southeast Asia', 'Southeast Asia'));
        options.add(new SelectOption('U.S.', 'U.S.'));
        options.add(new SelectOption('Western Europe', 'Western Europe'));
        return options;
    }
    
    public static List<SelectOption> getInfrastructureStratgey()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Core', 'Core'));
        options.add(new SelectOption('Value Added', 'Value Added'));
        options.add(new SelectOption('Opportunistic', 'Opportunistic'));
        options.add(new SelectOption('Listed', 'Listed'));
        options.add(new SelectOption('Fixed Income', 'Fixed Income'));
        return options;
    }
    
    public static List<SelectOption> getInfrastructureDirectStratgey()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Fixed Income', 'Fixed Income'));
        options.add(new SelectOption('Equity', 'Equity'));
        return options;
    }
    
    public static List<SelectOption> getInfrastructureGeoFocus()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('U.S.', 'U.S.'));
        options.add(new SelectOption('Developed OECD ex US.', 'Developed OECD ex US'));
        options.add(new SelectOption('Less Developed', 'Less Developed'));
        return options;
    }

    public static List<SelectOption> getInfrastructurePartnershipGeoFocus()
    {
        Schema.DescribeFieldResult fieldResult = Opportunity.Geographic_Focus__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        List<SelectOption> options = new List<SelectOption>();
        for( Schema.PicklistEntry f : ple)
        {
            String val = f.getValue();
            if ( val != 'U.S. - Midwest' 
                && val != 'U.S. - Northeast' 
                && val != 'U.S. - South' 
                && val != 'U.S. - West' 
                && val != 'U.S. - Nationwide'
                && val != 'Asia'
                && val != 'Europe'
                && val != 'South America'
                && val != 'UK'
                && val != 'Other'
            ){
                options.add(new SelectOption(f.getLabel(), f.getValue()));
            }
        }
        options.add(new SelectOption('U.S.', 'U.S.'));
        options.add(new SelectOption('Developed OECD ex US.', 'Developed OECD ex US'));
        options.add(new SelectOption('Less Developed', 'Less Developed'));
        return options;
    }
    
    public static List<SelectOption> getInfrastructurePartnershipMarket()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Public', 'Public'));
        options.add(new SelectOption('Private', 'Private'));
        return options;
    }
        
    public static List<SelectOption> getInfrastructurePartnershipIndFocus()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Energy', 'Energy'));
        options.add(new SelectOption('Transport', 'Transport'));
        options.add(new SelectOption('Water', 'Water'));
        options.add(new SelectOption('Social', 'Social'));
        options.add(new SelectOption('Power', 'Power'));
        options.add(new SelectOption('Waste', 'Waste'));
        options.add(new SelectOption('Communications', 'Communications'));
        options.add(new SelectOption('Listed', 'Listed'));
        options.add(new SelectOption('Diversified', 'Diversified'));
        options.add(new SelectOption('Other', 'Other'));
        return options;
    }
    
    public static List<SelectOption> getGlobalEquityGeoFocus()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '-- None --'));
        options.add(new SelectOption('US', 'US'));
        options.add(new SelectOption('Emerging-Markets', 'Emerging-Markets'));
        options.add(new SelectOption('Global', 'Global'));
        options.add(new SelectOption('International', 'International'));
        options.add(new SelectOption('Opportunistic', 'Opportunistic'));
        return options;
    }
    
    public static List<SelectOption> getHedgeFundsGeoFocus() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '-- None --'));
        options.add(new SelectOption('US', 'US'));
        options.add(new SelectOption('International', 'International'));
        options.add(new SelectOption('Emerging-Markets', 'Emerging-Markets'));
        options.add(new SelectOption('Global', 'Global'));
        options.add(new SelectOption('Opportunistic', 'Opportunistic'));
        return options;
    }
    
    public static List<SelectOption> getGlobalEquityGeoSubFocus()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '-- None --'));
        options.add(new SelectOption('Intl: Developed', 'Intl: Developed'));
        options.add(new SelectOption('Intl: All World x US', 'Intl: All World x US'));
        options.add(new SelectOption('Intl: Regional', 'Intl: Regional'));
        options.add(new SelectOption('Oppt: Sector', 'Oppt: Sector'));
        options.add(new SelectOption('Oppt: Country', 'Oppt: Country'));
        return options;
    }
    
    public static List<SelectOption> getGlobalEquityGeoSubFocusByGeoFocus(String geoFocus)
    {
        Map<String, List<SelectOption>> gsfMap = new Map<String, List<SelectOption>>();
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('None', 'None'));
        options.add(new SelectOption('Opportunistic Sector', 'Opportunistic Sector'));
        options.add(new SelectOption('Other', 'Other'));
        gsfMap.put('US', options);
        
        options = new List<SelectOption>();
        options.add(new SelectOption('None', 'None'));
        options.add(new SelectOption('Opportunistic Sector', 'Opportunistic Sector'));
        options.add(new SelectOption('Opportunistic Country', 'Opportunistic Country'));
        options.add(new SelectOption('Other', 'Other'));
        gsfMap.put('Opportunistic', options);
        
        options = new List<SelectOption>();
        options.add(new SelectOption('None', 'None'));
        options.add(new SelectOption('Opportunistic Country', 'Opportunistic Country'));
        options.add(new SelectOption('Opportunistic Sector', 'Opportunistic Sector'));
        options.add(new SelectOption('Other', 'Other'));
        gsfMap.put('Emerging-Markets', options);
        
        options = new List<SelectOption>();
        options.add(new SelectOption('None', 'None'));
        options.add(new SelectOption('Opportunistic Sector', 'Opportunistic Sector'));
        options.add(new SelectOption('Other', 'Other'));
        gsfMap.put('Global', options);
        
        options = new List<SelectOption>();
        options.add(new SelectOption('None', 'None'));
        options.add(new SelectOption('Developed Markets', 'Developed Markets'));
        options.add(new SelectOption('All World ex US', 'All World ex US'));
        options.add(new SelectOption('Regional', 'Regional'));
        options.add(new SelectOption('Opportunistic Sector', 'Opportunistic Sector'));
        options.add(new SelectOption('Opportunistic Country', 'Opportunistic Country'));
        options.add(new SelectOption('Other', 'Other'));
        gsfMap.put('International', options);
        
        options = new List<SelectOption>();
        options.add(new selectOption('','Please Select....'));
        if (geoFocus != null && gsfMap.containsKey(geoFocus))
        {
            options.addAll(gsfMap.get(geoFocus));
        }
        
        return options;
    }
    
    public static List<SelectOption> getGlobalEquityStrategy()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '-- None --'));
        options.add(new SelectOption('All Cap', 'All Cap'));
        options.add(new SelectOption('Large', 'Large'));
        options.add(new SelectOption('Mid', 'Mid'));
        options.add(new SelectOption('Small', 'Small'));
        options.add(new SelectOption('SMID', 'SMID'));
        options.add(new SelectOption('Other', 'Other'));
        return options;
    }
    
    public static List<SelectOption> getHedgeFundsStrategy() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '-- None --'));
        options.add(new SelectOption('CTA/manage futures', 'CTA/manage futures'));
        options.add(new SelectOption('Equity long/short', 'Equity long/short'));
        options.add(new SelectOption('Credit long/short', 'Credit long/short'));
        options.add(new SelectOption('Convertible arbitrage', 'Convertible arbitrage'));
        options.add(new SelectOption('Statistical arbitrage', 'Statistical arbitrage'));
        options.add(new SelectOption('Event driven', 'Event driven'));
        options.add(new SelectOption('Multi-strategy', 'Multi-strategy'));
        options.add(new SelectOption('Distressed', 'Distressed'));
        options.add(new SelectOption('Relative Value', 'Relative Value'));
        options.add(new SelectOption('Market Neutral', 'Market Neutral'));
        options.add(new SelectOption('Fund of Fund', 'Fund of Fund'));
        options.add(new SelectOption('Other', 'Other'));
        return options;
    }
    
    public static List<SelectOption> getGlobalEquitySubStrategy()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '-- None --'));
        options.add(new SelectOption('Core', 'Core'));
        options.add(new SelectOption('Growth', 'Growth'));
        options.add(new SelectOption('Value', 'Value'));
        options.add(new SelectOption('Other', 'Other'));
        return options;
    }
    
    public static List<SelectOption> getFixedIncomeGeoFocus()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '-- None --'));
        options.add(new SelectOption('US', 'US'));
        options.add(new SelectOption('Global', 'Global'));
        options.add(new SelectOption('International', 'International'));
        options.add(new SelectOption('Opportunistic', 'Opportunistic'));
        return options;
    }
    
    public static List<SelectOption> getFixedIncomeStrategy()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '-- None --'));
        options.add(new SelectOption('Asset-Backed', 'Asset-Backed'));
        options.add(new SelectOption('Bank Loans', 'Bank Loans'));
        options.add(new SelectOption('Corporate', 'Corporate'));
        options.add(new SelectOption('Currency', 'Currency'));
        options.add(new SelectOption('Global Linkers', 'Global Linkers'));
        options.add(new SelectOption('High Yield', 'High Yield'));
        options.add(new SelectOption('MBS', 'MBS'));
        options.add(new SelectOption('Sovereign', 'Sovereign'));
        options.add(new SelectOption('TIPS', 'TIPS'));
        options.add(new SelectOption('U.S. Government', 'U.S. Government'));
        return options;
    }
    
    public static List<SelectOption> getCommodityStrategy()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '-- None --'));
        options.add(new SelectOption('Enhanced Index', 'Enhanced Index'));
        options.add(new SelectOption('Active', 'Active'));
        options.add(new SelectOption('Absolute return', 'Absolute return'));
        options.add(new SelectOption('Other', 'Other'));
        return options;
    }
    
    public static List<SelectOption> getGlobalEquityCurrencyValues()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '-- None --'));
        options.add(new SelectOption('USD', 'USD'));
        return options;
    }

    
    public static List<SelectOption> getInvestmentTypeByAssetClass(String assetClass)
    {
        Map<String, List<SelectOption>> typeMap = new Map<String, List<SelectOption>>();
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Partnership', 'Partnership'));
        options.add(new SelectOption('Direct / Co-Invest', 'Direct / Co-Invest'));
        typeMap.put('Private Equity', options);
        
        options = new List<SelectOption>();
        options.add(new SelectOption('Partnership', 'Partnership'));
        options.add(new SelectOption('Direct', 'Direct'));
        typeMap.put('Infrastructure', options);
        
        options = new List<SelectOption>();
        options.add(new SelectOption('', 'Please Select...'));
        if (typeMap != null && typeMap.containsKey(assetClass))
        {
            options.addAll(typeMap.get(assetClass));
        }
        
        return options;
    }
}