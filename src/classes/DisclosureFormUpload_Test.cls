@isTest
private class DisclosureFormUpload_Test {

    static testMethod void myUnitTest() {
		Disclosure_Form__c df = new Disclosure_Form__c();
		insert df;

		Attachment a = new Attachment();
		a.name ='test';
		Blob body = blob.valueOf('test');
		a.body = body;
		
				
		
		test.startTest();

			ApexPages.StandardController sc = new ApexPages.standardController(df);
			DisclosureFormUpload_Extension ext = new DisclosureFormUpload_Extension(sc);
		
			ext.attachment = a;
			ext.uploadAttachment();
			ext.backToDisclosureForm();	
		test.stopTest();
    }
}