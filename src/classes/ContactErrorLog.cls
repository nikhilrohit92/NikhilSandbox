public with sharing class ContactErrorLog {
    public boolean submitterFirstNameError {get; set;}
    public boolean submitterLastNameError {get; set;}
    public boolean submitterPhoneError {get; set;}
    public boolean submitterEmailError {get; set;}
    public boolean submitterEmailFormatError {get; set;}
    public boolean submitterTitleError {get; set;}
    public boolean submitterTitleSizeError {get; set;}

    public boolean firstNameError {get; set;}
    public boolean LastNameError {get; set;}
    public boolean phoneError {get; set;}
    public boolean emailError {get; set;}
    public boolean emailFormatError {get; set;}
    public boolean streetError {get; set;}
    public boolean streetSizeError {get; set;}
    public boolean cityError {get; set;}
    public boolean stateError {get; set;}
    public boolean postalCodeError {get; set;}
    
    public boolean titleError {get; set;}
    public boolean titleSizeError {get; set;}
    public boolean countryError {get; set;}
    public boolean faxError {get; set;}
    
    //Lead
    public boolean nameError {get; set;}
    public boolean companyError {get; set;}
    public boolean emailError2 {get; set;}
    public boolean emailFormatError2 {get; set;}
    //Question
    public boolean askQuestionError {get; set;}

}