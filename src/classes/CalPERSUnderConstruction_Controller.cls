public with sharing class CalPERSUnderConstruction_Controller {
	
	public string websiteEmail {get;set;}
	public string passkey = '123456';
	
	public PageReference submit(){
		if ( websiteEmail == passkey){
			return page.IPTS_StartPage;
		}
		return null;
	}

}