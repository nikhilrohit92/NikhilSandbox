//Helper Class to manage Attachments
public without sharing class AttachmentManager 
{
    public Map<String, AttachmentWrapper> attachmentsById {get; set;}
    public String context {get; set;}
    public static final String LISTCONTEXT = 'LISTCONTEXT';
    public static final String ADDCONTEXT = 'ADDCONTEXT';
    public static final String EDITCONTEXT = 'EDITCONTEXT';
    public String folderName;
    public Attachment attachmentRef {get; set;}
    public boolean attachfieldfocus1  {get; set;}
    public boolean attachfieldfocus2  {get; set;}
    public ErrorHandler ehandler {get; set;}      
    
    public class AttachmentWrapper
    {
        public String  documentId {get; set;}
        public String fileName {get; set;}
        public String ContentType {get; set;}
        public String Description {get; set;}
    }
    
    public AttachmentManager()
    {
        attachmentsById = new Map<String, AttachmentWrapper>();
        attachmentRef = new Attachment();
        context = LISTCONTEXT;
    }
    

    
    public PageReference addAttachment()
    {
    system.debug('**add context** '+context);
        attachfieldfocus2 = true;
        if (context == ADDCONTEXT){
            system.debug(folderName);
            List<Folder> f = [Select Id, Name from Folder where Type = 'Document' and Name = :folderName limit 1];
            if ( f.size() == 0 ){
                f = [Select Id, Name from Folder where Type = 'Document' and Name = :constants.genericDocumentFolder limit 1];
                //send Email
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                //String[] toAddresses = new String[] {'logins@mkpartners.com'}; 
                String[] toAddresses = new String[] {'kevin.garcia@calpers.ca.gov','ipts_salesforce_support@calpers.ca.gov', 'ipts_system_process@calpers.ca.gov'}; 
                mail.setToAddresses(toAddresses);
                mail.setReplyTo('ipts_salesforce_support@calpers.ca.gov');
                mail.setSenderDisplayName('Salesforce Support');
                mail.setSubject('Alert: A Document Folder Name Change has Occurred');
                mail.setPlainTextBody('Be advised that the '+folderName+' IPTS Documents Folder name has been changed.  To avoid unintentional deletions of submission attachments, please make necessary checks to ensure attachments are being properly filed upon opportunity submission.');
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
            if ( f.size() > 0 ){
                
                //DML not allowed on folder. So Cannot create one here.
                Document newDocument = new Document();
                //newDocument.AuthorId = userId;
                newDocument.Body = attachmentRef.Body;
                newDocument.ContentType = attachmentRef.ContentType;
              //  newDocument.Description = attachmentRef.Description;
                newDocument.Name = attachmentRef.Name;
                newDocument.FolderId = f[0].Id;
                insert newDocument;
                
                //insert attachmentRef;
                AttachmentWrapper w = new AttachmentWrapper();
                w.documentId = newDocument.Id;
                w.fileName = newDocument.Name;
                w.ContentType = newDocument.ContentType;
                attachmentsById.put(newDocument.Id, w);
                attachmentRef = new Attachment();

            }

        }

        if (context == EDITCONTEXT){
            String aid = ApexPages.currentPage().getParameters().get('attachment.parentid');
            String ctype = ApexPages.currentPage().getParameters().get('attachment.ContentType');
            system.debug('**add EDITCONTEXT ** ');
            system.debug('**add attachment id ** ' + aid);
            system.debug('**add contenttype ** ' + ctype );
            List<Document> doc = [select Id, name, body, ContentType from document where Id = :aid limit 1]; 
            doc[0].ContentType = ctype ;
            upsert doc;
            
                AttachmentWrapper w = new AttachmentWrapper();
                w.documentId = doc[0].Id;
                w.fileName = doc[0].Name;
                w.ContentType = ctype ;
                attachmentsById.put(doc[0].Id, w);
                attachmentRef = new Attachment();            
                
        }
        context = LISTCONTEXT;
        return null;
    }
    
    public PageReference cancelAttachment()
    {
        attachmentRef = new Attachment();
        context = LISTCONTEXT;

        return null;
    }
    
    public PageReference newAttachment()
    {
        attachmentRef = new Attachment();
        attachfieldfocus2 = true;
        context = ADDCONTEXT;
        return null;
    }
    
    public PageReference addMoreAttachment()
    {
        addAttachment();
        return newAttachment();
    }

    public PageReference editAttachment()
    {
        attachfieldfocus1 = true;
        String aid = ApexPages.currentPage().getParameters().get('attachment.id');
        system.debug('**edit attachment id ** ' + aid);
        if (attachmentsById.containsKey(aid))
        {
            
            List<Folder> f = [Select Id, Name from Folder where Type = 'Document' and Name = :folderName limit 1];
            system.debug('**edit attachment id ** ' + aid);
            List<Document> doc = [select Id, name, body, ContentType from document where Id = :aid]; // and //folderid = :f[0].name limit 1];
            system.debug('**edit doc **' + doc);
            if (doc.size()>0)
            {
                
                attachmentRef.body= doc[0].body;
                attachmentRef.Name = doc[0].Name;
                attachmentRef.ContentType = doc[0].ContentType;
                attachmentRef.ParentId = doc[0].Id;

            }

        }
        context = EDITCONTEXT;
        return null;
    }
        
    public PageReference removeAttachment()
    {
        String aid = ApexPages.currentPage().getParameters().get('attachment.id');
        system.debug('**remove attachment id ** ' + aid);
        if (attachmentsById.containsKey(aid))
        {
            attachmentsById.remove(aid);
            List<Attachment> doc = [select Id from Attachment where Id = :aid];
            system.debug('**remove doc **' + doc);
            if (doc.size()>0)
            {
                delete doc;
            }
            else{
            List<Document> doc2 = [select Id from Document where Id = :aid];
            
            if (doc2.size()>0)
            {
                delete doc2;
            }
            
            
            }
        }
        context = LISTCONTEXT;
        return null;
    }

    public boolean docCheck(String docname)      
        {       
        for( AttachmentWrapper doclist : attachmentsById.values() ) {
            if (docname == doclist.ContentType) 
                return false;
                }
        return true;            
    }
                    
    public List<AttachmentWrapper> getAttachments()
    {
        return attachmentsById.values();
    }
    
    public Boolean getHasAttachments()
    {
        if (!attachmentsById.isEmpty())
        {
            return true;
        }
        return false;
    }
    
    public Boolean getIsListContext()
    {
        if (context == LISTCONTEXT)
        {
            return true;
        }
        return false;
    }
    
    public Boolean getIsAddContext()
    {
        if (context == ADDCONTEXT)
        {
            return true;
        }
        return false;
    }
    
    public Boolean getIsEditContext()
    {
        if (context == EDITCONTEXT)
        {
            return true;
        }
        return false;
    }
    


    
   @istest
   static void attachmentmanager(){
    AttachmentManager am = new AttachmentManager();     
   }  
      
}