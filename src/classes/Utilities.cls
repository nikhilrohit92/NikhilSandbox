global class Utilities {

	public static Decimal fixNulls(Decimal d){
		if ( d == null ){
			d = 0;
		}
		return d;
	}

	public static string fixStringCase(String s){
		if ( s != null ){
			s = s.trim();
			String newS = '';
			if ( s.length() > 0 ){
				s = s.toLowerCase();
				newS = s.substring(0,1).toUpperCase();
				for ( Integer i=1; i<s.length(); i++){
					String lastLetter = s.subString(i-1,i);
					if ( lastLetter == ' ' || lastLetter == '-' ){
						newS += s.subString(i,i+1).toUpperCase();
					} else {
						newS += s.subString(i,i+1);
					}
				}
			}
			return newS;
		} else {
			return null;
		}
	}
	
	public static String getSelectedString(List<CheckboxWrapper> lst){
		String selectedStr = null;
		if (lst != null && !lst.isEmpty()){
			selectedStr = '';
			for (CheckboxWrapper w : lst){
				if (w.isSelected()){
					if (selectedStr.length() > 0){
						selectedStr += ';';
					}
					selectedStr += w.getValue();
				}	
			}
		}
		return selectedStr;
	}

	public static void selectCheckboxesFromField(List<CheckboxWrapper> lst, String fieldValue){
		if ( string.isNotBlank(fieldValue) && lst != null ){
			Set<String> selectedValues = new Set<String>(fieldValue.split(';'));
			for ( CheckboxWrapper w : lst ){
				if ( selectedValues.contains( w.optionRef.getValue() ) ){
					w.selected = true;
				}
			}
		}
	}
	
	public static boolean isValidDateString(String dateString){
		boolean isValid = false;
		if (dateString != null && dateString.trim().length() > 0)
		{
			//(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d
			Pattern datePattern = Pattern.compile('(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\\d\\d');
			Matcher dateMatcher = datePattern.matcher(dateString);
			return dateMatcher.matches();
		}
		return isValid;
	}
	
	public static boolean isValidDateStringGE(String dateString)
	{
		boolean isValid = false;
		if (dateString != null && dateString.trim().length() > 0)
		{
			//(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d
			Pattern datePattern = Pattern.compile('(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](17|18|19|20)\\d\\d');
			Matcher dateMatcher = datePattern.matcher(dateString);
			return dateMatcher.matches();
		}
		return isValid;
	}
	public static boolean isValidEmailString(String emailString)
	{
		boolean isValid = true;
		if (emailString != null && emailString.trim().length() > 0)
		{
			MKP007.validator v = new MKP007.validator();
			isValid = v.validateEmail(emailString);
			//[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])? 
//			Pattern emailPattern = Pattern.compile('(\\w+)@(\\w+\\.)(\\w+)(\\.\\w+)*');
//			Matcher emailMatcher = emailPattern.matcher(emailString);
//			return emailMatcher.matches();
		}
		return isValid;
	}
	
	public static Date getDate(String dateString){
		Date dt = null;
		if (dateString != null && dateString.trim().length() > 0)
		{
			String[] dtArray = dateString.split('/');
			if (dtArray.size() == 3)
			{
				Integer month = Integer.valueOf(dtArray[0]);
				Integer day = Integer.valueOf(dtArray[1]);
				Integer year = Integer.valueOf(dtArray[2]);
				dt = Date.newInstance(year, month, day);
			}
		}
		return dt;
	}

	public static String toDateString(Date dt){
		String dstr ='';
		if(dt != null){
		dstr = (dt.month() < 10) ? ('0' + dt.month()) : ('' + dt.month());
		dstr += '/';
		dstr += (dt.day() < 10) ? ('0' + dt.day()) : ('' + dt.day());
		dstr += '/';
		dstr += dt.year();
		}
		return dstr;  
	}

	public static Opportunity queryOpportunityById(Id OpptyId){
		String queryString = 'Select '+opportunityHelper.getOpportunityFieldNamesString();
		for ( String f : constants.ACCOUNT_SCHEMA_MAP.keySet() ){
			queryString += ', Account.'+f;
		}
		queryString += ', Placement_Agent__r.Id, Placement_Agent__r.FirstName, Placement_Agent__r.LastName, Placement_Agent__r.Fax, Placement_Agent__r.Phone, Placement_Agent__r.Email, Placement_Agent__r.MailingStreet, Placement_Agent__r.MailingCity, Placement_Agent__r.MailingState, Placement_Agent__r.MailingPostalCode, Placement_Agent__r.MailingCountry';
		queryString += ', (Select Id, OpportunityId, ContactId, Contact.AccountId, Contact.Salutation, Contact.FirstName, Contact.LastName, Contact.Email, Contact.Phone, Contact.Fax, Contact.Id, Contact.MailingCity, Contact.MailingCountry, Contact.MailingState, Contact.MailingStreet, Contact.MailingPostalCode, Contact.Title, Role, IsPrimary From OpportunityContactRoles order by Contact.CreatedDate) ';
		queryString += ', (Select Id, Name, RecordTypeId, Opportunity__c, C_A_DPI__c, C_A_Net_IRR__c, C_A_RVPI__c, C_A_TVPI__c, Capital_Account_Date__c, DPI__c, Discount_Offered__c, EBITDA__c, Existing_Investment_Opportunity__c, Funded__c, GP_Catch_Up__c, Gross_Margin__c, Investment_Period_of_Years__c, Net_IRR__c, PEG_DPI__c, PEG_Net_IRR__c, PEG_RVPI__c, PEG_TVPI__c, Post_Investment_Period_Mgt_Fee__c, RVPI__c, Revenue__c, Seller_NAV_Local__c, TVPI__c, Total_Commitment_Local__c, Total_Exposure__c, Total_Value__c, Unfunded_Commitment_Local__c, VEX_DPI__c, VEX_Net_IRR__c, VEX_RVPI__c, VEX_TVPI__c, Vintage_Year__c, Series__c, Equity__c, PIK_Note__c, Mezzanine__c, Senior_Loans__c, Calendar_Year_Return__c, Committed_Capital_In_Millions__c, Contributed_Capital_In_Millions__c, Distributed_Value_In_Millions__c, Explanation_of_Returns__c, Gross_IRR__c, Gross_Multiple__c, Invested__c, Net_Multiple__c, No_of_Portfolio_Companies__c, Product_Benchmark_Return__c, Realized_In_Millions__c, Size__c, Total_Value_In_Millions__c, Unrealized_In_Millions__c, Product_Return__c, Benchmark_Return__c, As_of_Date__c, Product_Net_Return__c, Benchmark_Net_Return__c, Investment_Vehicle__c, Property_Type__c, Total_Net_Asset_Value_in_Millions__c, Start_Date_of_Track_Record__c, TWR_or_IRR__c, After_Fee_Leveraged_Return__c, After_Fee_Unleveraged_Return__c, Before_Fee_Leveraged_Return__c, Before_Fee_Unleveraged_Return__c, Description_on_Track_Record__c, Net_DPI__c From Historical_Returns__r order by createddate) ';
		queryString += ' from Opportunity where Id = \'' + opptyId + '\' limit 1';
		system.debug(queryString);
		
			System.debug('****** in here');
  		return Database.query(queryString);
  	
	}

	public static List<Opportunity> queryDuplicateOpportunities(Id opptyId, String email, String opptyName ){
		String queryString = 'Select Id from Opportunity where StageName = \'Draft\' and Submitter_Email__c = \'' + email + '\' and Name = \'' + opptyName + '\' ';
		if ( opptyId != null ){
			queryString += ' and Id != \''+opptyId+'\' ';
		}
		return database.query(queryString);
	}

	public static Opportunity queryOpportunityByEmailAndName(String email,String name,String confirmationNumber){
		String queryString = 'Select Id from Opportunity where StageName = \'Draft\' and Submitter_Email__c = \'' + email + '\' and ( ';
		if ( string.isNotBlank(name) ){
			queryString += ' Opportunity.Name = \'' + string.escapeSingleQuotes(name) + '\' or'; 
		}
		if ( string.isNotBlank(confirmationNumber) ){
			queryString += ' Opportunity.Confirmation_Number__c LIKE \'%' + confirmationNumber + '%\' or ';
		}
		queryString = queryString.subStringBeforeLast('or');
		queryString += ')  limit 1';
		List<Opportunity> results = Database.query(queryString);
		if ( results.size() == 1 ){
			return results[0];
		}
		return null;
	}


	//This method is used to encode a Salesforce plain text Id and pass to Magni
	public static String encodeSalesforceId(String r){
		for ( Integer z=0;z<14;z++){
			r += '0';
		}
		Blob b = crypto.encrypt('AES256',encodingUtil.base64Decode(constants.ENCODING_KEY),encodingUtil.base64Decode(constants.ENCODING_IV),encodingUtil.base64Decode(r));
		return encodingUtil.base64Encode(b);
	}

	//This method is used to decode an encoded plain text string from Magni into a plain text Salesforce Id
	public static String decodeSalesforceId(String i){
		Blob b = crypto.decrypt('AES256',encodingUtil.base64Decode(constants.ENCODING_KEY),encodingUtil.base64Decode(constants.ENCODING_IV),encodingUtil.base64Decode(i));
		String r = encodingUtil.base64Encode(b);
		return r = r.subString(0,18);
	}

}