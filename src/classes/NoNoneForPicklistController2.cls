public class NoNoneForPicklistController2 {

public Change_control_request__c ccRequest{get;set;}


public List<SelectOption> securityOptions {get;set;}

// Constructor called when page is accessed.
public NoNoneForPicklistController2() {

ccRequest= new Change_control_request__c(); 
securityOptions = new List<SelectOption>();

// Use DescribeFieldResult object to retrieve status field.
Schema.DescribeFieldResult securityFieldDescription = Change_control_request__c.Type_of_Request__c.getDescribe();

// For each picklist value, create a new select option
for (Schema.Picklistentry picklistEntry: securityFieldDescription.getPicklistValues()){

securityOptions.add(new SelectOption(pickListEntry.getValue(),pickListEntry.getLabel()));

// obtain and assign default value
if (picklistEntry.defaultValue){
ccRequest.Type_of_Request__c = pickListEntry.getValue();
} 
} 
}

public List<SelectOption> getAllsecurityOptions ()
{

List<SelectOption> securityOptions = new List<SelectOption>();

Schema.DescribeFieldResult securityFieldDescription = Change_control_request__c.Type_of_Request__c.getDescribe();

for (Schema.Picklistentry picklistEntry: securityFieldDescription.getPicklistValues()){

securityOptions.add(new SelectOption(pickListEntry.getValue(),pickListEntry.getLabel()));

if (picklistEntry.defaultValue){
ccRequest.Type_of_Request__c = pickListEntry.getValue();
} 
}
return securityOptions;
}

}