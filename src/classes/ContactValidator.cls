//Apex class to validate contact fields
public with sharing class ContactValidator 
{
	private static Boolean isValidFirstName(Contact cont)
	{
		return (cont != null && cont.FirstName == null) ? false : true;
	}
	
	private static Boolean isValidLastName(Contact cont)
	{
		return (cont != null && cont.LastName == null) ? false : true;
	}
	
	private static Boolean isValidPhone(Contact cont)
	{
		return (cont != null && cont.Phone == null) ? false : true;
	}
	
	private static Boolean isValidEmail(Contact cont)
	{
		return (cont != null && cont.Email == null) ? false : true;
	}
	
	private static Boolean isValidStreet(Contact cont)
	{
		return (cont != null && cont.MailingStreet != null && cont.MailingStreet.trim().length() > 0) ? true : false;
	}
	
	private static Boolean isValidCity(Contact cont)
	{
		return (cont != null && cont.MailingCity == null) ? false : true;
	}
	
	private static Boolean isValidState(Contact cont)
	{
		return (cont != null && cont.MailingState == null) ? false : true;
	}
	
	private static Boolean isValidPostalCode(Contact cont)
	{
		return (cont != null && cont.MailingPostalCode == null) ? false : true;
	}
	
	private static Boolean isValidCountry(Contact cont)
	{
		return (cont != null && cont.MailingCountry == null) ? false : true;
	}
	
	private static Boolean isValidTitle(Contact cont)
	{
		return (cont != null && cont.Title == null) ? false : true;
	}
	
	public static boolean isValidEmailString(Contact contactRef, String emailString, String fieldId, ErrorHandler ehandler, String idPrefix){
		boolean isValid = true;
		if (emailString != null && emailString.trim().length() > 0){
			if ( Utilities.isValidEmailString(emailString) ){
				contactRef.Email = emailString;
			} else {
				ehandler.addError(fieldId, 'Email', ErrorHandler.INVALID_EMAIL_FORMAT, ErrorHandler.INVALID_EMAIL_FORMAT);
				if ( idPrefix == 'Submitter' ){
					ehandler.clog.SubmitterEmailFormatError = true;
				} else {
					ehandler.clog.emailFormatError = true;
				}
				isValid = false;
			}
		} else {
			ehandler.addError(fieldId, 'Email', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			if ( idPrefix == 'Submitter' ){
				ehandler.clog.SubmitterEmailError = true;
			} else {
				ehandler.clog.emailError = true;
			}
		}
		return isValid;
	}
	
	public static boolean isValidReferrerContact(Contact referrerContact, String referrerEmailString, ErrorHandler ehandler){
		boolean isValid = true;
		if (!isValidFirstName(referrerContact)) {
			ehandler.addError('thisPage:thisForm:ReferrerFirstName', 'First Name', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.clog.firstNameError = true;
		}
		if (!isValidLastName(referrerContact)){
			ehandler.addError('thisPage:thisForm:ReferrerLastName', 'Last Name', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.clog.lastNameError = true;
		}
		if (!isValidPhone(referrerContact)){
			ehandler.addError('thisPage:thisForm:ReferrerPhone', 'Phone', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.clog.phoneError = true;
		}
		boolean validEmail = isValidEmailString(referrerContact, referrerEmailString, 'thisPage:thisForm:ReferrerEmail', ehandler, '');
		if (!isValidStreet(referrerContact)) {
			ehandler.addError('thisPage:thisForm:ReferrerStreet', 'Street', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.clog.streetError = true;
		} else if (referrerContact.MailingStreet.trim().length() > 255) {
			String msg = 'Field has ' + referrerContact.MailingStreet.trim().length() + ' characters. Only 255 allowed.';
			ehandler.addError('thisPage:thisForm:ReferrerStreet', 'Street', msg, msg);
			isValid = false;
			ehandler.clog.streetSizeError = true;
		}
		
		if (!isValidCity(referrerContact))
		{
			ehandler.addError('thisPage:thisForm:ReferrerCity', 'City', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.clog.cityError = true;
		}
		if (!isValidState(referrerContact))
		{
			ehandler.addError('thisPage:thisForm:ReferrerState', 'State', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.clog.stateError = true;
		}
		if (!isValidPostalCode(referrerContact))
		{
			ehandler.addError('thisPage:thisForm:ReferrerPostalCode', 'Postal Code', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.clog.postalCodeError = true;
		}
		return isValid && validEmail;
	}
	
	public static boolean isValidPlacementAgentContact(Contact placementAgent, String agentEmailString, ErrorHandler ehandler)
	{
		boolean isValid = true;
		if (!isValidFirstName(placementAgent))
		{
			ehandler.addError('thisPage:thisForm:AgentFirstName', 'First Name', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.clog.firstNameError = true;
		}
		if (!isValidLastName(placementAgent))
		{
			ehandler.addError('thisPage:thisForm:AgentLastName', 'Last Name', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.clog.lastNameError = true;
		}
		if (!isValidPhone(placementAgent))
		{
			ehandler.addError('thisPage:thisForm:AgentPhone', 'Phone', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.clog.phoneError = true;
		}
		
		boolean validEmail = isValidEmailString(placementAgent, agentEmailString, 'thisPage:thisForm:AgentEmail', ehandler, '');
		
		if (!isValidStreet(placementAgent))
		{
			ehandler.addError('thisPage:thisForm:AgentStreet', 'Street', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.clog.streetError = true;
		}
		else if (placementAgent.MailingStreet.trim().length() > 255)
		{
			String msg = 'Field has ' + placementAgent.MailingStreet.trim().length() + ' characters. Only 255 allowed.';
			ehandler.addError('thisPage:thisForm:AgentStreet', 'Street', msg, msg);
			isValid = false;
			ehandler.clog.streetSizeError = true;
		}
		
		if (!isValidCity(placementAgent))
		{
			ehandler.addError('thisPage:thisForm:AgentCity', 'City', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.clog.cityError = true;
		}
		if (!isValidState(placementAgent))
		{
			ehandler.addError('thisPage:thisForm:AgentState', 'State', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.clog.stateError = true;
		}
		if (!isValidPostalCode(placementAgent))
		{
			ehandler.addError('thisPage:thisForm:AgentPostalCode', 'Postal Code', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.clog.postalCodeError = true;
		}
		return isValid && validEmail;
	}
	
	
	public static boolean isValidFirmContact(Contact submittorContact, String submittorEmailString, ErrorHandler ehandler, String idPrefix){
		boolean isValid = true;
		if (!isValidFirstName(submittorContact)){
			ehandler.addError('thisPage:thisForm:'+idPrefix+'FirstName', 'First Name', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			if ( idPrefix == 'Submitter' ){
				ehandler.clog.submitterFirstNameError = true;
			} else {
				ehandler.clog.firstNameError = true;
			}
		}
		if (!isValidLastName(submittorContact)){
			ehandler.addError('thisPage:thisForm:'+idPrefix+'LastName', 'Last Name', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			if ( idPrefix == 'Submitter' ){
				ehandler.clog.submitterLastNameError = true;
			} else {
				ehandler.clog.lastNameError = true;
			}
		}
		if (!isValidTitle(submittorContact)){
			ehandler.addError('thisPage:thisForm:'+idPrefix+'Title', 'Title', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			if ( idPrefix == 'Submitter' ){
				ehandler.clog.submitterTitleError = true;
			} else {
				ehandler.clog.titleError = true;
			}
		}
		if (!isValidPhone(submittorContact)){
			ehandler.addError('thisPage:thisForm:'+idPrefix+'Phone', 'Phone', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			if ( idPrefix == 'Submitter' ){
				ehandler.clog.submitterPhoneError = true;
			} else {
				ehandler.clog.phoneError = true;
			}
		}
		
		boolean validEmail = isValidEmailString(submittorContact, submittorEmailString, 'thisPage:thisForm:'+idPrefix+'Email', ehandler, idPrefix);
		return isValid && validEmail;
	}

	public static boolean isValidContactRole(Contact contactRef, String emailString, ErrorHandler ehandler){
		boolean isValid = true;
		if (isValidTitle(contactRef))
		{
			if (contactRef.Title.trim().length() > 40)
			{
				String msg = 'Field has ' + contactRef.Title.trim().length() + ' characters. Only 40 allowed.';
				ehandler.addError('thisPage:thisForm:Title', 'Title', msg, msg);
				isValid = false;
				ehandler.clog.titleSizeError = true;
			}
		}
		return isValid;
	}
	
	public static boolean isValidContact(Contact contactRef, String emailString, ErrorHandler ehandler){
		boolean isValid = true;
		if (!isValidFirstName(contactRef)){
			ehandler.addError('thisPage:thisForm:FirstName', 'First Name', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.clog.firstNameError = true;
		}
		if (!isValidLastName(contactRef)){
			ehandler.addError('thisPage:thisForm:LastName', 'Last Name', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.clog.lastNameError = true;
		}
		if (!isValidTitle(contactRef)){
			ehandler.addError('thisPage:thisForm:Title', 'Title', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.clog.titleError = true;
		}
		if (!isValidPhone(contactRef)){
			ehandler.addError('thisPage:thisForm:Phone', 'Phone', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.clog.phoneError = true;
		}
		
		boolean validEmail = isValidEmailString(contactRef, emailString, 'thisPage:thisForm:Email', ehandler, '');
		
		if (!isValidStreet(contactRef))
		{
			ehandler.addError('thisPage:thisForm:Street', 'Street', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.clog.streetError = true;
		}
		else if (contactRef.MailingStreet.trim().length() > 255)
		{
			String msg = 'Field has ' + contactRef.MailingStreet.trim().length() + ' characters. Only 255 allowed.';
			ehandler.addError('thisPage:thisForm:Street', 'Street', msg, msg);
			isValid = false;
			ehandler.clog.streetSizeError = true;
		}
		
		if (!isValidCity(contactRef))
		{
			ehandler.addError('thisPage:thisForm:City', 'City', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.clog.cityError = true;
		}
		if (!isValidState(contactRef))
		{
			ehandler.addError('thisPage:thisForm:State', 'State', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.clog.stateError = true;
		}
		if (!isValidPostalCode(contactRef))
		{
			ehandler.addError('thisPage:thisForm:PostalCode', 'Postal Code', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.clog.postalCodeError = true;
		}
		if (!isValidCountry(contactRef))
		{
			ehandler.addError('thisPage:thisForm:Country', 'Country', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.clog.countryError = true;
		}
		return isValid && validEmail;
	}
}