global class ASPClone 
{
  webService static Id cloneASP(id camid) 
    {
        List<asp_component__c > comps = new List<asp_component__c >{};
        List<Proposal_Question__c> props = new List<Proposal_Question__c>{}; 

        
        Campaign newCam = new Campaign (name = 'test', Solicitation_No__c = 'test', Solicitation_Type__c = 'test', ASP_Short_Name__c = 'test', Published__c = 'No', 
        Asset_Class__c = 'test' , Status ='test');
        insert newCam;
        

        for (ASP_Document__c doc :  [Select Id, Document_Name__c, Short_Name__c, type__c, Tags__c from ASP_Document__c where ASP_Name__c = :camId]) {
             
            ASP_Document__c newDoc = doc.clone(false, true);
            newDoc.ASP_Name__c = newCam.id;

            insert newDoc;

        }      
                
        for (asp_component__c comp :  [Select Id, name, Title__c, TextValue__c, type__c, Page_No__c, Proposal_Name_Page__c, Order__c, ASP_ID__c, active__c,
            (Select Id, Field_Title__c, ASP_Component__c, Order__c, active__c   from proposal_questions__r)   from ASP_Component__c where ASP_ID__c = :camId]) {
            
            asp_component__c newComp = comp.clone(false, true);
            newComp.ASP_ID__c = newCam.id;

            insert newComp;
            
            for(proposal_question__c pq : comp.proposal_questions__r){

                proposal_question__c pqNew = pq.clone(false, true);
                pqNew.asp_component__c = newComp.id; //set parent
                if (pqNew.Field_Title__c != null)  props.add(pqNew);
            }
        }
        
  
        
        insert props;

        return newCam.id;
    }
}