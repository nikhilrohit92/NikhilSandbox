@isTest
private class PlacementAgentFirms_Test {

    static testMethod void myUnitTest() {
		Disclosure_Form__c df = new Disclosure_Form__c();
		insert df;

		Placement_Firm__c pf = new Placement_Firm__c();
		pf.Disclosure_Form__c = df.Id;
		pf.Name = 'test';
		insert pf;

		test.startTest();

			ApexPages.StandardController sc = new ApexPages.standardController(df);
			PlacementAgentFirms_Extension ext = new PlacementAgentFirms_Extension(sc);
		
			ext.addFirm();
			ext.addAgent();

		test.stopTest();
    }
}