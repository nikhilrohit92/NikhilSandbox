public with sharing class AccountErrorLog 
{
    public boolean nameError {get; set;}
    public boolean streetError {get; set;}
    public boolean cityError {get; set;}
    public boolean stateError {get; set;}
    public boolean postalCodeError {get; set;}
    public boolean countryError {get; set;}
    public boolean firmInceptionError {get; set;}
    public boolean firmInceptionFormatError {get; set;}
    public boolean streetSizeError {get; set;}
    public boolean submittorRoleError {get;set;}
    public boolean firmCAHeadquartersError {get; set;}
    public boolean assetsUnderManagementError {get; set;}
    public boolean separateCoMingledAccountError {get; set;}
    public boolean diverseBusinessError {get; set;}
    public boolean HQinUSError {get; set;}
    public boolean EmergingManagerError {get; set;}
    public boolean EmergingDomManagerError {get; set;}
    public boolean FEINError {get; set;}
    public boolean OrgTypeError {get; set;}
    public boolean QuestionSizeError {get; set;}
    
}