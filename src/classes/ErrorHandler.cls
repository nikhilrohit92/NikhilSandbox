//Apex class to handle custom data validation errors
public with sharing class ErrorHandler 
{
    public static final String CHECKBOX_MSG = 'Required field.  Please select a value.';
    public static final String INPUT_MSG = 'Required field.  Please enter a value.';
    public static final String SELECT_MSG = 'Required field.  Please select a value.';
    public static final String RADIO_MSG = 'Required field.  Please select a value.';
    public static final String INVALID_DATE_FORMAT = 'Invalid date format. mm/dd/yyyy is the required format.';
    public static final String INVALID_DATE_Quarter = 'Invalid date. Only the last day of the quarter is valid.';
    public static final String INVALID_DATE_Quarter2 = 'Invalid date. Only the quarters based on the Date of Submission will be accepted. See Performance Data Required.';
    public static final String INVALID_EMAIL_FORMAT = 'Invalid email.';
    public static final String SIZE_EXCEEDED = 'Exceeded 1000 character limit.';
    public static final String STREET_SIZE_EXCEEDED = 'Exceeded 255 character limit.';
    public static final String TITLE_SIZE_EXCEEDED = 'The Title field cannot exceed 40 characters, including spaces and symbols.';
    public static final String SIZE_GREATER_THAN_0 = 'Field must be greater than 0';

    
    public OpportunityErrorLog olog {get; set;}
    public ContactErrorLog clog {get; set;}
    public AccountErrorLog alog {get; set;}
    public HistoricalReturnErrorLog hrlog {get; set;}
    
    //Error Messages by field Id i.e. Id of the input element
    public Map<String, ErrorMessage> errorMessagesByFieldId {get; set;}
    public List<ErrorMessage> errors {get; set;}
    
    //Constructor
    public ErrorHandler() {
        clearMessages();
    }
    
    //Clear all error messages
    public void clearMessages() {
        errorMessagesByFieldId = new Map<String, ErrorMessage>();
        errors = new List<ErrorMessage>();
        
        olog = new OpportunityErrorLog();
        clog = new ContactErrorLog();
        alog = new AccountErrorLog();
        hrlog = new HistoricalReturnErrorLog();
    }
    
    //Method to find if there are error message
    public Boolean getHasErrors()
    {
        if (errorMessagesByFieldId != null && !errorMessagesByFieldId.isEmpty())
        {
            return true;
        }
        return false;
    }
    
    //Method to add a error message
    public void addError(String fieldId, String fieldLabel, String fieldMessage, String pageMessage) {
        ErrorMessage msg = new ErrorMessage(fieldId, fieldLabel, fieldMessage, pageMessage);    
        errorMessagesByFieldId.put(fieldId, msg);
        errors.add(msg);
    }
    
    //Method to find if a field has errors
    public Boolean fieldHasError(String fieldId)
    {
        if (errorMessagesByFieldId != null && errorMessagesByFieldId.containsKey(fieldId))
        {
            return true;
        }
        return false;
    }
    
    //Start Page Errors
    public Boolean getAcknowledgmentHasError()
    {
        return fieldHasError('thisPage:thisForm:ackChbx');
    }
    
    public ErrorMessage getAcknowledgmentError()
    {
        return errorMessagesByFieldId.get('thisPage:thisForm:ackChbx');
    }
    
    //Start Page Errors
    public Boolean getConfirmHasError()
    {
        return fieldHasError('thisPage:thisForm:confirmChbx');
    }
    
    public ErrorMessage getConfirmError()
    {
        return errorMessagesByFieldId.get('thisPage:thisForm:confirmChbx');
    }
}