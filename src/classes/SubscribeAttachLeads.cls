global class SubscribeAttachLeads 
{
  webService static Id AttachLeads(id camid ) 
    {
        List<CampaignMember> newLeads = new List<CampaignMember>();
system.debug('**camid ** ' + camid);
        Id pcamId = [select CampaignRecord__c from Communications__c where id = :camid].CampaignRecord__c;
system.debug('**pcamId ** ' + pcamId );        
        newLeads = [select id, Leadid from campaignmember where campaignid = :pcamId];
system.debug('**newLeads ** ' + newLeads );        
        List<Comm_Member__c> newComms = new List<Comm_Member__c>();
        
        for (CampaignMember c : newLeads) {
           Comm_Member__c comm = new Comm_Member__c();
           comm.Communications__c = camid;
           comm.Lead__c = c.Leadid;
           newComms.add(comm);
       }
       
       upsert newComms;
       return null;
    }   
  
}