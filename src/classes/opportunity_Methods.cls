public with sharing class opportunity_Methods {
	static Map<String, Schema.SObjectField> accountFieldMap = Schema.SObjectType.Account.fields.getMap();
	static Map<String, Schema.SObjectField> opportunityFieldMap = Schema.SObjectType.Opportunity.fields.getMap();
	static Map<String, Schema.SObjectField> contactFieldMap = Schema.SObjectType.Contact.fields.getMap();

	static Map<String,Set<String>> objectFieldPreserveList = new Map<String,Set<String>>{
		'opportunity'=>new Set<String>{'accountid','closedate','createdbyid','createddate','deleted_date__c','id','lastmodifieddate','lastmodifiedbyid','masterrecordid','name','ownerid','stagename'},
		'account'=>new Set<String>{'createddate','id','iscustomerportal','isdeleted','name','jigsaw','jigsawcompanyid','lastactivitydate','lastmodifieddate','lastmodifiedbyid','lastreferenceddate','lastvieweddate','masterrecordid','ownerid','parentid','systemmodstamp'},
		'contact'=>new Set<String>{'accountid','id','firstname','lastname','email','createddate','lastmodifieddate','masterrecordid','ownerid'}
	};

	public static void afterUpdate(List<Opportunity> newList, List<Opportunity> oldList){
		Set<Id> opptysToWipe = new Set<Id>();
		for (Integer i=0;i<newList.size();i++){
			if( newList[i].StageName == 'Deleted' && oldList[i].StageName != 'Deleted' && newList[i].Deleted_Date__c != null && oldList[i].Deleted_Date__c == null ){
				opptysToWipe.add(newList[i].Id);
			}
		}
		if ( opptysToWipe.size() > 0 ){
			wipeFields(opptysToWipe);
		}
	}
	@future
	public static void wipeFields(Set<id> opptyIds){
		Set<String> opptyFields = new Set<String>();
		Set<String> accountFields = new Set<String>();
		Set<String> contactFields = new Set<String>();
		String queryString = 'Select ';
		for ( String f : opportunityFieldMap.keySet() ){
			if ( !objectFieldPreserveList.get('opportunity').contains(f) ){
				queryString += f+', ';
				opptyFields.add(f);
			}
		}
		for ( String f : accountFieldMap.keySet() ){
			if ( !objectFieldPreserveList.get('account').contains(f) ){
				queryString += 'account.'+f+', ';
				accountFields.add(f);
			}
		}
		queryString += '( Select Id, Role, IsPrimary, ';
		for ( String f : contactFieldMap.keySet() ){
			if ( !objectFieldPreserveList.get('contact').contains(f) ){
				queryString += 'contact.'+f+', ';
				contactFields.add(f);
			}
		}
		queryString += ' contact.Id from OpportunityContactRoles where Role != \'Submitter\' ) from Opportunity where Id in: opptyIds';
		Map<Id,sObject> toWipe = new Map<Id,sObject>();
		for ( Opportunity o : database.query(queryString) ){
			for ( OpportunityContactRole ocr : o.OpportunityContactRoles ){
				Contact c = ocr.Contact;
				for ( String f : contactFields ){
					if ( c.get(f) != null && !(c.get(f) instanceof Boolean) && contactFieldMap.get(f).getDescribe().isUpdateable() ){
						c.put(f,null);
					}
				}
				toWipe.put(c.Id,c);
			}
			if ( o.AccountId != null ){
				Account a = o.Account;
				for ( String f : accountFields ){
					if ( a != null && a.get(f) != null && !(a.get(f) instanceof Boolean) && accountFieldMap.get(f).getDescribe().isUpdateable() ){
						a.put(f,null);
					}
				}
				toWipe.put(a.Id,a);
			}
			for ( String f : opptyFields ){
				if ( o.get(f) != null && !(o.get(f) instanceof Boolean) && opportunityFieldMap.get(f).getDescribe().isUpdateable() ){
					o.put(f,null);
				}
			}
			toWipe.put(o.Id,o);
		}
		update toWipe.values();
	}

}