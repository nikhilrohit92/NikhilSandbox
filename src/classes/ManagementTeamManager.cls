//Apex class to manage opportunity management team
public with sharing class ManagementTeamManager{
	//Error Handler reference
	public ErrorHandler ehandler {get; set;}
	public List<OpportunityContactRoleWrapper> team {get; set;}
	public Integer pointer {get; set;}
	public OpportunityContactRoleWrapper member {get; set;}
	public String roleString {get; set;}
	public String emailString {get; set;}
	public String context {get; set;}
	public static final String LISTCONTEXT = 'LISTCONTEXT';
	public static final String ADDCONTEXT = 'ADDCONTEXT';
	public static final String EDITCONTEXT = 'EDITCONTEXT';
	
	public class OpportunityContactRoleWrapper{
		public Id contactId {get;set;}
		public Contact contactRef {get; set;}
		public OpportunityContactRole roleRef {get; set;}
		public Integer listPosition {get; set;}
		
		public OpportunityContactRoleWrapper(){
			contactRef = new Contact();
			roleRef = new OpportunityContactRole();
			listPosition = -1;
		}

		public String getMemberName(){
			return InvestmentSubmissionController.getName(contactRef);
		}
	}
	
	public ManagementTeamManager(){
		pointer = -1; //EOF
		team = new List<OpportunityContactRoleWrapper>();
		member = new OpportunityContactRoleWrapper();
		context = LISTCONTEXT;
		ehandler = new ErrorHandler();
		roleString = '';
	}
	
	public boolean roleError {get; set;}
	public Boolean isValidRole(String role, ErrorHandler ehandler)
	{
		if (role == null || role.trim().length() == 0)
		{
			ehandler.addError('thisPage:thisForm:Role', 'Role', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			roleError = true;
			return false;
		}
		return true;
	}
		
	public PageReference addMember(){
		Boolean isValid = true;
		ehandler.clearMessages();
		roleError = false;
		if (!ContactValidator.isValidContact(member.contactRef, emailString, ehandler)){
			return null;
		}
		if (!ContactValidator.isValidContactRole(member.contactRef, emailString, ehandler)){
			return null;
		}
		member.contactRef.Email = emailString;
		member.roleRef.Role = member.contactRef.Title;
		if (context == ADDCONTEXT){
			++pointer;
			member.listPosition = pointer;
		
		//member.listPosition = team.size();
			team.add(member);
			
			
			for(integer i=0; i< team.size();i++){
				System.debug('list position' + team[i].listPosition);
				OpportunityContactRoleWrapper temp = team.get(i);
				temp.listPosition = i;
			
			}
		}
		context = LISTCONTEXT;
		return null;
	}
	
	public PageReference cancelMember(){
		ehandler.clearMessages();
		roleError = false;
		member = new OpportunityContactRoleWrapper();
		context = LISTCONTEXT;
		roleString = '';
		emailString = '';
		return null;
	}
	
	public PageReference newMember()
	{
		ehandler.clearMessages();
		roleError = false;
		member = new OpportunityContactRoleWrapper();
		roleString = '';
		emailString = '';
		context = ADDCONTEXT;
		return null;
	}
	
	public PageReference addMoreMember(){
		addMember();
		member = new OpportunityContactRoleWrapper();
		context = ADDCONTEXT;
		roleString = '';
		emailString = '';
		return null;
	}
	
	public PageReference editMember(){
		ehandler.clearMessages();
		roleError = false;
		String ndx = ApexPages.currentPage().getParameters().get('member.index');
		Integer location = Integer.valueOf(ndx);
		for (OpportunityContactRoleWrapper w : team){
			if (w.listPosition == location){
				member = w;
				roleString = member.roleRef.Role;
				emailString = member.contactRef.Email;
				break;
			}
		}
		context = EDITCONTEXT;
		return null;
	}
	
	public PageReference removeMember()
	{
		ehandler.clearMessages();
		roleError = false;
		String ndx = ApexPages.currentPage().getParameters().get('member.index');
		Integer location = Integer.valueOf(ndx);
		Integer toDeleteIndex = -1;
		for (Integer i=0; i<team.size(); i++)
		{
			OpportunityContactRoleWrapper w = team.get(i);
			if (w.listPosition == location)
			{
				toDeleteIndex = i;
				break;
			}
		}
		if (toDeleteIndex != -1)
		{
			OpportunityContactRoleWrapper tempContact = team[toDeleteIndex];
			
			team.remove(toDeleteIndex);
		
			if(tempContact.roleRef != null){
			
				List<OpportunityContactRole> contactRoleToDelete = [select id from OpportunityContactRole where id = :tempContact.roleRef.id];
				
				if(contactRoleToDelete.size()>0){
				
					delete contactRoleToDelete;
				
				}
				
			}
		
			if(tempContact.contactRef != null){
				
				List<Contact> contactToDelete = [select id from Contact where id = :tempContact.contactRef.id];
				
				if(contactToDelete.size()>0){
				
					delete contactToDelete;
				
				}
			
			}
		
		}
		context = LISTCONTEXT;
		cancelMember();
		return null;
	}
	
	public Boolean getHasMembers()
	{
		if (!team.isEmpty())
		{
			return true;
		}
		return false;
	}
	
	public Boolean getIsListContext()
	{
		if (context == LISTCONTEXT)
		{
			return true;
		}
		return false;
	}
	
	public Boolean getIsAddContext()
	{
		if (context == ADDCONTEXT)
		{
			return true;
		}
		return false;
	}
	
	public Boolean getIsEditContext()
	{
		if (context == EDITCONTEXT)
		{
			return true;
		}
		return false;
	}
}