public with sharing class changecontrolformCNTRL2 {

    public changecontrolformCNTRL2() {
        system.debug('===================normal con=====');


    }
    public void onloadattmethod()
    {
      isSuccess =true;
    objAttachmentList=new List<Attachment>();
    objAttachmentList.add(new attachment ());
     objAttachmentList.add(new attachment ());
      objAttachmentList.add(new attachment ());
       objAttachmentList.add(new attachment ());
        objAttachmentList.add(new attachment ());
        objAttachmentList.add(new attachment ());
    
    }

Public List<Environment__c> EnviList {get; set;}
public Approver__c UATApp {get;set;}
public List<Approver__c> UATAppList {get;set;}
public Approver__c MGMTApp {get;set;}
public List<Approver__c> MGMTAppList {get;set;}
Public List<attachment> objAttachmentList{get; set;}
Public Change_control_request__c ccRequest {get;set;}
public boolean isSuccess {get;set;}

public boolean DefectCorrection {get;set;}
public boolean Enhancement {get;set;}
public boolean NewRequest {get;set;}
public boolean Other {get;set;}

    public changecontrolformCNTRL2(ApexPages.StandardController controller) {
    system.debug('===================extcon=====');

    isSuccess =true;
    
    ccRequest= (Change_control_request__c)controller.getrecord();
    EnviList =new List<Environment__c>();
    EnviList.add(new Environment__c(name='Production'));
    UATAppList=new List<Approver__c>();
    //MGMTAppList=new List<Approver__c>();
    
    //MGMTAppList.add(new Approver__c ());
    UATAppList=new List<Approver__c>();
    UATAppList.add(new Approver__c ());    

    }
    
    Public void AddMoreEnvi()
    {
     EnviList.add(new Environment__c(name='Production'));

    }
     Public void AddMoreUAT()
    {
     UATAppList.add(new Approver__c ());

    }
     Public void AddMoreMgmt()
    {
     MGMTAppList.add(new Approver__c ());
    }  
    Public pagereference SaveChangeControlForm()
    {
        isSuccess =true;
        system.debug('============='+ccRequest.Initiator_Name__c);

     //pagereference pg=new pagereference('/apex/CCRFAddAttachments?parentID='+ccRequest.ID);
      // pagereference pg=page.CCRFAddAttachments;
           pagereference pg=new pagereference('/apex/CCRFAddAttachments');

       pg.setRedirect(false);
        return pg;
    }
    public String formatpklist(List<String> values) {
    if (values == null) return null;
    return String.join(values, ';');
    }


    public pagereference  SaveChangeControlatt()
{
     system.debug('===='+ccRequest) ;
     system.debug('============='+ccRequest.Initiator_Name__c);
List<String> selmPiklistVals=new List<String>();
if(DefectCorrection) {selmPiklistVals.add('DefectCorrection');  }
if(Enhancement) {selmPiklistVals.add('Enhancement');  }
if(NewRequest) {selmPiklistVals.add('New');  }
if(Other) {selmPiklistVals.add('Other');  }
if(!selmPiklistVals.isEmpty()) { 
ccRequest.Type_of_Request__c=formatpklist(selmPiklistVals);}
      insert ccRequest;    
      for(Environment__c ev:EnviList )
      {
      ev.Change_control_request_from__c=ccRequest.id;
      }     
      insert EnviList ;
       for(Approver__c UATApp: UATAppList)
      {
          UATApp.UAT_Approval__c =ccRequest.id;
      }
    insert UATAppList;
  
    List<Attachment> attToinsert=new List<Attachment>();
     for(Attachment objAttachment:objAttachmentList){
     if(objAttachment.name!=''&&objAttachment.body!=null)
     {
     objAttachment.parentID=ccRequest.ID;
     attToinsert.add(objAttachment);
     }
     }
     if(!attToinsert.isempty())
     insert attToinsert;
     isSuccess =false;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.confirm, '  Your Submission was Successful '));

     objAttachmentList.clear();

    return null;
    }
public String strSelectedValues {get;set;}
    
  public void callFunc()
    {
       system.debug('====='+strSelectedValues);
    }
    public List<string> selectedValues {get;set;}

    public List<selectoption> gettheSelectOptions() 
    {           
        list<selectoption> options = new list<selectoption>();            
        try 
        {               
        //Product Name is a MultiSelect Picklist               
        Schema.DescribeFieldResult fieldResult = opportunity.stagename.getDescribe();

        list<schema.picklistentry> values = fieldResult.getPickListValues();               
        for (Schema.PicklistEntry a : values) 
        {                  
        options.add(new SelectOption(a.getLabel(), a.getValue()));
        }           
        }  
        catch (Exception e) 
        {             
        ApexPages.addMessages(e);           
        }
        system.debug('## Product Name Options'+ options);          
        return options; 
    }
    public string mypicklistselected {get; set;}
public void runSearch ()
{
mypicklistselected  = Apexpages.currentPage().getParameters().get('selValues');
    
}

public class NoNoneForPicklistController2 {

public Change_control_request__c myContact {get;set;}
public List<SelectOption> securityOptions {get;set;}

// Constructor called when page is accessed.
public NoNoneForPicklistController2(changecontrolformCNTRL2 extension) {

myContact = new Change_control_request__c(); 
securityOptions = new List<SelectOption>();

// Use DescribeFieldResult object to retrieve status field.
Schema.DescribeFieldResult securityFieldDescription = Change_control_request__c.Type_of_Request__c.getDescribe();

// For each picklist value, create a new select option
for (Schema.Picklistentry picklistEntry: securityFieldDescription.getPicklistValues()){

securityOptions.add(new SelectOption(pickListEntry.getValue(),pickListEntry.getLabel()));

// obtain and assign default value
if (picklistEntry.defaultValue){
myContact.Type_of_Request__c = pickListEntry.getValue();
} 
} 
}
}


}