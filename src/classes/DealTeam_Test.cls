@isTest
private class DealTeam_Test {

    static testMethod void utilities_Test() {
		String nullString;
		
		Utilities.getDate('20090909');
		Utilities.getDate('200909');
		Boolean isValid = Utilities.isValidDateString('01/01/2001');
		isValid = Utilities.isValidDateString('200909');
    }

    static testMethod void myUnitTest() {
		Opportunity o = new Opportunity();
		o.Name = 'test';
		o.Amount = 0;
		o.StageName = 'Closed Won';
		o.CloseDate = system.today();
		database.insert( o, false);
		
		Id uid = [Select Id from User where isActive = true limit 1].Id;
		
		test.startTest();
			PageReference thisPage  =  Page.DealTeam_New;
			Test.setCurrentPage(thisPage);
			
			DealTeam_Controller controller = new DealTeam_Controller();
			controller.opportunityId = o.id;
			controller.d.OwnerId = uId;
			controller.saveDealTeam();
			
		test.stopTest();
    }


}