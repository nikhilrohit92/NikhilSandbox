//Wrapper class for a checkbox
public with sharing class CheckboxWrapper {
	public SelectOption optionRef {get; set;}
	public String styleClass {get; set;}
	public Boolean selected {get; set;}
	
	public CheckboxWrapper(SelectOption optionRef){
		styleClass = '';
		selected = false;
		this.optionRef = optionRef; 
	}
	
	public Boolean isSelected(){
		return selected;
	}
	
	public String getValue(){
		if (isSelected()){
			return optionRef.getValue();
		}
		return null;
	}
	
	public static List<CheckboxWrapper> convertOptions(List<SelectOption> optionsLst){
		String styleClassStr = 'leftcol';
		List<CheckboxWrapper> wlst = new List<CheckboxWrapper>();
		if (optionsLst != null && !optionsLst.isEmpty())
		{
			for (SelectOption opt : optionsLst)
			{
				CheckboxWrapper w = new CheckboxWrapper(opt);
				w.styleClass = 'multicheck ' + styleClassStr;
				wlst.add(w);
				if (styleClassStr == 'leftcol')
				{
					styleClassStr = 'rightcol';
				}
				else
				{
					styleClassStr = 'leftcol';
				}
			} 
		}
		return wlst;
	}
	
	static testmethod void testCheckboxWrapper(){
		Test.startTest();
		List<SelectOption> lst = new List<SelectOption>();
		lst.add(new SelectOption('Option 1', 'Option 1'));
		lst.add(new SelectOption('Option 2', 'Option 2'));
		lst.add(new SelectOption('Option 3', 'Option 3'));
		
		List<CheckboxWrapper> wlst = CheckboxWrapper.convertOptions(lst);
		System.assertEquals(lst.size(), wlst.size());
		
		System.assert(!wlst[0].isSelected());
		System.assertEquals(null, wlst[0].getValue());
		
		wlst[0].selected = true;
		
		System.assert(wlst[0].isSelected());
		System.assertNotEquals(null, wlst[0].getValue());
		
		Test.stopTest();
	}
}