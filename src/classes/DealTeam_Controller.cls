public class DealTeam_Controller {
	public string opportunityId {get;set;}
	public Deal_Team__c d {get;set;}
	
	public DealTeam_Controller(){
		opportunityId = apexpages.currentPage().getParameters().get('opportunityId');
		
		d = new Deal_Team__c();
		d.Opportunity__c = opportunityId;
		
	}

	public PageReference saveDealTeam(){
		String ownerid = d.ownerId;
		List<OpportunityShare> opptyShares = [Select Id, UserOrGroupId from OpportunityShare where UserOrGroupId =:ownerId];
		if ( opptyShares.size() == 0  ){
			OpportunityShare os = new OpportunityShare();
			os.UserOrGroupId = d.OwnerId;
			os.OpportunityId = opportunityId;
			os.OpportunityAccessLevel = 'Edit';
			insert os;
		}
		
		OpportunityTeamMember t = new OpportunityTeamMember();
		t.UserId = d.OwnerId;
		t.OpportunityId = opportunityId;
		database.insert( t,false);

		database.insert( d,false);

		PageReference p = new PageReference('/'+opportunityId);
		return p;
	}
}