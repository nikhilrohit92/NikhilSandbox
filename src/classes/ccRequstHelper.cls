Public without sharing class ccRequstHelper {

       @Future(callout=true)
  public static void SendEmailWithPDF( set<ID> ccRequestIDs)
        { 
       
       List<Change_control_request__c> ccRequestList=[select id, Name,Initiator_E_mail__c,CC_Email__c,Status__c,Request_Title__c from Change_control_request__c where ID IN : ccRequestIDS];

        List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>(); 
         //List<user> Usr=new List<user> ([SELECT email, name,status__c,LastModifiedBy__c,LastModifiedDate__c FROM user WHERE Id = : UserInfo.getUserId()]);
           //Usr = [SELECT email, name FROM User WHERE Id = : UserInfo.getUserId()]; 
           String loginuserEmail=UserInfo.getUseremail();
          for (Change_control_request__c ccRequest : ccRequestList) {
           
                   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                
                  List<String> sendTo = new List<String>();
                  sendTo.add(ccRequest.Initiator_E_mail__c );
                  mail.setToAddresses(sendTo);    
                  mail.setReplyTo(loginuserEmail);
                 // mail.setSenderDisplayName(UserInfo.getUserfullname());
                
                  List<String> ccTo = new List<String>();
                  if(ccRequest.CC_Email__c !=null && ccRequest.CC_Email__c !='') {ccTo.addAll(ccRequest.CC_Email__c.split(','));}
                  if(!ccTo.isempty()) {    mail.setCcAddresses(ccTo); }

                  mail.setSubject('Change control request status');
               string body='<HTML>';
                  if(ccRequest.Status__c =='Pending') {
           mail.setSubject('Change Control      '+    ccRequest.Name  + '--' + ccRequest.Request_Title__c  +'     Status is now  '+  ccRequest.Status__c);
          // body +=         'Title: Change Control      '+    ccRequest.Name  + '--' + ccRequest.Request_Title__c  +'     Status is now  '+  ccRequest.Status__c +'<br/>';
          body += 'Request: ';
           body+= ccRequest.Name +' - ';
            body+=ccRequest.Request_Title__c +'<br/>';
            body += ' Status: Changed to  ';
             body+=ccRequest.Status__c + '<br/>'; 
            }
                  mail.setHtmlBody(body);
              //PageReference pagePdf = new PageReference('/apex/changecontrolpageV5pdf?id='+ccRequest.id); 
              PageReference pagePdf = new PageReference('http://nikhil-calpers.cs40.force.com/CCRFPDF?ID='+ccRequest.id);
           Blob pdfPageBlob; pdfPageBlob = pagePdf.getContentAsPDF(); 
            Attachment a = new Attachment(); 
           a.Body = pdfPageBlob;
          //  a.body=blob.valueof('test body');
            a.Name = 'CCRequestForm.pdf'; 
            
            List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
          Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
          efa.setFileName(a.Name);
          efa.setBody(a.Body);
          fileAttachments.add(efa);
        mail.setFileAttachments(fileAttachments);
                 mails.add(mail);
                a.parentid=ccrequest.id;
                insert a;
            
          }
          Messaging.sendEmail(mails);


        }
}