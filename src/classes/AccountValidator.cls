public with sharing class AccountValidator 
{
	private static boolean isValidAccountName(Account accountRef)
	{
		return (accountRef != null && accountRef.Name == null) ? false : true;
	}
	
	private static Boolean isValidStreet(Account accountRef)
	{
		return (accountRef != null && accountRef.BillingStreet != null && accountRef.BillingStreet.trim().length() > 0) ? true : false;
	}
	
	private static Boolean isValidCity(Account accountRef)
	{
		return (accountRef != null && accountRef.BillingCity == null) ? false : true;
	}
	
	private static Boolean isValidState(Account accountRef)
	{
		return (accountRef != null && accountRef.BillingState == null) ? false : true;
	}
	
	private static Boolean isValidPostalCode(Account accountRef)
	{
		return (accountRef != null && accountRef.BillingPostalCode == null) ? false : true;
	}
	
	private static Boolean isValidCountry(Account accountRef)
	{
		return (accountRef != null && accountRef.BillingCountry == null) ? false : true;
	}
	
	private static Boolean isValidFirmInceptionDate(Account accountRef)
	{
		return ((accountRef != null) && (accountRef.Inception_Date__c == null)) ? false : true;
	}

	public static Boolean isValidResponse(String i){
		return (i != null && i.trim().length() > 0) ? true : false;
	}
	public static Boolean isValidResponse(Decimal i){
		return (i != null) ? true : false;
	}
	public static Boolean isValidResponse(Date i){
		return (i != null) ? true : false;
	}

	private static Boolean isValidDiverseInvestment(Account accountRef)
	{
		return ((accountRef != null) && (accountRef.Diverse_Investment_Management_Business__c == null)) ? false : true;
	}

	private static Boolean isValidHQinUS(Account accountRef)
	{
		return ((accountRef != null) && (accountRef.HQ_in_US__c == null)) ? false : true;
	}

	private static Boolean isValidEmergingManager(Account accountRef)
	{
		return ((accountRef != null) && (accountRef.Emerging_Manager__c == null)) ? false : true;
	}

	private static Boolean isValidEmergingDomManager(Account accountRef)
	{
		return ((accountRef != null) && (accountRef.Emerging_Domestic_Manager__c == null)) ? false : true;
	}

	public static boolean isValidFirmInformation(Account accountRef, ErrorHandler ehandler){
		boolean isValid = true;
		if (!isValidAccountName(accountRef)){
			ehandler.addError('thisPage:thisForm:AccountName', 'Partner or Proposing Entity Name', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.alog.nameError = true;
		}
		if (!isValidStreet(accountRef)){
			ehandler.addError('thisPage:thisForm:Street', 'Street', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.alog.streetError = true;
		} else if (accountRef.BillingStreet.trim().length() > 255) {
			String msg = 'Field has ' + accountRef.BillingStreet.trim().length() + ' characters. Only 255 allowed.';
			ehandler.addError('thisPage:thisForm:Street', 'Street', msg, msg);
			isValid = false;
			ehandler.alog.streetSizeError = true;
		}		
		if (!isValidCity(accountRef)){
			ehandler.addError('thisPage:thisForm:City', 'City', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.alog.cityError = true;
		}
		if (!isValidState(accountRef)){
			ehandler.addError('thisPage:thisForm:State', 'State', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.alog.stateError = true;
		}
		if (!isValidPostalCode(accountRef)){
			ehandler.addError('thisPage:thisForm:PostalCode', 'Postal Code', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.alog.postalCodeError = true;
		}
		if (!isValidCountry(accountRef)){
			ehandler.addError('thisPage:thisForm:Country', 'Country', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.alog.countryError = true;
		}
		return isValid;
	}

	private static boolean isValidFirmInformation(Account accountRef, Contact submittorContact, String emailString, ErrorHandler ehandler){
		boolean isValid = true;
		if (!isValidAccountName(accountRef)){
			ehandler.addError('thisPage:thisForm:AccountName', 'Partner or Proposing Entity Name', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.alog.nameError = true;
		}
		if (!isValidStreet(accountRef)){
			ehandler.addError('thisPage:thisForm:Street', 'Street', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.alog.streetError = true;
		} else if (accountRef.BillingStreet.trim().length() > 255) {
			String msg = 'Field has ' + accountRef.BillingStreet.trim().length() + ' characters. Only 255 allowed.';
			ehandler.addError('thisPage:thisForm:Street', 'Street', msg, msg);
			isValid = false;
			ehandler.alog.streetSizeError = true;
		}		
		if (!isValidCity(accountRef)){
			ehandler.addError('thisPage:thisForm:City', 'City', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.alog.cityError = true;
		}
		if (!isValidState(accountRef)){
			ehandler.addError('thisPage:thisForm:State', 'State', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.alog.stateError = true;
		}
		if (!isValidPostalCode(accountRef)){
			ehandler.addError('thisPage:thisForm:PostalCode', 'Postal Code', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.alog.postalCodeError = true;
		}
		if (!isValidCountry(accountRef)){
			ehandler.addError('thisPage:thisForm:Country', 'Country', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.alog.countryError = true;
		}
		if(!ContactValidator.isValidFirmContact(submittorContact, emailString, ehandler,'')){
			isValid = false;
		}
		return isValid;
	}
	
	public static boolean isValidFirmInceptionString(Account accountRef, String firmInceptionDateString, ErrorHandler ehandler)
	{
		boolean isValid = true;
		if (firmInceptionDateString == null || firmInceptionDateString.trim().length() == 0){
			ehandler.addError('thisPage:thisForm:FirmInception', 'Firm Inception Date', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.alog.firmInceptionError = true;
		} else
		if (Utilities.isValidDateString(firmInceptionDateString)){
			accountRef.Inception_Date__c = Utilities.getDate(firmInceptionDateString);
		} else {
			ehandler.addError('thisPage:thisForm:FirmInception', 'Firm Inception Date', ErrorHandler.INVALID_DATE_FORMAT, ErrorHandler.INVALID_DATE_FORMAT);
			isValid = false;
			ehandler.alog.firmInceptionFormatError = true;
		}
		return isValid;
	}
		
	public static boolean isValidFirmInceptionStringGE(Account accountRef, String firmInceptionDateString, ErrorHandler ehandler)
	{
		boolean isValid = true;
		if (firmInceptionDateString == null || firmInceptionDateString.trim().length() == 0){
			ehandler.addError('thisPage:thisForm:FirmInception', 'Firm Inception Date', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.alog.firmInceptionError = true;
		} else
		if (Utilities.isValidDateStringGE(firmInceptionDateString)){
			accountRef.Inception_Date__c = Utilities.getDate(firmInceptionDateString);
		} else {
			ehandler.addError('thisPage:thisForm:FirmInception', 'Firm Inception Date', ErrorHandler.INVALID_DATE_FORMAT, ErrorHandler.INVALID_DATE_FORMAT);
			isValid = false;
			ehandler.alog.firmInceptionFormatError = true;
		}
		return isValid;
	}
	public static boolean isValidFirmInformation(Account accountRef, String firmInceptionDateString, 
		Contact submittorContact, Opportunity proposal, String emailString, ErrorHandler ehandler)
	{
		boolean isValid = isValidFirmInformation(accountRef, submittorContact, emailString, ehandler);
		boolean isValidDate = isValidFirmInceptionString(accountRef, firmInceptionDateString, ehandler);
		if(!OpportunityValidator.isValidAUM(proposal, ehandler))
		{
			isValid = false;
		}
		if(!OpportunityValidator.isValidBriefHistory(proposal, ehandler))
		{
			isValid = false;
		}
		return isValid && isValidDate;
	}
	
	public static boolean isValidInfrastructureDirectFirmInformation(Account accountRef, String firmInceptionDateString, 
		Contact submittorContact, Opportunity proposal, String emailString, ErrorHandler ehandler)
	{
		boolean isValid = isValidFirmInformation(accountRef, submittorContact, emailString, ehandler);
		boolean isValidDate = isValidFirmInceptionString(accountRef, firmInceptionDateString, ehandler);
		if(!OpportunityValidator.isValidBriefHistory(proposal, ehandler))
		{
			isValid = false;
		}
		return isValid && isValidDate;
	}
	
	public static boolean isValidGlobalEquityFirmInformation(
		Account accountRef, 
		String firmInceptionDateString, 
		Contact submitterContact,
		Contact primaryContact, 
		Opportunity proposal, 
		String submitterEmailString, 
		String primaryEmailString, 
		ErrorHandler ehandler
	){
		Boolean isValid = true;
		if( isValidFirmInformation(accountRef, ehandler) ){
			isValid = false;
		}
		if(!ContactValidator.isValidFirmContact(submitterContact, submitterEmailString, ehandler,'Submitter')){
			isValid = false;
		}
		if(!ContactValidator.isValidFirmContact(primaryContact, primaryEmailString, ehandler,'')){
			isValid = false;
		}
//		boolean isValidPrimary = isValidFirmInformation(accountRef, primaryContact, primaryEmailString, ehandler);
		boolean isValidDate = isValidFirmInceptionStringGE(accountRef, firmInceptionDateString, ehandler);
		boolean isValidOpportunity = OpportunityValidator.isValidAUM(proposal, ehandler);
		return isValid && isValidDate && isValidOpportunity;
	}
	
	public static boolean isValidRealEstateFirmInformation(Account accountRef, String firmInceptionDateString, Contact submittorContact, Opportunity proposal, String emailString, ErrorHandler ehandler){
		boolean isValid = true;
		if (!isValidAccountName(accountRef)){
			ehandler.addError('thisPage:thisForm:AccountName', 'Partner or Proposing Entity Name', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.alog.nameError = true;
		}

//		if (!(proposal.Submittor_Role__c != null && proposal.Submittor_Role__c.trim().length() > 0))
//		{
//			ehandler.addError('thisPage:thisForm:SubmittorRole', 'Describe your role for this opportunity', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
//			isValid = false;
//			ehandler.alog.submittorRoleError = true;
//		}		

		if(!ContactValidator.isValidFirmContact(submittorContact, emailString, ehandler, '')){
			isValid = false;
		}
		if (!isValidStreet(accountRef)){
			ehandler.addError('thisPage:thisForm:Street', 'Street', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.alog.streetError = true;
		}
		else if (accountRef.BillingStreet.trim().length() > 255)
		{
			String msg = 'Field has ' + accountRef.BillingStreet.trim().length() + ' characters. Only 255 allowed.';
			ehandler.addError('thisPage:thisForm:Street', 'Street', msg, msg);
			isValid = false;
			ehandler.alog.streetSizeError = true;
		}
		
		if (!isValidCity(accountRef))
		{
			ehandler.addError('thisPage:thisForm:City', 'City', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.alog.cityError = true;
		}
		if (!isValidState(accountRef))
		{
			ehandler.addError('thisPage:thisForm:State', 'State', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.alog.stateError = true;
		}
		if (!isValidPostalCode(accountRef))
		{
			ehandler.addError('thisPage:thisForm:PostalCode', 'Postal Code', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.alog.postalCodeError = true;
		}
		if (!isValidCountry(accountRef))
		{
			ehandler.addError('thisPage:thisForm:Country', 'Country', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.alog.countryError = true;
		}
		boolean isValidDate = isValidFirmInceptionString(accountRef, firmInceptionDateString, ehandler);

		if (!isValidResponse(proposal.Firm_has_California_Headquarters_Offices__c)){
			ehandler.addError('thisPage:thisForm:FirmCAHeadquarters', 'Is the Firm Headquartered in California or has Offices in California?', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.firmCAHeadquartersError = true;
		}
		if (!isValidResponse(proposal.Assets_Under_Management__c))
		{
			ehandler.addError('thisPage:thisForm:AssetsUnderManagement', 'Assets Under Management', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.assetsUnderManagementError = true;
		}
		if (!isValidResponse(proposal.Managers_Separate_Co_Mingled_Account__c))
		{
			ehandler.addError('thisPage:thisForm:SeparateCoMingledAccount', 'Is This Vehicle the Manager\'s 1st, 2nd or 3rd Separate Account or Co-Mingled Account?', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.separateCoMingledAccountError = true;
		}

		return isValid && isValidDate;
	}
	
	public static boolean isValidForestlandFirmInformation(Account accountRef, String firmInceptionDateString, Contact submittorContact, Opportunity proposal, String emailString, ErrorHandler ehandler){
		boolean isValid = isValidFirmInformation(accountRef, submittorContact, emailString, ehandler);
		boolean isValidDate = isValidFirmInceptionString(accountRef, firmInceptionDateString, ehandler);
		if (!isValidResponse(proposal.Assets_Under_Management__c)){
			ehandler.addError('thisPage:thisForm:Aum', 'Partnership Firm Assets Under Management', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.partnershipAumError = true;
		}
		return isValid && isValidDate;
	}
	
	public static boolean isValidDiversityInformation(Account accountRef, ErrorHandler ehandler){
		boolean isValid = true;
		if (!isValidDiverseInvestment(accountRef))
		{
			ehandler.addError('thisPage:thisForm:DiverseBusiness:0', 'Please indicate if your firm meets the definition referenced above of a Diverse Investment Management Business.', ErrorHandler.SELECT_MSG, ErrorHandler.SELECT_MSG);
			isValid = false;
			ehandler.alog.DiverseBusinessError = true;
		}
		if (!isValidHQinUS(accountRef))
		{
			ehandler.addError('thisPage:thisForm:HQinUS:0', 'Is your firm physically headquartered in the United States or its trust territories?', ErrorHandler.SELECT_MSG, ErrorHandler.SELECT_MSG);
			isValid = false;
			ehandler.alog.HQinUSError = true;
		}
		return isValid;
	}
	
	public static boolean isValidPrivateEquityEMInformation(Account accountRef, ErrorHandler ehandler){
		boolean isValid = true;
		if (!isValidEmergingManager(accountRef))
		{
			ehandler.addError('thisPage:thisForm:EmergingManager:0', 'Please indicate if your firm meets the Private Equity Emerging Manager definition referenced above.', ErrorHandler.SELECT_MSG, ErrorHandler.SELECT_MSG);
			isValid = false;
			ehandler.alog.EmergingManagerError = true;
		}
		if (!isValidEmergingDomManager(accountRef))
		{
			ehandler.addError('thisPage:thisForm:EmergingDomManager:0', 'Please indicate if your firm meets the Emerging Domestic Private Equity Manager (Fund of Funds) Program definition of emerging manager referenced above.', ErrorHandler.SELECT_MSG, ErrorHandler.SELECT_MSG);
			isValid = false;
			ehandler.alog.EmergingDomManagerError = true;
		}
		return isValid;
	}
	
	public static boolean isValidEMInformation(Account accountRef, ErrorHandler ehandler){
		boolean isValid = true;
		if (!isValidEmergingManager(accountRef))
		{
			ehandler.addError('thisPage:thisForm:EmergingManager:0', 'Please indicate if your firm meets the Private Equity Emerging Manager definition referenced above.', ErrorHandler.SELECT_MSG, ErrorHandler.SELECT_MSG);
			isValid = false;
			ehandler.alog.EmergingManagerError = true;
		}

		return isValid;
	}

    public static Boolean isValidOrgType(Account accountRef, ErrorHandler ehandler){
        if (accountRef.Org_Type__c == null){
            ehandler.addError('thisPage:thisForm:OrgType', 'Organization Type', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            ehandler.alog.OrgTypeError = true;
            return false;
        }
        return true;
    }

    public static Boolean isValidFEIN(Account accountRef, ErrorHandler ehandler){
        if (accountRef.FEIN__c == null){
            ehandler.addError('thisPage:thisForm:FEIN', 'Federal Employer ID No. (FEIN)', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            ehandler.alog.FEINError = true;
            return false;
        }
        return true;
    }
    static testMethod void Account_Validator() {
        Test.startTest();
        Account accountRef = new Account();
        
        AccountValidator.isValidFirmInceptionDate(accountRef);
        AccountValidator.isValidDiverseInvestment(accountRef);
        AccountValidator.isValidHQinUS(accountRef);
        AccountValidator.isValidEmergingManager(accountRef);
        AccountValidator.isValidEmergingDomManager(accountRef);
        AccountValidator.isValidDiversityInformation(accountRef, new ErrorHandler());
        AccountValidator.isValidPrivateEquityEMInformation(accountRef, new ErrorHandler());
        AccountValidator.isValidEMInformation(accountRef, new ErrorHandler());

        
        Test.stopTest();
    }		
}