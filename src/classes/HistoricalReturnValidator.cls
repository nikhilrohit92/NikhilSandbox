public with sharing class HistoricalReturnValidator {
    private static Boolean isValidName(Historical_Summary__c hreturn) {
        return (hreturn.Name == null) ? false : true;
    }
     
    private static Boolean isValidSize(Historical_Summary__c hreturn) {
        return (hreturn.Size__c == null) ? false : true;
    }
    
    private static Boolean isValidYear(Historical_Summary__c hreturn) {
        return (hreturn.Calendar_Year_Return__c == null) ? false : true;
    }
    
    private static Boolean isValidProductReturn(Historical_Summary__c hreturn) {
        return (hreturn.Product_Return__c == null) ? false : true;
    }
    
    private static Boolean isValidBenchmarkReturn(Historical_Summary__c hreturn) {
        return (hreturn.Benchmark_Return__c == null) ? false : true;
    }
    
    private static Boolean isValidProductBenchMark(Historical_Summary__c hreturn) {
        return (hreturn.Product_Benchmark_Return__c == null) ? false : true;
    }
    
    private static Boolean isValidVintageYear(Historical_Summary__c hreturn) {
        return (hreturn.Vintage_Year__c == null) ? false : true;
    }
    
    private static Boolean isValidNumberOfPortfolioCompanies(Historical_Summary__c hreturn) {
        return (hreturn.No_of_Portfolio_Companies__c == null) ? false : true;
    }
    
    private static Boolean isValidCommittedCapital(Historical_Summary__c hreturn) {
        return (hreturn.Committed_Capital_In_Millions__c == null) ? false : true;
    }
    
    private static Boolean isValidContributedCapital(Historical_Summary__c hreturn) {
        return (hreturn.Contributed_Capital_In_Millions__c == null) ? false : true;
    }
    
    private static Boolean isValidInvested(Historical_Summary__c hreturn) {
        return (hreturn.Invested__c == null) ? false : true;
    }
    
    private static Boolean isValidDistributedValue(Historical_Summary__c hreturn) {
        return (hreturn.Distributed_Value_In_Millions__c == null) ? false : true;
    }
    
    private static Boolean isValidUnrealized(Historical_Summary__c hreturn) {
        return (hreturn.Unrealized_In_Millions__c == null) ? false : true;
    }
    
    private static Boolean isValidTotalValue(Historical_Summary__c hreturn) {
        return (hreturn.Total_Value_In_Millions__c == null) ? false : true;
    }
    
    private static Boolean isValidRealized(Historical_Summary__c hreturn) {
        return (hreturn.Realized_In_Millions__c == null) ? false : true;
    }
    
    private static Boolean isValidGrossMultiple(Historical_Summary__c hreturn) {
        return (hreturn.Gross_Multiple__c == null) ? false : true;
    }
    
    private static Boolean isValidNetMultiple(Historical_Summary__c hreturn) {
        return (hreturn.Net_Multiple__c == null) ? false : true;
    }
    
    private static Boolean isValidGrossIRR(Historical_Summary__c hreturn) {
        return (hreturn.Gross_IRR__c == null) ? false : true;
    }
    
    private static Boolean isValidNetIRR(Historical_Summary__c hreturn) {
        return (hreturn.Net_IRR__c == null) ? false : true;
    }

    private static Boolean isValidNetDPI(Historical_Summary__c hreturn) {
        return (hreturn.Net_DPI__c == null) ? false : true;
    }


    private static Boolean isValidAsOfDate(Historical_Summary__c hreturn) {
        return (hreturn.As_Of_Date__c == null) ? false : true;
    }

    public static boolean isValidAsOfDateString(Historical_Summary__c hreturn, String asOfDateString, ErrorHandler ehandler)
    {
        boolean isValid = true;
        if (asOfDateString != null && asOfDateString.trim().length() > 0)
        {
            if (Utilities.isValidDateString(asOfDateString))
            {
                hreturn.As_Of_Date__c = Utilities.getDate(asOfDateString);
            }
            else
            {
                ehandler.addError('thisPage:thisForm:AsOfDate', 'Please identify the As of Date for the Returns provided below.', ErrorHandler.INVALID_DATE_FORMAT, ErrorHandler.INVALID_DATE_FORMAT);
                isValid = false;
                ehandler.hrlog.asOfDateFormatError = true;
            }
        }
        return isValid;
    }

    public static boolean isValidStartDateofTrackRecordString(Historical_Summary__c hreturn, String startDateOfTrackRecordString, ErrorHandler ehandler)
    {
        boolean isValid = true;
        if (startDateOfTrackRecordString != null && startDateOfTrackRecordString.trim().length() > 0)
        {
            if (Utilities.isValidDateString(startDateOfTrackRecordString))
            {
                hreturn.Start_Date_Of_Track_Record__c = Utilities.getDate(startDateOfTrackRecordString);
            }
            else
            {
                ehandler.addError('thisPage:thisForm:StartDateofTrackRecord', 'Start Date of Track Record', ErrorHandler.INVALID_DATE_FORMAT, ErrorHandler.INVALID_DATE_FORMAT);
                isValid = false;
                ehandler.hrlog.startDateofTrackRecordFormatError = true;
            }
        }
        return isValid;
    }

    public static Boolean isValidAIMPartnershipHistoricalReturn(Historical_Summary__c hreturn, String asOfDateString, ErrorHandler ehandler) {
        boolean isValid = true;
        date mydate = date.today();
        //date mydate = date.parse('12/11/2012');
        //date mydate = date.parse(ApexPages.CurrentPage().getParameters().get('testdate'));
        //date mydate = date.parse(testdatestring);    
        
        if (!isValidName(hreturn)) {
            ehandler.addError('thisPage:thisForm:Name', 'Fund Name', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.nameError = true;
        }
        if (asOfDateString == null || asOfDateString.trim().length() == 0) {
            ehandler.addError('thisPage:thisForm:AsOfDate', 'Please identify the Reporting Date for the performance information entered', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.AsOfDateError = true;
        } else
        if (asOfDateString != null && asOfDateString.trim().length() > 0)
        {
            if (Utilities.isValidDateString(asOfDateString))
            {
                hreturn.As_Of_Date__c = Utilities.getDate(asOfDateString);
                if ((hreturn.As_Of_Date__c.month() == 1 || hreturn.As_Of_Date__c.month() == 2   ||
                    hreturn.As_Of_Date__c.month() == 4 || hreturn.As_Of_Date__c.month() == 5   ||
                    hreturn.As_Of_Date__c.month() == 7 || hreturn.As_Of_Date__c.month() == 8   ||
                    hreturn.As_Of_Date__c.month() == 10 || hreturn.As_Of_Date__c.month() == 11 ) ||
                    (hreturn.As_Of_Date__c.month() == 3 && hreturn.As_Of_Date__c.day() != 31 ) ||
                    (hreturn.As_Of_Date__c.month() == 6 && hreturn.As_Of_Date__c.day() != 30 ) ||
                    (hreturn.As_Of_Date__c.month() == 9 && hreturn.As_Of_Date__c.day() != 30 ) ||
                    (hreturn.As_Of_Date__c.month() == 12 && hreturn.As_Of_Date__c.day() != 31 ))
                {
                    ehandler.addError('thisPage:thisForm:AsOfDate', 'Please identify the Reporting Date for the performance information entered', ErrorHandler.INVALID_DATE_Quarter, ErrorHandler.INVALID_DATE_Quarter);
                    isValid = false;
                    ehandler.hrlog.AsOfDateQuarterError = true;
                }  
                else 
                {
                    if (mydate.month() > 9 && (hreturn.As_Of_Date__c.year() != mydate.year() || (hreturn.As_Of_Date__c.month() > 6 ) ) ||
                    mydate.month() < 4 && (hreturn.As_Of_Date__c.year() != mydate.year() - 1 || (hreturn.As_Of_Date__c.month() > 9 || hreturn.As_Of_Date__c.month() < 4 ) )  ||
                    (mydate.month()> 3 && mydate.month() < 7) && (hreturn.As_Of_Date__c.year() != mydate.year() - 1 || (hreturn.As_Of_Date__c.month() < 7  ) )  ||
                    (mydate.month()> 6 && mydate.month() < 10) && (hreturn.As_Of_Date__c.year() == mydate.year() - 1 && (hreturn.As_Of_Date__c.month() < 10  ) )  ||
                    (mydate.month()> 6 && mydate.month() < 10) && (hreturn.As_Of_Date__c.year() == mydate.year()  && (hreturn.As_Of_Date__c.month() > 3  ) )
                     )
                    {
                        ehandler.addError('thisPage:thisForm:AsOfDate', 'Please identify the Reporting Date for the performance information entered', ErrorHandler.INVALID_DATE_Quarter2, ErrorHandler.INVALID_DATE_Quarter2);
                        isValid = false;
                        ehandler.hrlog.AsOfDateQuarterError2 = true;                    
                    }
                }
            }
            else
            {
                ehandler.addError('thisPage:thisForm:AsOfDate', 'Please identify the Reporting Date for the performance information entered', ErrorHandler.INVALID_DATE_FORMAT, ErrorHandler.INVALID_DATE_FORMAT);
                isValid = false;
                ehandler.hrlog.asOfDateFormatError = true;
            }
        }

//      boolean isValidAsOfDate = isValidAsOfDateString(hreturn, asOfDateString, ehandler);

        if (!isValidSize(hreturn)) {
            ehandler.addError('thisPage:thisForm:Size', 'Size', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.sizeError = true;
        }
        if (!isValidVintageYear(hreturn)) {
            ehandler.addError('thisPage:thisForm:VintageYear', 'Vintage Year', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.vintageYearError = true;
        }
        if (!isValidNumberOfPortfolioCompanies(hreturn)) {
            ehandler.addError('thisPage:thisForm:NumberOfPortfolio', 'Number of Portfolio Companies', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.numberOfPortfolioError = true;
        }
        if (!isValidCommittedCapital(hreturn))
        {
            ehandler.addError('thisPage:thisForm:CommittedCapital', 'Committed Capital', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.committedCapitalError = true;
        }
        if (!isValidContributedCapital(hreturn)) {
            ehandler.addError('thisPage:thisForm:ContributedCapital', 'Contributed Capital', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.contributedCapitalError = true;
        }
        if (!isValidInvested(hreturn)) {
            ehandler.addError('thisPage:thisForm:InvestedCapital', 'Invested Capital', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.investedCapitalError = true;
        }
        if (!isValidDistributedValue(hreturn)) {
            ehandler.addError('thisPage:thisForm:DistributedValue', 'Distributed Value', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.distributedValueError = true;
        }
        if (!isValidUnrealized(hreturn)) {
            ehandler.addError('thisPage:thisForm:UnrealizedValue', 'Unrealized Value', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.unrealizedValueError = true;
        }
        if (!isValidTotalValue(hreturn)) {
            ehandler.addError('thisPage:thisForm:TotalValue', 'Total Value', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.totalValueError = true;
        }
        if (!isValidRealized(hreturn)) {
            ehandler.addError('thisPage:thisForm:RealizedValue', 'Realized Value', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.realizedValueError = true;
        }
        if (!isValidGrossMultiple(hreturn)) {
            ehandler.addError('thisPage:thisForm:GrossMultiple', 'Gross Multiple', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.grossMultipleError = true;
        }
        if (!isValidNetMultiple(hreturn)) {
            ehandler.addError('thisPage:thisForm:NetMultiple', 'Net Multiple', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.netMultipleError = true;
        }
        if (!isValidGrossIRR(hreturn)) {
            ehandler.addError('thisPage:thisForm:GrossIRR', 'Gross IRR', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.grossIrrError = true;
        }
        if (!isValidNetIRR(hreturn)) {
            ehandler.addError('thisPage:thisForm:NetIRR', 'Net IRR', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.netIrrError = true;
        }
        if (!isValidNetDPI(hreturn)) {
            ehandler.addError('thisPage:thisForm:NetDPI', 'Net DPI', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.netDPIError = true;
        }
        if (hreturn.Explanation_of_Returns__c != null && hreturn.Explanation_of_Returns__c.trim().length() > 1000)  {
            String msg = 'Field has ' + hreturn.Explanation_of_Returns__c.length() + ' characters. Only 1000 allowed.';
            ehandler.addError('thisPage:thisForm:Explanation', 'Explanation of Returns', msg, msg);
            isValid = false;
            ehandler.hrlog.explReturnsSizeError = true;
        }
        return isValid;
//      return isValid && isValidAsOfDate;
    }
    
    public static Boolean isValidInfrastructurePartnershipHistoricalReturn(Historical_Summary__c hreturn, String asOfDateString, ErrorHandler ehandler) {
        boolean isValid = true;
        if (!isValidName(hreturn)) {
            ehandler.addError('thisPage:thisForm:Name', 'Fund Name', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.nameError = true;
        }
        if (!isValidSize(hreturn)) {
            ehandler.addError('thisPage:thisForm:Size', 'Size', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.sizeError = true;
        }
        if (!isValidVintageYear(hreturn)) {
            ehandler.addError('thisPage:thisForm:VintageYear', 'Vintage Year', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.vintageYearError = true;
        }
        if (!isValidInvested(hreturn)) {
            ehandler.addError('thisPage:thisForm:InvestedCapital', 'Invested Capital', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.investedCapitalError = true;
        }
        if (!isValidTotalValue(hreturn)) {
            ehandler.addError('thisPage:thisForm:TotalValue', 'Total Value', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.totalValueError = true;
        }
        if (!isValidNumberOfPortfolioCompanies(hreturn)) {
            ehandler.addError('thisPage:thisForm:NumberOfPortfolio', 'Number of Portfolio Companies', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.numberOfPortfolioError = true;
        }
        if (!isValidRealized(hreturn)) {
            ehandler.addError('thisPage:thisForm:RealizedValue', 'Realized Value', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.realizedValueError = true;
        }
        if (!isValidUnrealized(hreturn)) {
            ehandler.addError('thisPage:thisForm:UnrealizedValue', 'Unrealized Value', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.unrealizedValueError = true;
        }
        if (hreturn.Explanation_of_Returns__c != null && hreturn.Explanation_of_Returns__c.trim().length() > 1000) {
            String msg = 'Field has ' + hreturn.Explanation_of_Returns__c.length() + ' characters. Only 1000 allowed.';
            ehandler.addError('thisPage:thisForm:Explanation', 'Explanation of Returns', msg, msg);
            isValid = false;
            ehandler.hrlog.explReturnsSizeError = true;
        }
        if (asOfDateString == null || asOfDateString.trim().length() == 0) {
            ehandler.addError('thisPage:thisForm:AsOfDate', 'As Of Date', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.AsOfDateError = true;
        }
        boolean isValidAsOfDate = isValidAsOfDateString(hreturn, asOfDateString, ehandler);
        return isValid && isValidAsOfDate;
    }


    public static Boolean isValidRealEstateHistoricalReturn(Historical_Summary__c hreturn, String startDateOfTrackRecordString, String asOfDateString, ErrorHandler ehandler) {
        boolean isValid = true;
        if (hReturn.Name == null || hReturn.Name.trim().length() == 0){
            ehandler.addError('thisPage:thisForm:Name', 'Prior Fund Name', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.NameError = true;
        }

        if (asOfDateString == null || asOfDateString.trim().length() == 0) {
            ehandler.addError('thisPage:thisForm:AsOfDate', 'Please identify the As of Date for the Returns provided below', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.hrlog.AsOfDateError = true;
        }
        boolean isValidAsOfDate = isValidAsOfDateString(hreturn, asOfDateString, ehandler);

        if (hreturn.Explanation_of_Returns__c != null && hreturn.Explanation_of_Returns__c.trim().length() > 1000)  {
            String msg = 'Field has ' + hreturn.Explanation_of_Returns__c.length() + ' characters. Only 1000 allowed.';
            ehandler.addError('thisPage:thisForm:Explanation', 'Desription on Track Record', msg, msg);
            isValid = false;
            ehandler.hrlog.explReturnsSizeError = true;
        }

        Boolean isValidStartDate = isValidStartDateofTrackRecordString(hreturn, startDateOfTrackRecordString, ehandler);

        return isValid && isValidStartDate && isValidAsOfDate;
    }


    
    public static Boolean isValidHistoricalReturn(Historical_Summary__c hreturn) {
        return isValidYear(hreturn) || isValidProductBenchMark(hreturn) 
            || isValidSize(hreturn) || isValidProductReturn(hreturn) 
            || isValidBenchmarkReturn(hreturn);
    }
    
    static testmethod void testValidator() {
        Test.startTest();
        //HistoricalReturnValidator.isValidAIMPartnershipHistoricalReturn(new Historical_Summary__c(), null, new ErrorHandler());
        HistoricalReturnValidator.isValidInfrastructurePartnershipHistoricalReturn(new Historical_Summary__c(), null, new ErrorHandler());
        HistoricalReturnValidator.isValidHistoricalReturn(new Historical_Summary__c());
        Test.stopTest();
    }
}