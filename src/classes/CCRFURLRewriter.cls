global with sharing class CCRFURLRewriter implements Site.UrlRewriter {

 
 private static map < String, String > reverseRedirectMap = new Map < string, string > {'/CCRFAddAttachments' => '/AddAttach' };
   private static map < String, String > redirectMap = new Map < string, string > {'/AddAttach' => 'CCRFAddAttachments'};
        
        
    global PageReference mapRequestUrl(PageReference myFriendlyUrl) {
    String url = myFriendlyUrl.getUrl();
     String urlVal = Apexpages.currentPage().getUrl();
String paurl;
   url = url.substring(1);
        system.debug('-------- ' + url);
        system.debug('----1234---- ' + urlVal);
        paurl = url.contains('/') ? url.substring(0, url.indexOf('/')) : url.substring(0);
        system.debug('-----paurl------' + paurl);
        
        if(url.contains('CCRFAddAttachments')){
   // pagereference pg=page.CCRFAddAttachments;
    //pg.setRedirect(true);
    return null;
     }
     
     else{ return null;}
     
     }
      global List < PageReference > generateUrlFor(List < PageReference > mySalesforceUrls) {
      List < PageReference > myFriendlyUrls = new List < PageReference > ();
      for (PageReference mySalesforceUrl: mySalesforceUrls) {

                    myFriendlyUrls.add(new PageReference( mySalesforceUrl.getUrl()));
}
      return myFriendlyUrls;
      }
}