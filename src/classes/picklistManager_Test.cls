@isTest
private class picklistManager_Test {

    static testMethod void myUnitTest() {
		test.startTest();
			picklistManager.getAIMPartnershipGeoFocus();
			picklistManager.getAIMPartnershipStrategy();
			picklistManager.getBasisForFundManagementFeeOptions();
			picklistManager.getCAFocusOptions();
			picklistManager.getCommodityStrategy();
			picklistManager.getFixedIncomeGeoFocus();
			picklistManager.getFixedIncomeStrategy();
			picklistManager.getForestlandDevStage();
			picklistManager.getForestlandGeoFocus();
			picklistManager.getForestlandInvestmentVehicle();
			picklistManager.getGlobalEquityCurrencyValues();
			picklistManager.getGlobalEquityGeoFocus();
			picklistManager.getGlobalEquityGeoSubFocus();
			picklistManager.getGlobalEquityGeoSubFocusByGeoFocus('US');
			picklistManager.getGlobalEquityStrategy();
			picklistManager.getGlobalEquitySubStrategy();
			picklistManager.getHedgeFundsGeoFocus();
			picklistManager.getHedgeFundsStrategy();
			picklistManager.gethowDidYouHearAboutUsOptions();
			picklistManager.getInfrastructureDirectStratgey();
			picklistManager.getInfrastructureGeoFocus();
			picklistManager.getInfrastructurePartnershipGeoFocus();
			picklistManager.getInfrastructurePartnershipIndFocus();
			picklistManager.getInfrastructurePartnershipMarket();
			picklistManager.getInfrastructureStratgey();
			picklistManager.getInvestmentTypeByAssetClass('Infrastructure');
			picklistManager.getOwnershipPositionOptions();
			picklistManager.getRealEstateCurrencyValues();
			picklistManager.getRealEstateFundSectorValues();
			picklistManager.getRealEstateInvestmentVehicle();
			picklistManager.getRealEstateMarket();
			picklistManager.getRealEstatePartnershipGeoFocus();
			picklistManager.getRealEstatePropertyType();
			picklistManager.getRealEstateStrategy();
			picklistManager.getSalesForceDiversityOptions();
			picklistManager.getSalesForceDiversityRanges();
			picklistManager.getSalesForceExistingCalpersRelationship();
			picklistManager.getSalesForceFundSector();
			picklistManager.getSalesForceFundSector();
			picklistManager.getSalesForceGeoFocus();
			picklistManager.getSalesForceIndFocus();
			picklistManager.getSalesForceStrategy();
			picklistManager.getSBASBICOptions();
			picklistManager.getSubmittorRoleOptions();
			picklistManager.getTrackRecordReturnsOptions();
			picklistManager.getYesNoOptions();
		test.stopTest();
    }
}