// *********************************************************************************
// Copyright 2010 MK Partners, Inc. - www.mkpartners.com
// Version 2010-04-20
// *********************************************************************************
/*
	To Run:
		OpportunityPrincipals_Scheduler m = new  OpportunityPrincipals_Scheduler();
		String sch  =  '0 0 * * * ?';
		String jobId = system.schedule('OpportunityPrincipals Job', sch, m);

	To Check Status:
		CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

*/

global  class  OpportunityPrincipals_Scheduler  implements  Schedulable{
	global  void  execute(SchedulableContext  sc)  {
		OpportunityPrincipals_Batch b = new OpportunityPrincipals_Batch();
		database.executebatch(b);
	}
}