public with sharing class AccountHelper {
	private Account accountRef;
	
	public AccountHelper(Account accountRef) {
		this.accountRef = accountRef;
	}
	
	public String getInceptionDate() {
		return (accountRef.Inception_Date__c != null 
			&& String.valueOf(accountRef.Inception_Date__c).trim().length() > 0) ? Utilities.toDateString(accountRef.Inception_Date__c) : Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getNumberOfEmployees() {
		return (accountRef.NumberOfEmployees != null) ? String.valueOf(accountRef.NumberOfEmployees) : Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getNumberOfOffices() {
		return (accountRef.Number_Of_Offices__c != null) ? String.valueOf(accountRef.Number_Of_Offices__c) : Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getNumberOfInvestProfs() {
		return (accountRef.Number_of_Investment_Professionals__c != null) ? String.valueOf(accountRef.Number_of_Investment_Professionals__c) : Constants.NO_RESPONSE_PROVIDED;
	}
	
	static testmethod void testUtilMethods() {
		Test.startTest();
		AccountHelper h = new AccountHelper(new Account());
		System.assertEquals(Constants.NO_RESPONSE_PROVIDED, h.getInceptionDate());
		System.assertEquals(Constants.NO_RESPONSE_PROVIDED, h.getNumberOfEmployees());
		System.assertEquals(Constants.NO_RESPONSE_PROVIDED, h.getNumberOfOffices());
		System.assertEquals(Constants.NO_RESPONSE_PROVIDED, h.getNumberOfInvestProfs());
		h = new AccountHelper(new Account(Inception_Date__c=System.today(), NumberOfEmployees=100, Number_Of_Offices__c=5, Number_of_Investment_Professionals__c=60));
		System.assertNotEquals(Constants.NO_RESPONSE_PROVIDED, h.getInceptionDate());
		System.assertNotEquals(Constants.NO_RESPONSE_PROVIDED, h.getNumberOfEmployees());
		System.assertNotEquals(Constants.NO_RESPONSE_PROVIDED, h.getNumberOfOffices());
		System.assertNotEquals(Constants.NO_RESPONSE_PROVIDED, h.getNumberOfInvestProfs());
		Test.stopTest();
	}
}