public with sharing class changecontrolformCNTRL {

    public changecontrolformCNTRL() {
        system.debug('===================normal con=====');


    }
    public void onloadattmethod()
    {
      isSuccess =true;
    objAttachmentList=new List<Attachment>();
    objAttachmentList.add(new attachment ());
     objAttachmentList.add(new attachment ());
      objAttachmentList.add(new attachment ());
       objAttachmentList.add(new attachment ());
        objAttachmentList.add(new attachment ());
        objAttachmentList.add(new attachment ());
    
    }
//public List <SelectOption> allType { get; set; }
 //public List<selectoption> gettheSelectOption(){
  //public pagereference  SaveChangeControlatt(){
    //   allType.add(new SelectOption('Defect Correction','Defect Correction'));
      // allType.add(new SelectOption('Enhancement','Enhancement'));
       //allType.add(new SelectOption('New','New'));
       //allType.add(new SelectOption('Other','Other'));}}




Public List<Environment__c> EnviList {get; set;}
public Approver__c UATApp {get;set;}
public List<Approver__c> UATAppList {get;set;}
public Approver__c MGMTApp {get;set;}
public List<Approver__c> MGMTAppList {get;set;}
Public List<attachment> objAttachmentList{get; set;}
Public Change_control_request__c ccRequest {get;set;}
public boolean isSuccess {get;set;}

public boolean DefectCorrection {get;set;}
public boolean Enhancement {get;set;}
public boolean NewRequest {get;set;}
public boolean Other {get;set;}
 public Boolean regBoolean{get;set;}
 public Boolean istrequest{get;set;}
  public ErrorHandler ehandler {get; set;}
   public Lead regLead {get; set;}
   
    public changecontrolformCNTRL(ApexPages.StandardController controller) {
    system.debug('===================extcon=====');
 selSecurityOptions='';
    isSuccess =true;
    istrequest=false;
    ccRequest= (Change_control_request__c)controller.getrecord();
    EnviList =new List<Environment__c>();
    EnviList.add(new Environment__c(name='Production'));
    UATAppList=new List<Approver__c>();
    //MGMTAppList=new List<Approver__c>();
    
    //MGMTAppList.add(new Approver__c ());
    UATAppList=new List<Approver__c>();
    UATAppList.add(new Approver__c ());    

    }
    
    Public void AddMoreEnvi()
    {
     EnviList.add(new Environment__c(name='Production'));

    }
    
      Public void removeEnvi()
    {
     if(EnviList.size()>1) EnviList.remove(UATAppList.size()-1);

    }
    
     Public void AddMoreUAT()
    {
     UATAppList.add(new Approver__c ());

    }
    
      Public void removeUAT()
    {
     //UATAppList.add(new Approver__c ());
     if(UATAppList.size()>1) UATAppList.remove(UATAppList.size()-1);

    }
    
     Public void AddMoreMgmt()
    {
     MGMTAppList.add(new Approver__c ());
    }

//public String getInputRequiredMessage() {
  //      return ErrorHandler.INPUT_MSG;
    //}    
    
    Public pagereference SaveChangeControlForm()
    {
isSuccess =true;
//istrequest=true;


 if(!selSecurityOptions.contains('Defect Correction') &&  !selSecurityOptions.contains('Enhancement') &&  !selSecurityOptions.contains('New') &&  !selSecurityOptions.contains('Other')) {
istrequest=true;
   ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter value'));

return null;}
else{


//ehandler.clearMessages();
  //      Boolean isValid = true;
//ehandler = new ErrorHandler();

  //      if (string.isBlank(selSecurityOptions)) {
    //        ehandler.addError('thisPage:thisForm:FirstName', 'First Name', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
      //      isValid = false;
        //    ehandler.clog.firstNameError = true;
        //}










system.debug('============='+ccRequest.Initiator_Name__c);

     //pagereference pg=new pagereference('/apex/CCRFAddAttachments?parentID='+ccRequest.ID);
      // pagereference pg=page.CCRFAddAttachments;
           pagereference pg=new pagereference('/apex/changecontrolformattachmentsCF');

       pg.setRedirect(false);
        return pg;
        }
    }
    public String formatpklist(List<String> values) {
    if (values == null) return null;
    return String.join(values, ';');
}


    public pagereference  SaveChangeControlatt()  
{
     
     system.debug('===='+ccRequest) ;
     system.debug('============='+ccRequest.Initiator_Name__c);
     if(selSecurityOptions!=''&&selSecurityOptions !=null)
     {
     List<String> selmPiklistVals=new List<String>();
     
         if(selSecurityOptions.contains('Defect Correction')) selmPiklistVals.add('Defect Correction');
          if(selSecurityOptions.contains('Enhancement')) selmPiklistVals.add('Enhancement');
           if(selSecurityOptions.contains('New')) selmPiklistVals.add('New');
            if(selSecurityOptions.contains('Other')) selmPiklistVals.add('Other');
            
//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error Message.'));

if(!selmPiklistVals.isEmpty()) { 
ccRequest.Type_of_Request__c=formatpklist(selmPiklistVals);}
}

      insert ccRequest;
      
      for(Environment__c ev:EnviList )
      {
      ev.Change_control_request_from__c=ccRequest.id;
      }
      
      insert EnviList ;
       for(Approver__c UATApp: UATAppList)
       {
          UATApp.UAT_Approval__c =ccRequest.id;
          }
    insert UATAppList;
  
    List<Attachment> attToinsert=new List<Attachment>();
     for(Attachment objAttachment:objAttachmentList){
     if(objAttachment.name!=''&&objAttachment.body!=null)
     {
     objAttachment.parentID=ccRequest.ID;
     attToinsert.add(objAttachment);
     }
     }
     if(!attToinsert.isempty())
     insert attToinsert;
     isSuccess =false;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.confirm, '  Your Submission was Successful '));

     objAttachmentList.clear();

    return null;
    }
public String strSelectedValues {get;set;}
    
  public void callFunc()
    {
       system.debug('====='+strSelectedValues);
    }
    public List<string> selectedValues {get;set;}

    public List<selectoption> gettheSelectOptions() 
    {           
        list<selectoption> options = new list<selectoption>();            
        try 
        {               
        //Product Name is a MultiSelect Picklist               
        Schema.DescribeFieldResult fieldResult = opportunity.stagename.getDescribe();

        list<schema.picklistentry> values = fieldResult.getPickListValues();               
        for (Schema.PicklistEntry a : values) 
        {                  
        options.add(new SelectOption(a.getLabel(), a.getValue()));
        }           
        }  
        catch (Exception e) 
        {             
        ApexPages.addMessages(e);           
        }
        system.debug('## Product Name Options'+ options);          
        return options; 
    }
    public string mypicklistselected {get; set;}
public void runSearch ()
{
mypicklistselected  = Apexpages.currentPage().getParameters().get('selValues');
    
}


//===========================
public string selSecurityOptions {get; set;}

public List<SelectOption> getAllsecurityOptions ()
{

List<SelectOption> securityOptions = new List<SelectOption>();

Schema.DescribeFieldResult securityFieldDescription = Change_control_request__c.Type_of_Request__c.getDescribe();

for (Schema.Picklistentry picklistEntry: securityFieldDescription.getPicklistValues()){

securityOptions.add(new SelectOption(pickListEntry.getValue(),pickListEntry.getLabel()));

if (picklistEntry.defaultValue){
ccRequest.Type_of_Request__c = pickListEntry.getValue();
} 
}
return securityOptions;
}


}