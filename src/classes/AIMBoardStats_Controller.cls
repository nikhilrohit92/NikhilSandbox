public with sharing class AIMBoardStats_Controller {

	public Date startDate {get;set;}
	public Date endDate {get;set;}
	public metric m {get;set;}
	
	public class metric{
		public string startDateFormatted {get;set;}
		public string endDateFormatted {get;set;}
		public string yearP {get;set;}	//report period
		public string year0 {get;set;}	//current year
		public string year1 {get;set;}	//last year
		public string year2 {get;set;}	//year before that
		public string year3 {get;set;}	//year before that
		public string yearAll {get;set;}	//all years
		public string yearP_Entered {get;set;}	//report period
		public string yearP_Declined {get;set;}	//report period
		public string yearP_Screened {get;set;}	//report period
		public string yearP_Reviewed {get;set;}	//report period
		public string year0_Entered {get;set;}	//current year
		public string year0_Declined {get;set;}	//current year
		public string year0_Screened {get;set;}	//current year
		public string year0_Reviewed {get;set;}	//current year
		public string year1_Entered {get;set;}	//last year
		public string year1_Declined {get;set;}	//last year
		public string year1_Screened {get;set;}	//last year
		public string year1_Reviewed {get;set;}	//last year
		public string year2_Entered {get;set;}	//year before that
		public string year2_Declined {get;set;}	//year before that
		public string year2_Reviewed {get;set;}	//year before that
		public string year3_Entered {get;set;}	//year before that
		public string year3_Declined {get;set;}	//year before that
		public string year3_Screened {get;set;}	//year before that
		public string year3_Reviewed {get;set;}	//year before that
		public string yearAll_Entered {get;set;}	//all years
		public string yearAll_Declined {get;set;}	//all years
		public string yearAll_Screened {get;set;}	//all years
		public string yearAll_Reviewed {get;set;}	//all years
	}

	public AIMBoardStats_Controller(){
		m = new Metric();
		List<Opportunity> opps = new List<Opportunity>();
		for ( Opportunity o : [Select Id from Opportunity]){
			
		}
	}

}