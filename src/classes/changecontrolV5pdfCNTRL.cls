public with sharing class changecontrolV5pdfCNTRL {

Public List<Environment__c> EnviList {get; set;}
public Approver__c UATApp {get;set;}
public List<Approver__c> UATAppList {get;set;}
public Approver__c MGMTApp {get;set;}
public List<Approver__c> MGMTAppList {get;set;}
Public List<attachment> objAttachmentList{get; set;}
Public Change_control_request__c ccRequest;
public string URLL  {get; set;}

    public changecontrolV5pdfCNTRL(ApexPages.StandardController controller) {
    String hostVal  = ApexPages.currentPage().getHeaders().get('Host');
String urlVal = Apexpages.currentPage().getUrl();
URLL = 'https://' + hostVal+ urlVal;  
    
    ccRequest= (Change_control_request__c)controller.getrecord();
    EnviList =new List<Environment__c>();
    EnviList.addall([select id, Name, Applications_affected__c,Business_Units_Impacted__c,Change_control_request_from__c,Contacts_Notified__c,Record_submission_date__c,Required_Rsources__c from Environment__c where  Change_control_request_from__c=:ccrequest.id]);
    UATAppList=new List<Approver__c>();
    //MGMTAppList=new List<Approver__c>();
    
    //MGMTAppList.add(new Approver__c ());
    UATAppList=new List<Approver__c>();
    UATAppList.addall([select id, Name,Approver__c.Approval_Date__c,UAT_Approval__c,Unit__c from Approver__c where UAT_Approval__c=:ccrequest.id ]);    

    }
    
    Public void AddMoreEnvi()
    {
     EnviList.add(new Environment__c(name='Production'));

    }
     Public void AddMoreUAT()
    {
     UATAppList.add(new Approver__c ());

    }
     Public void AddMoreMgmt()
    {
     MGMTAppList.add(new Approver__c ());
    }
    
    Public pagereference SaveChangeControlForm()
    {
     system.debug('===='+ccRequest) ;
      insert ccRequest;
      
      for(Environment__c ev:EnviList )
      {
      ev.Change_control_request_from__c=ccRequest.id;
      }
      
      insert EnviList ;
       for(Approver__c UATApp: UATAppList)
       {
          UATApp.UAT_Approval__c =ccRequest.id;
          }
    insert UATAppList;
   /* for(Approver__c MGMTApp: MGMTAppList)
       {
      MGMTApp.Management_sign_Off__c=ccRequest.id;
      }
     insert MGMTAppList; */
     List<Attachment> attToinsert=new List<Attachment>();
     for(Attachment objAttachment:objAttachmentList){
     if(objAttachment.name!=''&&objAttachment.body!=null)
     {
     objAttachment.parentID=ccRequest.id;
     attToinsert.add(objAttachment);
     }
     }
     if(!attToinsert.isempty())
     insert attToinsert;
     
     objAttachmentList.clear();
     pagereference pg=new pagereference('/apex/AddAttachments?parentID='+ccRequest.ID);
    return pg;
    }




}