//APEX class to encapsulate an error message the needs to rendered in the User Interface
public with sharing class ErrorMessage 
{
	public String fieldId {get; set;}
	public String fieldLabel {get; set;}
	public String fieldMessage {get; set;}
	public String pageMessage {get; set;}
	
	public ErrorMessage(String fieldId, String fieldLabel, String fieldMessage, String pageMessage)
	{
		this.fieldId = fieldId;
		this.fieldLabel = fieldLabel;
		this.fieldMessage = fieldMessage;
		this.pageMessage = pageMessage;
	}
}