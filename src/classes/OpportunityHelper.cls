public with sharing class OpportunityHelper {
	private Opportunity proposal;
	static Map<String, Schema.SObjectField> opportunitySchema = Schema.SObjectType.Opportunity.fields.getMap();
	
	//Helper method to all creatable fields from Plan__c as a comma sperated string
	public static String getOpportunityFieldNamesString() {
    	String str = '';
    	for (String k : opportunitySchema.keySet()) {
      		Schema.DescribeFieldResult fr = opportunitySchema.get(k).getDescribe();
      		if (fr.isCreateable() || fr.isAutoNumber() || fr.isCalculated()) {
      			str += fr.getName() + ', ';
      		} else
      		if ( k.toLowerCase() == 'createddate' ){
      			str += fr.getName() + ', ';
      		}
    	}
    	str = str.substring(0,str.length()-2);
    	return str;
  	}
  	
  	private String format(Schema.DescribeFieldResult fr, String value)
  	{
  		if (fr.getType() == Schema.Displaytype.PERCENT)
  		{
  			return value + '%';
  		}	
  		else if (fr.getType() == Schema.Displaytype.CURRENCY)
  		{
  			return '$' + value;
  		}
  		return value;
  	}

 	
  	public static Opportunity getOpportunity(String opptyId)
  	{
  		String query = 'Select ' + getOpportunityFieldNamesString() + ' from Opportunity where Id = \'' + opptyId + '\'';
  		return Database.query(query);
  	}
  	
	public OpportunityHelper(Opportunity proposal)
	{
		this.proposal = proposal;
	}
	
	public String getPriorConfirmationNumber()
	{
		return (proposal.Prior_Confirmation_Number__c != null) ? String.valueOf(proposal.Prior_Confirmation_Number__c) : Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getProposedHoldingPeriod()
	{
		return (proposal.Proposed_Holding_Period_In_Years__c != null) ? String.valueOf(proposal.Proposed_Holding_Period_In_Years__c) : Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getTotalDealSize()
	{
		if (proposal.Total_Deal_Size_In_Millions__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.Total_Deal_Size_In_Millions__c.getDescribe();
			return format(fr, String.valueOf(proposal.Total_Deal_Size_In_Millions__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getDealsInAYear()
	{
		return (proposal.How_many_deals_do_you_see_a_year__c != null) ? String.valueOf(proposal.How_many_deals_do_you_see_a_year__c) : Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getEquity()
	{
		if (proposal.Equity__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.Equity__c.getDescribe();
			return format(fr, String.valueOf(proposal.Equity__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getCoInvestMinimum()
	{
		if (proposal.Co_Investment_Minimum_In_Millions__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.Co_Investment_Minimum_In_Millions__c.getDescribe();
			return format(fr, String.valueOf(proposal.Co_Investment_Minimum_In_Millions__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getCoInvestMaximum()
	{
		if (proposal.Co_Investment_Maximum_In_Millions__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.Co_Investment_Maximum_In_Millions__c.getDescribe();
			return format(fr, String.valueOf(proposal.Co_Investment_Maximum_In_Millions__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getLTMRevenue()
	{
		if (proposal.LTM_Revenue_In_Millions__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.LTM_Revenue_In_Millions__c.getDescribe();
			return format(fr, String.valueOf(proposal.LTM_Revenue_In_Millions__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getLTMEBITDA()
	{
		if (proposal.LTM_EBITDA_In_Millions__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.LTM_EBITDA_In_Millions__c.getDescribe();
			return format(fr, String.valueOf(proposal.LTM_EBITDA_In_Millions__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getTotalValue()
	{
		if (proposal.Total_Value_In_Millions__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.Total_Value_In_Millions__c.getDescribe();
			return format(fr, String.valueOf(proposal.Total_Value_In_Millions__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getTotalDebt()
	{
		if (proposal.Total_Debt_In_Millions__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.Total_Debt_In_Millions__c.getDescribe();
			return format(fr, String.valueOf(proposal.Total_Debt_In_Millions__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getOwnershipPercent()
	{
		if (proposal.Percent_Equity_Interest_being_offered__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.Percent_Equity_Interest_being_offered__c.getDescribe();
			return format(fr, String.valueOf(proposal.Percent_Equity_Interest_being_offered__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	 
	public String getSoftCircled()
	{
		if (proposal.Soft_Circled_In_Millions__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.Soft_Circled_In_Millions__c.getDescribe();
			return format(fr, String.valueOf(proposal.Soft_Circled_In_Millions__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getHardCommitments()
	{
		if (proposal.Hard_Commitments_In_Millions__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.Hard_Commitments_In_Millions__c.getDescribe();
			return format(fr, String.valueOf(proposal.Hard_Commitments_In_Millions__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getCatchUp()
	{
		if (proposal.Catch_up__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.Catch_up__c.getDescribe();
			return format(fr, String.valueOf(proposal.Catch_up__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getFundTerm()
	{
		return (proposal.Fund_Term_In_Years__c != null) ? String.valueOf(proposal.Fund_Term_In_Years__c) : Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getInvestmentPeriod()
	{
		return (proposal.Investment_Period_in_Years__c != null) ? String.valueOf(proposal.Investment_Period_in_Years__c) : Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getGPCommitmentPercent()
	{
		if (proposal.GP_commitment_percent__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.GP_commitment_percent__c.getDescribe();
			return format(fr, String.valueOf(proposal.GP_commitment_percent__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getGPCommitmentAmount()
	{
		if (proposal.GP_Commitment_Amount_In_Millions__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.GP_Commitment_Amount_In_Millions__c.getDescribe();
			return format(fr, String.valueOf(proposal.GP_Commitment_Amount_In_Millions__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getNumberOfInvestmentProfessionals()
	{
		return (proposal.of_Investment_Prof_on_product__c != null) ? String.valueOf(proposal.of_Investment_Prof_on_product__c) : Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getManagementFee()
	{
		if (proposal.Management_Fee__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.Management_Fee__c.getDescribe();
			return format(fr, String.valueOf(proposal.Management_Fee__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getPerformanceFee()
	{
		if (proposal.Performance_Fee__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.Performance_Fee__c.getDescribe();
			return format(fr, String.valueOf(proposal.Performance_Fee__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getOtherFee()
	{
		return (proposal.Other_Fee_Narrative__c != null) ? String.valueOf(proposal.Other_Fee_Narrative__c) : Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String get3YearExcessReturn()
	{
		if (proposal.X3_Year_Excess_Return__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.X3_Year_Excess_Return__c.getDescribe();
			return format(fr, String.valueOf(proposal.X3_Year_Excess_Return__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String get3YearNetExcessReturn()
	{
		if (proposal.X3_Year_Net_Excess_Return__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.X3_Year_Net_Excess_Return__c.getDescribe();
			return format(fr, String.valueOf(proposal.X3_Year_Net_Excess_Return__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}

	public String get3YearTrackingError()
	{
		return (proposal.X3_Year_Tracking_Error__c != null) ? String.valueOf(proposal.X3_Year_Tracking_Error__c) : Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String get3YearInformationRatio()
	{
		return (proposal.X3_Year_Information_Ratio__c != null) ? String.valueOf(proposal.X3_Year_Information_Ratio__c) : Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String get5YearExcessReturn()
	{
		if (proposal.X5_Year_Excess_Return__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.X5_Year_Excess_Return__c.getDescribe();
			return format(fr, String.valueOf(proposal.X5_Year_Excess_Return__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String get5YearNetExcessReturn()
	{
		if (proposal.X5_Year_Net_Excess_Return__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.X5_Year_Net_Excess_Return__c.getDescribe();
			return format(fr, String.valueOf(proposal.X5_Year_Net_Excess_Return__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	public String get5YearTrackingError()
	{
		return (proposal.X5_Year_Tracking_Error__c != null) ? String.valueOf(proposal.X5_Year_Tracking_Error__c) : Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String get5YearInformationRatio()
	{
		return (proposal.X5_Year_Information_Ratio__c != null) ? String.valueOf(proposal.X5_Year_Information_Ratio__c) : Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getSinceInceptionInformationRatio()
	{
		return (proposal.Since_Inception_Information_Ratio__c != null) ? String.valueOf(proposal.Since_Inception_Information_Ratio__c) : Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getExtensionPeriod()
	{
		return (proposal.Extension_Period_in_Years__c != null) ? String.valueOf(proposal.Extension_Period_in_Years__c) : Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getPropertyLevelLeverage()
	{
		if (proposal.Property_Level_Leverage__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.Property_Level_Leverage__c.getDescribe();
			return format(fr, String.valueOf(proposal.Property_Level_Leverage__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getDistributionNotes()
	{
		return (proposal.Distribution_Notes_waterfall__c != null) ? String.valueOf(proposal.Distribution_Notes_waterfall__c) : Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getCarry()
	{
		return (proposal.Carry__c != null) ? String.valueOf(proposal.Carry__c) : Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getPreferredReturn()
	{
		if (proposal.Preferred_Return__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.Preferred_Return__c.getDescribe();
			return format(fr, String.valueOf(proposal.Preferred_Return__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getBeforeFeeLeveraged()
	{
		if (proposal.Before_Fee_Leveraged_IRR__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.Before_Fee_Leveraged_IRR__c.getDescribe();
			return format(fr, String.valueOf(proposal.Before_Fee_Leveraged_IRR__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getBeforeFeeUnLeveraged()
	{
		if (proposal.Before_Fee_Unleveraged_IRR__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.Before_Fee_Unleveraged_IRR__c.getDescribe();
			return format(fr, String.valueOf(proposal.Before_Fee_Unleveraged_IRR__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getAfterFeeLeveraged()
	{
		if (proposal.After_Fee_Leveraged_IRR__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.After_Fee_Leveraged_IRR__c.getDescribe();
			return format(fr, String.valueOf(proposal.After_Fee_Leveraged_IRR__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getAfterFeeUnLeveraged()
	{
		if (proposal.After_Fee_Unleveraged_IRR__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.After_Fee_Unleveraged_IRR__c.getDescribe();
			return format(fr, String.valueOf(proposal.After_Fee_Unleveraged_IRR__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getTargetGross()
	{
		if (proposal.Target_Gross_IRR__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.Target_Gross_IRR__c.getDescribe();
			return format(fr, String.valueOf(proposal.Target_Gross_IRR__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getTargetNet()
	{
		if (proposal.Target_Net_IRR__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.Target_Net_IRR__c.getDescribe();
			return format(fr, String.valueOf(proposal.Target_Net_IRR__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getExpectedCashYield()
	{
		if (proposal.Expected_Cash_Yield__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.Expected_Cash_Yield__c.getDescribe();
			return format(fr, String.valueOf(proposal.Expected_Cash_Yield__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getTieredManagementFeeStructure()
	{
		return (proposal.Tiered_Management_Fee_Structure__c != null) ? String.valueOf(proposal.Tiered_Management_Fee_Structure__c) : Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getMinimumCommitmentAmount()
	{
		if (proposal.Minimum_Commitment_Amt__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.Minimum_Commitment_Amt__c.getDescribe();
			return format(fr, String.valueOf(proposal.Minimum_Commitment_Amt__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getProposedInvestment()
	{
		return (proposal.Proposed_CalPERS_Investment__c != null) ? String.valueOf(proposal.Proposed_CalPERS_Investment__c) : Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getAssetUnderMgmt()
	{
		if (proposal.Assets_Under_Management__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.Assets_Under_Management__c.getDescribe();
			return format(fr, String.valueOf(proposal.Assets_Under_Management__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getInvSoftCircled()
	{
		return (proposal.Investors_Soft_Circled__c != null) ? String.valueOf(proposal.Investors_Soft_Circled__c) : Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getInvHardCommitments()
	{
		return (proposal.Investors_Hard_Commitments__c != null) ? String.valueOf(proposal.Investors_Hard_Commitments__c) : Constants.NO_RESPONSE_PROVIDED;
	}

	public String getExpectedAvergaeLeverage()
	{
		return (proposal.Expected_Average_Leverage__c != null) ? String.valueOf(proposal.Expected_Average_Leverage__c) : Constants.NO_RESPONSE_PROVIDED;
	}

	public String getAimCatchUp()
	{
		return (proposal.AIM_Partnership_Catch_Up__c != null) ? String.valueOf(proposal.AIM_Partnership_Catch_Up__c) : Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getDebtEBITDA()
	{
		return (proposal.Debt_EBITDA_In_Millions__c != null) ? String.valueOf(proposal.Debt_EBITDA_In_Millions__c) : Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getLeverage()
	{
		if (proposal.Leverage__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.Leverage__c.getDescribe();
			return format(fr, String.valueOf(proposal.Leverage__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getEnterpriseValue()
	{
		return (proposal.Enterprise_Value_In_Millions__c != null) ? String.valueOf(proposal.Enterprise_Value_In_Millions__c) : Constants.NO_RESPONSE_PROVIDED;
	}
	
	public String getTotalFinancingRequested()
	{
		if (proposal.Total_Financing_Requested_USD__c != null)
		{
			Schema.DescribeFieldResult fr = Opportunity.Total_Financing_Requested_USD__c.getDescribe();
			return format(fr, String.valueOf(proposal.Total_Financing_Requested_USD__c));
		} 
		return Constants.NO_RESPONSE_PROVIDED;
	}
}