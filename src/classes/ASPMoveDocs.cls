global class ASPMoveDocs 
{
  webService static Id moveASPDocs(id camid, string shortname) 
    {
        List<ContentVersion> newdocs = new List<ContentVersion>();
        List<opportunity> oppids=new List<opportunity>();
        string tempAccountName;
        Id LibraryId = [select id from ContentWorkspace where name = :shortname].id;
        
        for(Opportunity o : [select id, Confirmation_Number__c, Account.Name from Opportunity where campaignid = :camid and (stagename != 'Draft' or stagename != 'Deleted')]){
            oppids.add(o);
        }            
        List<ASP_Document__c> ASPDocument = [Select Id, Document_Name__c, Short_Name__c, Type__c,  Tags__c from ASP_Document__c where ASP_Name__r.ASP_Short_Name__c = :shortname];
        List<Attachment> attchments = [select id, name, body, ContentType, parentid from Attachment where parentid = :oppids];
        system.debug('**attchments ** ' + attchments[0] );
        integer counter = 1;
        for (Attachment attch :  attchments) {
            
            //ContentVersion existingdoc = [select id from ContentVersion where FirstPublishLocationId = :LibraryId and opportunity__c = :attch.parentid and title = :attch.Name];
            // doc.id = existingdoc.id;
            ContentVersion doc = new ContentVersion();
            if (attch.ContentType == 'Form ADV' || attch.ContentType == 'Other' || attch.ContentType == 'Prospectus' || string.isBlank(attch.ContentType)) {
                doc.PathOnClient = attch.Name.substringBeforeLast('.') +'_'+counter  +'.'+attch.Name.substringAfterLast('.');
                doc.title = attch.Name.substringBeforeLast('.') +'_'+counter  +'.'+attch.Name.substringAfterLast('.');
                doc.tagcsv = attch.ContentType;
                counter++;
            } else {
                
                for (ASP_Document__c aspdoc :  ASPDocument ) {
                    if (aspdoc.Document_Name__c == attch.ContentType) {
                        for ( Opportunity tempopp : oppids)
                            if (attch.parentid == tempopp.id) {
                                // check to see the length of the firm name - tempopp.Account.Name.substring(0,30) and message box
                                if (tempopp.Account.Name.length() > 30) 
                                    tempAccountName = tempopp.Account.Name.substring(0,29);
                                else 
                                    tempAccountName = tempopp.Account.Name;
                                doc.title = aspdoc.Short_Name__c+'_'+tempopp.Confirmation_Number__c+'_'+tempAccountName +'_'+counter  +'.'+attch.Name.substringAfterLast('.');
                                doc.PathOnClient = aspdoc.Short_Name__c+'_'+tempopp.Confirmation_Number__c+'_'+tempAccountName +'_'+counter  +'.'+attch.Name.substringAfterLast('.');
                                break;
                            }
                        counter++;
                        doc.tagcsv = aspdoc.Tags__c;
                        break;
                    }
                }
            }
            
            doc.VersionData = attch.Body;
            doc.FirstPublishLocationId = LibraryId; //shortname;
            
            doc.opportunity__c = attch.parentid;
            newdocs.add(doc);

        }
        system.debug('**newdocs ** ' + newdocs[0] );
        try {    upsert newdocs; }
         catch(exception  e)
             { system.debug('**exception  ** ' + e ); }
        
        return null;
    }
}