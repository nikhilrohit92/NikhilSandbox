public with sharing class OpportunityErrorLog 
{
    public boolean leadSourceError {get; set;}
    public boolean assetClassError {get; set;}
    public boolean typeError {get; set;}
    public boolean aumError {get; set;}
    public boolean partnershipAumError {get; set;}
    public boolean briefHistoryError {get; set;}
    public boolean briefHistorySizeError {get; set;}
    public boolean numberOfDealsError {get; set;}
    public boolean proprietaryError {get; set;}
    public boolean fundraisingDateError {get; set;}
    public boolean fundraisingDateFormatError {get; set;}
    public boolean subsequentCloseDateError {get; set;}
    public boolean subsequentCloseDateFormatError{get; set;}
    public boolean nameError{get; set;}
    public boolean geoFocusError{get; set;}
    public boolean geoSubFocusError{get; set;}
    public boolean indFocusError{get; set;}
    public boolean strategyError{get; set;}
    public boolean subStrategyError{get; set;}
    public boolean ifDevelopedSpecifyCountryNameError{get; set;}
    public boolean currencyError{get; set;}
    public boolean caFocusError{get; set;}
    public boolean sbaSbicError{get; set;}
    public boolean execSummaryError{get; set;}
    public boolean execSummarySizeError{get; set;}
    public boolean fundSizeCoverError {get; set;} 
    public boolean fundSizeCoverUSDError {get; set;} 
    public boolean marketError {get; set;}
    public boolean productError {get; set;}
    public boolean productInceptionDateError {get; set;}
    public boolean productInceptionDateFormatError {get; set;}
    public boolean productAumError {get; set;}
    public boolean productAumUSDError {get; set;}
    public boolean processDescError {get; set;}
    public boolean processDescSizeError {get; set;}
    public boolean numberOfProfessionalsError {get; set;}
    public boolean productUsesLeverageError {get; set;}
    public boolean productCoMingledFundError {get; set;}
    public boolean providesIncomePaymentStreamError {get; set;}
    public boolean externallyManagedInvestmentStrategyError {get; set;}
    public boolean currencyOnlyCompositeProvidedError {get; set;}
    public boolean currencyOnlyCompositeCarveOutError {get; set;}
    public boolean partnershipNameError {get; set;}
    public boolean investmentDevelopmentStageError {get; set;}
    public boolean investmentVehicleError {get; set;}
    public boolean investmentPeriodError {get; set;}
    public boolean fundTermError {get; set;}
    public boolean fundSectorError {get; set;}
    public boolean extensionPeriodError {get; set;}
    public boolean managementFeeError {get; set;}
    public boolean gpCarryError {get; set;}
    public boolean preferredReturnError {get; set;}
    public boolean anticipatedFinalClosingDateError {get; set;}
    public boolean gpCommitmentPercentError {get; set;}
    public boolean gpCommitmentAmountError {get; set;}
    public boolean fundCapError {get; set;}
    public boolean averageInvestmentSizeError {get; set;}
    public boolean expectedNumberOfInvestmentsError {get; set;}
    public boolean firstCloseError {get; set;}
    public boolean firstCloseFormatError {get; set;}
    public boolean anticipatedError {get; set;}
    public boolean anticipatedFormatError {get; set;}
    public boolean expectedMaxLeverageError {get; set;}
    public boolean anticaptedFundSizeError {get; set;}
    public boolean anticaptedFundSizeUSDError {get; set;}
    public boolean minimumCommitmentAmountError {get; set;}
    public boolean minimumAllocationAmountError {get; set;}
    public boolean coInvestRightsError {get; set;}
    public boolean carryingChargeError {get; set;}
    public boolean catchupError {get; set;}
    public boolean advisoryRightsError {get; set;}
    public boolean productCapacityError {get; set;}
    public boolean initialLockupPeriodError {get; set;}
    public boolean propertyLevelLeverageError {get; set;}
    public boolean beforeFeeLeveragedError {get; set;}
    public boolean productBenchmarkError {get; set;}
    public boolean percentOfProductError {get; set;}
    public boolean sinceInceptionExcessReturnError {get; set;}
    public boolean sinceInceptionTrackingError {get; set;}
    public boolean otherFeeError {get; set;}
    public boolean leverageFormatError {get; set;}
    public boolean timingSizeError {get; set;}
    public boolean otherFeeNarrativeSizeError {get; set;}
    public boolean distNotesSizeError {get; set;}
    public boolean descriptionSizeError {get; set;}
    public boolean addInfoSizeError {get; set;}
    public boolean commodityFuturesError {get; set;}
    public boolean firmCAHeadquartersError {get; set;}
    public boolean separateCoMingledAccountError {get; set;}
    public boolean assetsUnderManagementError {get; set;}
    
    //Phase 2 changes by ML
    public boolean regulatoryBodyError {get; set;}
    public boolean regulatoryBodySizeError {get; set;}
    public boolean registerWithSecError {get; set;}
    public boolean managedAccountsError {get; set;}
    public boolean fullSecurityLevelTransparencyError {get; set;}
    public boolean ftseIndexError {get; set;}
    public boolean securityLevelError {get; set;}
    public boolean regulatorySanctionsError {get; set;}
    public boolean regulatorySanctionsExplanationError {get; set;}
    public boolean regulatorySanctionsExplanationSizeError {get; set;}
    public boolean convictedOfFelonyError {get; set;}
    public boolean convictedOfFelonyExplanationError {get; set;}
    public boolean convictedOfFelonyExplanationSizeError {get; set;}
    
    public boolean targetReturnError {get; set;}
    public boolean targetVolatilityError {get; set;}
    public boolean threeYearAnnualizedReturnError {get; set;}
    public boolean threeYearStandardDeviationError {get; set;}
    public boolean fiveYearAnnualizedReturnError {get; set;}
    public boolean fiveYearStandardDeviationError {get; set;}
    public boolean sinceInceptionAnnualizedReturnError {get; set;}
    public boolean sinceInceptionStandardDeviationError {get; set;}
    
    //Phase 3 changes
    public boolean dealSizeError {get;set;}
    public boolean dealUSDSizeError {get;set;}
    public boolean submittorRoleError {get;set;}
    public boolean propTypeError {get;set;}
    public boolean ownershipPositionError {get;set;}
    public boolean basisOfManagementFeeError {get;set;}
    public boolean preferredReturnRealLeveragedError {get;set;}
    public boolean preferredReturnRealUnleveragedError {get;set;}
    public boolean preferredReturnNominalLeveragedError {get;set;}
    public boolean preferredReturnNominalUnleveragedError {get;set;}
    public boolean minimumFundSizeError {get;set;}
    public boolean targetPortfolioCoEnterpriseValueError {get; set;}
    public boolean howDidYouHearAboutUsError {get; set;}
    public boolean performanceFeeError {get; set;}
    public boolean asOfDateError {get; set;}
    public boolean returnsAsOfDateError {get; set;}
    public boolean returnsAsOfDateFormatError {get; set;}

    public boolean threeYearAnnualizedNetExcessReturnError {get; set;}
    public boolean fiveYearAnnualizedNetExcessReturnError {get; set;}
    public boolean sinceInceptionAnnualizedNetExcessReturnError {get; set;}
    public boolean sinceInceptionNetAnnualizedExcessReturnError {get; set;}
    public boolean averageEquityInvestmentSizeError {get; set;}
    public boolean conciseDescriptionofProposalOpportunityError{get; set;}
    public boolean conciseDescriptionofProposalOpportunitySizeError{get; set;}
    
    //Changes by ATS 10/1/2014
    
    public boolean opportunityMQError{get;set;}
    
    //ASP
    public boolean  CAD_AckError {get;set;}
    public boolean  CADSudanError {get;set;}
    public boolean  CADIranError {get;set;}
    public boolean  CADCongoError {get;set;}
    public boolean  CADFirstNameError {get;set;}
    public boolean  CADLastNameError {get;set;}
    public boolean  CADFirmNameError {get;set;}
    public boolean  CADTitleError {get;set;}
    public boolean  JVSubmittalError {get;set;}
    public boolean  CompanyNameError {get;set;}
    public boolean  ContactNameError {get;set;}
    public boolean  ContactPhoneError {get;set;}
    public boolean  StreetError {get;set;}
    public boolean  CityError {get;set;}
    public boolean  StateError {get;set;}
    public boolean  PostalError {get;set;}
    public boolean  WorkError {get;set;}
    public boolean  AgreeAmtError {get;set;}
    public boolean  CompanyNameError2 {get;set;}
    public boolean  ContactNameError2 {get;set;}
    public boolean  ContactPhoneError2 {get;set;}
    public boolean  StreetError2 {get;set;}
    public boolean  CityError2 {get;set;}
    public boolean  StateError2 {get;set;}
    public boolean  PostalError2 {get;set;}
    public boolean  WorkError2 {get;set;}
    public boolean  AgreeAmtError2 {get;set;}
    public boolean  SubAdvisorError {get;set;}    
    public boolean  CTPDateError {get;set;}  
    public boolean  CTPDateFormatError {get;set;}   
    public boolean  FirstNameError {get;set;}
    public boolean  LastNameError {get;set;}
    public boolean  TitleError {get;set;}
    public boolean  FileNameError {get;set;}
    public boolean  DocNameError {get;set;}

    


}