public with sharing class TermsValidator 
{
	private static Boolean isValidAnticipatedFundSize(Opportunity proposal)
	{
		return (proposal.Anticipated_Fund_Size__c == null) ? false : true;
	}
	private static Boolean isValidAnticipatedFundSize_0(Opportunity proposal)
	{
		return (proposal.Anticipated_Fund_Size__c <= 0 || proposal.Anticipated_Fund_Size__c == null) ? false : true;
	}
    private static Boolean isValidBasisOfManagementFee(Opportunity proposal)
    {
        return (proposal.Basis_of_Management_Fee__c == null) ? false : true;
    }

	private static Boolean isValidManagementFee(Opportunity proposal)
	{
		return (proposal.Management_Fee__c == null) ? false : true;
	}

    private static Boolean isValidMinimumFundSize(Opportunity proposal)
    {
        return (proposal.Minimum_Fund_Size_in_Millions__c == null) ? false : true;
    }

    private static Boolean isValidOwnershipPosition(Opportunity proposal)
    {
        return (proposal.Ownership_Position__c == null) ? false : true;
    }
    
    private static Boolean isValidPreferredReturnRealLeveraged(Opportunity proposal)
    {
        return (proposal.Preferred_Return_Real_Leveraged__c == null) ? false : true;
    }

    private static Boolean isValidPreferredReturnRealUnleveraged(Opportunity proposal)
    {
        return (proposal.Preferred_Return_Real_Unleveraged__c == null) ? false : true;
    }

    private static Boolean isValidPreferredReturnNominalLeveraged(Opportunity proposal)
    {
        return (proposal.Preferred_Return_Nominal_Leveraged__c == null) ? false : true;
    }

    private static Boolean isValidPreferredReturnNominalUnleveraged(Opportunity proposal)
    {
        return (proposal.Preferred_Return_Nominal_Unleveraged__c == null) ? false : true;
    }
	
	private static Boolean isValidGPCarry(Opportunity proposal)
	{
		return (proposal.GP_Carry__c == null) ? false : true;
	}
	
	private static Boolean isValidPreferredReturn(Opportunity proposal)
	{
		return (proposal.Preferred_Return__c == null) ? false : true;
	}
	
	private static Boolean isValidFundTerm(Opportunity proposal)
	{
		return (proposal.Fund_Term_In_Years__c == null) ? false : true;
	}
	
	private static Boolean isValidInvestmentPeriod(Opportunity proposal)
	{
		return (proposal.Investment_Period_in_Years__c == null) ? false : true;
	}
	
	private static Boolean isValidFundSizeCover(Opportunity proposal)
	{
		return (proposal.Fund_Size_Cover_In_Millions__c == null) ? false : true;
	}
	
	private static Boolean isValidFundCap(Opportunity proposal)
	{
		return (proposal.Fund_Cap__c == null) ? false : true;
	}
	
	private static Boolean isValidAverageInvestmentSize(Opportunity proposal)
	{
		return (proposal.Average_Investment_Size__c == null) ? false : true;
	}
	
	private static Boolean isValidExpectedNumberOfInvestments(Opportunity proposal)
	{
		return (proposal.Expected_number_of_Investments__c == null) ? false : true;
	}
	
	private static Boolean isValidExpectedMaxLeverage(Opportunity proposal)
	{
		return (proposal.Expected_Max_Leverage__c == null) ? false : true;
	}
	
	private static Boolean isValidMinimumCommitmentAmount(Opportunity proposal)
	{
		return (proposal.Minimum_Commitment_Amt__c == null) ? false : true;
	}
	
	private static Boolean isValidMinimumAllocationAmount(Opportunity proposal) {
		return (proposal.Minimum_Allocation_Amt__c == null) ? false : true;
	}
	
	private static Boolean isValidCoInvestmentRights(Opportunity proposal)
	{
		return (proposal.Co_investment_rights__c == null) ? false : true;
	}
	
	private static Boolean isValidAdvisoryRights(Opportunity proposal)
	{
		return (proposal.Advisory_Committee_Rights__c == null) ? false : true;
	}
	
	private static Boolean isValidCarryingCharge(Opportunity proposal)
	{
		return (proposal.Carrying_Charge__c == null) ? false : true;
	}
	
	private static Boolean isValidCatchup(Opportunity proposal)
	{
		return (proposal.Catch_up__c == null) ? false : true;
	}
	
	private static Boolean isValidProductCapacity(Opportunity proposal)
	{
		return (proposal.Product_Capacity__c == null) ? false : true;
	}
	
	private static Boolean isValidInitialLockupPeriod(Opportunity proposal)
	{
		return (proposal.Initial_Lock_up_period__c == null) ? false : true;
	}
	
	private static Boolean isValidPropertyLevelLeverage(Opportunity proposal)
	{
		return (proposal.Property_Level_Leverage__c == null) ? false : true;
	}

	private static Boolean isValidDealSize(Opportunity proposal)
	{
		return (proposal.Total_Deal_Size_In_Millions__c == null) ? false : true;
	}

	private static Boolean isValidTargetPortfolioCoEnterpriseValue(Opportunity proposal)
	{
		return (proposal.Target_Portfolio_Co_Enterprise_Value__c == null) ? false : true;
	}

	private static Boolean isValidPerformanceFee(Opportunity proposal)
	{
		return (proposal.Performance_Fee__c == null) ? false : true;
	}

	private static Boolean isValidReturnsAsOfDate(Opportunity proposal)
	{
		return (proposal.Returns_As_of_Date__c == null) ? false : true;
	}

	public static Boolean isValidResponse(String i){
		return (i != null && i.trim().length() > 0) ? true : false;
	}
	public static Boolean isValidResponse(Decimal i){
		return (i == null) ? false : true;
	}
	public static Boolean isValidResponse(Date i){
		return (i == null) ? false : true;
	}		
	public static boolean isValidReturnsAsOfDateString(Opportunity proposal, String returnsAsOfDateString, ErrorHandler ehandler)
	{
		boolean isValid = true;
		if (returnsAsOfDateString != null && returnsAsOfDateString.trim().length() > 0)
		{
			if (Utilities.isValidDateString(returnsAsOfDateString))
			{
				proposal.Returns_As_Of_Date__c = Utilities.getDate(returnsAsOfDateString);
			}
			else
			{
				ehandler.addError('thisPage:thisForm:ReturnsAsOfDate', 'Please identify the As of Date for the Returns provided below', ErrorHandler.INVALID_DATE_FORMAT, ErrorHandler.INVALID_DATE_FORMAT);
				isValid = false;
				ehandler.olog.returnsAsOfDateFormatError = true;
			}
		}
		return isValid;
	}


	public static boolean isValidFirstClosingDateString(Opportunity proposal, String firstCloseDateString, ErrorHandler ehandler)
	{
		boolean isValid = true;
		if (firstCloseDateString != null && firstCloseDateString.trim().length() > 0)
		{
			if (Utilities.isValidDateString(firstCloseDateString))
			{
				proposal.First_close_actual_or_expected__c = Utilities.getDate(firstCloseDateString);
			}
			else
			{
				ehandler.addError('thisPage:thisForm:FirstCloseDate', 'First Close (Actual or Expected)', ErrorHandler.INVALID_DATE_FORMAT, ErrorHandler.INVALID_DATE_FORMAT);
				isValid = false;
				ehandler.olog.firstCloseFormatError = true;
			}
		}
		return isValid;
	}
	
	public static boolean isValidAnticipatedClosingDateString(Opportunity proposal, String anticipatedDateString, ErrorHandler ehandler)
	{
		boolean isValid = true;
		if (anticipatedDateString != null && anticipatedDateString.trim().length() > 0)
		{
			if (Utilities.isValidDateString(anticipatedDateString))
			{
				proposal.Anticipated_Fund_Final_Closing_Date__c = Utilities.getDate(anticipatedDateString);
			}
			else
			{
				ehandler.addError('thisPage:thisForm:Anticipated', 'Anticipated Fund Final Closing Date', ErrorHandler.INVALID_DATE_FORMAT, ErrorHandler.INVALID_DATE_FORMAT);
				isValid = false;
				ehandler.olog.anticipatedFormatError = true;
			}
		}
		return isValid;
	}
	
	public static Boolean isValidAIMPartnershipTerms(Opportunity proposal, String firstCloseDateString, 
		String anticipatedDateString, String fundraisingBeginDate, String subsequentCloseDate, ErrorHandler ehandler)
	{
		boolean isValid = true;
		
		if (!isValidFundSizeCover(proposal))
		{
			ehandler.addError('thisPage:thisForm:FundSizeCover', 'Fund Size - Cover', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.fundSizeCoverError = true;
		}
		if (!isValidFundCap(proposal))
		{
			ehandler.addError('thisPage:thisForm:FundCap', 'Fund Cap', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.fundCapError = true;
		}

        if (fundraisingBeginDate == null || fundraisingBeginDate.trim().length() == 0)
        {
            ehandler.addError('thisPage:thisForm:FundraisingBeginDate', 'Date Fundraising Began', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.olog.fundraisingDateError = true;
        }
        boolean isValidFunraisingBeganDate = opportunityValidator.isValidFundraisingBeginDateString(proposal, fundraisingBeginDate, ehandler);

		if (firstCloseDateString == null || firstCloseDateString.trim().length() == 0)
		{
			ehandler.addError('thisPage:thisForm:FirstCloseDate', 'First Close (Actual or Expected)', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.firstCloseError = true;
		}
		Boolean isValidDate = isValidFirstClosingDateString(proposal, firstCloseDateString, ehandler);
		
        if (subsequentCloseDate == null || subsequentCloseDate.trim().length() == 0)
        {
            ehandler.addError('thisPage:thisForm:SubsequentCloseDate', 'Subsequent Close Date', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.olog.subsequentCloseDateError = true;
        }
        boolean isValidSubsequent = opportunityValidator.isValidSubsequentCloseDateString(proposal, subsequentCloseDate, ehandler);

		if (anticipatedDateString == null || anticipatedDateString.trim().length() == 0)
		{
			ehandler.addError('thisPage:thisForm:Anticipated', 'Anticipated Fund Final Closing Date', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.anticipatedError = true;
		}
		Boolean isValidAntDate = isValidAnticipatedClosingDateString(proposal, anticipatedDateString, ehandler);

		if (!isValidAverageInvestmentSize(proposal))
		{
			ehandler.addError('thisPage:thisForm:AverageEquityInvestmentSize', 'Average Equity Investment Size', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.averageEquityInvestmentSizeError = true;
		}
		if (!isValidTargetPortfolioCoEnterpriseValue(proposal))
		{
			ehandler.addError('thisPage:thisForm:TargetPortfolioCoEnterpriseValue', 'Target Portfolio Company Enterprise Value', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.targetPortfolioCoEnterpriseValueError = true;
		}
		if (!isValidExpectedNumberOfInvestments(proposal))
		{
			ehandler.addError('thisPage:thisForm:ExpectedInvestmentSize', 'Expected Number of Investments', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.expectedNumberOfInvestmentsError = true;
		}
		
        if (!opportunityValidator.isValidDealSize(proposal))
        {
            ehandler.addError('thisPage:thisForm:DealSize', 'How many deals do you see a year?', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.olog.numberOfDealsError = true;
        }
       
        if (!opportunityValidator.isValidProprietary(proposal))
        {
            ehandler.addError('thisPage:thisForm:Proprietary', 'Proprietary (No intermediary)', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
            isValid = false;
            ehandler.olog.proprietaryError = true;
        }

		if (!isValidManagementFee(proposal))
		{
			ehandler.addError('thisPage:thisForm:ManagementFee', 'Management Fee', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.managementFeeError = true;
		}
		if (!isValidGPCarry(proposal))
		{
			ehandler.addError('thisPage:thisForm:GPCarry', 'GP Carry', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.gpCarryError = true;
		}
		if (!isValidPreferredReturn(proposal))
		{
			ehandler.addError('thisPage:thisForm:PreferredReturn', 'Preferred Return', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.preferredReturnError = true;
		}
		if (proposal.GP_commitment_percent__c == null && proposal.GP_Commitment_Amount_In_Millions__c == null)
		{
			ehandler.addError('thisPage:thisForm:GPCommitmentPercent', 'GP Commitment Percent', 'Required if Amount not given', 'Required if Amount not given');
			ehandler.olog.gpCommitmentPercentError = true;
			ehandler.addError('thisPage:thisForm:GPCommitmentAmount', 'GP Commitment Amount', 'Required if % not given', 'Required if % not given');
			ehandler.olog.gpCommitmentAmountError = true;
			isValid = false;
		}
		

		return isValid && isValidDate && isValidAntDate && isValidFunraisingBeganDate && isValidSubsequent;		
	}
	
	public static Boolean isValidForestlandTerms(Opportunity proposal, String anticipatedDateString, ErrorHandler ehandler)
	{
		boolean isValid = true;

		if (!isValidManagementFee(proposal))
		{
			ehandler.addError('thisPage:thisForm:ManagementFee', 'Management Fee', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.managementFeeError = true;
		}
		
		if (proposal.Other_Fee_Narrative__c != null && proposal.Other_Fee_Narrative__c.trim().length() > 1000)
		{
			String msg = 'Field has ' + proposal.Other_Fee_Narrative__c.length() + ' characters. Only 1000 allowed.';
			ehandler.addError('thisPage:thisForm:OtherFee', 'Other Fee Narrative', msg, msg);
			isValid = false;
			ehandler.olog.otherFeeNarrativeSizeError = true; 
		}
		
		if (!isValidExpectedMaxLeverage(proposal))
		{
			ehandler.addError('thisPage:thisForm:ExpectedMaxLeverage', 'Expected Max Leverage', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.expectedMaxLeverageError = true;
		}
		
		if (proposal.Distribution_Notes_waterfall__c != null && proposal.Distribution_Notes_waterfall__c.trim().length() > 1000)
		{
			String msg = 'Field has ' + proposal.Distribution_Notes_waterfall__c.length() + ' characters. Only 1000 allowed.';
			ehandler.addError('thisPage:thisForm:Distribution', 'Distribution Notes (waterfall)', msg, msg);
			isValid = false;
			ehandler.olog.distNotesSizeError = true; 
		}
		
		if (!isValidMinimumCommitmentAmount(proposal))
		{
			ehandler.addError('thisPage:thisForm:MinCommitmentAmount', 'Minimum Commitment Amount', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.minimumCommitmentAmountError = true;
		}
		
		if (!isValidCoInvestmentRights(proposal))
		{
			ehandler.addError('thisPage:thisForm:CoInvestRights:0', 'Co-Investment Rights', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.coInvestRightsError = true;
		}
		if (anticipatedDateString == null || anticipatedDateString.trim().length() == 0)
		{
			ehandler.addError('thisPage:thisForm:Anticipated', 'Anticipated Fund Final Closing Date', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.anticipatedError = true;
		}
		Boolean isValidAntDate = isValidAnticipatedClosingDateString(proposal, anticipatedDateString, ehandler);
		if (proposal.GP_commitment_percent__c == null && proposal.GP_Commitment_Amount_In_Millions__c == null)
		{
			ehandler.addError('thisPage:thisForm:GPCommitmentPercent', 'GP Commitment Percent', 'Required if Amount not given', 'Required if Amount not given');
			ehandler.olog.gpCommitmentPercentError = true;
			ehandler.addError('thisPage:thisForm:GPCommitmentAmount', 'GP Commitment Amount', 'Required if % not given', 'Required if % not given');
			ehandler.olog.gpCommitmentAmountError = true;
			isValid = false;
		}
		return isValid && isValidAntDate;
	}
	
	public static boolean isValidInfrastructureDirectTerms(Opportunity proposal, String anticipatedDateString, ErrorHandler ehandler){
		boolean isValid = true;
		if (proposal.Other_Fee_Narrative__c == null || proposal.Other_Fee_Narrative__c.trim().length() == 0)
		{
			ehandler.addError('thisPage:thisForm:OtherFee', 'Please Describe Fees', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.otherFeeError = true;
		}
		else if (proposal.Other_Fee_Narrative__c.trim().length() > 1000)
		{
			String msg = 'Field has ' + proposal.Other_Fee_Narrative__c.length() + ' characters. Only 1000 allowed.';
			ehandler.addError('thisPage:thisForm:OtherFee', 'Please Describe Fees, if any', msg, msg);
			isValid = false;
			ehandler.olog.otherFeeNarrativeSizeError = true; 
		}
		if (!isValidPreferredReturn(proposal))
		{
			ehandler.addError('thisPage:thisForm:PreferredReturn', 'Target Return', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.preferredReturnError = true;
		}
		if (anticipatedDateString == null || anticipatedDateString.trim().length() == 0){
			ehandler.addError('thisPage:thisForm:AnticipatedFinalClosingDate', 'Anticipated Final Closing Date', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.anticipatedError = true;
		} else 
		if (Utilities.isValidDateString(anticipatedDateString)){
			proposal.Anticipated_Fund_Final_Closing_Date__c = Utilities.getDate(anticipatedDateString);
		} else {
			ehandler.addError('thisPage:thisForm:AnticipatedFinalClosingDate', 'Anticipated Final Closing Date', ErrorHandler.INVALID_DATE_FORMAT, ErrorHandler.INVALID_DATE_FORMAT);
			isValid = false;
			ehandler.olog.anticipatedFormatError = true;
		}

		if (proposal.Leverage__c != null && proposal.Leverage__c >= 100)
		{
			ehandler.addError('thisPage:thisForm:Leverage', 'Leverage', 'Leverage must be less than 100%', 'Leverage must be less than 100%');
			isValid = false;
			ehandler.olog.leverageFormatError = true;
		}
		if (proposal.Timing_Considerations__c != null && proposal.Timing_Considerations__c.length() > 1000)
		{
			String msg = 'Field has ' + proposal.Timing_Considerations__c.length() + ' characters. Only 1000 allowed.';
			ehandler.addError('thisPage:thisForm:AdditionalInfo', 'Additional Information', msg, msg);
			isValid = false;
			ehandler.olog.timingSizeError = true;
		}
		return isValid;
	}
	
	public static boolean isValidAimDirectTerms(Opportunity proposal, ErrorHandler ehandler)
	{
		boolean isValid = true;
		if (proposal.Timing_Considerations__c != null && proposal.Timing_Considerations__c.length() > 1000)
		{
			String msg = 'Field has ' + proposal.Timing_Considerations__c.length() + ' characters. Only 1000 allowed.';
			ehandler.addError('thisPage:thisForm:TimingConsiderations', 'Timing Considerations', msg, msg);
			isValid = false;
			ehandler.olog.timingSizeError = true;
		}

		return isValid;
	}
	
	public static Boolean isValidInfrastructurePartnershipTerms(Opportunity proposal, String firstCloseDateString, String anticipatedDateString, ErrorHandler ehandler)
	{
		boolean isValid = true;
		if (!isValidManagementFee(proposal))
		{
			ehandler.addError('thisPage:thisForm:ManagementFee', 'Management Fee', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.managementFeeError = true;
		}
		if (!isValidGPCarry(proposal))
		{
			ehandler.addError('thisPage:thisForm:GPCarry', 'GP Carry', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.gpCarryError = true;
		}
		if (!isValidPreferredReturn(proposal))
		{
			ehandler.addError('thisPage:thisForm:PreferredReturn', 'Preferred Return', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.preferredReturnError = true;
		}
		
		if (proposal.Infrastructure_Catch_Up__c == null)
		{
			ehandler.addError('thisPage:thisForm:Catchup:0', 'Catchup', ErrorHandler.SELECT_MSG, ErrorHandler.SELECT_MSG);
			isValid = false;
			ehandler.olog.catchupError = true;
		}
		
		if (proposal.Other_Fee_Narrative__c != null && proposal.Other_Fee_Narrative__c.trim().length() > 1000)
		{
			String msg = 'Field has ' + proposal.Other_Fee_Narrative__c.length() + ' characters. Only 1000 allowed.';
			ehandler.addError('thisPage:thisForm:OtherFeeNarrative', 'Other Fee Narrative', msg, msg);
			isValid = false;
			ehandler.olog.otherFeeNarrativeSizeError = true; 
		}
		
		if (!isValidFundTerm(proposal))
		{
			ehandler.addError('thisPage:thisForm:FundTerm', 'Fund Term', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.fundTermError = true;
		}
		if (!isValidInvestmentPeriod(proposal))
		{
			ehandler.addError('thisPage:thisForm:InvestmentPeriod', 'Investment Period', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.investmentPeriodError = true;
		}
		
		if (proposal.GP_commitment_percent__c == null)
		{
			ehandler.addError('thisPage:thisForm:GPCommitmentPercent', 'GP Commitment Percent', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			ehandler.olog.gpCommitmentPercentError = true;
			isValid = false;
		}
		
		if (!isValidCoInvestmentRights(proposal))
		{
			ehandler.addError('thisPage:thisForm:CoInvestRights:0', 'Co-Investment Rights', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.coInvestRightsError = true;
		}
		if (!isValidAdvisoryRights(proposal))
		{
			ehandler.addError('thisPage:thisForm:AdvisoryRights:0', 'Advisory Committee Rights', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.advisoryRightsError = true;
		}
		
		if (firstCloseDateString == null || firstCloseDateString.trim().length() == 0)
		{
			ehandler.addError('thisPage:thisForm:FirstCloseDate', 'First Close (Actual or Expected)', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.firstCloseError = true;
		}
		Boolean isValidDate = isValidFirstClosingDateString(proposal, firstCloseDateString, ehandler);
		
		if (anticipatedDateString == null || anticipatedDateString.trim().length() == 0)
		{
			ehandler.addError('thisPage:thisForm:Anticipated', 'Anticipated Fund Final Closing Date', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.anticipatedError = true;
		}
		Boolean isValidAntDate = isValidAnticipatedClosingDateString(proposal, anticipatedDateString, ehandler);
					
		if (!isValidFundCap(proposal))
		{
			ehandler.addError('thisPage:thisForm:FundCap', 'Fund Cap', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.fundCapError = true;
		}
		if (!isValidAverageInvestmentSize(proposal))
		{
			ehandler.addError('thisPage:thisForm:AverageInvestmentSize', 'Average Investment Size', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.averageInvestmentSizeError = true;
		}
		
		if (!isValidExpectedNumberOfInvestments(proposal))
		{
			ehandler.addError('thisPage:thisForm:ExpectedInvestmentSize', 'Expected Number of Investments', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.expectedNumberOfInvestmentsError = true;
		}
		
		return isValid && isValidDate && isValidAntDate;
	}
	
	public static Boolean isValidInvestmentTerms(Opportunity proposal, ErrorHandler ehandler)
	{
		boolean isValid = true;
		if (!isValidProductCapacity(proposal))
		{
			ehandler.addError('thisPage:thisForm:ProductCapacity', 'Product Capacity', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.productCapacityError = true;
		}
		if (!isValidMinimumCommitmentAmount(proposal))
		{
			ehandler.addError('thisPage:thisForm:MinCommitmentAmount', 'Minimum Commitment Amount', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.minimumCommitmentAmountError = true;
		}
		if (!isValidInitialLockupPeriod(proposal))
		{
			ehandler.addError('thisPage:thisForm:InitialLockupPeriod', 'Initial Lock up Period', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.initialLockupPeriodError = true;
		}
		if (proposal.Other_Fee_Narrative__c != null && proposal.Other_Fee_Narrative__c.trim().length() > 1000)
		{
			String msg = 'Field has ' + proposal.Other_Fee_Narrative__c.length() + ' characters. Only 1000 allowed.';
			ehandler.addError('thisPage:thisForm:OtherFee', 'Other Fee Narrative', msg, msg);
			isValid = false;
			ehandler.olog.otherFeeNarrativeSizeError = true; 
		}
		return isValid;
	}
	
	public static Boolean isValidRealEstateInvestmentTerms(Opportunity proposal, String anticipatedDateString, String returnsAsOfDate, ErrorHandler ehandler)
	{
		boolean isValid = true;
		if ( returnsAsOfDate != null && returnsAsOfDate.trim().length() > 0)
		{
			if (Utilities.isValidDateString(returnsAsOfDate))
			{
				proposal.Returns_As_Of_Date__c = Utilities.getDate(returnsAsOfDate);
			}
			else
			{
				ehandler.addError('thisPage:thisForm:ReturnsAsOfDate', 'Please identify the As of Date for the Returns provided below', ErrorHandler.INVALID_DATE_FORMAT, ErrorHandler.INVALID_DATE_FORMAT);
				isValid = false;
				ehandler.olog.returnsAsOfDateFormatError = true;
			}
		} else
		if (!isValidReturnsAsOfDate(proposal))
		{
			ehandler.addError('thisPage:thisForm:ReturnsAsOfDate', 'Please identify the As of Date for the Returns provided below', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.returnsAsOfDateError = true;
		}
		if (!isValidManagementFee(proposal))
		{
			ehandler.addError('thisPage:thisForm:ManagementFee', 'Management Fee', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.managementFeeError = true;
		}
		if (!isValidExpectedMaxLeverage(proposal))
		{
			ehandler.addError('thisPage:thisForm:ExpectedMaxLeverage', 'Expected Max Leverage', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.expectedMaxLeverageError = true;
		}
		if (!isValidPreferredReturn(proposal))
		{
			ehandler.addError('thisPage:thisForm:PreferredReturn', 'Preferred Return', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.preferredReturnError = true;
		}
		if (anticipatedDateString == null || anticipatedDateString.trim().length() == 0)
		{
			ehandler.addError('thisPage:thisForm:Anticipated', 'Anticipated Fund Final Closing Date', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.anticipatedError = true;
		}
		Boolean isValidAntDate = isValidAnticipatedClosingDateString(proposal, anticipatedDateString, ehandler);
		if (!isValidAnticipatedFundSize_0(proposal))
		{
			ehandler.addError('thisPage:thisForm:AnticipatedFundSize', 'Anticipated Fund Size in USD', ErrorHandler.SIZE_GREATER_THAN_0, ErrorHandler.SIZE_GREATER_THAN_0);
			isValid = false;
			ehandler.olog.anticaptedFundSizeError = true;
		}
		if (!isValidOwnershipPosition(proposal))
		{
			ehandler.addError('thisPage:thisForm:OwnershipPosition', 'Ownership Position', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.ownershipPositionError = true;
		}
		if (!isValidBasisOfManagementFee(proposal))
		{
			ehandler.addError('thisPage:thisForm:BasisOfManagementFee', 'What is the basis for the Fund\'s Management Fee', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.basisOfManagementFeeError = true;
		}
		if (!isValidPreferredReturnRealLeveraged(proposal))
		{
			ehandler.addError('thisPage:thisForm:PreferredReturnRealLeveraged', 'Preferred Return - Real (Percentage) Leveraged', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.preferredReturnRealLeveragedError = true;
		}
		if (!isValidPreferredReturnNominalLeveraged(proposal))
		{
			ehandler.addError('thisPage:thisForm:PreferredReturnNominalLeveraged', 'Preferred Return - Nominal (Percentage) Leveraged', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.preferredReturnNominalLeveragedError = true;
		}
		if (!isValidPreferredReturnRealUnleveraged(proposal))
		{
			ehandler.addError('thisPage:thisForm:PreferredReturnRealUnleveraged', 'Preferred Return - Real (Percentage) Unleveraged', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.preferredReturnRealUnleveragedError = true;
		}
		if (!isValidPreferredReturnNominalUnleveraged(proposal))
		{
			ehandler.addError('thisPage:thisForm:PreferredReturnNominalUnleveraged', 'Preferred Return - Nominal (Percentage) Unleveraged', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.preferredReturnNominalUnleveragedError = true;
		}
		if (!isValidMinimumFundSize(proposal))
		{
			ehandler.addError('thisPage:thisForm:MinimumFundSize', 'Minimum Fund Size', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.minimumFundSizeError = true;
		}

//		if (proposal.Other_Fee_Narrative__c != null && proposal.Other_Fee_Narrative__c.trim().length() > 1000)
//		{
//			String msg = 'Field has ' + proposal.Other_Fee_Narrative__c.length() + ' characters. Only 1000 allowed.';
//			ehandler.addError('thisPage:thisForm:OtherFee', 'Other Fee Narrative', msg, msg);
//			isValid = false;
//			ehandler.olog.otherFeeNarrativeSizeError = true; 
//		}
//		if (!isValidPropertyLevelLeverage(proposal))
//		{
//			ehandler.addError('thisPage:thisForm:PropertyLevelLeverage', 'Property Level Leverage', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
//			isValid = false;
//			ehandler.olog.propertyLevelLeverageError = true;
//		}
//		if (proposal.Distribution_Notes_waterfall__c != null && proposal.Distribution_Notes_waterfall__c.trim().length() > 1000)
//		{
//			String msg = 'Field has ' + proposal.Distribution_Notes_waterfall__c.length() + ' characters. Only 1000 allowed.';
//			ehandler.addError('thisPage:thisForm:Distribution', 'Distribution Notes (waterfall)', msg, msg);
//			isValid = false;
//			ehandler.olog.distNotesSizeError = true; 
//		}
		

		return isValid && isValidAntDate ;
	}
	
	public static Boolean isValidHedgeFundsInvestmentTerms(Opportunity proposal, ErrorHandler ehandler) {
		boolean isValid = true;
		if (!isValidInitialLockUpPeriod(proposal)) {
			ehandler.addError('thisPage:thisForm:InitialLockUpPeriod', 'Initial Lock Up Period (In Years)', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.initialLockUpPeriodError = true;
		}
		if (!isValidPerformanceFee(proposal)) {
			ehandler.addError('thisPage:thisForm:PerformanceFee', 'Performance Fee (Percentage)', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.performanceFeeError = true;
		}


		if (!isValidProductCapacity(proposal)) {
			ehandler.addError('thisPage:thisForm:ProductCapacity', 'Product Capacity', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.productCapacityError = true;
		}
		if (!isValidMinimumAllocationAmount(proposal)) {
			ehandler.addError('thisPage:thisForm:MinAllocationAmount', 'Minimum Allocation Amount', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.minimumAllocationAmountError = true;
		}
		if (!isValidResponse(proposal.Management_Fee__c)) {
			ehandler.addError('thisPage:thisForm:ManagementFee', 'Management Fee (Percentage)', ErrorHandler.INPUT_MSG, ErrorHandler.INPUT_MSG);
			isValid = false;
			ehandler.olog.managementFeeError = true;
		}


		if (proposal.Other_Fee_Narrative__c != null && proposal.Other_Fee_Narrative__c.trim().length() > 1000) {
			String msg = 'Field has ' + proposal.Other_Fee_Narrative__c.length() + ' characters. Only 1000 allowed.';
			ehandler.addError('thisPage:thisForm:OtherFee', 'Other Fee Narrative', msg, msg);
			isValid = false;
			ehandler.olog.otherFeeNarrativeSizeError = true; 
		}
		return isValid;
	}
	
	static testmethod void testMethods()
	{
		Test.startTest();
		Opportunity proposal = new Opportunity();
		TermsValidator.isValidAnticipatedFundSize(proposal);
		TermsValidator.isValidAnticipatedFundSize_0(proposal);
		TermsValidator.isValidBasisOfManagementFee(proposal);
		TermsValidator.isValidMinimumFundSize(proposal);
		TermsValidator.isValidOwnershipPosition(proposal);
		TermsValidator.isValidPreferredReturnRealLeveraged(proposal);
		TermsValidator.isValidPreferredReturnRealUnleveraged(proposal);
		TermsValidator.isValidPreferredReturnNominalLeveraged(proposal);
		TermsValidator.isValidPreferredReturnNominalUnleveraged(proposal);
		TermsValidator.isValidGPCarry(proposal);
		TermsValidator.isValidPreferredReturn(proposal);
		TermsValidator.isValidFundTerm(proposal);
		TermsValidator.isValidInvestmentPeriod(proposal);
		TermsValidator.isValidFundSizeCover(proposal);
		TermsValidator.isValidFundCap(proposal);
		TermsValidator.isValidAverageInvestmentSize(proposal);
		TermsValidator.isValidExpectedNumberOfInvestments(proposal);
		TermsValidator.isValidExpectedMaxLeverage(proposal);
		TermsValidator.isValidMinimumCommitmentAmount(proposal);
		TermsValidator.isValidMinimumAllocationAmount(proposal);
		TermsValidator.isValidCoInvestmentRights(proposal);
		TermsValidator.isValidAdvisoryRights(proposal);
		TermsValidator.isValidCarryingCharge(proposal);
		TermsValidator.isValidCatchup(proposal);
		TermsValidator.isValidProductCapacity(proposal);
		TermsValidator.isValidInitialLockupPeriod(proposal);
		TermsValidator.isValidPropertyLevelLeverage(proposal);
		TermsValidator.isValidDealSize(proposal);
		TermsValidator.isValidTargetPortfolioCoEnterpriseValue(proposal);
		TermsValidator.isValidPerformanceFee(proposal);
		TermsValidator.isValidReturnsAsOfDate(proposal);
	
		
		TermsValidator.isValidForestlandTerms(proposal, '01/01/2001', new ErrorHandler());
		TermsValidator.isValidInfrastructureDirectTerms(proposal, '01/01/2001', new ErrorHandler());
		TermsValidator.isValidInfrastructurePartnershipTerms(proposal, '01/01/2001', '01/01/2001', new ErrorHandler());
		TermsValidator.isValidAIMDirectTerms(proposal,  new ErrorHandler());
		TermsValidator.isValidInvestmentTerms(proposal,  new ErrorHandler());
		TermsValidator.isValidRealEstateInvestmentTerms(proposal, '01/01/2001', '01/01/2001', new ErrorHandler());
		TermsValidator.isValidHedgeFundsInvestmentTerms(proposal,  new ErrorHandler());
		Test.stopTest();
	}
}