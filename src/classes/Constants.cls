public with sharing class Constants {

    public static Map<String, Schema.SObjectField> OPPORTUNITY_SCHEMA_MAP = Schema.SObjectType.Opportunity.fields.getMap();
    public static Map<String, Schema.SObjectField> ACCOUNT_SCHEMA_MAP = Schema.SObjectType.Account.fields.getMap();
    public static Map<String, Schema.SObjectField> CONTACT_SCHEMA_MAP = Schema.SObjectType.Contact.fields.getMap();
    public static Set<String> CONTACT_FIELD_SET = new Set<String>{'salutation', 'firstname', 'lastname', 'email', 'phone', 'fax', 'title'};


    //Asset class constants
    public static final String AIM = 'Private Equity';
    public static final String REAL_ESTATE = 'Real Estate';
    public static final String FORESTLAND = 'Forestland';
    public static final String INFRASTRUCTURE = 'Infrastructure';
    public static final String GLOBAL_EQUITY = 'Global Equities';
    public static final String GLOBAL_FIXED_INCOME = 'Global Fixed Income';
    public static final String COMMODITY = 'Commodities';
    public static final String HEDGE_FUNDS = 'Absolute Return Strategies';
    
    //Document Folder Name constants
    public static final String AIM_DocumentFolder = 'AIM';
    public static final String REAL_ESTATE_DocumentFolder = 'Real Estate';
    public static final String FORESTLAND_DocumentFolder = 'Forestland';
    public static final String INFRASTRUCTURE_DocumentFolder = 'Infrastructure';
    public static final String GLOBAL_EQUITY_DocumentFolder = 'Global Equity';
    public static final String GLOBAL_FIXED_INCOME_DocumentFolder = 'Fixed Income';
    public static final String COMMODITY_DocumentFolder = 'Commodities';
    public static final String HEDGE_FUNDS_DocumentFolder = 'Hedge Funds';
    public static final String genericDocumentFolder = 'Investment Submission Attachments';
    
    //Boolean constants
    public static final String YES = 'Yes';
    public static final String NO = 'No';
    public static final String NA = 'N/A';
    
    //Investment type constants
    public static final String PARTNERSHIP = 'Partnership';
    public static final String DIRECT_COINVEST = 'Direct';
    public static final String AIM_DIRECT_COINVEST = 'Direct / Co-Invest';
    public static final String INFRASTRUCTURE_DIRECT = 'Direct';
    //Constants
    public static final String NOT_AGENT_NOT_WORKING_WITH_AGENT_LABEL = 'I am neither a Placement Agent nor working with one';
    public static final String NOT_AGENT_NOT_WORKING_WITH_AGENT_VALUE = 'No placement agent involved';
    public static final String WORKING_AGENT_VALUE = 'I am working with a Placement Agent';
    public static final String REFERRAL_VALUE = 'I was referred by someone';
    public static final String AGENT_VALUE = 'I am a Placement Agent';
    public static final String OTHER_VALUE = 'Other';
    public static final String WORKING_AGENT_LABEL = 'I am working with a Placement Agent';
    public static final String REFERRAL_LABEL = 'I was referred by someone';
    public static final String AGENT_LABEL = 'I am a Placement Agent';
    public static final String OTHER_LABEL = 'Other';
    
    public static final String NO_RESPONSE_PROVIDED = 'No Response Provided';


    //Generated with: encodingUtil.base64Encode(Crypto.generateAesKey(256));
    public static String ENCODING_KEY = '9IpAV/16FoEOh0QKuK15/tkYhDIq1u4Z7GzNY4dfW2c=';
    
    //Generated with: encodingUtil.base64Encode(Crypto.generateAesKey(128));
    public static String ENCODING_IV = 'pViLq3fa/DHZ9RuCIsZtxg==';
    
    //ASP
    public static final String FREEFORM = 'Free Form';
    public static final String CUSTOM = 'Custom';
    public static final String CUSTOM_ACK = 'Custom Acknowledge';
    public static final String PROPOSAL = 'Proposal';
    public static final String COMPLIANCE = 'Compliance';
    public static final String FREEFORM_ACK = 'Free Form Acknowledge';
    public static final String RADIO_ATTACH = 'Radio Attachment';
    public static final String ASP_FirmInformation = 'ASP_FirmInformation';
    public static final String ASP_Diversity = 'ASP_Diversity';
    //public static final String ASP_GlobalEquity_OpportunityInfo = 'ASP_GlobalEquity_OpportunityInfo';
    public static final String ASP_Attachments = 'ASP_Attachments';
    public static final String ASP_Review = 'ASP_Review';
    public static final String ASP_California_Tax_Payer= 'ASP_California_Tax_Payer';
    public static final String ASP_Contract_Act_Declarations= 'ASP_Contract_Act_Declarations';
    public static final String ASP_Joint_Venture= 'ASP_Joint_Venture';
    public static final String ASP_Placement_Agent= 'ASP_Placement_Agent';
    public static final String ASP_Sub_Advisors= 'ASP_Sub_Advisors';
    public static final String ASP_Acknowledgement= 'ASP_Acknowledgement';
    
    public static final String ContractAct1a = 'This firm does not currently have, and has not had within the previous three years, business activities or other operations outside of the United States, and in accordance with Public Contract Code section 10478, this firm hereby certifies it is not a “scrutinized company” (as defined in Public Contract Code section 10476).';
    public static final String ContractAct1b = 'This firm is a “scrutinized company” (as defined in Public Contract Code section 10476), but has received written permission from the Department of General Services (DGS) to submit a bid or proposal pursuant to Public Contract Code section 10477(b).  A copy of the written permission from DGS is included with the firm’s bid or proposal.';
    public static final String ContractAct1c = 'This firm currently has, or has had within the previous three years, business activities or other operations outside of the United States, but hereby certifies that is not a “scrutinized company” due to specific activities in Sudan (as defined in Public Contract Code section 10476).';
    public static final String ContractAct2a = 'This firm, or person, hereby certifies that it (or he or she) is not identified on the DGS list created pursuant to Public Contract Code section 2203(b) as a firm or person engaging in “investment activities” in Iran described in Public Contract Code section 2202.5(a) or 2202.5(b).';
    public static final String ContractAct2b = 'The firm hereby certifies it is permitted to submit a bid or proposal to a public agency pursuant to Public Contract Code section 2203(c) because its investment activities in Iran were (1) made before July 1, 2010, (2) were not expanded or renewed after July 1, 2010, and (3) the firm has adopted, publicized, and is implementing a formal plan to cease the investment activities in Iran and to refrain from engaging in any new investments in Iran.';
    public static final String ContractAct2c = 'This firm hereby certifies that although it is a financial institution that extends twenty million dollars ($20,000,000) or more in credit to another person, for 45 days or more, the person who will use the credit to provide goods or services in the energy sector in Iran is a person permitted to submit a bid or proposal to the public agency pursuant to Public Contract Code section 2203(c).  (See Public Contract Code section 2202.3(d)).';
    public static final String ContractAct3a = 'This firm is not a “scrutinized company” or does not currently use any conflict mineral products that are necessary to the functionality or production of a product manufactured by the firm from the Democratic Public of the Congo or an “adjoining country” (see 15 U.S.C.A § 78(m)(p)), and section 1502 of the Dodd-Frank Wall Street Reform and Consumer Protection Act (Pub.L. No.111-203, (July 21, 2010) 124 Stat. 1376)).';
    public static final String ContractAct3b = 'This firm has ceased to be regarded as a “scrutinized company” (as defined by Cal. Public Contract Code section 10490(b)) because (1) it is not in violation of section 13(p) of the Securities Exchange Act of 1934 (15 U.S.C.A. § 78(m)(p)); or (2) it has filed an amended or corrective filing under section 13(p) of the Securities Exchange Act of 1934, which filing corrected the violations; or (3) three years have passed since the date of the by final judgment or settlement entered in a civil or administrative action brought by the Securities and Exchange Commission (SEC).';
    public static final String ContractAct3c = 'Disclosure requirements have been temporarily waived by the SEC because the President of the United States has transmitted a determination that the waiver or revision is in the national security interest of the United States, or the disclosure requirements have been terminated because the President has determined and certified to the appropriate congressional committees that no armed groups continue to be directly involved and benefitting from this firm’s commercial activity involving conflict minerals.';

}