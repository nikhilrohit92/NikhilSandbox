public with sharing class PlacementAgentFirms_Extension {

	class placementFirmWrapper{
		public Placement_Firm__c firm {get;set;}
		public List<Placement_Agent__c> agents {get;set;}
	}
	public Disclosure_Form__c df {get;set;}
	public String thisPFId {get;set;} 
	public String thisPFName {get;set;} 
	public List<placementFirmWrapper> placementFirms {get;set;}

    public PlacementAgentFirms_Extension(ApexPages.StandardController stdController) {
		placementFirms = new List<placementFirmWrapper>();
        this.df = (Disclosure_Form__c)stdController.getRecord();

		for (Placement_Firm__c f : [Select Id, Name, Currency__c, Placement_Agent_Fee__c, (Select Id, Name, Disclosure_Form__c, First_Name__c, Last_Name__c, Relationship__c, Placement_Agent_Firm_Name__c, Affiliated_with_CalPERS__c, CalPERS_Affiliation_Explanation__c, Registered_with_SEC_FINRA__c, Registered_as_a_Lobbyist__c, Placement_Firm__c From Placement_Agents__r) From Placement_Firm__c where Disclosure_Form__c = :df.Id ]){
			placementFirmWrapper pfw = new placementFirmWrapper();
			pfw.firm = f;
			pfw.agents = f.Placement_Agents__r;
			placementFirms.add(pfw);
		}

    }

	public pageReference addFirm() {
		PageReference p = new PageReference('/a09/e?CF00N80000004GMyt=' + df.Name + '&CF00N80000004GMyt_lkid=' + df.Id + '&retURL=%2F' + df.Id + '&saveURL=' + df.Id);
		p.setRedirect(true);
		return p;
	}

	public pageReference addAgent() {
		PageReference p = new PageReference('/a08/e?CF00N80000004GJWg=' + df.Name + '&CF00N80000004GJWg_lkid=' + df.Id + '&retURL=%2F' + df.Id + '&saveURL=%2F' + df.Id + '&CF00N80000004GN1O=' + thisPFName + '&CF00N80000004GN1O_lkid=' + thisPFId);
		p.setRedirect(true);
		return p;
	}

}