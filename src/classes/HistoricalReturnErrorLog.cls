public with sharing class HistoricalReturnErrorLog 
{
    public boolean nameError {get; set;}
    public boolean sizeError {get; set;}
    public boolean vintageYearError {get; set;}
    public boolean numberOfPortfolioError {get; set;}
    public boolean committedCapitalError {get; set;}
    public boolean contributedCapitalError {get; set;}
    public boolean investedCapitalError {get; set;}
    public boolean distributedValueError {get; set;}
    public boolean unrealizedValueError {get; set;}
    public boolean totalValueError {get; set;}
    public boolean realizedValueError {get; set;}
    public boolean grossMultipleError {get; set;}
    public boolean netMultipleError {get; set;}
    public boolean grossIrrError {get; set;}
    public boolean netIrrError {get; set;}
    public boolean netDPIError {get; set;} 
    public boolean explReturnsSizeError {get; set;}
    public boolean asOfDateError {get; set;}
    public boolean asOfDateFormatError {get; set;}
    public boolean asOfDateQuarterError {get; set;}
    public boolean asOfDateQuarterError2 {get; set;}    
    public boolean investmentVehicleError {get; set;}
    public boolean propertyTypeError {get; set;}
    public boolean startDateOfTrackRecordError {get; set;}
    public boolean startDateOfTrackRecordFormatError {get; set;}
}