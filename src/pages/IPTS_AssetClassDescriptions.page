<apex:page cache="false" name="IPTS_AssetClassDescriptions" id="thisPage" controller="InvestmentSubmissionController" title="Asset Class Descriptions" showHeader="false" sidebar="false" standardStylesheets="false">
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_Styles, '/ipts/css/reset.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_Styles, '/ipts/css/style.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_CustomStyles)}"/>
    <script src="{!$Resource.IPTS_Script}" type="text/javascript"/>
    <body>
        <div id="mainContent">
            <h1>Investment Proposal</h1>
                     <h2>Asset Class Descriptions</h2>
            <p>On the next page, you will select the asset class that best identifies your proposal.  We classify assets into seven categories: Commodities, Forestland,  Global Equities, Global Fixed Income, Infrastructure, Private Equity, and Real Estate.  The current allocations and market value for each Asset Class can be found at the following link:  <a href="http://www.calpers.ca.gov/index.jsp?bc=/investments/assets/assetallocation.xml" title="Asset Allocations" target="_blank">Asset Allocations <span class="newwindow" style="display: none;">opens in a new window</span><apex:image id="newWindowIcon1" value="{!$Resource.NewWindowIcon}" alt="opens in a new window"/></a>.</p>
            <p>A portion of this investment program may be managed externally by specialized investment managers.  External Managers typically operate one or more fund-of-funds or similar vehicles where they place capital with a focus on a particular strategy, geography, or industry section.  Additionally, several managers invest in companies or assets directly.  Please refer to the following link, as this may be a better opportunity for your proposal than submitting to CalPERS directly: <a href="http://www.calpers.ca.gov/index.jsp?bc=/investments/extrnl-mgrs.xml" title="External Managers" target="_blank">External Managers<span class="newwindow" style="display: none;">opens in a new window</span><apex:image id="newWindowIcon2" value="{!$Resource.NewWindowIcon}" alt="opens in a new window"/></a>. </p>
            <p> A portion of this investment program may be managed externally by specialized investment managers who focus on Emerging Managers. Please refer to the following link to learn about CalPERS’ Emerging Manager platforms by asset class:   <a href=" http://www.calpers.ca.gov/eip-docs/investments/calpers-emerging-manager-platforms.pdf" title=" Emerging Managers" target="_blank"> Emerging Managers<span class="newwindow" style="display: none;">opens in a new window</span><apex:image id="newWindowIcon3" value="{!$Resource.NewWindowIcon}" alt="opens in a new window"/></a>. </p>
            <p>You will find description of each asset class below:</p>
            <apex:outputPanel id="proposalPanel">
                <apex:form id="thisForm" styleClass="cmxform" target="_self">
                    <dl>
                        <apex:repeat value="{!assetClassDescriptions}" var="assetDesc">
                            <dt><h2>{!assetDesc.Asset_Class__c}&nbsp;
                                <apex:outputLink target="blank" value="{!assetDesc.Printable_URL__c}" rendered="{!assetDesc.Printable_URL__c != null}" >Preview Asset Class questions {!if(assetDesc.Asset_Class__c=='Private Equity',' for Partnership',if(assetDesc.Asset_Class__c=='Infrastructure',' for Partnership',''))}</apex:outputLink>
                                <apex:outputText rendered="{!assetDesc.Printable_URL2__c != null}" >
                                    &nbsp;&nbsp;
                                    <apex:outputLink target="blank" value="{!assetDesc.Printable_URL2__c}" >Preview Asset Class questions {!if(assetDesc.Asset_Class__c=='Private Equity',' for Direct / Co-invest',if(assetDesc.Asset_Class__c=='Infrastructure',' for Direct',''))}</apex:outputLink>
                                </apex:outputText>
                            </h2></dt>
                            <dd><apex:outputText value="{!assetDesc.Value__c}" escape="false" /></dd>
                            <br />
                        </apex:repeat>
                    </dl>
                    <ol class="buttons">
                        <li>
                            <apex:commandButton value="Next" action="{!continueFromAssetClassDescriptions}" styleClass="submit"/>
                        </li>
                        <li>
                            <apex:commandLink value="Cancel" action="{!cancel}" target="_top" immediate="true" onClick="return confirm('{!JSENCODE(cancelAlert)}');"/>
                        </li>
                    </ol>
                </apex:form>
            </apex:outputPanel>
        </div>
        <iframe id="grandChild" style="display: none" title="Frame to display the Investment Proposal form."></iframe>
        <script src="{!$Resource.IPTS_Resize}" type="text/javascript"/>
        <script src="{!$Resource.IPTS_Session}" type="text/javascript"/>
    </body>
</apex:page>