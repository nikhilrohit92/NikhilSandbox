<apex:page cache="false" name="IPTS_RealEstate_EmergingManager" id="thisPage" standardStylesheets="false" controller="InvestmentSubmissionController" title="Investment Proposal::{!pageTitle}" showHeader="false" sidebar="false">
<html lang="en">
    <head><title>Investment Proposal::{!pageTitle}</title>

    <apex:stylesheet value="{!URLFOR($Resource.IPTS_Styles, '/ipts/css/reset.css')}"/> 
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_Styles, '/ipts/css/style.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_CustomStyles)}"/>
    <script src="{!$Resource.IPTS_Script}" type="text/javascript"/> 
    </head>
    <body>
        <div id="mainContent">
            <br/>
            <apex:image url="{!URLFOR($Resource.logo)}" width="201" height="46" alt="CalPERS Logo"  />
            <br/>
            <h1>Investment Proposal Submission</h1>

            <apex:outputPanel id="proposalPanel">
                
                <apex:outputPanel rendered="{!ehandler.hasErrors}">
                    <div id="error-message" class="error">
                        <h3>Please correct the highlighted form fields before continuing</h3>
                        <ol>
                            <apex:repeat value="{!ehandler.errors}" var="e">
                                <li>
                                    <a href="#{!e.fieldId}" onclick="document.getElementById('{!e.fieldId}').focus(); return false;">{!e.fieldLabel}</a> - {!e.pageMessage}
                                </li>
                            </apex:repeat>
                        </ol>
                    </div>
                </apex:outputPanel>

                <ul class="legend">
                    <li><em>Required</em><span class="required">*</span></li>
                </ul>

                <apex:form id="thisForm" styleClass="cmxform" target="_self">
                        <fieldset>
                        <legend>Firm Information</legend>
                        <br />
                        <h3>Emerging Manager Definition and Questionnaire</h3>
                        <br />
                        <p>CalPERS Emerging Manager definitions are established by each asset class.  Emerging managers are generally defined as newly formed firms or firms raising first or second time funds.  At CalPERS, emerging manager definitions are based upon assets under management and/or length of track record. CalPERS real estate emerging manager program only seeks firms pursuing investment strategies in California urban markets.  CalPERS may work with specialist advisory firms to implement emerging manager investment programs.  Suitable investment proposals will be referred to appropriate advisory partners.</p>
                        <br />
                        <h3>Real Estate Emerging Manager Program Definition of Emerging Manager</h3>
                        <br />
                        <p><u>Strategy Category</u>:  Separate Accounts and Commingled Funds</p>
                        <p><u>Product Size</u>:  No Requirement</p>
                        <p><u>Total Assets Under Management by the Firm</u>:  &lt; $1 billion</p>
                        <p><u>Length of Track Record</u>:  A firm raising a first, second or third time separate account or commingled fund.</p>
                        <p><u>Geographic Focus</u>:  Urban California</p>
                        <ol>
                            <li class="{!IF(ehandler.alog.EmergingManagerError,'error', '')}"> 
                                <fieldset>
                                    <legend class="{!IF(isIe,'useaslabel', '')}"><em>Please indicate if your firm meets the definition of emerging manager referenced above.</em><span class="required">*</span></legend>
                                    <ol>
                                        <li>
                                            <apex:selectRadio id="EmergingManager" value="{!submittorAccount.Emerging_Manager__c}" layout="pageDirection">
                                                <apex:selectOptions value="{!radioOptions}" />
                                            </apex:selectRadio>
                                        </li>
                                    </ol>
                                </fieldset>
                                <apex:outputPanel rendered="{!ehandler.alog.EmergingManagerError}" style="width: 100%;">
                                    <p class="error">{!selectRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>               
                      </ol> 
                    </fieldset>
                    <ol class="buttons">
                        <li>
                            <apex:commandButton value="Next" action="{!setEMInformation}" styleClass="btn btn-primary btn-sm"/>
                        </li>
                        <li>
                            <apex:commandLink value="Previous" action="{!previous}" immediate="true"/>
                        </li>
                        <li>
                            <apex:commandLink value="Cancel" action="{!cancel}" target="_top" immediate="true" onClick="return confirm('{!JSENCODE(cancelAlert)}');" />
                        </li>
                    </ol>
                    <div class="bigspacer"></div>
                    <apex:actionFunction name="clearSession" action="{!cancel}" immediate="true" />
                </apex:form>
            </apex:outputPanel>

        </div>
        
        <script src="{!$Resource.IPTS_Resize}" type="text/javascript"/>
        <script src="{!$Resource.IPTS_Session}" type="text/javascript"/>
    </body>
    <DIV class="footer text-center">
      <SPAN class="tagline">We serve those who serve California.</SPAN><BR/>© 
        Copyright 2015 California Public Employees' Retirement System (CalPERS)
        <BR/><BR/>
    </DIV>
 </html> 
    
</apex:page>