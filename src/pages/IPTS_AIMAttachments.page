<apex:page cache="true" name="IPTS_AIMAttachments" id="thisPage" standardStylesheets="false" controller="InvestmentSubmissionController" title="Investment Proposal::{!pageTitle}" showHeader="false" sidebar="false">
<html lang="en">
    <head><title>Investment Proposal::{!pageTitle}</title>

    <apex:stylesheet value="{!URLFOR($Resource.IPTS_Styles, '/ipts/css/reset.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_Styles, '/ipts/css/style.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_CustomStyles)}"/>
    <script src="{!$Resource.IPTS_Script}" type="text/javascript"/>
    <script type="text/javascript">
        function validateFile()
        {
            var elm = document.getElementsByName("thisPage:thisForm:FileNames:inputFile:file");
            return validateAttachment(elm[0]);
        }
    </script>
    </head>
    <body>
        <div id="mainContent">
            <br/>
            <apex:image url="{!URLFOR($Resource.logo)}" width="201" height="46" alt="CalPERS Logo"  />
            <br/>
            <h1>Investment Proposal Submission</h1>

            <apex:outputPanel id="proposalPanel">
                                
                <apex:form id="thisForm" styleClass="cmxform" target="_self">
                    <fieldset>
                        <legend>Attachments</legend>
                        <ol>
                            <li>
                                <fieldset>
                                    <legend class="{!IF(isIe,'useaslabel', '')}">Do you have a PPM or OM?</legend>
                                    <ol>
                                        <li>
                                            <apex:selectRadio id="PPMOM" value="{!proposal.Do_you_have_a_PPM_or_OM__c}" layout="pageDirection">
                                                <apex:selectOptions value="{!attachmentOptions}" />
                                            </apex:selectRadio>
                                        </li>
                                    </ol>
                                </fieldset>
                            </li>
                            
                            <li>
                                <fieldset>
                                    <legend class="{!IF(isIe,'useaslabel', '')}">Do you have any Presentation Materials?</legend>
                                    <ol>
                                        <li>
                                            <apex:selectRadio id="Presentation" value="{!proposal.Do_you_have_any_presentation_materials__c}" layout="pageDirection">
                                                <apex:selectOptions value="{!attachmentOptions}" />
                                            </apex:selectRadio>
                                        </li>
                                    </ol>
                                </fieldset>
                            </li>
                            <li>
                                <label><h3>Special Instructions:</h3></label>
                                <p>1. Each attached file is limited to a 20 MB file size. Attachment file name is limited to 80 characters.</p>
                            </li>
                            <li>
                                <apex:outputPanel rendered="{!attachManager.hasAttachments}" style="width: 100%">
                                    <table summary="Attachments to be included in the proposal" border="0" cellspacing="0" width="100%">
                                        <caption>Attachments to be included in the proposal</caption>
                                        <tr>
                                            <th id="h1" scope="col" style="border-bottom: 1px solid black;">File Name</th>
                                            <th width="15%" id="h2" scope="col" style="border-bottom: 1px solid black;">Action</th>
                                        </tr>
                                        <apex:repeat value="{!attachManager.attachments}" var="a">
                                            <tr>
                                                <td id="thisPage:thisForm:{!a.documentId}" headers="h1" scope="row">
                                                    <p>{!a.fileName}</p>
                                                </td>
                                                <td width="15%" headers="thisPage:thisForm:{!a.documentId} h2">
                                                    <p>
                                                        <apex:commandLink action="{!attachManager.removeAttachment}" immediate="true" onClick="return confirm('Do you want to remove this attachment from the proposal?');">Remove
                                                            <apex:param name="attachment.id" value="{!a.documentId}" />
                                                        </apex:commandLink> 
                                                    </p>
                                                </td>
                                            </tr>
                                        </apex:repeat>
                                    </table>
                                </apex:outputPanel>
                                <apex:outputPanel rendered="{!NOT(attachManager.hasAttachments)}" style="width: 100%">
                                    <p>No attachment(s) added to proposal.</p>
                                </apex:outputPanel>
                            </li>
                            <li>
                                <apex:commandLink value="Add Another Attachment" action="{!attachManager.newAttachment}" immediate="true" rendered="{!attachManager.isListContext}"/>
                            </li>
                        </ol>
                    </fieldset>
                    <apex:outputPanel rendered="{!attachManager.isAddContext || attachManager.isEditContext}">
                        <fieldset> 
                            <legend>Add Attachment</legend> 
                                <ol>
                                    <li>
                                        <label for="thisPage:thisForm:FileNames:inputFile:file">File Name</label>
                                        <apex:inputFile id="FileNames" value="{!attachManager.attachmentRef.body}" filename="{!attachManager.attachmentRef.Name}" title="Please enter a file name or tab to the browse button"/>
                                    </li>
                                </ol> 
                                <ol class="buttons">
                                    <li><apex:commandLink value="Add Another Attachment" action="{!attachManager.addMoreAttachment}" onClick="return validateFile();" /></li>
                                    <li><apex:commandLink value="OK" action="{!attachManager.addAttachment}" onClick="return validateFile();" /></li>
                                    <li><apex:commandLink value="Cancel" action="{!attachManager.cancelAttachment}" immediate="true"/></li>
                                </ol>
                        </fieldset>
                    </apex:outputPanel>
                    <ol class="buttons">
                        <li><apex:commandButton value="Review Your Submission" action="{!next}" rendered="{!attachManager.isListContext}" styleClass="btn btn-primary btn-sm"/></li>
                        <li><apex:commandLink value="Previous" action="{!previous}" immediate="true" rendered="{!attachManager.isListContext}"/></li>
                        <li><apex:commandButton value="Save and Exit" action="{!nextAndSave}" rendered="{!attachManager.isListContext}" styleClass="btn btn-primary btn-sm" onClick="return confirm('{!JSENCODE(saveAlert)}');" /></li>
                        <li><apex:commandLink value="Cancel" action="{!cancel}" target="_top" immediate="true" rendered="{!attachManager.isListContext}" onClick="return confirm('{!JSENCODE(cancelAlert)}');" /></li>
                    </ol>
                    <div class="spacer"></div><div class="spacer"></div>
                    <apex:actionFunction name="clearSession" action="{!cancel}" immediate="true" />
                </apex:form>
            </apex:outputPanel>
        </div>
        
        <script src="{!$Resource.IPTS_Resize}" type="text/javascript"/>
        <script src="{!$Resource.IPTS_Session}" type="text/javascript"/>
        <script type="text/javascript" >
            <apex:outputPanel layout="none" rendered="{!dupeExists}" >
                alert('{!JSENCODE(dupeAlert)}');
            </apex:outputPanel>
            <apex:outputPanel layout="none" rendered="{!!dupeExists && isSaveAndContinue}" >
                top.location.href = "{!cancelURL}";
            </apex:outputPanel>
        </script>
    </body>
    <DIV class="footer text-center">
      <SPAN class="tagline">We serve those who serve California.</SPAN><BR/>© 
        Copyright 2015 California Public Employees' Retirement System (CalPERS)
        <BR/><BR/>
    </DIV>
 </html> 
    
</apex:page>