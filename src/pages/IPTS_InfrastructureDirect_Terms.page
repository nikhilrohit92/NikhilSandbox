<apex:page cache="false" name="IPTS_InfrastructureDirect_Terms" id="thisPage" standardStylesheets="false" controller="InvestmentSubmissionController" title="Investment Proposal::{!pageTitle}" showHeader="false" sidebar="false">
<html lang="en">
    <head><title>Investment Proposal::{!pageTitle}</title>

    <apex:stylesheet value="{!URLFOR($Resource.IPTS_Styles, '/ipts/css/reset.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_Styles, '/ipts/css/style.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_CustomStyles)}"/>
    <script src="{!$Resource.IPTS_Script}" type="text/javascript"/>

    <script type="text/javascript">
        function validateFields(){
            var fieldIdsArray = new Array();
            fieldIdsArray[0] = new FieldTuple("thisPage:thisForm:EquityInterest", "If Equity, Percent Equity Interest being offered");
            fieldIdsArray[1] = new FieldTuple("thisPage:thisForm:Leverage", "Leverage");
            return validatePercentFields(fieldIdsArray);
        }

        function validateAndSaveFields(){
            if ( validateFields() ){
                return confirm('{!JSENCODE(saveAlert)}')
            } else {
                return false;
            }
        }
    </script>
    </head>
    <body>
        <div id="mainContent">
            <br/>
            <apex:image url="{!URLFOR($Resource.logo)}" width="201" height="46" alt="CalPERS Logo"  />
            <br/>
            <h1>Investment Proposal Submission</h1>
            
            <h2>Due Diligence Information</h2>
            <br/>

            <apex:outputPanel id="proposalPanel">
                
                <apex:outputPanel rendered="{!ehandler.hasErrors}">
                    <div id="error-message" class="error">
                        <h3>Please correct the highlighted form fields before continuing</h3>
                        <ol>
                            <apex:repeat value="{!ehandler.errors}" var="e">
                                <li>
                                    <a href="#{!e.fieldId}" onclick="document.getElementById('{!e.fieldId}').focus(); return false;">{!e.fieldLabel}</a> - {!e.pageMessage}
                                </li>
                            </apex:repeat>
                        </ol>
                    </div>
                </apex:outputPanel>

                <ul class="legend">
                    <li><em>Required</em><span class="required">*</span></li>
                </ul>
                
                <apex:form id="thisForm" styleClass="cmxform" target="_self">
                    <fieldset>
                        <legend>Investment Terms</legend>
                        <ol>
                            <li class="{!IF(ehandler.olog.otherFeeError || ehandler.olog.otherFeeNarrativeSizeError,'error', '')}">
                                <label for="thisPage:thisForm:OtherFee"><em>Please Describe Fees, if any - 1000 character limit</em><span class="required">*</span></label>
                                <apex:inputTextArea id="OtherFee" value="{!proposal.Other_Fee_Narrative__c}" styleClass="text" style="width: 550px; height: 170px;"/>
                                <apex:outputPanel rendered="{!ehandler.olog.otherFeeError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                                <apex:outputPanel rendered="{!ehandler.olog.otherFeeNarrativeSizeError}" style="width: 100%;">
                                    <p class="error">{!sizeExceededMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.preferredReturnError,'error', '')}">
                                <label for="thisPage:thisForm:PreferredReturn"><em>Target Return</em><span class="required">*</span></label>
                                <apex:inputField id="PreferredReturn" value="{!proposal.Preferred_Return__c}" 
                                    required="false" styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                                <apex:outputPanel rendered="{!ehandler.olog.preferredReturnError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.anticipatedError || ehandler.olog.anticipatedFormatError,'error', '')}">
                                <label for="thisPage:thisForm:AnticipatedFinalClosingDate"><em>Anticipated Final Closing Date</em><span class="required">*</span></label>
                                <apex:inputText id="AnticipatedFinalClosingDate" value="{!anticipatedDate}" styleClass="text short" required="false"/>
                                <p><label for="thisPage:thisForm:AnticipatedFinalClosingDate">mm/dd/yyyy</label></p>
                                <apex:outputPanel rendered="{!ehandler.olog.anticipatedError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                                <apex:outputPanel rendered="{!ehandler.olog.anticipatedFormatError}" style="width: 100%;">
                                    <p class="error">{!invalidDateFormatMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:ProposedCalPERS">Proposed CalPERS Investment (In Millions- One billion entered as 1000)</label>
                                <apex:inputField id="ProposedCalPERS" value="{!proposal.Proposed_CalPERS_Investment__c}" 
                                    styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:HoldingPeriod">Proposed Holding Period (In Years)</label>
                                <apex:inputField id="HoldingPeriod" value="{!proposal.Proposed_Holding_Period_In_Years__c}" 
                                    styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:EquityAmount">Equity Amount (In Millions- One billion entered as 1000)</label>
                                <apex:inputField id="EquityAmount" value="{!proposal.Equity__c}" 
                                    styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:EquityInterest">If Equity, Percent Equity Interest being offered (Percentage)</label>
                                <apex:inputField id="EquityInterest" value="{!proposal.Percent_Equity_Interest_being_offered__c}" 
                                    styleclass="text short" onkeypress="return validateChar2(event, this);"/>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:LTM">LTM Revenue (In Millions- One billion entered as 1000)</label>
                                <apex:inputField id="LTM" value="{!proposal.LTM_Revenue_In_Millions__c}" 
                                    styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:LTMEBITDA">LTM EBITDA (In Millions- One billion entered as 1000)</label>
                                <apex:inputField id="LTMEBITDA" value="{!proposal.LTM_EBITDA_In_Millions__c}" 
                                    styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:EnterpriseValue">Enterprise Value (In Millions- One billion entered as 1000)</label>
                                <apex:inputField id="EnterpriseValue" value="{!proposal.Total_Value_In_Millions__c}" 
                                    styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                            </li>
                            <li class="{!IF(ehandler.olog.leverageFormatError,'error', '')}"> 
                                <label for="thisPage:thisForm:Leverage">Leverage (Percentage)</label>
                                <apex:inputField id="Leverage" value="{!proposal.Leverage__c}" 
                                    styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                                <apex:outputPanel rendered="{!ehandler.olog.leverageFormatError}" style="width: 100%;">
                                    <p class="error">Leverage must be less than 100%</p>
                                </apex:outputPanel> 
                            </li>
                            <li class="{!IF(ehandler.olog.timingSizeError,'error', '')}">
                                <label for="thisPage:thisForm:AdditionalInfo">Additional Information - 1000 character limit</label>
                                <apex:inputTextarea id="AdditionalInfo" value="{!proposal.Timing_Considerations__c}" 
                                    style="width: 550px; height: 170px;" styleClass="text"/>
                                <apex:outputPanel rendered="{!ehandler.olog.timingSizeError}" style="width: 100%;">
                                    <p class="error">{!sizeExceededMessage}</p>
                                </apex:outputPanel>
                            </li>
                        </ol>
                    </fieldset>
                    <ol class="buttons">
                        <li><apex:commandButton value="Next" action="{!validateInfrastructureDirectTermsData}" styleClass="btn btn-primary btn-sm" onClick="return validateFields();"/></li> 
                        <li><apex:commandLink value="Previous" action="{!previous}" immediate="true"/></li>
                        <li><apex:commandButton value="Save and Exit" action="{!validateAndSaveInfrastructureDirectTermsData}" styleClass="btn btn-primary btn-sm" onClick="validateAndSaveFields();" /></li>
                        <li><apex:commandLink value="Cancel" action="{!cancel}" target="_top" immediate="true" onClick="return confirm('{!JSENCODE(cancelAlert)}');" /></li>
                    </ol>
                    <div class="spacer"></div>
                    <apex:actionFunction name="clearSession" action="{!cancel}" immediate="true" />
                </apex:form>
            </apex:outputPanel>
        </div>
        
        <script src="{!$Resource.IPTS_Resize}" type="text/javascript"/>
        <script src="{!$Resource.IPTS_Session}" type="text/javascript"/>
        <script type="text/javascript" >
            <apex:outputPanel layout="none" rendered="{!dupeExists}" >
                alert('{!JSENCODE(dupeAlert)}');
            </apex:outputPanel>
            <apex:outputPanel layout="none" rendered="{!!dupeExists && isSaveAndContinue}" >
                window.top.location = "{!cancelURL}";
            </apex:outputPanel>
        </script>
    </body>
    <DIV class="footer text-center">
      <SPAN class="tagline">We serve those who serve California.</SPAN><BR/>© 
        Copyright 2015 California Public Employees' Retirement System (CalPERS)
        <BR/><BR/>
    </DIV>
 </html> 
    
</apex:page>