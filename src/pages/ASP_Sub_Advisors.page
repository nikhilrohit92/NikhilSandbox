<apex:page cache="false" name="ASP_Sub_Advisors" id="thisPage" standardStylesheets="false" controller="InvestmentSubmissionController" title="Investment Proposal::{!pageTitle}" showHeader="false" sidebar="false">

<html lang="en">

<HEAD> <title>Investment Proposal::{!pageTitle}</title>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_Styles, '/ipts/css/reset.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS2_Styles, '/ipts-2/css/style2.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_CustomStyles)}"/>
    <script src="{!$Resource.IPTS_Script}" type="text/javascript"/>
</HEAD>       
    <body>
        <div id="ackContent">
            <br/>
            <apex:image url="{!URLFOR($Resource.logo)}" width="201" height="46" alt="CalPERS Logo"  />
            <br/>
            <h1><apex:outputText id="name"  value="{!tempCampaignName}"  /></h1>
            
            <apex:outputPanel id="proposalPanel">
                
                <apex:outputPanel rendered="{!ehandler.hasErrors}">
                    <div id="error-message" class="error">
                        <h3>Please correct the highlighted form fields before continuing</h3>
                        <ol>
                            <apex:repeat value="{!ehandler.errors}" var="e">
                                <li>
                                    <a href="#{!e.fieldId}" onclick="document.getElementById('{!e.fieldId}').focus(); return false;">{!e.fieldLabel}</a> - {!e.pageMessage}
                                </li>
                            </apex:repeat>
                        </ol>
                    </div>
                </apex:outputPanel>

                <ul class="legend">
                    <li><em>Required</em><span class="required">*</span></li>
                </ul>            
                <apex:form id="thisForm" styleClass="cmxform" target="_self">
                <fieldset>
                <ol>
                    <h3>Sub-Advisors</h3>
                                <apex:outputPanel id="classPanel" style="width: 100%;">
                                    <fieldset>
                                        <legend class="{!IF(isIe,'useaslabel', '')}">Please advise if your firm anticipates in utilizing Sub-Advisors in order to provide the services CalPERS is requesting.<span class="required">*</span></legend>
                                        <ol>
                                            <li>
                                                <apex:actionRegion >
                                                    <apex:selectRadio id="SubAdvisors" value="{!proposal.SA_Sub_Advisors__c}" layout="pageDirection">
                                                        <apex:selectOptions value="{!radioOptions}" />
                                                        <apex:actionSupport event="onclick" action="{!setExistingRelationshipOption}" rerender="classPanel"/>
                                                    </apex:selectRadio>
                                                </apex:actionRegion>
                                            </li>
                                        </ol>
                                    </fieldset>
                                    <apex:outputPanel rendered="{!RenderSubAdvisors}" style="width: 100%;">
                                    
                    <fieldset>
                        <legend>Sub-Advisor No. 1</legend>
                        <ol>

                            <li class="{!IF(ehandler.olog.CompanyNameError,'error', '')}">
                                <label for="thisPage:thisForm:CompanyName">Company Name<span class="required">*</span></label>
                                <apex:inputField id="CompanyName" value="{!proposal.SA_Company_Name1__c}" required="false" style="width: 550px;" styleClass="text medium"/>
                                <apex:outputPanel rendered="{!ehandler.olog.CompanyNameError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.ContactNameError,'error', '')}">
                                <label for="thisPage:thisForm:ContactName"><em>Contact Name</em><span class="required">*</span></label>
                                <apex:inputField id="ContactName" value="{!proposal.SA_Contact_Name1__c}" required="false" style="width: 250px;" styleClass="text medium"/>
                                <apex:outputPanel rendered="{!ehandler.olog.ContactNameError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>                            

                            <li class="{!IF(ehandler.olog.ContactPhoneError,'error', '')}">                            
                                <label for="thisPage:thisForm:ContactPhone">Contact Telephone Number<span class="required">*</span></label>
                                <apex:inputField id="ContactPhone" value="{!proposal.SA_Contact_Phone1__c}" required="false" styleClass="text medium"/>
                                <apex:outputPanel rendered="{!ehandler.olog.ContactPhoneError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>                                
                            </li>
                            <li class="{!IF(ehandler.olog.StreetError || ehandler.alog.streetSizeError,'error', '')}">
                                <label for="thisPage:thisForm:Street"><em>Street - 255 character limit</em><span class="required">*</span></label>
                                <apex:inputTextArea id="Street" value="{!proposal.SA_Street1__c}" style="width: 300px; height: 40px;" required="false" styleClass="text medium"/>
                                <apex:outputPanel rendered="{!ehandler.olog.streetError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                                <apex:outputPanel rendered="{!ehandler.alog.streetSizeError}" style="width: 100%;">
                                    <p class="error">{!streetSizeExceededMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.cityError,'error', '')}">
                                <label for="thisPage:thisForm:City"><em>City</em><span class="required">*</span></label>
                                <apex:inputField id="City" value="{!proposal.SA_City1__c}" required="false" styleClass="text medium"/>
                                <apex:outputPanel rendered="{!ehandler.olog.cityError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.stateError,'error', '')}">
                                <label for="thisPage:thisForm:State"><em>State</em><span class="required">*</span></label>
                                <apex:inputField id="State" value="{!proposal.SA_State1__c}" required="false" styleClass="text medium"/>
                                <apex:outputPanel rendered="{!ehandler.olog.stateError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.postalError,'error', '')}">
                                <label for="thisPage:thisForm:PostalCode"><em>Postal Code</em><span class="required">*</span></label>
                                <apex:inputField id="PostalCode" value="{!proposal.SA_Postal_Code1__c}" required="false"  styleClass="text medium"/>
                                <apex:outputPanel rendered="{!ehandler.olog.postalError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.WorkError,'error', '')}">
                                <label for="thisPage:thisForm:Work"><em>Description of Work to be Performed by Sub-Advisor</em><span class="required">*</span></label>
                                <apex:inputTextArea id="Work" value="{!proposal.SA_Work_to_be_Performed1__c}" required="false" styleClass="text"/>
                                <apex:outputPanel rendered="{!ehandler.olog.WorkError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.AgreeAmtError, 'error','')}">
                                <label for="thisPage:thisForm:AgreeAmt">Percentage of Total Agreement Amount<span class="required">*</span></label>
                                <apex:inputField id="AgreeAmt" value="{!proposal.SA_Pct_Total_Amt1__c}" styleClass="text medium"/>
                                <apex:outputPanel rendered="{!ehandler.olog.AgreeAmtError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>                            
                            </li>
                            <br/>
                        <legend>Sub-Advisor No. 2</legend>

                            <li class="{!IF(ehandler.olog.CompanyNameError2,'error', '')}">
                                <label for="thisPage:thisForm:CompanyName2">Company Name<span class="required">*</span></label>
                                <apex:inputField id="CompanyName2" value="{!proposal.SA_Company_Name2__c}" required="false" style="width: 550px;" styleClass="text medium"/>
                                <apex:outputPanel rendered="{!ehandler.olog.CompanyNameError2}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.ContactNameError2,'error', '')}">
                                <label for="thisPage:thisForm:ContactName2"><em>Contact Name</em><span class="required">*</span></label>
                                <apex:inputField id="ContactName2" value="{!proposal.SA_Contact_Name2__c}" required="false" style="width: 250px;" styleClass="text medium"/>
                                <apex:outputPanel rendered="{!ehandler.olog.ContactNameError2}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>                            

                            <li class="{!IF(ehandler.olog.ContactPhoneError2,'error', '')}">                            
                                <label for="thisPage:thisForm:ContactPhone2">Contact Telephone Number<span class="required">*</span></label>
                                <apex:inputField id="ContactPhone2" value="{!proposal.SA_Contact_Phone2__c}" required="false" styleClass="text medium"/>
                                <apex:outputPanel rendered="{!ehandler.olog.ContactPhoneError2}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>                                
                            </li>
                            <li class="{!IF(ehandler.olog.StreetError2 || ehandler.alog.streetSizeError,'error', '')}">
                                <label for="thisPage:thisForm:Street2"><em>Street - 255 character limit</em><span class="required">*</span></label>
                                <apex:inputTextArea id="Street2" value="{!proposal.SA_Street2__c}" style="width: 300px; height: 40px;" required="false" styleClass="text medium"/>
                                <apex:outputPanel rendered="{!ehandler.olog.streetError2}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                                <apex:outputPanel rendered="{!ehandler.alog.streetSizeError}" style="width: 100%;">
                                    <p class="error">{!streetSizeExceededMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.cityError2,'error', '')}">
                                <label for="thisPage:thisForm:City2"><em>City</em><span class="required">*</span></label>
                                <apex:inputField id="City2" value="{!proposal.SA_City2__c}" required="false" styleClass="text medium"/>
                                <apex:outputPanel rendered="{!ehandler.olog.cityError2}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.stateError2,'error', '')}">
                                <label for="thisPage:thisForm:State2"><em>State</em><span class="required">*</span></label>
                                <apex:inputField id="State2" value="{!proposal.SA_State2__c}" required="false" styleClass="text medium"/>
                                <apex:outputPanel rendered="{!ehandler.olog.stateError2}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.postalError2,'error', '')}">
                                <label for="thisPage:thisForm:PostalCode2"><em>Postal Code</em><span class="required">*</span></label>
                                <apex:inputField id="PostalCode2" value="{!proposal.SA_Postal_Code2__c}" required="false"  styleClass="text medium"/>
                                <apex:outputPanel rendered="{!ehandler.olog.postalError2}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.WorkError2,'error', '')}">
                                <label for="thisPage:thisForm:Work2"><em>Description of Work to be Performed by Sub-Advisor</em><span class="required">*</span></label>
                                <apex:inputTextArea id="Work2" value="{!proposal.SA_Work_to_be_Performed2__c}" required="false" styleClass="text"/>
                                <apex:outputPanel rendered="{!ehandler.olog.WorkError2}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.AgreeAmtError2, 'error','')}">
                                <label for="thisPage:thisForm:AgreeAmt2">Percentage of Total Agreement Amount<span class="required">*</span></label>
                                <apex:inputField id="AgreeAmt2" value="{!proposal.SA_Pct_Total_Amt2__c}" styleClass="text medium"/>
                                <apex:outputPanel rendered="{!ehandler.olog.AgreeAmtError2}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>                            
                            </li>

                            </ol>
                           </fieldset>
                           
                                    </apex:outputPanel>
                                </apex:outputPanel>                     

                  
                    </ol>                                
                    </fieldset>
                    <div class="spacer"></div>

                    <ol class="buttons">
                        <li><apex:commandButton value="Previous"  action="{!previous}" immediate="true" styleClass="btn btn-primary btn-sm"/></li>
                        <li><apex:commandButton value="Next" action="{!setSAInfo}" styleClass="btn btn-primary btn-sm"/></li>
                         <li><apex:commandButton value="Save and Exit" rendered="{!canSaveAndExit}" action="{!validateAndSaveSA}" styleClass="btn btn-primary btn-sm" onClick="return confirm('{!JSENCODE(saveAlert)}');" /></li>
                        <li><apex:commandButton value="Cancel"  action="{!cancel}" styleClass="btn btn-primary btn-sm" immediate="true" onClick="return confirm('{!JSENCODE(cancelAlert)}');" /></li>
                    </ol>
                    <div class="bigspacer"></div>
                    <div class="bigspacer"></div>
                    <apex:actionFunction name="clearSession" action="{!cancel}" immediate="true" />
                </apex:form>
            </apex:outputPanel>

        </div>
        <script type="text/javascript">
            <apex:outputPanel layout="none" rendered="{!copyContactInformation}" >
                document.getElementById("thisPage:thisForm:ExistingRel:0").focus();
            </apex:outputPanel>
        </script>

        <script src="{!$Resource.IPTS_Resize}" type="text/javascript"/>
        <script src="{!$Resource.IPTS_Session}" type="text/javascript"/>
    </body>
    <DIV class="footer text-center">
      <SPAN class="tagline">We serve those who serve California.</SPAN><BR/>© 
        Copyright 2015 California Public Employees' Retirement System (CalPERS)
        <BR/><BR/>
    </DIV>
 </html>    
</apex:page>