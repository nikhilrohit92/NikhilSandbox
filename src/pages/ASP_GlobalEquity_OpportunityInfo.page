<apex:page cache="false" name="ASP_GlobalEquity_OpportunityInfo" id="thisPage" standardStylesheets="false" controller="InvestmentSubmissionController" title="Investment Proposal::{!pageTitle}" showHeader="false" sidebar="false">
<html lang="en">
    <head><title>Investment Proposal::{!pageTitle}</title>

    <apex:stylesheet value="{!URLFOR($Resource.IPTS_Styles, '/ipts/css/reset.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS2_Styles, '/ipts-2/css/style2.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_CustomStyles)}"/>
    <script src="{!$Resource.IPTS_Script}" type="text/javascript"/>
    </head>
    <body>
        <div id="ackContent">
            <br/>
            <apex:image url="{!URLFOR($Resource.logo)}" width="201" height="46" alt="CalPERS Logo"  />
            <br/>
            <h1><apex:outputText id="name"  value="{!tempCampaignName}"  /></h1>

            <apex:outputPanel id="proposalPanel">
                
                <apex:outputPanel rendered="{!ehandler.hasErrors}">
                    <div id="error-message" class="error">
                        <h3>Please correct the highlighted form fields before continuing</h3>
                        <ol>
                            <apex:repeat value="{!ehandler.errors}" var="e">
                                <li>
                                    <a href="#{!e.fieldId}" onclick="document.getElementById('{!e.fieldId}').focus(); return false;">{!e.fieldLabel}</a> - {!e.pageMessage}
                                </li>
                            </apex:repeat>
                        </ol>
                    </div>
                </apex:outputPanel>

                <ul class="legend">
                    <li><em>Required</em><span class="required">*</span></li>
                </ul>
                
                <apex:form id="thisForm" styleClass="cmxform" target="_self">
                    <apex:actionFunction action="{!confirmProductAUM}" name="confirmProductAUM" rerender="thisPage:thisForm:ProductAUM"/>
                    <apex:outputPanel id="onehundthou" >
                        <apex:outputPanel rendered="{!acceptedProductAUM == false}" >
                            <script>
                            function overOneHundThou()
                            {
                                var tempStr = "{!proposal.Product_AUM__c}";
                                if(tempStr != "") {
                                    var temp = {!proposal.Product_AUM__c};
                                    if(temp >=100000 || temp == 0) {
                                        var oht = confirm("You have entered a value of 0 or $100B or greater. If correct select \"OK\" to navigate to the next field. Select \"Cancel\" to return and change the value.");
                                        if(oht == false) {
                                            //setToZero();
                                            document.getElementById("{!$Component.thisPage:thisForm:ProductAUM}").focus();
                                        }
                                        else {
                                            confirmProductAUM();
                                        }
                                    }
                                }
                            }
                            window.onload = overOneHundThou();
                            </script>
                        </apex:outputPanel>
                    </apex:outputPanel>
                    <fieldset>
                        <legend>Investment Opportunity Information</legend>
                        <ol>
                            <li class="{!IF(ehandler.olog.productError,'error', '')}">
                                <label for="thisPage:thisForm:Product"><em>Product</em><span class="required">*</span></label>
                                <apex:inputField id="Product" value="{!proposal.Product__c}" style="width: 550px;" required="false" styleClass="text"/>
                                <apex:outputPanel rendered="{!ehandler.olog.productError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.productAumError,'error', '')}">
                                <label for="thisPage:thisForm:ProductAUM"><em>Product AUM in USD (In Millions- One billion entered as 1000)</em><span class="required">*</span></label>
                                <apex:inputField id="ProductAUM" value="{!proposal.Product_AUM__c}" required="false" 
                                    styleClass="text short" onkeypress="return validateChar2(event, this);" >
                                    <apex:actionSupport event="onblur" action="{!resetProductAUM}" rerender="onehundthou" />
                                </apex:inputField>
                                <apex:outputPanel rendered="{!ehandler.olog.productAumError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:Currency">Select Currency to be used for fields below<span class="required">*</span></label>
                                <apex:selectList id="Currency" value="{!proposal.Local_Currency__c}" multiselect="false" size="1">
                                    <apex:selectOptions value="{!currencyOptions}" />
                                </apex:selectList>
                            </li>
                            <li class="{!IF(ehandler.olog.geoFocusError,'error', '')}">
                                <fieldset>
                                    <label for="thisPage:thisForm:GeoFocus"><em>Geographic Focus</em><span class="required">*</span></label>
                                    <ol>
                                        <li>
                                            <apex:actionRegion >
                                                <apex:selectList id="GeoFocus" value="{!proposal.Geographic_Focus__c}" size="1" multiselect="false">
                                                    <apex:selectOptions value="{!geoFocusOptions}" />
                                                    <apex:actionSupport event="onchange" action="{!getGeoSubFocus}" rerender="gsf"/>
                                                </apex:selectList>
                                            </apex:actionRegion>
                                        </li>
                                    </ol>
                                </fieldset>
                                <apex:outputPanel rendered="{!ehandler.olog.geoFocusError}" style="width: 100%;">
                                    <p class="error">{!selectRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.geoSubFocusError,'error', '')}">
                                <apex:outputPanel id="gsf" style="width: 100%;">
                                    <fieldset>
                                        <label for="thisPage:thisForm:GeoSubFocus"><em>Geographic Sub-Focus</em><span class="required">*</span></label>
                                        <ol>
                                            <li>
                                                <apex:selectList id="GeoSubFocus" value="{!proposal.Geographic_Sub_Focus__c}" size="1" multiselect="false">
                                                    <apex:selectOptions value="{!subGeoFocusOptions}" />
                                                </apex:selectList>
                                            </li>
                                        </ol>
                                    </fieldset>
                                    <apex:outputPanel rendered="{!ehandler.olog.geoSubFocusError}" style="width: 100%;">
                                        <p class="error">{!selectRequiredMessage}</p>
                                    </apex:outputPanel>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.strategyError,'error', '')}">
                                <fieldset>
                                    <label for="thisPage:thisForm:Strategy"><em>Strategy</em><span class="required">*</span></label>
                                    <ol>
                                        <li>
                                            <apex:selectList id="Strategy" value="{!proposal.Strategy__c}" size="1" multiselect="false">
                                                <apex:selectOptions value="{!strategyOptions}" />
                                            </apex:selectList>
                                        </li>
                                    </ol>
                                </fieldset>
                                <apex:outputPanel rendered="{!ehandler.olog.strategyError}" style="width: 100%;">
                                    <p class="error">{!selectRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.subStrategyError,'error', '')}">
                                <fieldset>
                                    <label for="thisPage:thisForm:SubStrategy"><em>Sub-Strategy</em><span class="required">*</span></label>
                                    <ol>
                                        <li>
                                            <apex:selectList id="SubStrategy" value="{!proposal.Sub_Strategy__c}" size="1" multiselect="false">
                                                <apex:selectOptions value="{!subStrategyOptions}" />
                                            </apex:selectList>
                                        </li>
                                    </ol>
                                </fieldset>
                                <apex:outputPanel rendered="{!ehandler.olog.subStrategyError}" style="width: 100%;">
                                    <p class="error">{!selectRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.productInceptionDateFormatError || ehandler.olog.productInceptionDateError,'error', '')}">
                                <label for="thisPage:thisForm:ProductInceptionDate"><em>Product Inception Date</em><span class="required">*</span></label>
                                <apex:inputText id="ProductInceptionDate" value="{!productInceptionDate}" required="false" styleClass="text short"/>
                                <p><label for="thisPage:thisForm:ProductInceptionDate">mm/dd/yyyy</label></p>
                                <apex:outputPanel rendered="{!ehandler.olog.productInceptionDateFormatError}" style="width: 100%;">
                                    <p class="error">{!invalidDateFormatMessage}</p>
                                </apex:outputPanel>
                                <apex:outputPanel rendered="{!ehandler.olog.productInceptionDateError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.numberOfProfessionalsError,'error', '')}">
                                <label for="thisPage:thisForm:NumberOfProfs"><em>Number of Investment Professionals on Product</em><span class="required">*</span></label>
                                <apex:inputField id="NumberOfProfs" value="{!proposal.of_Investment_Prof_on_product__c}" 
                                    required="false" styleClass="text short" onkeypress="return validateChar2(event, this);" />
                                <apex:outputPanel rendered="{!ehandler.olog.numberOfProfessionalsError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.processDescError || ehandler.olog.processDescSizeError,'error', '')}">
                                <label for="thisPage:thisForm:ProcessDesc"><em>Investment Process Description - 1000 character limit</em><span class="required">*</span></label>
                                <apex:inputTextArea id="ProcessDesc" value="{!proposal.Investment_Process_Description__c}" style="width: 550px; height: 170px;" required="false" styleClass="text"/>
                                <apex:outputPanel rendered="{!ehandler.olog.processDescError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                                <apex:outputPanel rendered="{!ehandler.olog.processDescSizeError}" style="width: 100%;">
                                    <p class="error">{!sizeExceededMessage}</p>
                                </apex:outputPanel>
                            </li>
                        </ol>
                    </fieldset>
                    <ol class="buttons">
                        <li><apex:commandButton value="Next" action="{!validateGlobalEquityOpptyInfo}" styleClass="btn btn-primary btn-sm"/></li>
                        <li><apex:commandLink value="Previous" action="{!previous}" immediate="true"/></li>
                        <li><apex:commandButton value="Save and Exit" action="{!validateAndSaveGlobalEquityOpptyInfo}" styleClass="btn btn-primary btn-sm" onClick="return confirm('{!JSENCODE(saveAlert)}');" /></li>
                        <li><apex:commandLink value="Cancel" action="{!cancel}" target="_top" immediate="true" onClick="return confirm('{!JSENCODE(cancelAlert)}');" /></li>
                    </ol>
                    <div class="spacer"></div>
                    <apex:actionFunction name="clearSession" action="{!cancel}" immediate="true" />
                </apex:form>
            </apex:outputPanel>

        </div>
        
        <script src="{!$Resource.IPTS_Resize}" type="text/javascript"/>
        <script src="{!$Resource.IPTS_Session}" type="text/javascript"/>
        <script type="text/javascript" >
            <apex:outputPanel layout="none" rendered="{!dupeExists}" >
                alert('{!JSENCODE(dupeAlert)}');
            </apex:outputPanel>
            <apex:outputPanel layout="none" rendered="{!!dupeExists && isSaveAndContinue}" >
                top.location.href = "{!cancelURL}";
            </apex:outputPanel>
        </script>
    </body>
    <DIV class="footer text-center">
      <SPAN class="tagline">We serve those who serve California.</SPAN><BR/>© 
        Copyright 2015 California Public Employees' Retirement System (CalPERS)
        <BR/><BR/>
    </DIV>
 </html> 
    
</apex:page>