<apex:page cache="false" name="IPTS_ReferrerAgentInformation" id="thisPage" controller="InvestmentSubmissionController" title="Investment Proposal" showHeader="false" sidebar="false" standardStylesheets="false">
<html lang="en">
    <head><title>Investment Proposal::{!pageTitle}</title>

    <apex:stylesheet value="{!URLFOR($Resource.IPTS_Styles, '/ipts/css/reset.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_Styles, '/ipts/css/style.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_CustomStyles)}"/>
    </head>
    <body>
        <div id="mainContent">
            <br/>
            <apex:image url="{!URLFOR($Resource.logo)}" width="201" height="46" alt="CalPERS Logo"  />
            <br/>
            <h1>Investment Proposal Submission</h1>
            
            <apex:outputPanel id="proposalPanel">
                <apex:outputPanel rendered="{!ehandler.hasErrors}">
                    <div id="error-message" class="error">
                        <h3>Please correct the highlighted form fields before continuing</h3>
                        <ol>
                            <apex:repeat value="{!ehandler.errors}" var="e">
                                <li>
                                    <a href="#{!e.fieldId}" onclick="document.getElementById('{!e.fieldId}').focus(); return false;">{!e.fieldLabel}</a> - {!e.pageMessage}
                                </li>
                            </apex:repeat>
                        </ol>
                    </div>
                </apex:outputPanel>
                <ul class="legend">
                    <li><em>Required</em><span class="required">*</span></li>
                </ul>
                <apex:form id="thisForm" styleClass="cmxform" target="_self">
                    <apex:outputPanel rendered="{!isReferred}">
                        <fieldset>
                            <legend>Referrer Information</legend>
                            <ol>
                                <li class="{!IF(ehandler.clog.firstNameError || ehandler.clog.lastNameError,'error', '')}">
                                    <fieldset>
                                        <legend class="{!IF(isIe,'useaslabel', '')}"><em>Referrer Name</em><span class="required">*</span></legend>
                                        <ol>
                                            <li>
                                                <span>
                                                    <apex:inputField id="ReferrerFirstName" value="{!referrer.FirstName}" styleClass="text medium" required="false"/>
                                                    <p><label for="thisPage:thisForm:ReferrerFirstName">First Name</label></p>
                                                </span>
                                                <span>
                                                    <apex:inputField id="ReferrerLastName" value="{!referrer.LastName}" styleClass="text medium" required="false"/>
                                                    <p><label for="thisPage:thisForm:ReferrerLastName">Last Name</label></p>
                                                </span>
                                                <apex:outputPanel rendered="{!ehandler.clog.firstNameError || ehandler.clog.lastNameError}" style="width: 100%;">
                                                    <p class="error">{!inputRequiredMessage}</p>
                                                </apex:outputPanel>
                                            </li>
                                        </ol>
                                    </fieldset>
                                </li>
                                <li class="{!IF(ehandler.clog.phoneError,'error', '')}">
                                    <label for="thisPage:thisForm:ReferrerPhone"><em>Phone</em><span class="required">*</span></label>
                                    <apex:inputField id="ReferrerPhone" value="{!referrer.Phone}" required="false" styleClass="text medium"/>
                                    <apex:outputPanel rendered="{!ehandler.clog.phoneError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li class="{!IF(ehandler.clog.emailError || ehandler.clog.emailFormatError,'error', '')}">
                                    <label for="thisPage:thisForm:ReferrerEmail"><em>Email</em><span class="required">*</span></label>
                                    <apex:inputText id="ReferrerEmail" value="{!referrerEmail}" styleClass="text medium"/>
                                    <apex:outputPanel rendered="{!ehandler.clog.emailError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                    <apex:outputPanel rendered="{!ehandler.clog.emailFormatError}" style="width: 100%;">
                                        <p class="error">{!invalidEmailMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li class="{!IF(ehandler.clog.streetError || ehandler.clog.streetSizeError,'error', '')}">
                                    <label for="thisPage:thisForm:ReferrerStreet"><em>Street - 255 character limit</em><span class="required">*</span></label>
                                    <apex:inputTextArea id="ReferrerStreet" value="{!referrer.MailingStreet}" style="width: 300px; height: 40px;" styleClass="text" required="false"/>
                                    <apex:outputPanel rendered="{!ehandler.clog.streetError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                    <apex:outputPanel rendered="{!ehandler.clog.streetSizeError}" style="width: 100%;">
                                        <p class="error">{!streetSizeExceededMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li  class="{!IF(ehandler.clog.cityError,'error', '')}">
                                    <label for="thisPage:thisForm:ReferrerCity"><em>City</em><span class="required">*</span></label>
                                    <apex:inputField id="ReferrerCity" value="{!referrer.MailingCity}" styleClass="text medium" required="false"/>
                                    <apex:outputPanel rendered="{!ehandler.clog.cityError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li  class="{!IF(ehandler.clog.stateError,'error', '')}">
                                    <label for="thisPage:thisForm:ReferrerState"><em>State/Region</em><span class="required">*</span></label>
                                    <apex:inputField id="ReferrerState" value="{!referrer.MailingState}" styleClass="text medium" required="false"/>
                                    <apex:outputPanel rendered="{!ehandler.clog.stateError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li  class="{!IF(ehandler.clog.postalCodeError,'error', '')}">
                                    <label for="thisPage:thisForm:ReferrerPostalCode"><em>Postal Code</em><span class="required">*</span></label>
                                    <apex:inputField id="ReferrerPostalCode" value="{!referrer.MailingPostalCode}" styleClass="text medium" required="false"/>
                                    <apex:outputPanel rendered="{!ehandler.clog.postalCodeError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li>
                                    <label for="thisPage:thisForm:ReferrerCountry">Country</label>
                                    <apex:inputField id="ReferrerCountry" value="{!referrer.MailingCountry}" styleClass="text medium"/>
                                </li>
                            </ol>
                        </fieldset>
                    </apex:outputPanel>
                    
                    <apex:outputPanel rendered="{!isReferredByPlacementAgent || isPlacementAgent}">
                        <fieldset>
                            <legend>Placement Agent Information</legend>
                            <ol>
                                <li class="{!IF(ehandler.clog.firstNameError || ehandler.clog.lastNameError,'error', '')}">
                                    <fieldset>
                                        <legend class="{!IF(isIe,'useaslabel', '')}"><em>Placement Agent Name</em><span class="required">*</span></legend>
                                        <ol>
                                            <li>
                                                <span>
                                                    <apex:inputField id="AgentFirstName" value="{!placementAgent.FirstName}" styleClass="text medium" required="false"/>
                                                    <p><label for="thisPage:thisForm:AgentFirstName">First Name</label></p>
                                                </span>
                                                <span>
                                                    <apex:inputField id="AgentLastName" value="{!placementAgent.LastName}" styleClass="text medium" required="false"/>
                                                    <p><label for="thisPage:thisForm:AgentLastName">Last Name</label></p>
                                                </span>
                                                <apex:outputPanel rendered="{!ehandler.clog.firstNameError || ehandler.clog.lastNameError}" style="width: 100%;">
                                                    <p class="error">{!inputRequiredMessage}</p>
                                                </apex:outputPanel>
                                            </li>
                                        </ol>
                                    </fieldset>
                                </li>
                                <li class="{!IF(ehandler.clog.phoneError,'error', '')}">
                                    <label for="thisPage:thisForm:AgentPhone"><em>Phone</em><span class="required">*</span></label>
                                    <apex:inputField id="AgentPhone" value="{!placementAgent.Phone}" required="false" styleClass="text medium"/>
                                    <apex:outputPanel rendered="{!ehandler.clog.phoneError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li class="{!IF(ehandler.clog.emailError || ehandler.clog.emailFormatError,'error', '')}">
                                    <label for="thisPage:thisForm:AgentEmail"><em>Email</em><span class="required">*</span></label>
                                    <apex:inputText id="AgentEmail" value="{!agentEmail}" styleClass="text medium"/>
                                    <apex:outputPanel rendered="{!ehandler.clog.emailError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                    <apex:outputPanel rendered="{!ehandler.clog.emailFormatError}" style="width: 100%;">
                                        <p class="error">{!invalidEmailMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li class="{!IF(ehandler.clog.streetError || ehandler.clog.streetSizeError,'error', '')}">
                                    <label for="thisPage:thisForm:AgentStreet"><em>Street - 255 character limit</em><span class="required">*</span></label>
                                    <apex:inputTextArea id="AgentStreet" value="{!placementAgent.MailingStreet}" styleClass="text" style="width: 300px; height: 40px;" required="false"/>
                                    <apex:outputPanel rendered="{!ehandler.clog.streetError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                    <apex:outputPanel rendered="{!ehandler.clog.streetSizeError}" style="width: 100%;">
                                        <p class="error">{!streetSizeExceededMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li class="{!IF(ehandler.clog.cityError,'error', '')}">
                                    <label for="thisPage:thisForm:AgentCity"><em>City</em><span class="required">*</span></label>
                                    <apex:inputField id="AgentCity" value="{!placementAgent.MailingCity}" styleClass="text medium" required="false"/>
                                    <apex:outputPanel rendered="{!ehandler.clog.cityError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li class="{!IF(ehandler.clog.stateError,'error', '')}">
                                    <label for="thisPage:thisForm:AgentState"><em>State/Region</em><span class="required">*</span></label>
                                    <apex:inputField id="AgentState" value="{!placementAgent.MailingState}" styleClass="text medium" required="false"/>
                                    <apex:outputPanel rendered="{!ehandler.clog.stateError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li class="{!IF(ehandler.clog.postalCodeError,'error', '')}">
                                    <label for="thisPage:thisForm:AgentPostalCode"><em>Postal Code</em><span class="required">*</span></label>
                                    <apex:inputField id="AgentPostalCode" value="{!placementAgent.MailingPostalCode}" styleClass="text medium" required="false"/>
                                    <apex:outputPanel rendered="{!ehandler.clog.postalCodeError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li>
                                    <label for="thisPage:thisForm:AgentCountry">Country</label>
                                    <apex:inputField id="AgentCountry" value="{!placementAgent.MailingCountry}" styleClass="text medium"/>
                                </li>
                            </ol>
                        </fieldset>
                    </apex:outputPanel>
                    <ol class="buttons">
                        <li>
                            <apex:commandButton value="Next" action="{!validateReferrerOrAgentInfo}" styleClass="btn btn-primary btn-sm"/>
                        </li>
                        <li>
                            <apex:commandLink value="Previous" action="{!previous}" immediate="true"/>
                        </li>
                        <li>
                            <apex:commandLink value="Cancel" action="{!cancel}" target="_top" immediate="true" onClick="return confirm('Are you sure you want to cancel?');"/>
                        </li>
                    </ol>
                    <div class="bigspacer"></div><div class="bigspacer"></div>
                    <apex:actionFunction name="clearSession" action="{!cancel}" immediate="true" />
                </apex:form>
            </apex:outputPanel>
        </div>
        
        <script src="{!$Resource.IPTS_Resize}" type="text/javascript"/>
        <script src="{!$Resource.IPTS_Session}" type="text/javascript"/>
    </body>
    <DIV class="footer text-center">
      <SPAN class="tagline">We serve those who serve California.</SPAN><BR/>© 
        Copyright 2015 California Public Employees' Retirement System (CalPERS)
        <BR/><BR/>
    </DIV>
 </html>    
</apex:page>