<apex:page cache="false" name="IPTS_InfrastructurePartnership_Terms" id="thisPage" standardStylesheets="false" controller="InvestmentSubmissionController" title="Investment Proposal::{!pageTitle}" showHeader="false" sidebar="false">
<html lang="en">
    <head><title>Investment Proposal::{!pageTitle}</title>

    <apex:stylesheet value="{!URLFOR($Resource.IPTS_Styles, '/ipts/css/reset.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_Styles, '/ipts/css/style.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_CustomStyles)}"/>
    <script src="{!$Resource.IPTS_Script}" type="text/javascript"/>
    <script type="text/javascript">
        function validateFields()
        {
            var fieldIdsArray = new Array();
            fieldIdsArray[0] = new FieldTuple("thisPage:thisForm:ManagementFee", "Management Fee");
            fieldIdsArray[1] = new FieldTuple("thisPage:thisForm:GPCarry", "GP Carry");
            fieldIdsArray[2] = new FieldTuple("thisPage:thisForm:PreferredReturn", "Preferred Return");
            fieldIdsArray[3] = new FieldTuple("thisPage:thisForm:GPCommitmentPercent", "GP Commitment Percent");
            fieldIdsArray[4] = new FieldTuple("thisPage:thisForm:ExpectedAverageLeverage", "Expected Average Leverage");
            return validatePercentFields(fieldIdsArray);
        }
    </script>
    </head>
    <body>
        <div id="mainContent">
            <br/>
            <apex:image url="{!URLFOR($Resource.logo)}" width="201" height="46" alt="CalPERS Logo"  />
            <br/>
            <h1>Investment Proposal Submission</h1>
            
            <h2>Due Diligence Information</h2>
            <br/>

            <apex:outputPanel id="proposalPanel">
                
                <apex:outputPanel rendered="{!ehandler.hasErrors}">
                    <div id="error-message" class="error">
                        <h3>Please correct the highlighted form fields before continuing</h3>
                        <ol>
                            <apex:repeat value="{!ehandler.errors}" var="e">
                                <li>
                                    <a href="#{!e.fieldId}" onclick="document.getElementById('{!e.fieldId}').focus(); return false;">{!e.fieldLabel}</a> - {!e.pageMessage}
                                </li>
                            </apex:repeat>
                        </ol>
                    </div>
                </apex:outputPanel>

                <ul class="legend">
                    <li><em>Required</em><span class="required">*</span></li>
                </ul>
                
                <apex:form id="thisForm" styleClass="cmxform" target="_self">
                    <fieldset>
                        <legend>Investment Terms</legend>
                        <ol>
                            <li class="{!IF(ehandler.olog.managementFeeError,'error', '')}">
                                <label for="thisPage:thisForm:ManagementFee"><em>Management Fee (Percentage)</em><span class="required">*</span></label>
                                <apex:inputField id="ManagementFee" value="{!proposal.Management_Fee__c}" required="false" 
                                    styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                                <apex:outputPanel rendered="{!ehandler.olog.managementFeeError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li>
                                <fieldset>
                                    <legend class="{!IF(isIe,'useaslabel', '')}">Tiered Management Fee offered</legend>
                                    <p><label for="thisPage:thisForm:TieredMgmtFeeOffered:0">If Yes, explain below in Other Fee Narrative</label></p>
                                    <ol>
                                        <li>
                                            <apex:selectRadio id="TieredMgmtFeeOffered" value="{!proposal.Tiered_Management_Fee_Offered__c}" layout="pageDirection">
                                                <apex:selectOptions value="{!radioOptions}" />
                                            </apex:selectRadio>
                                        </li>
                                    </ol>
                                </fieldset>
                            </li>
                            <li class="{!IF(ehandler.olog.gpCarryError,'error', '')}">
                                <label for="thisPage:thisForm:GPCarry"><em>GP Carry (Percentage)</em><span class="required">*</span></label>
                                <apex:inputField id="GPCarry" value="{!proposal.GP_Carry__c}" required="false" 
                                    styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                                <apex:outputPanel rendered="{!ehandler.olog.gpCarryError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.preferredReturnError,'error', '')}">
                                <label for="thisPage:thisForm:PreferredReturn"><em>Preferred Return (Percentage)</em><span class="required">*</span></label>
                                <apex:inputField id="PreferredReturn" value="{!proposal.Preferred_Return__c}" 
                                    required="false" styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                                <apex:outputPanel rendered="{!ehandler.olog.preferredReturnError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.catchupError,'error', '')}">
                                <fieldset>
                                    <legend class="{!IF(isIe,'useaslabel', '')}"><em>Is there a Catch Up?</em><span class="required">*</span></legend>
                                    <p><label for="thisPage:thisForm:Catchup:0">If Yes, explain below in Other Fee Narrative</label></p>
                                    <ol>
                                        <li>
                                            <apex:selectRadio id="Catchup" value="{!proposal.Infrastructure_Catch_up__c}" layout="pageDirection">
                                                <apex:selectOptions value="{!radioOptions}" />
                                            </apex:selectRadio>
                                        </li>
                                    </ol>
                                </fieldset>
                                <apex:outputPanel rendered="{!ehandler.olog.catchupError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.otherFeeNarrativeSizeError,'error', '')}">
                                <label for="thisPage:thisForm:OtherFeeNarrative">Other Fee Narrative - 1000 character limit</label>
                                <apex:inputTextArea id="OtherFeeNarrative" value="{!proposal.Other_Fee_Narrative__c}" styleClass="text" style="width: 550px; height: 170px;"/>
                                <apex:outputPanel rendered="{!ehandler.olog.otherFeeNarrativeSizeError}" style="width: 100%;">
                                    <p class="error">{!sizeExceededMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.fundTermError,'error', '')}">
                                <label for="thisPage:thisForm:FundTerm"><em>Fund Term (In Years)</em><span class="required">*</span></label>
                                <apex:inputField id="FundTerm" value="{!proposal.Fund_Term_In_Years__c}" 
                                    styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                                <apex:outputPanel rendered="{!ehandler.olog.fundTermError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.investmentPeriodError,'error', '')}">
                                <label for="thisPage:thisForm:InvestmentPeriod"><em>Investment Period (In Years)</em><span class="required">*</span></label>
                                <apex:inputField id="InvestmentPeriod" value="{!proposal.Investment_Period_in_Years__c}" 
                                    styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                                <apex:outputPanel rendered="{!ehandler.olog.investmentPeriodError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.gpCommitmentPercentError,'error', '')}">
                                <label for="thisPage:thisForm:GPCommitmentPercent"><em>GP Commitment (Percentage)</em><span class="required">*</span></label>
                                <apex:inputField id="GPCommitmentPercent" value="{!proposal.GP_commitment_percent__c}" 
                                    styleClass="text short" onkeypress="return validateChar2(event, this);" />
                                <apex:outputPanel rendered="{!ehandler.olog.gpCommitmentPercentError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.coInvestRightsError,'error', '')}">
                                <fieldset>
                                    <legend class="{!IF(isIe,'useaslabel', '')}"><em>Co-Investment Rights</em><span class="required">*</span></legend>
                                    <ol>
                                        <li>
                                            <apex:selectRadio id="CoInvestRights" value="{!proposal.Co_investment_rights__c}" layout="pageDirection">
                                                <apex:selectOptions value="{!radioOptions}" />
                                            </apex:selectRadio>
                                        </li>
                                    </ol>
                                </fieldset>
                                <apex:outputPanel rendered="{!ehandler.olog.coInvestRightsError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.advisoryRightsError,'error', '')}">
                                <fieldset>
                                    <legend class="{!IF(isIe,'useaslabel', '')}"><em>Advisory Committee Rights</em><span class="required">*</span></legend>
                                    <ol>
                                        <li>
                                            <apex:selectRadio id="AdvisoryRights" value="{!proposal.Advisory_Committee_Rights__c}" layout="pageDirection">
                                                <apex:selectOptions value="{!radioOptions}" />
                                            </apex:selectRadio>
                                        </li>
                                    </ol>
                                </fieldset>
                                <apex:outputPanel rendered="{!ehandler.olog.advisoryRightsError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.firstCloseError || ehandler.olog.firstCloseFormatError,'error', '')}">
                                <label for="thisPage:thisForm:FirstCloseDate"><em>First Close (Actual or Expected)</em><span class="required">*</span></label>
                                <apex:inputText id="FirstCloseDate" value="{!firstCloseDate}" styleClass="text short" required="false"/>
                                <p><label for="thisPage:thisForm:FirstCloseDate">mm/dd/yyyy</label></p>
                                <apex:outputPanel rendered="{!ehandler.olog.firstCloseFormatError}" style="width: 100%;">
                                    <p class="error">{!invalidDateFormatMessage}</p>
                                </apex:outputPanel>
                                <apex:outputPanel rendered="{!ehandler.olog.firstCloseError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.anticipatedError || ehandler.olog.anticipatedFormatError,'error', '')}">
                                <label for="thisPage:thisForm:Anticipated"><em>Anticipated Fund Final Closing Date</em><span class="required">*</span></label>
                                <apex:inputText id="Anticipated" value="{!anticipatedDate}" styleClass="text short" required="false"/>
                                <p><label for="thisPage:thisForm:Anticipated">mm/dd/yyyy</label></p>
                                <apex:outputPanel rendered="{!ehandler.olog.anticipatedFormatError}" style="width: 100%;">
                                    <p class="error">{!invalidDateFormatMessage}</p>
                                </apex:outputPanel>
                                <apex:outputPanel rendered="{!ehandler.olog.anticipatedError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.fundCapError,'error', '')}">
                                <label for="thisPage:thisForm:FundCap"><em>Fund Cap (In Millions- One billion entered as 1000)</em><span class="required">*</span></label>
                                <apex:inputField id="FundCap" value="{!proposal.Fund_Cap__c}" 
                                    required="false" styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                                <apex:outputPanel rendered="{!ehandler.olog.fundCapError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.averageInvestmentSizeError,'error', '')}">
                                <label for="thisPage:thisForm:AverageInvestmentSize"><em>Average Investment Size (In Millions- One billion entered as 1000)</em><span class="required">*</span></label>
                                <apex:inputField id="AverageInvestmentSize" value="{!proposal.Average_Investment_Size__c}" 
                                    required="false" styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                                <apex:outputPanel rendered="{!ehandler.olog.averageInvestmentSizeError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.expectedNumberOfInvestmentsError,'error', '')}">
                                <label for="thisPage:thisForm:ExpectedInvestmentSize"><em>Expected Number of Investments</em><span class="required">*</span></label>
                                <apex:inputField id="ExpectedInvestmentSize" value="{!proposal.Expected_number_of_Investments__c}" 
                                    required="false" styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                                <apex:outputPanel rendered="{!ehandler.olog.expectedNumberOfInvestmentsError}" style="width: 100%;">
                                    <p class="error">{!inputRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:ExpectedAverageLeverage">Expected Average Leverage (Percentage)</label> 
                                <apex:inputField id="ExpectedAverageLeverage" value="{!proposal.Expected_Average_Leverage__c}" 
                                    required="false" styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                            </li>
                        </ol>
                    </fieldset>
                    <ol class="buttons">
                        <li><apex:commandButton value="Next" action="{!validateInfrastructurePartnershipTermsData}" styleClass="btn btn-primary btn-sm" onClick="return validateFields();" /></li> 
                        <li><apex:commandLink value="Previous" action="{!previous}" immediate="true"/></li>
                        <li><apex:commandButton value="Save and Exit" action="{!validateAndSaveInfrastructurePartnershipTermsData}" styleClass="btn btn-primary btn-sm" onClick="validateFields(); return confirm('{!JSENCODE(saveAlert)}');" /></li>
                        <li><apex:commandLink value="Cancel" action="{!cancel}" target="_top" immediate="true" onClick="return confirm('{!JSENCODE(cancelAlert)}');" /></li>
                    </ol>
                    <div class="spacer"></div>
                    <apex:actionFunction name="clearSession" action="{!cancel}" immediate="true" />
                </apex:form>
            </apex:outputPanel>

        </div>
        
        <script src="{!$Resource.IPTS_Resize}" type="text/javascript"/>
        <script src="{!$Resource.IPTS_Session}" type="text/javascript"/>
        <script type="text/javascript" >
            <apex:outputPanel layout="none" rendered="{!dupeExists}" >
                alert('{!JSENCODE(dupeAlert)}');
            </apex:outputPanel>
            <apex:outputPanel layout="none" rendered="{!!dupeExists && isSaveAndContinue}" >
                window.top.location = "{!cancelURL}";
            </apex:outputPanel>
        </script>
    </body>
    <DIV class="footer text-center">
      <SPAN class="tagline">We serve those who serve California.</SPAN><BR/>© 
        Copyright 2015 California Public Employees' Retirement System (CalPERS)
        <BR/><BR/>
    </DIV>
 </html> 
    
</apex:page>