<apex:page cache="false" name="IPTS_RealizedReturns" id="thisPage" standardStylesheets="false" controller="InvestmentSubmissionController" title="Investment Proposal::{!pageTitle}" showHeader="false" sidebar="false">
	<apex:stylesheet value="{!URLFOR($Resource.IPTS_Styles, '/ipts/css/reset.css')}"/>
	<apex:stylesheet value="{!URLFOR($Resource.IPTS_Styles, '/ipts/css/style.css')}"/>
	<apex:stylesheet value="{!URLFOR($Resource.IPTS_CustomStyles)}"/>
	<script src="{!$Resource.IPTS_Script}" type="text/javascript"/>
	<script type="text/javascript">
    	function validateFields()
    	{
    		var fieldIdsArray = new Array();
    		fieldIdsArray[0] = new FieldTuple("thisPage:thisForm:ProductPercent", "Percent of product assets in composite");
    		fieldIdsArray[1] = new FieldTuple("thisPage:thisForm:ThreeYearExcess", "3 Year Annualized Excess Return");
    		fieldIdsArray[2] = new FieldTuple("thisPage:thisForm:ThreeYearError", "3 Year Tracking Error");
    		fieldIdsArray[3] = new FieldTuple("thisPage:thisForm:ThreeYearRatio", "3 Year Information Ratio");
    		fieldIdsArray[4] = new FieldTuple("thisPage:thisForm:FiveYearExcess", "5 Year Annualized Excess Return");
    		fieldIdsArray[5] = new FieldTuple("thisPage:thisForm:FiveYearError", "5 Year Tracking Error");
    		fieldIdsArray[6] = new FieldTuple("thisPage:thisForm:FiveYearRatio", "5 Year Information Ratio");
    		fieldIdsArray[7] = new FieldTuple("thisPage:thisForm:ExcessReturn", "Since Inception Annualized Excess Return");
    		fieldIdsArray[8] = new FieldTuple("thisPage:thisForm:TrackingError", "Since Inception Tracking Error");
    		fieldIdsArray[9] = new FieldTuple("thisPage:thisForm:Ratio", "Since Inception Information Ratio");
    		return validatePercentFields(fieldIdsArray);
    	}
    </script>
	<body>
		<div id="mainContent">
			<h1>Investment Proposal</h1>
			<apex:outputPanel id="proposalPanel">
				
				<apex:outputPanel rendered="{!ehandler.hasErrors}">
					<div id="error-message" class="error">
					    <h2>Please correct the highlighted form fields before continuing</h2>
					    <ol>
						    <apex:repeat value="{!ehandler.errors}" var="e">
						    	<li>
						    		<a href="#{!e.fieldId}" onclick="document.getElementById('{!e.fieldId}').focus(); return false;">{!e.fieldLabel}</a> - {!e.pageMessage}
						    	</li>
						    </apex:repeat>
					    </ol>
					</div>
				</apex:outputPanel>

				<ul class="legend">
					<li><em>Required</em><span class="required">*</span></li>
				</ul>
				
				<apex:form id="thisForm" styleClass="cmxform" target="_self">
					<fieldset>
						<legend>Realized Return</legend>
						<ol>
							<li class="{!IF(ehandler.olog.productBenchmarkError,'error', '')}">
								<label for="thisPage:thisForm:ProductBenchmark"><em>Product Benchmark</em><span class="required">*</span></label>
								<apex:inputField id="ProductBenchmark" value="{!proposal.Product_Benchmark__c}" 
									required="false" styleClass="text short" />
								<apex:outputPanel rendered="{!ehandler.olog.productBenchmarkError}" style="width: 100%;">
		    						<p class="error">{!inputRequiredMessage}</p>
		    					</apex:outputPanel>
							</li>
							<li class="{!IF(ehandler.olog.percentOfProductError,'error', '')}">
								<label for="thisPage:thisForm:ProductPercent"><em>Percent of product assets in composite</em><span class="required">*</span></label>
								<apex:inputField id="ProductPercent" value="{!proposal.of_product_assets_in_composite__c}" 
									required="false" styleClass="text short" onkeypress="return validateChar2(event, this);"/>
								<apex:outputPanel rendered="{!ehandler.olog.percentOfProductError}" style="width: 100%;">
		    						<p class="error">{!inputRequiredMessage}</p>
		    					</apex:outputPanel>
							</li>
							<li>
								<label for="thisPage:thisForm:ThreeYearExcess">3 Year Annualized Excess Return (Percentage)</label>
								<apex:inputField id="ThreeYearExcess" value="{!proposal.X3_Year_Excess_Return__c}" 
									styleClass="text short" onkeypress="return validateChar2(event, this);"/>
							</li>
							<li>
								<label for="thisPage:thisForm:ThreeYearError">3 Year Tracking Error</label>
								<apex:inputField id="ThreeYearError" value="{!proposal.X3_Year_Tracking_Error__c}" 
									styleClass="text short" onkeypress="return validateChar2(event, this);"/>
							</li>
							<li>
								<label for="thisPage:thisForm:ThreeYearRatio">3 Year Information Ratio</label>
								<apex:inputField id="ThreeYearRatio" value="{!proposal.X3_Year_Information_Ratio__c}" 
									styleClass="text short" onkeypress="return validateChar2(event, this);"/>
							</li>
							<li>
								<label for="thisPage:thisForm:FiveYearExcess">5 Year Annualized Excess Return (Percentage)</label>
								<apex:inputField id="FiveYearExcess" value="{!proposal.X5_Year_Excess_Return__c}" 
									styleClass="text short" onkeypress="return validateChar2(event, this);"/>
							</li>
							<li>
								<label for="thisPage:thisForm:FiveYearError">5 Year Tracking Error</label>
								<apex:inputField id="FiveYearError" value="{!proposal.X5_Year_Tracking_Error__c}" 
									styleClass="text short" onkeypress="return validateChar2(event, this);"/>
							</li>
							<li>
								<label for="thisPage:thisForm:FiveYearRatio">5 Year Information Ratio</label>
								<apex:inputField id="FiveYearRatio" value="{!proposal.X5_Year_Information_Ratio__c}" 
									styleClass="text short" onkeypress="return validateChar2(event, this);"/>
							</li>
							<li class="{!IF(ehandler.olog.sinceInceptionExcessReturnError,'error', '')}">
								<label for="thisPage:thisForm:ExcessReturn"><em>Since Inception Annualized Excess Return (Percentage)</em><span class="required">*</span></label>
								<apex:inputField id="ExcessReturn" value="{!proposal.Since_Inception_Excess_Return__c}" 
									required="false" styleClass="text short" onkeypress="return validateChar2(event, this);"/>
								<apex:outputPanel rendered="{!ehandler.olog.sinceInceptionExcessReturnError}" style="width: 100%;">
		    						<p class="error">{!inputRequiredMessage}</p>
		    					</apex:outputPanel>
							</li>
							<li class="{!IF(ehandler.olog.sinceInceptionTrackingError,'error', '')}">
								<label for="thisPage:thisForm:TrackingError"><em>Since Inception Tracking Error</em><span class="required">*</span></label>
								<apex:inputField id="TrackingError" value="{!proposal.Since_Inception_Tracking_Error__c}" 
									required="false" styleClass="text short" onkeypress="return validateChar2(event, this);"/>
								<apex:outputPanel rendered="{!ehandler.olog.sinceInceptionTrackingError}" style="width: 100%;">
		    						<p class="error">{!inputRequiredMessage}</p>
		    					</apex:outputPanel>
							</li>
							<li>
								<label for="thisPage:thisForm:Ratio">Since Inception Information Ratio</label>
								<apex:inputField id="Ratio" value="{!proposal.Since_Inception_Information_Ratio__c}" 
									styleClass="text short" onkeypress="return validateChar2(event, this);"/>
							</li>
						</ol>
					</fieldset>
					<ol class="buttons">
						<li><apex:commandButton value="Next" action="{!validateRealizedReturns}" styleClass="submit" onClick="return validateFields();" /></li> 
						<li><apex:commandLink value="Previous" action="{!previous}" immediate="true"/></li>
						<li><apex:commandButton value="Save and Exit" action="{!validateAndSaveRealizedReturns}" styleClass="submit" onClick="validateFields(); return confirm('{!JSENCODE(saveAlert)}');" /></li>
						<li><apex:commandLink value="Cancel" action="{!cancel}" target="_top" immediate="true" onClick="return confirm('{!JSENCODE(cancelAlert)}');" /></li>
					</ol>
					<apex:actionFunction name="clearSession" action="{!cancel}" immediate="true" />
				</apex:form>
			</apex:outputPanel>

		</div>
		<iframe id="grandChild" style="display: none" title="Frame to display the Investment Proposal form."></iframe>
		<script src="{!$Resource.IPTS_Resize}" type="text/javascript"/>
		<script src="{!$Resource.IPTS_Session}" type="text/javascript"/>
		<script type="text/javascript" >
			<apex:outputPanel layout="none" rendered="{!dupeExists}" >
				alert('{!JSENCODE(dupeAlert)}');
			</apex:outputPanel>
			<apex:outputPanel layout="none" rendered="{!!dupeExists && isSaveAndContinue}" >
				window.top.location = "{!cancelURL}";
			</apex:outputPanel>
		</script>
	</body>
</apex:page>