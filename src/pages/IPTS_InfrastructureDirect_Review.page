<apex:page cache="false" name="IPTS_InfrastructureDirect_Review" id="thisPage" controller="InvestmentSubmissionController" title="Investment Proposal" showHeader="false" sidebar="false" standardStylesheets="false">
<html lang="en">
    <head><title>Investment Proposal::{!pageTitle}</title>

    <apex:stylesheet value="{!URLFOR($Resource.IPTS_Styles, '/ipts/css/reset.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_Styles, '/ipts/css/style.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_CustomStyles)}"/>
    </head>
    <body>
        <div id="mainContent">
            <br/>
            <apex:image url="{!URLFOR($Resource.logo)}" width="201" height="46" alt="CalPERS Logo"  />
            <br/>
            <h1>Investment Proposal Submission</h1>

            <apex:outputPanel rendered="{!ehandler.hasErrors}">
                <div id="error-message" class="error">
                    <h3>Please correct the highlighted form fields before continuing</h3>
                    <ol>
                        <apex:repeat value="{!ehandler.errors}" var="e">
                            <li>
                                <a href="#{!e.fieldId}" onclick="document.getElementById('{!e.fieldId}').focus(); return false;">{!e.fieldLabel}</a> - {!e.pageMessage}
                            </li>
                        </apex:repeat>
                    </ol>
                </div>
            </apex:outputPanel>
            <ul class="legend">
                <li><em>Required</em><span class="required">*</span></li>
            </ul>
            <apex:outputPanel id="proposalPanel">
                <apex:form id="thisForm" styleClass="cmxform" target="_self">
                    <fieldset>
                        <ol> 
                            <li>
                                <label for="thisPage:thisForm:PriorConfirmationNumber">If you are resubmitting an opportunity, what is your prior confirmation number?</label>
                                <apex:outputText id="PriorConfirmationNumber" value="{!ohelper.priorConfirmationNumber}" styleClass="text"/>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:HowDidYouHearAboutUs">Please tell us how you were referred to CalPERS.</label>
                                <apex:outputText id="HowDidYouHearAboutUs" value="{!proposal.How_did_you_hear_about_us__c}" styleClass="text"/>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:LeadSource">In an effort to better understand how investment opportunities are sourced, please select the appropriate option?</label>
                                <p><apex:outputField id="LeadSource" value="{!proposal.LeadSource}" /></p>
                            </li>
                        </ol>
                    </fieldset>
                
                    <apex:outputPanel rendered="{!isReferred}">
                        <fieldset>
                            <legend>Referrer Information</legend>
                            <ol>
                                <li>
                                    <label>Referrer Name</label>
                                    <p><apex:outputText id="ReferrerName" value="{!referrerName}" styleClass="text"/></p>
                                </li>
                                <li>
                                    <label for="thisPage:thisForm:ReferrerPhone">Phone</label>
                                    <p><apex:outputField id="ReferrerPhone" value="{!referrer.Phone}"  styleClass="text"/></p>
                                </li>
                                <li>
                                    <label for="thisPage:thisForm:ReferrerEmail">Email</label>
                                    <p><apex:outputField id="ReferrerEmail" value="{!referrer.Email}"  styleClass="text"/></p>
                                </li>
                                <li>
                                    <label for="thisPage:thisForm:ReferrerStreet">Street</label>
                                    <p><apex:outputField id="ReferrerStreet" value="{!referrer.MailingStreet}" style="width: 250px;" styleClass="text" /></p>
                                </li>
                                <li>
                                    <label for="thisPage:thisForm:ReferrerCity">City</label>
                                    <p><apex:outputField id="ReferrerCity" value="{!referrer.MailingCity}" styleClass="text" /></p>
                                </li>
                                <li>
                                    <label for="thisPage:thisForm:ReferrerState">State/Region</label>
                                    <p><apex:outputField id="ReferrerState" value="{!referrer.MailingState}" styleClass="text" /></p>
                                </li>
                                <li>
                                    <label for="thisPage:thisForm:ReferrerPostalCode">Postal Code</label>
                                    <p><apex:outputField id="ReferrerPostalCode" value="{!referrer.MailingPostalCode}" styleClass="text" /></p>
                                </li>
                                <li>
                                    <label for="thisPage:thisForm:ReferrerCountry">Country</label>
                                    <p>
                                        <apex:outputField id="ReferrerCountry" value="{!referrer.MailingCountry}" styleClass="text" rendered="{!referrer.MailingCountry != null}"/>
                                        <apex:outputText value="{!emptyMessage}" rendered="{!referrer.MailingCountry == null}" styleClass="text"/>
                                    </p>
                                </li>
                            </ol>
                        </fieldset>
                    </apex:outputPanel>
                    
                    <apex:outputPanel rendered="{!isReferredByPlacementAgent || isPlacementAgent}">
                        <fieldset>
                            <legend>Placement Agent Information</legend>
                            <ol>
                                <li>
                                    <label for="thisPage:thisForm:AgentName">Placement Agent Name</label>
                                    <p><apex:outputText id="AgentName" value="{!placementAgentName}" styleClass="text"/></p>
                                </li>
                                <li>
                                    <label for="thisPage:thisForm:AgentPhone">Phone</label>
                                    <p><apex:outputField id="AgentPhone" value="{!placementAgent.Phone}"  styleClass="text"/></p>
                                </li>
                                <li>
                                    <label for="thisPage:thisForm:AgentEmail">Email</label>
                                    <p><apex:outputField id="AgentEmail" value="{!placementAgent.Email}"  styleClass="text"/></p>
                                </li>
                                <li>
                                    <label for="thisPage:thisForm:AgentStreet">Street</label>
                                    <p><apex:outputField id="AgentStreet" value="{!placementAgent.MailingStreet}" styleClass="text" style="width: 250px;" /></p>
                                </li>
                                <li>
                                    <label for="thisPage:thisForm:AgentCity">City</label>
                                    <p><apex:outputField id="AgentCity" value="{!placementAgent.MailingCity}" styleClass="text" /></p>
                                </li>
                                <li>
                                    <label for="thisPage:thisForm:AgentState">State/Region</label>
                                    <p><apex:outputField id="AgentState" value="{!placementAgent.MailingState}" styleClass="text" /></p>
                                </li>
                                <li>
                                    <label for="thisPage:thisForm:AgentPostalCode">Postal Code</label>
                                    <p><apex:outputField id="AgentPostalCode" value="{!placementAgent.MailingPostalCode}" styleClass="text" /></p>
                                </li>
                                <li>
                                    <label for="thisPage:thisForm:AgentCountry">Country</label>
                                    <p>
                                        <apex:outputField id="AgentCountry" value="{!placementAgent.MailingCountry}" styleClass="text" rendered="{!placementAgent.MailingCountry != null}"/>
                                        <apex:outputText value="{!emptyMessage}" rendered="{!placementAgent.MailingCountry == null}" styleClass="text" />
                                    </p>
                                </li>
                            </ol>
                        </fieldset>
                    </apex:outputPanel>
                    
                    <fieldset>
                        <legend>Asset Class and Investment Type</legend>
                        <ol>
                            <li>
                                <fieldset>
                                    <label for="thisPage:thisForm:AssetClass">Asset Class</label>  
                                    <p><apex:outputField id="AssetClass" value="{!proposal.Admin_Asset_Class__c}" /></p>
                                </fieldset>
                            </li>
                            <li>
                                <fieldset>
                                    <label for="thisPage:thisForm:InvestmentType">Investment Type</label>
                                    <p><apex:outputField id="InvestmentType" value="{!proposal.Admin_Type__c}" /></p>
                                </fieldset>
                            </li>
                        </ol>
                    </fieldset>
                    
                    <fieldset>
                        <legend>Firm Information</legend>
                        <ol>
                            <li>
                                <label for="thisPage:thisForm:AccountName">Partner or Proposing Entity Name</label>
                                <p><apex:outputField id="AccountName" value="{!submittorAccount.Name}"  style="width: 550px;" styleClass="text"/></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:Website">Web site</label>
                                <p>
                                    <apex:outputField id="Website" value="{!submittorAccount.Website}" styleClass="text" rendered="{!submittorAccount.Website != null}"/>
                                    <apex:outputText value="{!emptyMessage}" styleClass="text" rendered="{!submittorAccount.Website == null}"/>
                                </p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:Fax">Fax</label>
                                <p>
                                    <apex:outputField id="Fax" value="{!submittorAccount.Fax}"  rendered="{!submittorAccount.Fax != null}" styleClass="text"/>
                                    <apex:outputText value="{!emptyMessage}"  rendered="{!submittorAccount.Fax == null}" styleClass="text"/>
                                </p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:Street">Street</label>
                                <p><apex:outputField id="Street" value="{!submittorAccount.BillingStreet}" style="width: 250px;"  styleClass="text"/></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:City">City</label>
                                <p><apex:outputField id="City" value="{!submittorAccount.BillingCity}"  styleClass="text"/></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:State">State</label>
                                <p><apex:outputField id="State" value="{!submittorAccount.BillingState}"  styleClass="text"/></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:PostalCode">Postal Code</label>
                                <p><apex:outputField id="PostalCode" value="{!submittorAccount.BillingPostalCode}"   styleClass="text"/></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:Country">Country</label>
                                <p><apex:outputField id="Country" value="{!submittorAccount.BillingCountry}"   styleClass="text"/></p>
                            </li>

                            <li>
                                <label for="thisPage:thisForm:SubmitterContactName">Submitter Contact Information</label>
                                <p><apex:outputText id="SubmitterContactName" value="{!submitterName}" styleClass="text"/></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:SubmitterTitle">Title</label>
                                <p><apex:outputField id="SubmitterTitle" value="{!submitter.Title}"  styleClass="text"/></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:SubmitterPhone">Phone</label>
                                <p><apex:outputField id="SubmitterPhone" value="{!submitter.Phone}"  styleClass="text"/></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:SubmitterEmail">Email</label>
                                <p><apex:outputField id="SubmitterEmail" value="{!submitter.Email}"  styleClass="text"/></p>
                            </li>

                            <li>
                                <label for="thisPage:thisForm:ContactName">Firm Primary Contact Information</label>
                                <p><apex:outputText id="ContactName" value="{!submittorName}" styleClass="text"/></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:Title">Title</label>
                                <p><apex:outputField id="Title" value="{!submittor.Title}"  styleClass="text"/></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:Phone">Phone</label>
                                <p><apex:outputField id="Phone" value="{!submittor.Phone}"  styleClass="text"/></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:Email">Email</label>
                                <p><apex:outputField id="Email" value="{!submittor.Email}"  styleClass="text"/></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:HasRels">Does your firm, or the firm you represent, have an existing relationship with CalPERS?</label>
                                <p>
                                    <apex:outputField id="HasRels" styleClass="text" value="{!submittorAccount.Do_You_Have_Existing_Relationship__c}" rendered="{!submittorAccount.Do_You_Have_Existing_Relationship__c != null}" />
                                    <apex:outputText styleClass="text" value="{!emptyMessage}" rendered="{!submittorAccount.Do_You_Have_Existing_Relationship__c == null}" />
                                </p>
                            </li>
                            <li>
                                <apex:outputPanel rendered="{!renderPriorRelationshipClasses}" style="width: 100%;">
                                <fieldset>
                                    <label for="thisPage:thisForm:RelAssetClass">If so, through which asset class(es)?</label>
                                    <ol>
                                        <apex:repeat value="{!SelectedExistingRelAssetClasses}" var="mso">
                                            <li>
                                                <p><apex:outputText id="RelAssetClass" value="{!mso}" /></p>
                                            </li>
                                        </apex:repeat>
                                    </ol>
                                </fieldset>
                                </apex:outputPanel>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:FirmInception">Firm Inception Date</label>
                                <p><apex:outputText id="FirmInception" value="{!ahelper.inceptionDate}" styleClass="text"/></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:Aum">Assets Under Management (USD Millions- One billion entered as 1000)</label>
                                <p><apex:outputText id="Aum" value="{!ohelper.AssetUnderMgmt}"  styleClass="text"/></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:BriefHistory">Brief Firm History</label>
                                <p><apex:outputText id="BriefHistory" value="{!proposal.Brief_Firm_History__c}" style="width: 550px;" styleClass="text" /></p>
                            </li>
                        </ol>
                    </fieldset>
                    <fieldset>
                        <legend>Diversity Definitions and Questionnaire</legend>
                        <ol>
                            <li>
                                <label for="thisPage:thisForm:DiverseBusiness">Please indicate if your firm meets the definition referenced above of a Diverse Investment Management Business.</label>
                                <p><apex:outputField id="DiverseBusiness" value="{!submittorAccount.Diverse_Investment_Management_Business__c}" /></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:HQinUS">Is your firm physically headquartered in the United States or its trust territories?</label>
                                <p><apex:outputField id="HQinUS" value="{!submittorAccount.HQ_in_US__c}" /></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:PctWomenOwned">Percent of the firm owned by women employees</label>
                                <p>
                                    <apex:outputField id="PctWomenOwned" value="{!submittorAccount.Range_Women_Owned__c}"  rendered="{!submittorAccount.Range_Women_Owned__c != null}" styleClass="text"/>
                                    <apex:outputText value="{!emptyMessage}"  rendered="{!submittorAccount.Range_Women_Owned__c == null}" styleClass="text"/>
                                </p>
                            </li>           
                            <ol>             
                            <li>
                                <label for="thisPage:thisForm:AfricanAmerican">African American</label>
                                <p>
                                    <apex:outputField id="AfricanAmerican" value="{!submittorAccount.Range_African_American__c}"  rendered="{!submittorAccount.Range_African_American__c != null}" styleClass="text"/>
                                    <apex:outputText value="{!emptyMessage}"  rendered="{!submittorAccount.Range_African_American__c == null}" styleClass="text"/>
                                </p>
                            </li>                        
                            <li>
                                <label for="thisPage:thisForm:AsianAmerican">Asian American</label>
                                <p>
                                    <apex:outputField id="AsianAmerican" value="{!submittorAccount.Range_Asian_American__c}"  rendered="{!submittorAccount.Range_Asian_American__c != null}" styleClass="text"/>
                                    <apex:outputText value="{!emptyMessage}"  rendered="{!submittorAccount.Range_Asian_American__c == null}" styleClass="text"/>
                                </p>
                            </li>                        
                            <li>
                                <label for="thisPage:thisForm:HispanicAmerican">Hispanic American</label>
                                <p>
                                    <apex:outputField id="HispanicAmerican" value="{!submittorAccount.Range_Hispanic_American__c}"  rendered="{!submittorAccount.Range_Hispanic_American__c != null}" styleClass="text"/>
                                    <apex:outputText value="{!emptyMessage}"  rendered="{!submittorAccount.Range_Hispanic_American__c == null}" styleClass="text"/>
                                </p>
                            </li>                        
                            <li>
                                <label for="thisPage:thisForm:NativeAmerican">Native American</label>
                                <p>
                                    <apex:outputField id="NativeAmerican" value="{!submittorAccount.Range_Native_American__c}"  rendered="{!submittorAccount.Range_Native_American__c != null}" styleClass="text"/>
                                    <apex:outputText value="{!emptyMessage}"  rendered="{!submittorAccount.Range_Native_American__c == null}" styleClass="text"/>
                                </p>
                            </li>                        
                            <li>
                                <label for="thisPage:thisForm:OtherTitle">Other</label>
                                <p>
                                    <apex:outputField id="OtherTitle" value="{!submittorAccount.Range_Other_Title__c}"  rendered="{!submittorAccount.Range_Other_Title__c != null}" styleClass="text"/>
                                    <apex:outputText value="{!emptyMessage}"  rendered="{!submittorAccount.Range_Other_Title__c == null}" styleClass="text"/>
                                    <apex:outputField id="OtherRange" value="{!submittorAccount.Range_Other__c}"  rendered="{!submittorAccount.Range_Other__c != null}" styleClass="text"/>
                                    <apex:outputText value="{!emptyMessage}"  rendered="{!submittorAccount.Range_Other__c == null}" styleClass="text"/>
                                </p>
                            </li>   
                            </ol>
                            <li>
                                <label for="thisPage:thisForm:PctTotal">Total percent owned combining gender, race, and ethnicity</label>
                                <p>
                                    <apex:outputField id="PctTotal" value="{!submittorAccount.Range_Diversity__c}"  rendered="{!submittorAccount.Range_Diversity__c != null}" styleClass="text"/>
                                    <apex:outputText value="{!emptyMessage}"  rendered="{!submittorAccount.Range_Diversity__c == null}" styleClass="text"/>
                                </p>
                            </li>                        
                                                 
                        </ol>
                    </fieldset>
                    
                    <fieldset>
                        <legend>Investment Opportunity Information</legend>
                        <ol>
                            <li>
                                <label for="thisPage:thisForm:OpptyName">Investment/Project Name</label>
                                <p><apex:outputField id="OpptyName" value="{!proposal.Name}" style="width: 550px;" /></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:DealSizeUSD">Total Estimated Deal Size in USD (In Millions- One billion entered as 1000)</label>
                                <p><apex:outputText id="DealSizeUSD" value="{!proposal.Total_Deal_Size_USD_In_Millions__c}" styleClass="text"/></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:Currency">Select Currency to be used for fields below</label>
                                <p><apex:outputField id="Currency" value="{!proposal.Local_Currency__c}" /></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:DealSize">Total Estimated Deal Size in Local Currency (In Millions- One billion entered as 1000)</label>
                                <p><apex:outputText id="DealSize" value="{!proposal.Total_Deal_Size_In_Millions__c}" styleClass="text"/></p>
                            </li>
                            <li>
                                <fieldset>
                                    <label for="thisPage:thisForm:Strategy">Offering</label>
                                    <ol>
                                        <apex:repeat value="{!SelectedStrategyOptions}" var="mso">
                                            <li>
                                                <p><apex:outputText id="Strategy" value="{!mso}" /></p>
                                            </li>
                                        </apex:repeat>
                                    </ol>
                                </fieldset>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:CAFocus">CA Focus</label>
                                <p><apex:outputField id="CAFocus" value="{!proposal.CA_Focus__c}" /></p>
                            </li>
                            <li>
                                <fieldset>
                                    <label for="thisPage:thisForm:GeoFocus">Geographic Focus</label>
                                    <ol>
                                        <apex:repeat value="{!selectedGeoFocusOptions}" var="mso">
                                            <li>
                                                <p><apex:outputText id="GeoFocus" value="{!mso}" /></p>
                                            </li>
                                        </apex:repeat>
                                    </ol>
                                </fieldset>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:IfDevelopedSpecifyCountryName">If Developed OECD ex US or Less Developed, please specify Country Name</label>
                                <p><apex:outputField id="IfDevelopedSpecifyCountryName" value="{!proposal.Country_Name__c}"  styleClass="text"/></p>
                            </li>
                            <li>
                                <fieldset>
                                    <label for="thisPage:thisForm:IndustryFocus">Industry Focus</label>
                                    <ol>
                                        <apex:repeat value="{!SelectedIndustryFocusOptions}" var="mso"> 
                                            <li>
                                                <p><apex:outputText id="IndustryFocus" value="{!mso}" /></p>
                                            </li>
                                        </apex:repeat>
                                    </ol>
                                </fieldset>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:ExecutiveSummary">Concise Description of Proposal/Opportunity</label>
                                <p><apex:outputText id="ExecutiveSummary" value="{!proposal.Executive_Summary__c}" style="width: 550px;"  styleClass="text"/></p>
                            </li>
                        </ol>
                    </fieldset>
                    
                    <fieldset>
                        <legend>Investment Terms</legend>
                        <ol>
                            <li>
                                <label for="thisPage:thisForm:OtherFees">Please Describe Fees, if any</label>
                                <p>
                                    <apex:outputField id="OtherFees" value="{!proposal.Other_Fee_Narrative__c}" rendered="{!proposal.Other_Fee_Narrative__c != null}" styleClass="text"/>
                                    <apex:outputText value="{!emptyMessage}" rendered="{!proposal.Other_Fee_Narrative__c == null}" styleClass="text"/>
                                </p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:PreferredReturn">Target Return</label>
                                <p><apex:outputField id="PreferredReturn" value="{!proposal.Preferred_Return__c}"  styleClass="text"/></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:AnticipatedFinalClosingDate">Anticipated Final Closing Date</label>
                                <p><apex:outputField id="AnticipatedFinalClosingDate" value="{!proposal.Anticipated_Fund_Final_Closing_Date__c}"  styleClass="text"/></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:ProposedCalPERS">Proposed CalPERS Investment (In Millions- One billion entered as 1000)</label>
                                <apex:outputText id="ProposedCalPERS" value="{!ohelper.proposedInvestment}" styleClass="text"/>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:HoldingPeriod">Proposed Holding Period (In Years)</label>
                                <p><apex:outputText id="HoldingPeriod" value="{!ohelper.proposedHoldingPeriod}" styleClass="text"/></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:EquityAmount">Equity Amount (In Millions- One billion entered as 1000)</label>
                                <p><apex:outputText id="EquityAmount" value="{!ohelper.equity}" styleClass="text"/></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:EquityInterest">If Equity, Percent Equity Interest being offered (Percentage)</label>
                                <p><apex:outputText id="EquityInterest" value="{!ohelper.ownershipPercent}" styleClass="text"/></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:LTM">LTM Revenue (In Millions- One billion entered as 1000)</label>
                                <p><apex:outputText id="LTM" value="{!ohelper.LTMRevenue}" styleClass="text"/></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:LTMEBITDA">LTM EBITDA (In Millions- One billion entered as 1000)</label>
                                <p><apex:outputText id="LTMEBITDA" value="{!ohelper.LTMEBITDA}" styleClass="text"/></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:EnterpriseValue">Enterprise Value (In Millions- One billion entered as 1000)</label>
                                <p><apex:outputText id="EnterpriseValue" value="{!ohelper.totalValue}" styleClass="text"/></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:Leverage">Leverage (Percentage)</label>
                                <p><apex:outputText id="Leverage" value="{!ohelper.leverage}" styleClass="text"/></p>
                            </li>
                            <li>
                                <label for="thisPage:thisForm:AdditionalInfo">Additional Information</label>
                                <p>
                                    <apex:outputText id="AdditionalInfo" value="{!proposal.Timing_Considerations__c}" rendered="{!proposal.Timing_Considerations__c != null}" style="width: 550px;" styleClass="text" />
                                    <apex:outputText value="{!emptyMessage}" rendered="{!proposal.Timing_Considerations__c == null}" styleClass="text" />
                                </p>
                            </li>
                        </ol>
                    </fieldset>
                    
                    <fieldset>
                        <legend>Attachments</legend>
                        <ol>
                            <li>
                                <label for="thisPage:thisForm:PPMOM">Do you have a PPM or OM</label>
                                <p>
                                    <apex:outputField id="PPMOM" value="{!proposal.Do_you_have_a_PPM_or_OM__c}" rendered="{!proposal.Do_you_have_a_PPM_or_OM__c != null}" styleClass="text"/>
                                    <apex:outputText value="{!emptyMessage}" rendered="{!proposal.Do_you_have_a_PPM_or_OM__c == null}" styleClass="text"/>
                                </p>
                            </li>
                            
                            <li>
                                <label for="thisPage:thisForm:Presentation">Do you have any Presentation Materials</label>
                                <p>
                                    <apex:outputField id="Presentation" value="{!proposal.Do_you_have_any_presentation_materials__c}" rendered="{!proposal.Do_you_have_any_presentation_materials__c != null}" styleClass="text"/>
                                    <apex:outputText value="{!emptyMessage}" rendered="{!proposal.Do_you_have_any_presentation_materials__c == null}" styleClass="text"/>
                                </p>
                            </li>
                            <li>
                                <fieldset>
                                    <ol>
                                        <li>
                                            <apex:outputPanel rendered="{!NOT(attachManager.hasAttachments)}" style="width: 100%">
                                                <p>No attachment(s) added to proposal.</p>
                                            </apex:outputPanel>
                                        </li>
                                        <apex:outputPanel rendered="{!attachManager.hasAttachments}" style="width: 100%">
                                            <fieldset>
                                                <label for="thisPage:thisForm:FileName"><b>Files Attached</b></label>
                                                <ol>
                                                <apex:repeat value="{!attachManager.attachments}" var="currAttachment">
                                                    <li>
                                                        <p><apex:outputText id="FileName" value="{!currAttachment.fileName}"/></p>
                                                    </li>
                                                </apex:repeat>
                                                </ol>
                                            </fieldset>
                                        </apex:outputPanel>
                                    </ol>
                                </fieldset>
                            </li>
                        </ol>
                    </fieldset>
                    <fieldset>
                        <legend><em>Acknowledgment</em><span class="required">*</span></legend>
                        <p><label for="thisPage:thisForm:confirmChbx">You acknowledge that you have read the Notes on Confidentiality and understand that CalPERS is subject to disclosure laws, regulations, and policies that may require disclosure of some or all of the information you have submitted for consideration by CalPERS.</label></p>
                        <ol>
                            <li class="{!IF(ehandler.confirmHasError,'error', '')}">
                                <apex:inputCheckBox id="confirmChbx" value="{!reviewConfirm}" styleClass="checkbox" title="I have read and understand the above information"/>
                                <label for="thisPage:thisForm:confirmChbx" class="inlinelabel">I have read and understand the above information</label>
                                <apex:outputPanel rendered="{!ehandler.confirmHasError}" style="width: 100%;">
                                    <p class="error">{!ehandler.confirmError.fieldMessage}</p>
                                </apex:outputPanel>
                            </li>
                        </ol>   
                    </fieldset>
                    <p>Note: Please click the “Submit Proposal” button only once to avoid duplicate entries. Please note it may take up to a minute for the page to refresh. You will have the opportunity to print your submitted proposal.  After clicking ‘Submit Proposal’, you will no longer be able to review or make changes to this proposal.</p>
                    <ol class="buttons">
                        <li><apex:commandButton value="Submit Proposal" action="{!submitProposal}" styleClass="btn btn-primary btn-sm"/></li>
                        <li><apex:commandLink value="Previous" action="{!previous}" immediate="true"/></li>
                         <li><apex:commandButton value="Save and Exit" action="{!saveAndExit}" styleClass="btn btn-primary btn-sm" onClick="return confirm('{!JSENCODE(saveAlert)}');" /></li>
                        <li><apex:commandLink value="Cancel" action="{!cancel}" target="_top" immediate="true" onClick="return confirm('{!JSENCODE(cancelAlert)}');" /></li>
                    </ol>
                    <div class="spacer"></div>
                    <apex:actionFunction name="clearSession" action="{!cancel}" immediate="true" />
                </apex:form>
            </apex:outputPanel>
        </div>
       
        <script src="{!$Resource.IPTS_Resize}" type="text/javascript"/>
        <script src="{!$Resource.IPTS_Session}" type="text/javascript"/>
        <script type="text/javascript" >
            <apex:outputPanel layout="none" rendered="{!dupeExists}" >
                alert('{!JSENCODE(dupeAlert)}');
            </apex:outputPanel>
            <apex:outputPanel layout="none" rendered="{!!dupeExists && isSaveAndContinue}" >
                window.top.location = "{!cancelURL}";
            </apex:outputPanel>
        </script>
    </body>
    <DIV class="footer text-center">
      <SPAN class="tagline">We serve those who serve California.</SPAN><BR/>© 
        Copyright 2015 California Public Employees' Retirement System (CalPERS)
        <BR/><BR/>
    </DIV>
 </html> 
    
</apex:page>