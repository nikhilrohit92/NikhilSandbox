<apex:page cache="false" name="IPTS_InfrastructureDirect_Print" id="thisPage" renderAs="PDF" controller="InvestmentSubmissionController" title="Investment Proposal" showHeader="false" sidebar="false" standardStylesheets="false">
<html lang="en">
    <head><title>Investment Proposal::{!pageTitle}</title>

    <style type="text/css">
        html, body, div, span, applet, object, iframe,
        h1, h2, h3, h4, h5, h6, p, blockquote, pre,
        a, abbr, acronym, address, big, cite, code,
        del, dfn, em, font, img, ins, kbd, q, s, samp,
        small, strike, strong, sub, sup, tt, var,
        b, u, i, center,
        dl, dt, dd, ol, ul, li,
        fieldset, form, label, legend,
        table, caption, tbody, tfoot, thead, tr, th, td {
            margin: 0;
            padding: 0;
            border: 0;
            outline: 0;
            font-size: 100%;
            vertical-align: baseline;
            background: transparent;
        }
        body {
            line-height: 1.4em;
        }
                
        /* tables still need 'cellspacing="0"' in the markup */
        table {
            border-collapse: collapse;
            border-spacing: 0;
        }
                
                
        body {
            background: #ffffff;
            color: #003366;
            font: 62.5%/1.5 Verdana, Geneva, sans-serif;
        }
        
        h1 {
            font-size: 1.8em;   
            margin: 0 0 .8em 0;
            color: #336600;
            font-family: "Times New Roman", Times, serif;
        }
        
        h2 {
            font-size: 1.2em;   
            
        }
        
        p {
            font-size: 1.1em;   
            margin: 0 0 1.2em 0;    
        }
        
        form.cmxform hr
        {
            border: 0px;
            background-color: #dddddd;
            height: 1px;
            color: #dddddd;
        }
        
        #mainContent {
            text-align: left; 
            padding: 0 1em; 
        }
        
        form.cmxform {
            border-top: .2em solid #dddddd;
            padding: 0;
        }
        
        form.cmxform p {
            margin: 0;  
        }
        
        
        form.cmxform label {
            display: block; 
            margin: 0 0 0 0;
            font-weight: bold;
            cursor: pointer;
            font-size: 1.1em;
        }
        
        form.cmxform table 
        {
            width: 100%;
            border: 0px;    
        }
        form.cmxform table tr 
        {
            /*height: 4em;*/    
        }
        form.cmxform table tr td{
            padding: 0 0 1em 1em;
            
        }
        form.cmxform table tr th {      
            font-weight: bold;
            padding: 0 0 1em 0;
            margin: 0;
            font-size: 1.2em;
        }
    </style>
    </head>
    <body>
        <div id="mainContent">
            <h1>Investment Proposal</h1>
            <apex:outputPanel id="proposalPanel">
                <apex:form id="thisForm" styleClass="cmxform" target="_self">
                    <table> 
                        <tr>
                            <th>Confirmation and Opportunity Source Information</th>
                        </tr>
                        <tr>
                            <td>
                                <label>Confirmation Number</label>
                                <apex:outputText value="{!proposal.Confirmation_Number__c}" styleClass="text" rendered="{!$CurrentPage.Parameters.conf == null}"/>
                                <apex:outputText value="{!$CurrentPage.Parameters.conf}" styleClass="text" rendered="{!$CurrentPage.Parameters.conf != null}" />
                            </td>
                        </tr>
                        <tr>    
                            <td>
                                <label>If you are resubmitting an opportunity, what is your prior confirmation number?</label>
                                <apex:outputText value="{!ohelper.priorConfirmationNumber}" styleClass="text"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Please tell us how you were referred to CalPERS.</label>
                                <p><apex:outputField value="{!proposal.How_did_you_hear_about_us__c}" /></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>In an effort to better understand how investment opportunities are sourced, please select the appropriate option?</label>
                                <p><apex:outputField value="{!proposal.LeadSource}" /></p>
                            </td>
                        </tr>
                    </table>
                    <hr/>   
                    <apex:outputPanel rendered="{!isReferred}">
                        <table>
                            <tr>
                                <th>Referrer Information</th>
                            </tr>
                            <tr>
                                <td>
                                    <label>Referrer Name</label>
                                    <p><apex:outputText value="{!referrerName}" styleClass="text"/></p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Phone</label>
                                    <p><apex:outputField id="ReferrerPhone" value="{!referrer.Phone}"  styleClass="text"/></p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Email</label>
                                    <p><apex:outputField id="ReferrerEmail" value="{!referrer.Email}"  styleClass="text"/></p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Street</label>
                                    <p><apex:outputField id="ReferrerStreet" value="{!referrer.MailingStreet}" style="width: 250px;" styleClass="text" /></p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>City</label>
                                    <p><apex:outputField id="ReferrerCity" value="{!referrer.MailingCity}" styleClass="text" /></p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>State/Region</label>
                                    <p><apex:outputField id="ReferrerState" value="{!referrer.MailingState}" styleClass="text" /></p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Postal Code</label>
                                    <p><apex:outputField id="ReferrerPostalCode" value="{!referrer.MailingPostalCode}" styleClass="text" /></p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Country</label>
                                    <p>
                                        <apex:outputField id="ReferrerCountry" value="{!referrer.MailingCountry}" styleClass="text" rendered="{!referrer.MailingCountry != null}"/>
                                        <apex:outputText value="{!emptyMessage}" rendered="{!referrer.MailingCountry == null}" styleClass="text"/>
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <hr/>
                    </apex:outputPanel>
                    
                    <apex:outputPanel rendered="{!isReferredByPlacementAgent || isPlacementAgent}">
                        <table>
                            <tr>
                                <th>Placement Agent Information</th>
                            </tr>
                            <tr>
                                <td>
                                    <label>Placement Agent Name</label>
                                    <p><apex:outputText value="{!placementAgentName}" styleClass="text"/></p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Phone</label>
                                    <p><apex:outputField id="AgentPhone" value="{!placementAgent.Phone}"  styleClass="text"/></p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Email</label>
                                    <p><apex:outputField id="AgentEmail" value="{!placementAgent.Email}"  styleClass="text"/></p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Street</label>
                                    <p><apex:outputField id="AgentStreet" value="{!placementAgent.MailingStreet}" styleClass="text" style="width: 250px;" /></p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>City</label>
                                    <p><apex:outputField id="AgentCity" value="{!placementAgent.MailingCity}" styleClass="text" /></p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>State/Region</label>
                                    <p><apex:outputField id="AgentState" value="{!placementAgent.MailingState}" styleClass="text" /></p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Postal Code</label>
                                    <p><apex:outputField id="AgentPostalCode" value="{!placementAgent.MailingPostalCode}" styleClass="text" /></p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Country</label>
                                    <p>
                                        <apex:outputField id="AgentCountry" value="{!placementAgent.MailingCountry}" styleClass="text" rendered="{!placementAgent.MailingCountry != null}"/>
                                        <apex:outputText value="{!emptyMessage}" rendered="{!placementAgent.MailingCountry == null}" styleClass="text" />
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <hr/>
                    </apex:outputPanel>
                    
                    <table>
                        <tr>
                            <th>Asset Class and Investment Type</th>
                        </tr>
                        <tr>
                            <td>
                                <label>Asset Class</label> 
                                <p><apex:outputField id="AssetClass" value="{!proposal.Admin_Asset_Class__c}" /></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Investment Type</label>
                                <p><apex:outputField id="InvestmentType" value="{!proposal.Admin_Type__c}" /></p>
                            </td>
                        </tr>
                    </table>
                    <hr/>
                    <table>
                        <tr>
                            <th>Firm Information</th>
                        </tr>
                        <tr>
                            <td>
                                <label>Partner or Proposing Entity Name</label>
                                <p><apex:outputField id="AccountName" value="{!submittorAccount.Name}"  style="width: 550px;" styleClass="text"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Web site</label>
                                <p>
                                    <apex:outputField value="{!submittorAccount.Website}" styleClass="text" rendered="{!submittorAccount.Website != null}"/>
                                    <apex:outputText value="{!emptyMessage}" styleClass="text" rendered="{!submittorAccount.Website == null}"/>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Fax</label>
                                <p>
                                    <apex:outputField id="Fax" value="{!submittorAccount.Fax}"  rendered="{!submittorAccount.Fax != null}" styleClass="text"/>
                                    <apex:outputText value="{!emptyMessage}"  rendered="{!submittorAccount.Fax == null}" styleClass="text"/>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Street</label>
                                <p><apex:outputField id="Street" value="{!submittorAccount.BillingStreet}" style="width: 250px;"  styleClass="text"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>City</label>
                                <p><apex:outputField id="City" value="{!submittorAccount.BillingCity}"  styleClass="text"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>State</label>
                                <p><apex:outputField id="State" value="{!submittorAccount.BillingState}"  styleClass="text"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Postal Code</label>
                                <p><apex:outputField id="PostalCode" value="{!submittorAccount.BillingPostalCode}"   styleClass="text"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Country</label>
                                <p>
                                    <apex:outputField value="{!submittorAccount.BillingCountry}" styleClass="text" rendered="{!submittorAccount.BillingCountry != null}"/>
                                    <apex:outputText value="{!emptyMessage}" styleClass="text" rendered="{!submittorAccount.BillingCountry == null}"/>
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label>Submitter Contact Name</label>
                                <p><apex:outputText value="{!submitterName}" styleClass="text"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Title</label>
                                <p><apex:outputField id="SubmitterTitle" value="{!submitter.Title}"  styleClass="text"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Phone</label>
                                <p><apex:outputField id="SubmitterPhone" value="{!submitter.Phone}"  styleClass="text"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Email</label>
                                <p><apex:outputField id="SubmitterEmail" value="{!submitter.Email}"  styleClass="text"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Firm Primary Contact Name</label>
                                <p><apex:outputText value="{!submittorName}" styleClass="text"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Title</label>
                                <p><apex:outputField id="Title" value="{!submittor.Title}"  styleClass="text"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Phone</label>
                                <p><apex:outputField id="Phone" value="{!submittor.Phone}"  styleClass="text"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Email</label>
                                <p><apex:outputField id="Email" value="{!submittor.Email}"  styleClass="text"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Does your firm, or the firm you represent, have an existing relationship with CalPERS?</label>
                                <apex:outputField styleClass="text" value="{!submittorAccount.Do_You_Have_Existing_Relationship__c}" rendered="{!submittorAccount.Do_You_Have_Existing_Relationship__c != null}" />
                                <apex:outputText styleClass="text" value="{!emptyMessage}" rendered="{!submittorAccount.Do_You_Have_Existing_Relationship__c == null}" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <apex:outputPanel rendered="{!renderPriorRelationshipClasses}" style="width: 100%;">
                                    <label>If so, through which asset class(es)?</label>
                                    <apex:repeat value="{!SelectedExistingRelAssetClasses}" var="mso">
                                        <p><apex:outputText value="{!mso}" /></p>
                                    </apex:repeat>
                                </apex:outputPanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Firm Inception Date</label>
                                <p><apex:outputText id="FirmInception" value="{!ahelper.inceptionDate}" styleClass="text"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Assets Under Management (USD Millions- One billion entered as 1000)</label>
                                <p><apex:outputText id="Aum" value="{!ohelper.AssetUnderMgmt}"  styleClass="text"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Brief Firm History</label>
                                <p><apex:outputText id="BriefHistory" value="{!proposal.Brief_Firm_History__c}" style="width: 550px;" styleClass="text"/></p>
                            </td>
                        </tr>
                    </table>
                    <hr/>
                    <table>
                        <tr>
                            <th>Diversity Definitions and Questionnaire</th>
                        </tr>
                        <tr>                    


                            <tr>
                            <td>
                                <label>Please indicate if your firm meets the definition referenced above of a Diverse Investment Management Business.</label>
                                <p><apex:outputField id="DiverseBusiness" value="{!submittorAccount.Diverse_Investment_Management_Business__c}" /></p>
                            </td>
                        </tr>
                            <tr>
                            <td>
                                <label>Is your firm physically headquartered in the United States or its trust territories?</label>
                                <p><apex:outputField id="HQinUS" value="{!submittorAccount.HQ_in_US__c}" /></p>
                            </td>
                        </tr>
                            <tr>
                            <td>
                                <label>Percent of the firm owned by women employees</label>
                                <p>
                                    <apex:outputField id="PctWomenOwned" value="{!submittorAccount.Range_Women_Owned__c}"  rendered="{!submittorAccount.Range_Women_Owned__c != null}" styleClass="text"/>
                                    <apex:outputText value="{!emptyMessage}"  rendered="{!submittorAccount.Range_Women_Owned__c == null}" styleClass="text"/>
                                </p>
                            </td>
                        </tr>           
                            <tr>
                            <td>
                                <label>African American</label>
                                <p>
                                    <apex:outputField id="AfricanAmerican" value="{!submittorAccount.Range_African_American__c}"  rendered="{!submittorAccount.Range_African_American__c != null}" styleClass="text"/>
                                    <apex:outputText value="{!emptyMessage}"  rendered="{!submittorAccount.Range_African_American__c == null}" styleClass="text"/>
                                </p>
                            </td>
                        </tr>                        
                            <tr>
                            <td>
                                <label>Asian American</label>
                                <p>
                                    <apex:outputField id="AsianAmerican" value="{!submittorAccount.Range_Asian_American__c}"  rendered="{!submittorAccount.Range_Asian_American__c != null}" styleClass="text"/>
                                    <apex:outputText value="{!emptyMessage}"  rendered="{!submittorAccount.Range_Asian_American__c == null}" styleClass="text"/>
                                </p>
                            </td>
                        </tr>                       
                            <tr>
                            <td>
                                <label>Hispanic American</label>
                                <p>
                                    <apex:outputField id="HispanicAmerican" value="{!submittorAccount.Range_Hispanic_American__c}"  rendered="{!submittorAccount.Range_Hispanic_American__c != null}" styleClass="text"/>
                                    <apex:outputText value="{!emptyMessage}"  rendered="{!submittorAccount.Range_Hispanic_American__c == null}" styleClass="text"/>
                                </p>
                            </td>
                        </tr>                        
                            <tr>
                            <td>
                                <label>Native American</label>
                                <p>
                                    <apex:outputField id="NativeAmerican" value="{!submittorAccount.Range_Native_American__c}"  rendered="{!submittorAccount.Range_Native_American__c != null}" styleClass="text"/>
                                    <apex:outputText value="{!emptyMessage}"  rendered="{!submittorAccount.Range_Native_American__c == null}" styleClass="text"/>
                                </p>
                            </td>
                        </tr>                        
                            <tr>
                            <td>
                                <label>Other</label>
                                <p>
                                    <apex:outputField id="OtherTitle" value="{!submittorAccount.Range_Other_Title__c}"  rendered="{!submittorAccount.Range_Other_Title__c != null}" styleClass="text"/>
                                    <apex:outputText value="{!emptyMessage}"  rendered="{!submittorAccount.Range_Other_Title__c == null}" styleClass="text"/>
                                    <apex:outputField id="OtherRange" value="{!submittorAccount.Range_Other__c}"  rendered="{!submittorAccount.Range_Other__c != null}" styleClass="text"/>
                                    <apex:outputText value="{!emptyMessage}"  rendered="{!submittorAccount.Range_Other__c == null}" styleClass="text"/>
                                </p>
                            </td>
                        </tr>   

                            <tr>
                            <td>
                                <label>Total percent owned combining gender, race, and ethnicity</label>
                                <p>
                                    <apex:outputField id="PctTotal" value="{!submittorAccount.Range_Diversity__c}"  rendered="{!submittorAccount.Range_Diversity__c != null}" styleClass="text"/>
                                    <apex:outputText value="{!emptyMessage}"  rendered="{!submittorAccount.Range_Diversity__c == null}" styleClass="text"/>
                                </p>
                            </td>
                        </tr>                        
                    </table>
                    
                    <hr/>
                    <table>
                        <tr>
                            <th>Investment Opportunity Information</th>
                        </tr>
                        <tr>
                            <td>
                                <label>Investment/Project Name</label>
                                <p><apex:outputField id="OpptyName" value="{!proposal.Name}" style="width: 550px;" /></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Total Estimated Deal Size in USD (In Millions- One billion entered as 1000)</label>
                                <p><apex:outputText value="{!proposal.Total_Deal_Size_USD_In_Millions__c}" styleClass="text"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Select Currency to be used for fields below</label>
                                <p><apex:outputField id="Currency" value="{!proposal.Local_Currency__c}" /></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Total Estimated Deal Size in Local Currency (In Millions- One billion entered as 1000)</label>
                                <p><apex:outputText value="{!proposal.Total_Deal_Size_In_Millions__c}" styleClass="text"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Offering</label>
                                <apex:repeat value="{!SelectedStrategyOptions}" var="mso">
                                    <p><apex:outputText value="{!mso}" /></p>
                                </apex:repeat>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>CA Focus</label>
                                <p><apex:outputField id="CAFocus" value="{!proposal.CA_Focus__c}" /></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Geographic Focus</label>
                                <apex:repeat value="{!selectedGeoFocusOptions}" var="mso">
                                    <p><apex:outputText value="{!mso}" /></p>
                                </apex:repeat>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>If Developed OECD ex US or Less Developed, please specify Country Name</label>
                                <p><apex:outputText id="IfDevelopedSpecifyCountryName" value="{!proposal.Country_Name__c}" /></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Industry Focus</label>
                                <apex:repeat value="{!SelectedIndustryFocusOptions}" var="mso"> 
                                    <p><apex:outputText value="{!mso}" /></p>
                                </apex:repeat>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Concise Description of Proposal/Opportunity</label>
                                <p><apex:outputText id="ExecutiveSummary" value="{!proposal.Executive_Summary__c}" style="width: 550px;"  styleClass="text"/></p>
                            </td>
                        </tr>
                    </table>
                    <hr/>
                    <table>
                        <tr>
                            <th>Investment Terms</th>
                        </tr>
                        <tr>
                            <td>
                                <label>Please Describe Fees, if any</label>
                                <p>
                                    <apex:outputField value="{!proposal.Other_Fee_Narrative__c}" rendered="{!proposal.Other_Fee_Narrative__c != null}" styleClass="text"/>
                                    <apex:outputText value="{!emptyMessage}" rendered="{!proposal.Other_Fee_Narrative__c == null}" styleClass="text"/>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Target Return</label>
                                <p><apex:outputField id="PreferredReturn" value="{!proposal.Preferred_Return__c}"  styleClass="text"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Anticipated Final Closing Date</label>
                                <p><apex:outputField id="AnticipatedFinalClosingDate" value="{!proposal.Anticipated_Fund_Final_Closing_Date__c}"  styleClass="text"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Proposed CalPERS Investment (In Millions- One billion entered as 1000)</label>
                                <apex:outputText value="{!ohelper.proposedInvestment}" styleClass="text"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Proposed Holding Period (In Years)</label>
                                <p><apex:outputText value="{!ohelper.proposedHoldingPeriod}" styleClass="text"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Equity Amount (In Millions- One billion entered as 1000)</label>
                                <p><apex:outputText value="{!ohelper.equity}" styleClass="text"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>If Equity, Percent Equity Interest being offered (Percentage)</label>
                                <p><apex:outputText value="{!ohelper.ownershipPercent}" styleClass="text"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>LTM Revenue (In Millions- One billion entered as 1000)</label>
                                <p><apex:outputText value="{!ohelper.LTMRevenue}" styleClass="text"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>LTM EBITDA (In Millions- One billion entered as 1000)</label>
                                <p><apex:outputText value="{!ohelper.LTMEBITDA}" styleClass="text"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Enterprise Value (In Millions- One billion entered as 1000)</label>
                                <p><apex:outputText value="{!ohelper.totalValue}" styleClass="text"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Leverage (Percentage)</label>
                                <p><apex:outputText value="{!ohelper.leverage}" styleClass="text"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Additional Information</label>
                                <p>
                                    <apex:outputText value="{!proposal.Timing_Considerations__c}" rendered="{!proposal.Timing_Considerations__c != null}" style="width: 550px;" styleClass="text" />
                                    <apex:outputText value="{!emptyMessage}" rendered="{!proposal.Timing_Considerations__c == null}" styleClass="text" />
                                </p>
                            </td>
                        </tr>
                    </table>
                    <hr/>
                    <table>
                        <tr><th>Attachments</th></tr>
                        <tr>
                            <td>
                                <label>Do you have a PPM or OM</label>
                                <p>
                                    <apex:outputField value="{!proposal.Do_you_have_a_PPM_or_OM__c}" rendered="{!proposal.Do_you_have_a_PPM_or_OM__c != null}" styleClass="text"/>
                                    <apex:outputText value="{!emptyMessage}" rendered="{!proposal.Do_you_have_a_PPM_or_OM__c == null}" styleClass="text"/>
                                </p>
                            </td>
                        </tr>
                        <tr>    
                            <td>
                                <label>Do you have any Presentation Materials</label>
                                <p>
                                    <apex:outputField value="{!proposal.Do_you_have_any_presentation_materials__c}" rendered="{!proposal.Do_you_have_any_presentation_materials__c != null}" styleClass="text"/>
                                    <apex:outputText value="{!emptyMessage}" rendered="{!proposal.Do_you_have_any_presentation_materials__c == null}" styleClass="text"/>
                                </p>
                            </td>
                        </tr>
                        <tr>    
                            <td>
                                <apex:outputPanel rendered="{!NOT(attachManager.hasAttachments)}" style="width: 100%">
                                    <p>No attachment(s) added to proposal.</p>
                                </apex:outputPanel>
                                <apex:outputPanel rendered="{!attachManager.hasAttachments}" style="width: 100%">
                                    <label>Files Attached</label>
                                    <apex:repeat value="{!attachManager.attachments}" var="currAttachment">
                                        <p><apex:outputText value="{!currAttachment.fileName}"/></p>
                                    </apex:repeat>
                                </apex:outputPanel>
                            </td>
                        </tr>
                    </table>
                </apex:form>
            </apex:outputPanel>
        </div>
    </body>
</html>     
</apex:page>