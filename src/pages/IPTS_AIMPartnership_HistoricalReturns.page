<apex:page cache="false" name="IPTS_AIMPartnership_HistoricalReturns" id="thisPage" standardStylesheets="false" controller="InvestmentSubmissionController" title="Investment Proposal::{!pageTitle}" showHeader="false" sidebar="false">
<html lang="en">
    <head><title>Investment Proposal::{!pageTitle}</title>

    <apex:stylesheet value="{!URLFOR($Resource.IPTS_Styles, '/ipts/css/reset.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_Styles, '/ipts/css/style.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_CustomStyles)}"/>
    <script src="{!$Resource.IPTS_Script}" type="text/javascript"/> 
    <script type="text/javascript">
        function validateFields()
        {
            var fieldIdsArray = new Array();
            fieldIdsArray[0] = new FieldTuple("thisPage:thisForm:GrossMultiple", "Gross Multiple");
            fieldIdsArray[1] = new FieldTuple("thisPage:thisForm:NetMultiple", "Net Multiple");
            fieldIdsArray[2] = new FieldTuple("thisPage:thisForm:GrossIRR", "Gross IRR");
            fieldIdsArray[3] = new FieldTuple("thisPage:thisForm:NetIRR", "Net IRR");
            fieldIdsArray[4] = new FieldTuple("thisPage:thisForm:NetDPI", "Net DPI");
            
            return validatePercentFields(fieldIdsArray);
        }
    </script>
    </head>
    <body>
        <div id="mainContent">
            <br/>
            <apex:image url="{!URLFOR($Resource.logo)}" width="201" height="46" alt="CalPERS Logo"  />
            <br/>
            <h1>Investment Proposal Submission</h1>

            <apex:outputPanel id="proposalPanel">
                <apex:outputPanel rendered="{!hrManager.ehandler.hasErrors}">
                    <div id="error-message" class="error">
                        <h3>Please correct the highlighted form fields before continuing</h3>
                        <ol>
                            <apex:repeat value="{!hrManager.ehandler.errors}" var="e">
                                <li>
                                    <a href="#{!e.fieldId}" onclick="document.getElementById('{!e.fieldId}').focus(); return false;">{!e.fieldLabel}</a> - {!e.pageMessage}
                                </li>
                            </apex:repeat>
                        </ol>
                    </div>
                </apex:outputPanel>

                <ul class="legend">
                    <li><em>Required</em><span class="required">*</span></li>
                </ul>
                <apex:form id="thisForm" styleClass="cmxform" target="_self">
                    <fieldset>
                        <legend><h3>Prior Fund(s) Return Details</h3></legend>
   <br/>
        <div >
<table>
<tr><td  align="center" style="width:400px"><h3>Performance Quarter Requirements</h3></td></tr>
<tr><td>&nbsp;</td></tr> 
<tr><td>Please submit the performance data indicated based on your date of submission. </td></tr> 
<tr><td>&nbsp;</td></tr> 
<tr><td>For evaluation purposes, please provide performance information for the last three fund investments, if applicable. </td></tr> 
<tr><td>&nbsp;</td></tr> 
<tr><td>    
    <table summary="Table provides information on performance data required based on your date of submission" 
    style="border-bottom: solid black 2px; border-top: solid black 2px; border-left: solid black 2px; border-right: solid black 2px; width:450px; ">
                        <tr>
                              <th id="h1" scope="col" align=center style="width:200px; border-bottom: solid black 1px; border-right: solid black 1px;"> Date of Submission</th>
                              <th id="h2" scope="col" align=center style="width:250px; border-bottom: solid black 1px;"> Performance Quarters</th>
                        </tr>
                        <tr>
                              <td id="row1" scope="row" headers="h1" align=center style="width:200px; border-right: solid black 1px;">January 1 - March 31</td>
                              <td headers="row1 h2" align=center style="width:250px">Q2 and Q3 (of prior year)</td>
                        </tr>
                        <tr>
                              <td id="row2" scope="row" headers="h1"  align=center style="width:200px; border-right: solid black 1px;">April 1 - June 30</td>
                              <td headers="row2 h2"  align=center style="width:250px">Q3 and Q4 (of prior year)</td>
                        </tr>
                        <tr>
                              <td id="row3" scope="row" headers="h1"  align=center style="width:200px; border-right: solid black 1px;">July 1 - September 30</td>
                              <td headers="row3 h2" align=center style="width:250px">Q4 (of prior year) and Q1 (of current year)</td>
                        </tr>
                        <tr>
                              <td id="row4" scope="row" headers="h1"  align=center style="width:200px; border-right: solid black 1px;">October 1 - December 31</td>
                              <td headers="row4 h2"  align=center style="width:250px">Q1 and Q2 (of current year)</td>
                        </tr>
                  </table>
</td></tr>
<tr><td>&nbsp;</td></tr> 
<tr><td>Example: A submission date of December 11th requires performance information from Q1 and Q2 of the current year.</td></tr> 
</table>
        </div>

                    <br/>
                        <ol>
                            <li>
                                <apex:outputPanel rendered="{!hrManager.hasReturns}" style="width: 100%">
                                    <table summary="Prior fund(s) information to be included in the investment proposal" cellspacing="0" border="0" width="100%">
                                        <caption>Prior fund(s) information to be included in the investment proposal</caption>
                                        <tr>
                                            <th width="30%" id="h1" scope="col" style="border-bottom: 1px solid black;">Fund Name</th>
                                            <th width="20%" id="h5" scope="col" style="border-bottom: 1px solid black;">Reporting Date</th>
                                            <th width="10%" id="h2" scope="col" style="border-bottom: 1px solid black;">Vintage Year</th>
                                            <th width="10%" id="h3" scope="col" style="border-bottom: 1px solid black;">Size</th>
                                            <th width="20%" id="h4" scope="col" style="border-bottom: 1px solid black;">Action</th>
                                        <tr>
                                        <apex:repeat value="{!hrManager.historicalReturns}" var="hr">
                                            <tr>
                                                <td width="30%" id="thisPage:thisForm:{!hr.listPosition}" headers="h1" scope="row"><p>{!hr.areturn.Name}</p></td>
                                                <td width="20%" headers="thisPage:thisForm:{!hr.listPosition} h5"><p>   
                                                <apex:outputText value="{0,date,MM'/'dd'/'yyyy}">
                                                 <apex:param value="{!hr.areturn.As_of_Date__c}" />
                                                </apex:outputText>
                                                </p></td>
                                                <td width="10%" headers="thisPage:thisForm:{!hr.listPosition} h2"><p>{!hr.areturn.Vintage_Year__c}</p></td>
                                                <td width="10%" headers="thisPage:thisForm:{!hr.listPosition} h3"><p>{!hr.areturn.Size__c}</p></td>
                                                <td width="20%" headers="thisPage:thisForm:{!hr.listPosition} h4">
                                                    <apex:commandLink action="{!hrManager.editHistoricalReturn}">Edit
                                                        <apex:param name="return.index" value="{!hr.listPosition}" />
                                                    </apex:commandLink>&nbsp;|&nbsp;
                                                    <apex:commandLink action="{!hrManager.removeHistoricalReturn}" onClick="return confirm('Do you want to remove this return from the proposal?');">Remove
                                                        <apex:param name="return.index" value="{!hr.listPosition}" />
                                                    </apex:commandLink>
                                                </td>
                                            </tr>
                                        </apex:repeat>
                                    </table>
                                </apex:outputPanel>
                                <apex:outputPanel rendered="{!NOT(hrManager.hasReturns)}" style="width: 100%;">
                                    <p>No prior fund return(s) added to proposal.</p>
                                </apex:outputPanel>
                            </li>
                            <li>
                                <apex:commandLink value="Add Prior Fund Return" action="{!hrManager.newHistoricalReturn}" immediate="true" rendered="{!hrManager.isListContext}"/>
                            </li>
                        </ol>
                    </fieldset>
                    <apex:outputPanel rendered="{!hrManager.isAddContext || hrManager.isEditContext}">
                        <fieldset>

                            <legend>Add/Edit Prior Fund Return</legend>
                            <ol>
                                <li class="{!IF(hrManager.ehandler.hrlog.nameError,'error', '')}">
                                    <label for="thisPage:thisForm:Name"><em>Fund Name</em><span class="required">*</span></label>
                                    <apex:inputField id="Name" value="{!hrManager.historicalReturn.areturn.Name}" style="width: 550px;" 
                                        required="false" styleClass="text"/>
                                    <apex:outputPanel rendered="{!hrManager.ehandler.hrlog.nameError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li class="{!IF(hrManager.ehandler.hrlog.AsOfDateError || hrManager.ehandler.hrlog.asOfDateFormatError 
                                            || hrManager.ehandler.hrlog.asOfDateQuarterError2 || hrManager.ehandler.hrlog.asOfDateQuarterError,'error', '')}">
                                    <label for="thisPage:thisForm:AsOfDate"><em>Please identify the Reporting Date for the performance information entered (3/31,6/30,9/30,12/31)</em><span class="required">*</span></label>
                                    <apex:inputText id="AsOfDate" value="{!hrManager.historicalReturn.wrapperAsOfDate}" styleClass="text short" required="false"/>
                                    <p><label for="thisPage:thisForm:AsOfDate">mm/dd/yyyy</label></p>
                                    <apex:outputPanel rendered="{!hrManager.ehandler.hrlog.asOfDateFormatError}" style="width: 100%;">
                                        <p class="error">{!invalidDateFormatMessage}</p>
                                    </apex:outputPanel>
                                    <apex:outputPanel rendered="{!hrManager.ehandler.hrlog.asOfDateQuarterError}" style="width: 100%;">
                                        <p class="error">{!invalidDateQuarterMessage}</p>
                                    </apex:outputPanel>
                                    <apex:outputPanel rendered="{!hrManager.ehandler.hrlog.asOfDateQuarterError2}" style="width: 100%;">
                                        <p class="error">{!invalidDateQuarterMessage2}</p>
                                    </apex:outputPanel>
                                    <apex:outputPanel rendered="{!hrManager.ehandler.hrlog.asOfDateError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li class="{!IF(hrManager.ehandler.hrlog.sizeError,'error', '')}">
                                    <label for="thisPage:thisForm:Size"><em>Size (In Millions)</em><span class="required">*</span></label>
                                    <apex:inputField id="Size" value="{!hrManager.historicalReturn.areturn.Size__c}" required="false" 
                                        styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                                    <apex:outputPanel rendered="{!hrManager.ehandler.hrlog.sizeError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li class="{!IF(hrManager.ehandler.hrlog.vintageYearError,'error', '')}">
                                    <label for="thisPage:thisForm:VintageYear"><em>Vintage Year</em><span class="required">*</span></label>
                                    <apex:inputField id="VintageYear" value="{!hrManager.historicalReturn.areturn.Vintage_Year__c}" 
                                        required="false" styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                                    <apex:outputPanel rendered="{!hrManager.ehandler.hrlog.vintageYearError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li class="{!IF(hrManager.ehandler.hrlog.numberOfPortfolioError,'error', '')}">
                                    <label for="thisPage:thisForm:NumberOfPortfolio"><em>Number of Portfolio Companies</em><span class="required">*</span></label>
                                    <apex:inputField id="NumberOfPortfolio" value="{!hrManager.historicalReturn.areturn.No_of_Portfolio_Companies__c}" 
                                        required="false" styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                                    <apex:outputPanel rendered="{!hrManager.ehandler.hrlog.numberOfPortfolioError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li class="{!IF(hrManager.ehandler.hrlog.committedCapitalError,'error', '')}">
                                    <label for="thisPage:thisForm:CommittedCapital"><em>Committed Capital (In Millions- One billion entered as 1000)</em><span class="required">*</span></label>
                                    <apex:inputField id="CommittedCapital" value="{!hrManager.historicalReturn.areturn.Committed_Capital_In_Millions__c}" 
                                        required="false" styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                                    <apex:outputPanel rendered="{!hrManager.ehandler.hrlog.committedCapitalError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li class="{!IF(hrManager.ehandler.hrlog.contributedCapitalError,'error', '')}">
                                    <label for="thisPage:thisForm:ContributedCapital"><em>Contributed Capital (In Millions- One billion entered as 1000)</em><span class="required">*</span></label>
                                    <apex:inputField id="ContributedCapital" value="{!hrManager.historicalReturn.areturn.Contributed_Capital_In_Millions__c}" 
                                        required="false" styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                                    <apex:outputPanel rendered="{!hrManager.ehandler.hrlog.contributedCapitalError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li class="{!IF(hrManager.ehandler.hrlog.investedCapitalError,'error', '')}">
                                    <label for="thisPage:thisForm:InvestedCapital"><em>Invested Capital (In Millions- One billion entered as 1000)</em><span class="required">*</span></label>
                                    <apex:inputField id="InvestedCapital" value="{!hrManager.historicalReturn.areturn.Invested__c}" required="false" 
                                        styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                                    <apex:outputPanel rendered="{!hrManager.ehandler.hrlog.investedCapitalError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li class="{!IF(hrManager.ehandler.hrlog.distributedValueError,'error', '')}">
                                    <label for="thisPage:thisForm:DistributedValue"><em>Distributed Value (In Millions- One billion entered as 1000)</em><span class="required">*</span></label>
                                    <apex:inputField id="DistributedValue" value="{!hrManager.historicalReturn.areturn.Distributed_Value_In_Millions__c}" 
                                        required="false" styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                                    <apex:outputPanel rendered="{!hrManager.ehandler.hrlog.distributedValueError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li class="{!IF(hrManager.ehandler.hrlog.unrealizedValueError,'error', '')}">
                                    <label for="thisPage:thisForm:UnrealizedValue"><em>Unrealized Value (In Millions- One billion entered as 1000)</em><span class="required">*</span></label>
                                    <apex:inputField id="UnrealizedValue" value="{!hrManager.historicalReturn.areturn.Unrealized_In_Millions__c}" 
                                        required="false" styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                                    <apex:outputPanel rendered="{!hrManager.ehandler.hrlog.unrealizedValueError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li class="{!IF(hrManager.ehandler.hrlog.totalValueError,'error', '')}">
                                    <label for="thisPage:thisForm:TotalValue"><em>Total Value (In Millions- One billion entered as 1000)</em><span class="required">*</span></label>
                                    <apex:inputField id="TotalValue" value="{!hrManager.historicalReturn.areturn.Total_Value_In_Millions__c}" 
                                        required="false" styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                                    <apex:outputPanel rendered="{!hrManager.ehandler.hrlog.totalValueError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li class="{!IF(hrManager.ehandler.hrlog.realizedValueError,'error', '')}">
                                    <label for="thisPage:thisForm:RealizedValue"><em>Realized Value (In Millions- One billion entered as 1000)</em><span class="required">*</span></label>
                                    <apex:inputField id="RealizedValue" value="{!hrManager.historicalReturn.areturn.Realized_In_Millions__c}" 
                                        required="false" styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                                    <apex:outputPanel rendered="{!hrManager.ehandler.hrlog.realizedValueError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li class="{!IF(hrManager.ehandler.hrlog.grossMultipleError,'error', '')}">
                                    <label for="thisPage:thisForm:GrossMultiple"><em>Gross Multiple</em><span class="required">*</span></label>
                                    <apex:inputField id="GrossMultiple" value="{!hrManager.historicalReturn.areturn.Gross_Multiple__c}" 
                                        required="false" styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                                    <apex:outputPanel rendered="{!hrManager.ehandler.hrlog.grossMultipleError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li class="{!IF(hrManager.ehandler.hrlog.netMultipleError,'error', '')}">
                                    <label for="thisPage:thisForm:NetMultiple"><em>Net Multiple</em><span class="required">*</span></label>
                                    <apex:inputField id="NetMultiple" value="{!hrManager.historicalReturn.areturn.Net_Multiple__c}" 
                                        required="false" styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                                    <apex:outputPanel rendered="{!hrManager.ehandler.hrlog.netMultipleError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li class="{!IF(hrManager.ehandler.hrlog.grossIrrError,'error', '')}">
                                    <label for="thisPage:thisForm:GrossIRR"><em>Gross IRR (Percentage)</em><span class="required">*</span></label>
                                    <apex:inputField id="GrossIRR" value="{!hrManager.historicalReturn.areturn.Gross_IRR__c}" 
                                        required="false" styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                                    <apex:outputPanel rendered="{!hrManager.ehandler.hrlog.grossIrrError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li class="{!IF(hrManager.ehandler.hrlog.netIrrError,'error', '')}">
                                    <label for="thisPage:thisForm:NetIRR"><em>Net IRR (Percentage)</em><span class="required">*</span></label>
                                    <apex:inputField id="NetIRR" value="{!hrManager.historicalReturn.areturn.Net_IRR__c}" required="false" 
                                        styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                                    <apex:outputPanel rendered="{!hrManager.ehandler.hrlog.netIrrError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li class="{!IF(hrManager.ehandler.hrlog.netDPIError,'error', '')}">
                                    <label for="thisPage:thisForm:NetDPI"><em>Net DPI </em><span class="required">*</span></label>
                                    <apex:inputField id="NetDPI" value="{!hrManager.historicalReturn.areturn.Net_DPI__c}" required="false" 
                                        styleClass="text short" onkeypress="return validateChar2(event, this);"/>
                                    <apex:outputPanel rendered="{!hrManager.ehandler.hrlog.netDPIError}" style="width: 100%;">
                                        <p class="error">{!inputRequiredMessage}</p>
                                    </apex:outputPanel>
                                </li>
                                <li class="{!IF(hrManager.ehandler.hrlog.explReturnsSizeError,'error', '')}">
                                    <label for="thisPage:thisForm:Explanation">Explanation of Returns - 1000 character limit</label>
                                    <apex:inputTextArea id="Explanation" value="{!hrManager.historicalReturn.areturn.Explanation_of_Returns__c}" 
                                        style="width: 550px; height:175px;" styleClass="text"/>
                                    <apex:outputPanel rendered="{!hrManager.ehandler.hrlog.explReturnsSizeError}" style="width: 100%;">
                                        <p class="error">{!sizeExceededMessage}</p>
                                    </apex:outputPanel>
                                </li>

                            </ol>
                            <ol class="buttons">
                                <li><apex:commandLink value="Add Another Historical Return" action="{!hrManager.addMoreAIMHistoricalReturn}" onClick="return validateFields();"/></li>
                                <li><apex:commandLink value="OK" action="{!hrManager.saveAIMHistoricalReturn}" onClick="return validateFields();"/></li>
                                <li><apex:commandLink value="Previous" action="{!hrManager.cancelHistoricalReturn}" immediate="true"/></li>
                            </ol>
                        </fieldset>
                    </apex:outputPanel>
                    <ol class="buttons">
                        <li><apex:commandButton value="Next" action="{!next}" rendered="{!hrManager.isListContext}" styleClass="btn btn-primary btn-sm"/></li>
                        <li><apex:commandLink value="Previous" action="{!previous}" immediate="true" rendered="{!hrManager.isListContext}"/></li>
                        <li><apex:commandButton value="Save and Exit" action="{!saveAndExit}" rendered="{!hrManager.isListContext}" styleClass="btn btn-primary btn-sm" onClick="return confirm('{!JSENCODE(saveAlert)}');" /></li>
                        <li><apex:commandLink value="Cancel" action="{!cancel}" immediate="true" target="_top" rendered="{!hrManager.isListContext}" onClick="return confirm('{!JSENCODE(cancelAlert)}');" /></li>
                    </ol>
                    <div class="spacer"></div><div class="spacer"></div>
                    <apex:actionFunction name="clearSession" action="{!cancel}" immediate="true" />
                </apex:form>
            </apex:outputPanel>

        </div>
        
        <script src="{!$Resource.IPTS_Resize}" type="text/javascript"/>
        <script src="{!$Resource.IPTS_Session}" type="text/javascript"/>
        <script type="text/javascript" >
            <apex:outputPanel layout="none" rendered="{!dupeExists}" >
                alert('{!JSENCODE(dupeAlert)}');
            </apex:outputPanel>
            <apex:outputPanel layout="none" rendered="{!!dupeExists && isSaveAndContinue}" >
                top.location.href = "{!cancelURL}";
            </apex:outputPanel>
        </script>
    </body>
    <DIV class="footer text-center">
      <SPAN class="tagline">We serve those who serve California.</SPAN><BR/>© 
        Copyright 2015 California Public Employees' Retirement System (CalPERS)
        <BR/><BR/>
    </DIV>
 </html> 
    
</apex:page>