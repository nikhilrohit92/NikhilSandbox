<apex:page cache="false" name="IPTS_AssetClass" id="thisPage" controller="InvestmentSubmissionController" title="Investment Proposal" showHeader="false" sidebar="false" standardStylesheets="false">

<html lang="en">

<HEAD> 
<TITLE>Investment Proposal Submission - CalPERS</TITLE> 
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_Styles, '/ipts/css/reset.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_Styles, '/ipts/css/style.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_CustomStyles)}"/>
    <script src="{!$Resource.IPTS_Script}" type="text/javascript"/>
</HEAD>       
    <body>
        <div id="mainContent">
            <br/>
            <apex:image url="{!URLFOR($Resource.logo)}" width="201" height="46" alt="CalPERS Logo"  />
            <br/>
            <h1>Investment Proposal Submission</h1>
            
            <apex:outputPanel id="proposalPanel">
            
                <apex:outputPanel rendered="{!ehandler.hasErrors}">
                    <div id="error-message" class="error">
                        <h3>Please correct the highlighted form fields before continuing</h3>
                        <ol>
                            <apex:repeat value="{!ehandler.errors}" var="e">
                                <li>
                                    <a href="#{!e.fieldId}" onclick="document.getElementById('{!e.fieldId}').focus(); return false;">{!e.fieldLabel}</a> - {!e.pageMessage}
                                </li>
                            </apex:repeat>
                        </ol>
                    </div>
                </apex:outputPanel>
                
                <ul class="legend">
                    <li><em>Required</em><span class="required">*</span></li>
                </ul>
                
                <apex:form id="thisForm" styleClass="cmxform" target="_self">
                    <fieldset>
                        <legend>Prior Confirmation Number</legend>
                        <ol> 
                            <li>
                                <label for="thisPage:thisForm:PriorConfirmationNumber">If you are resubmitting an opportunity, what is your prior confirmation number?</label>
                                <apex:inputField id="PriorConfirmationNumber" value="{!proposal.Prior_Confirmation_Number__c}" styleClass="text short" onkeypress="return validateChar(event);"/>
                            </li>
                        </ol>
                    </fieldset>
                    
                    <fieldset>
                        <legend>Asset Class and Investment Type</legend>
                        <p>Please make sure that you review the asset class descriptions. To return to the Asset Class Description page, please click here:  &nbsp;<apex:outputLink value="https://www.calpers.ca.gov/page/investments/asset-classes" target="_blank" title="Asset Class Descriptions open in a new window">Asset Class Descriptions <span class="newwindow" style="display: none;">opens in a new window</span><apex:image id="theImagenc3" value="{!$Resource.NewWindowIcon}" alt="opens in a new window"/></apex:outputLink></p>
                        <ol>
                            <li class="{!IF(ehandler.olog.assetClassError,'error', '')}">
                                <label for="thisPage:thisForm:AssetClass"><em>Asset Class</em><span class="required">*</span></label> 
                                <apex:actionRegion rendered="{!!hasSelectedAssetClass}" >
                                    <apex:selectList id="AssetClass" value="{!proposal.Admin_Asset_Class__c}" multiselect="false" size="1">
                                        <apex:selectOptions value="{!assetClasses}" />
                                        <apex:actionSupport event="onchange" action="{!setAssetClass}" rerender="typePanel, helpPanel" status="typePanelStatus"/>
                                    </apex:selectList>
                                </apex:actionRegion>
                                <apex:outputText id="AssetClassOutput" value="{!proposal.Admin_Asset_Class__c}" rendered="{!hasSelectedAssetClass}" />
                                <apex:outputPanel rendered="{!ehandler.olog.assetClassError}" style="width: 100%;">
                                    <p class="error">{!selectRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.typeError,'error', '')}">
                                <apex:outputPanel id="typePanel" style="width: 100%;">
                                    <apex:outputPanel rendered="{!InvestmentTypeRequired}" style="width: 100%;">
                                        <label for="thisPage:thisForm:InvestmentType"><em>Investment Type</em><span class="required">*</span></label>
                                        <apex:selectList id="InvestmentType" value="{!proposal.Admin_Type__c}" multiselect="false" size="1" rendered="{!!hasSelectedAssetClass}" >
                                            <apex:actionSupport event="onchange" rerender="helpPanel" />
                                            <apex:selectOptions value="{!investmentTypeOptions}" />
                                        </apex:selectList>
                                        <apex:outputText id="InvestmentTypeOutput" value="{!proposal.Admin_Type__c}" rendered="{!hasSelectedAssetClass}" />
                                        <apex:outputPanel rendered="{!ehandler.olog.typeError}" style="width: 100%;">
                                            <p class="error">{!selectRequiredMessage}</p>
                                        </apex:outputPanel>
                                    </apex:outputPanel>
                                </apex:outputPanel>
                            </li>
                        </ol>
                        <apex:outputPanel layout="block" id="helpPanel" >
                            <apex:repeat value="{!assetClassDescriptions}" var="assetDesc" >
                                <apex:outputpanel layout="block" rendered="{!assetDesc.Asset_Class__c == proposal.Admin_Asset_Class__c}" >
                                    <apex:outputLink target="blank" value="{!assetDesc.Printable_URL__c}" rendered="{!assetDesc.Printable_URL__c != null && (proposal.Admin_Type__c == null || ( proposal.Admin_Type__c != AIM_DIRECT_TYPE_NAME && proposal.Admin_Type__c != INFRASTRUCTURE_DIRECT_TYPE_NAME))}" >Preview Asset Class questions</apex:outputLink>
                                    <apex:outputLink target="blank" value="{!assetDesc.Printable_URL2__c}" rendered="{!assetDesc.Printable_URL2__c != null && ( proposal.Admin_Type__c == AIM_DIRECT_TYPE_NAME || proposal.Admin_Type__c == INFRASTRUCTURE_DIRECT_TYPE_NAME)}" >Preview Asset Class questions</apex:outputLink>
                                    <br/>
                                    <apex:outputLink target="blank" value="{!assetDesc.User_Manual_URL__c}" rendered="{!assetDesc.User_Manual_URL__c != null && (proposal.Admin_Type__c == null || ( proposal.Admin_Type__c != AIM_DIRECT_TYPE_NAME && proposal.Admin_Type__c != INFRASTRUCTURE_DIRECT_TYPE_NAME))}" >User Manual</apex:outputLink>
                                    <apex:outputLink target="blank" value="{!assetDesc.User_Manual_URL2__c}" rendered="{!assetDesc.User_Manual_URL2__c != null && ( proposal.Admin_Type__c == AIM_DIRECT_TYPE_NAME || proposal.Admin_Type__c == INFRASTRUCTURE_DIRECT_TYPE_NAME)}" >User Manual</apex:outputLink>
                                </apex:outputpanel>
                            </apex:repeat>
                        </apex:outputPanel>
                    </fieldset>
                    
                    <fieldset>
                        <legend>Investment Opportunity Source</legend>
                        <ol> 
                            <li class="{!IF(ehandler.olog.howDidYouHearAboutUsError,'error', '')}">
                                <label for="thisPage:thisForm:HowDidYouHearAboutUs"><em>Please tell us how you were referred to CalPERS:</em><span class="required">*</span></label>
                                <apex:selectList id="HowDidYouHearAboutUs" value="{!proposal.How_did_you_hear_about_us__c}" size="1">
                                    <apex:selectOptions value="{!howDidYouHearAboutUsOptions}" />
                                </apex:selectList>
                                <apex:outputPanel rendered="{!ehandler.olog.howDidYouHearAboutUsError}" style="width: 100%;">
                                    <p class="error">{!selectRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                        </ol>
                        <ol> 
                            <li class="{!IF(ehandler.olog.leadSourceError,'error', '')}">
                                <label for="thisPage:thisForm:LeadSource"><em>In an effort to better understand how investment opportunities are sourced, please select the appropriate option:</em><span class="required">*</span></label>
                                <apex:actionRegion >
                                    <apex:selectList id="LeadSource" value="{!proposal.LeadSource}" size="1">
                                        <apex:selectOptions value="{!leadSources}" />
                                    </apex:selectList>
                                </apex:actionRegion>
                                <apex:outputPanel rendered="{!ehandler.olog.leadSourceError}" style="width: 100%;">
                                    <p class="error">{!selectRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                        </ol>
                    </fieldset>
                    
                    <ol class="buttons">
                        <li><apex:commandButton value="Next" action="{!continueFromRoot}" styleClass="btn btn-primary btn-sm"/></li>
                        <li><apex:commandLink value="Cancel" action="{!cancel}" target="_top" immediate="true" onClick="return confirm('{!JSENCODE(cancelAlert)}');"/></li>
                    </ol>
                    <div class="spacer"></div>
                    <div class="spacer"></div>
                    <div class="spacer"></div>
                    <div class="spacer"></div>                    
                    <apex:actionFunction name="clearSession" action="{!cancel}" immediate="true" />
                </apex:form>
            </apex:outputPanel>

        </div>

        <script src="{!$Resource.IPTS_Resize}" type="text/javascript"/>
        <script src="{!$Resource.IPTS_Session}" type="text/javascript"/>
    </body>
    <DIV class="footer text-center">
      <SPAN class="tagline">We serve those who serve California.</SPAN><BR/>© 
        Copyright 2015 California Public Employees' Retirement System (CalPERS)
        <BR/><BR/>
    </DIV>
 </html>
</apex:page>