<apex:page cache="false" name="IPTS_ManagementTeam" id="thisPage" standardStylesheets="false" controller="InvestmentSubmissionController" title="Investment Proposal::{!pageTitle}" showHeader="false" sidebar="false">
<html lang="en">
    <head><title>Investment Proposal::{!pageTitle}</title>

    <apex:stylesheet value="{!URLFOR($Resource.IPTS_Styles, '/ipts/css/reset.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_Styles, '/ipts/css/style.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_CustomStyles)}"/>
    </head>
    <body>
        <div id="mainContent">
            <br/>
            <apex:image url="{!URLFOR($Resource.logo)}" width="201" height="46" alt="CalPERS Logo"  />
            <br/>
            <h1>Investment Proposal Submission</h1>

            <apex:outputPanel id="proposalPanel">
                
                <apex:outputPanel rendered="{!teamManager.ehandler.hasErrors}">
                    <div id="error-message" class="error">
                        <h3>Please correct the highlighted form fields before continuing</h3>
                        <ol>
                            <apex:repeat value="{!teamManager.ehandler.errors}" var="e">
                                <li>
                                    <a href="#{!e.fieldId}" onclick="document.getElementById('{!e.fieldId}').focus(); return false;">{!e.fieldLabel}</a> - {!e.pageMessage}
                                </li>
                            </apex:repeat>
                        </ol>
                    </div>
                </apex:outputPanel>

                <ul class="legend">
                    <li><em>Required</em><span class="required">*</span></li>
                </ul>
                
                <apex:form id="thisForm" styleClass="cmxform" target="_self">
                    <fieldset>
                        <legend>Opportunity Management Team</legend>
                        <ol>
                            <li>
                                <apex:outputPanel rendered="{!teamManager.hasMembers}" style="width: 100%">
                                    <table summary="Opportunity Management Team to be included in the proposal" border="0" cellspacing="0" width="100%">
                                        <caption>Management team to be included in the investment proposal</caption>
                                        <tr>
                                            <th width="27%" id="h1" scope="col" style="border-bottom: 1px solid black;">First Name</th>
                                            <th width="27%" id="h2" scope="col" style="border-bottom: 1px solid black;">Last Name</th>
                                            <th width="26%" id="h3" scope="col" style="border-bottom: 1px solid black;">Title</th>
                                            <th width="20%" id="h4" scope="col" style="border-bottom: 1px solid black;">Action</th>
                                        </tr>
                                        <apex:repeat value="{!teamManager.team}" var="m">
                                            <tr>
                                                <td width="27%" id="{!m.contactRef.FirstName}{!m.contactRef.LastName}" headers="h1" scope="row"><p>{!m.contactRef.FirstName}</p></td>
                                                <td width="27%" headers="{!m.contactRef.FirstName}{!m.contactRef.LastName} h2"><p>{!m.contactRef.LastName}</p></td>
                                                <td width="26%" headers="{!m.contactRef.FirstName}{!m.contactRef.LastName} h3"><p>{!m.contactRef.Title}</p></td>
                                                <td width="20%" headers="{!m.contactRef.FirstName}{!m.contactRef.LastName} h4">
                                                    <p>
                                                        <apex:commandLink action="{!teamManager.editMember}" >Edit
                                                            <apex:param name="member.index" value="{!m.listPosition}" />
                                                        </apex:commandLink>&nbsp;|&nbsp;
                                                        <apex:commandLink action="{!teamManager.removeMember}" immediate="true" onClick="return confirm('Do you want to remove this contact from the proposal?');">Remove
                                                            <apex:param name="member.index" value="{!m.listPosition}" />
                                                        </apex:commandLink>
                                                    </p>
                                                </td>
                                            </tr>
                                        </apex:repeat>
                                    </table>
                                </apex:outputPanel>
                                <apex:outputPanel rendered="{!NOT(teamManager.hasMembers)}" style="width: 100%;">
                                    <p>No Opportunity Management Team member(s) added to proposal.</p>
                                </apex:outputPanel>
                            </li>
                            <li>
                                <apex:commandLink value="Add Opportunity Management Team Member" action="{!teamManager.newMember}" immediate="true" rendered="{!teamManager.isListContext}"/>
                            </li>
                        </ol>
                    </fieldset>
                    <apex:outputPanel layout="block" id="editPanel" >
                        <apex:outputPanel rendered="{!teamManager.isAddContext || teamManager.isEditContext}" style="width: 100%;"  >
                            <fieldset>
                                <legend>Add/Edit Member Information</legend>
                                <ol>
                                    <li class="{!IF(teamManager.ehandler.clog.firstNameError || teamManager.ehandler.clog.lastNameError,'error', '')}">
                                        <fieldset>
                                            <legend class="{!IF(isIe,'useaslabel', '')}"><em>Member Name</em><span class="required">*</span></legend>
                                            <ol>
                                                <li>
                                                    <span style="width: 13em;">
                                                        <apex:selectList id="Salutation" value="{!teamManager.member.contactRef.Salutation}" multiselect="false" size="1">
                                                            <apex:selectOptions value="{!salutationOptions}" />
                                                        </apex:selectList>
                                                        <p><label for="{!$Component.Salutation}">Salutation</label></p>
                                                    </span>
                                                    <span>
                                                        <apex:inputField id="FirstName" value="{!teamManager.member.contactRef.FirstName}" required="false" styleClass="text medium"/>
                                                        <p><label for="{!$Component.FirstName}">First Name</label></p>
                                                    </span>
                                                    <span>
                                                        <apex:inputField id="LastName" value="{!teamManager.member.contactRef.LastName}" required="false" styleClass="text medium"/>
                                                        <p><label for="{!$Component.LastName}">Last Name</label></p>
                                                    </span>
                                                    <apex:outputPanel rendered="{!teamManager.ehandler.clog.firstNameError || teamManager.ehandler.clog.lastNameError}" style="width: 100%;">
                                                        <p class="error">{!inputRequiredMessage}</p>
                                                    </apex:outputPanel>
                                                </li>
                                            </ol>
                                        </fieldset>
                                    </li>
                                    <li class="{!IF(teamManager.ehandler.clog.titleError || teamManager.ehandler.clog.titleSizeError,'error', '')}">
                                        <label for="thisPage:thisForm:Title"><em>Title - 40 character limit</em><span class="required">*</span></label>
                                        <apex:inputField id="Title" value="{!teamManager.member.contactRef.Title}" required="false" styleClass="text medium"/>
                                        <apex:outputPanel rendered="{!teamManager.ehandler.clog.titleError}" style="width: 100%;">
                                            <p class="error">{!inputRequiredMessage}</p>
                                        </apex:outputPanel>
                                        <apex:outputPanel rendered="{!teamManager.ehandler.clog.titleSizeError}" style="width: 100%;">
                                            <p class="error">{!titleSizeExceededMessage}</p>
                                        </apex:outputPanel>
                                    </li>
                                    <li class="{!IF(teamManager.ehandler.clog.phoneError,'error', '')}">
                                        <label for="thisPage:thisForm:Phone"><em>Phone</em><span class="required">*</span></label>
                                        <apex:inputField id="Phone" value="{!teamManager.member.contactRef.Phone}" required="false" styleClass="text medium"/>
                                        <apex:outputPanel rendered="{!teamManager.ehandler.clog.phoneError}" style="width: 100%;">
                                            <p class="error">{!inputRequiredMessage}</p>
                                        </apex:outputPanel>
                                    </li>
                                    <li>
                                        <label for="thisPage:thisForm:Fax">Fax</label>
                                        <apex:inputField id="Fax" value="{!teamManager.member.contactRef.Fax}" required="false" styleClass="text medium"/>
                                    </li>
                                    <li class="{!IF(teamManager.ehandler.clog.emailError || teamManager.ehandler.clog.emailFormatError,'error', '')}">
                                        <label for="thisPage:thisForm:Email"><em>Email</em><span class="required">*</span></label>
                                        <apex:inputText id="Email" value="{!teamManager.emailString}" styleClass="text medium"/>
                                        <apex:outputPanel rendered="{!teamManager.ehandler.clog.emailError}" style="width: 100%;">
                                            <p class="error">{!inputRequiredMessage}</p>
                                        </apex:outputPanel>
                                        <apex:outputPanel rendered="{!teamManager.ehandler.clog.emailFormatError}" style="width: 100%;">
                                            <p class="error">{!invalidEmailMessage}</p>
                                        </apex:outputPanel>
                                    </li>
                                    
                                    <li class="{!IF(teamManager.ehandler.clog.streetError || teamManager.ehandler.clog.streetSizeError,'error', '')}">
                                        <label for="thisPage:thisForm:Street"><em>Street - 255 character limit</em><span class="required">*</span></label>
                                        <apex:inputTextArea id="Street" value="{!teamManager.member.contactRef.MailingStreet}" style="width: 300px; height: 40px;" required="false" styleClass="text"/>
                                        <apex:outputPanel rendered="{!teamManager.ehandler.clog.streetError}" style="width: 100%;">
                                            <p class="error">{!inputRequiredMessage}</p>
                                        </apex:outputPanel>
                                        <apex:outputPanel rendered="{!teamManager.ehandler.clog.streetSizeError}" style="width: 100%;">
                                            <p class="error">{!streetSizeExceededMessage}</p>
                                        </apex:outputPanel>
                                    </li>
                                    <li class="{!IF(teamManager.ehandler.clog.cityError,'error', '')}">
                                        <label for="thisPage:thisForm:City"><em>City</em><span class="required">*</span></label>
                                        <apex:inputField id="City" value="{!teamManager.member.contactRef.MailingCity}" required="false" styleClass="text medium"/>
                                        <apex:outputPanel rendered="{!teamManager.ehandler.clog.cityError}" style="width: 100%;">
                                            <p class="error">{!inputRequiredMessage}</p>
                                        </apex:outputPanel>
                                    </li>
                                    <li class="{!IF(teamManager.ehandler.clog.stateError,'error', '')}">
                                        <label for="thisPage:thisForm:State"><em>State</em><span class="required">*</span></label>
                                        <apex:inputField id="State" value="{!teamManager.member.contactRef.MailingState}" required="false" styleClass="text medium"/>
                                        <apex:outputPanel rendered="{!teamManager.ehandler.clog.stateError}" style="width: 100%;">
                                            <p class="error">{!inputRequiredMessage}</p>
                                        </apex:outputPanel>
                                    </li>
                                    <li class="{!IF(teamManager.ehandler.clog.postalCodeError,'error', '')}">
                                        <label for="thisPage:thisForm:PostalCode"><em>Postal Code</em><span class="required">*</span></label>
                                        <apex:inputField id="PostalCode" value="{!teamManager.member.contactRef.MailingPostalCode}" required="false"  styleClass="text medium"/>
                                        <apex:outputPanel rendered="{!teamManager.ehandler.clog.postalCodeError}" style="width: 100%;">
                                            <p class="error">{!inputRequiredMessage}</p>
                                        </apex:outputPanel>
                                    </li>
                                    <li class="{!IF(teamManager.ehandler.clog.countryError,'error', '')}">
                                        <label for="thisPage:thisForm:Country"><em>Country</em><span class="required">*</span></label>
                                        <apex:inputField id="Country" value="{!teamManager.member.contactRef.MailingCountry}" styleClass="text medium"/>
                                        <apex:outputPanel rendered="{!teamManager.ehandler.clog.countryError}" style="width: 100%;">
                                            <p class="error">{!inputRequiredMessage}</p>
                                        </apex:outputPanel>
                                    </li>
                                </ol>
                                <ol class="buttons">
                                    <li><apex:commandLink value="Add Another Opportunity Management Team Member" action="{!teamManager.addMoreMember}" /></li>
                                    <li><apex:commandLink value="OK" action="{!teamManager.addMember}" /></li>
                                    <li><apex:commandLink value="Previous" action="{!teamManager.cancelMember}" immediate="true"/></li>
                                </ol>
                            </fieldset>
                        </apex:outputPanel>
                    </apex:outputPanel>
                    <ol class="buttons">
                        <li><apex:commandButton value="Next" action="{!next}" rendered="{!teamManager.isListContext}" styleClass="btn btn-primary btn-sm"/></li>
                        <li><apex:commandLink value="Previous" action="{!previous}" immediate="true" rendered="{!teamManager.isListContext}" /></li>
                        <li><apex:commandLink value="Cancel" action="{!cancel}" target="_top" immediate="true" rendered="{!teamManager.isListContext}" onClick="return confirm('{!JSENCODE(cancelAlert)}');" /></li>
                    </ol>
                    <div class="bigspacer"></div><div class="spacer"></div><div class="spacer"></div><div class="spacer"></div>
                    <apex:actionFunction name="clearSession" action="{!cancel}" immediate="true" />
                </apex:form>
            </apex:outputPanel>

        </div>
        
        <script src="{!$Resource.IPTS_Resize}" type="text/javascript"/>
        <script src="{!$Resource.IPTS_Session}" type="text/javascript"/>
    </body>
    <DIV class="footer text-center">
      <SPAN class="tagline">We serve those who serve California.</SPAN><BR/>© 
        Copyright 2015 California Public Employees' Retirement System (CalPERS)
        <BR/><BR/>
    </DIV>
 </html> 
    
</apex:page>