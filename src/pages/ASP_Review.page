<apex:page cache="false" name="ASP_Review" id="thisPage" controller="InvestmentSubmissionController" title="ASP_Review" showHeader="false" sidebar="false" standardStylesheets="false">
<html lang="en">
    <head><title>PrintASP</title>
    
    <apex:stylesheet value="{!URLFOR($Resource.IPTS2_Styles, '/ipts-2/css/style2.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_CustomStyles)}"/>
    <script src="{!$Resource.IPTS_Script}" type="text/javascript"/>
    </head>
    <body>
        <div id="ackContent">
            <br/>
            <apex:image url="{!URLFOR($Resource.logo)}" width="201" height="46" alt="CalPERS Logo"  />
            <br/>
            <h1><apex:outputText id="name"  value="{!tempCampaignName}"  /></h1>
            

            <apex:outputPanel rendered="{!ehandler.hasErrors}">
                <div id="error-message" class="error">
                    <h3>Please correct the highlighted form fields before continuing</h3>
                    <ol>
                        <apex:repeat value="{!ehandler.errors}" var="e">
                            <li>
                                <a href="#{!e.fieldId}" onclick="document.getElementById('{!e.fieldId}').focus(); return false;">{!e.fieldLabel}</a> - {!e.pageMessage}
                            </li>
                        </apex:repeat>
                    </ol>
                </div>
            </apex:outputPanel>
            
                <apex:dynamicComponent componentValue="{!report}"/>
            
            <apex:outputPanel id="proposalPanel">
                <apex:form id="thisForm" styleClass="cmxform" target="_self">
                    <fieldset>
                        <legend>Attachments</legend>
                        <ol>
                            <li>
                                <label><h3>Special Instructions: Please attach the following documentation</h3></label>
                                <p>* Each attached file is limited to a 20 MB file size. Attachment file name is limited to 80 characters.</p>
                            </li>
                            <li>
                                <fieldset>
                                    <ol>
                                        <li>
                                            <apex:outputPanel rendered="{!NOT(attachManager.hasAttachments)}" style="width: 100%">
                                                <p>No attachment(s) added to proposal.</p>
                                            </apex:outputPanel>
                                        </li>
                                        <apex:outputPanel rendered="{!attachManager.hasAttachments}"  >
                                            <fieldset>
                                                <label for="thisPage:thisForm:FileName"><b>Files Attached</b></label>
                                                <ol>
                                                <apex:repeat value="{!attachManager.attachments}" var="currAttachment">
                                                    <li>
                                                        <p><apex:outputText id="FileName" value="{!currAttachment.fileName}" style="width: 300%"/></p>
                                                    </li>
                                                </apex:repeat>
                                                </ol>
                                            </fieldset>
                                        </apex:outputPanel>
                                    </ol>
                                </fieldset>
                            </li>
                        </ol>
                    </fieldset>
         
                    <fieldset>
                        <legend><em>Acknowledgment</em><span class="required">*</span></legend>
                        <p><label for="thisPage:thisForm:confirmChbx">You acknowledge that you have read the <a href="https://calpers.secure.force.com/IPTS_Disclosures" target="_newwindow" >Notes on Confidentiality</a>  and understand that CalPERS is subject to disclosure laws, regulations, and policies that may require disclosure of some or all of the information you have submitted for consideration by CalPERS.</label></p>
                        <ol>
                            <li class="{!IF(ehandler.confirmHasError,'error', '')}">
                                <apex:inputCheckBox id="confirmChbx" value="{!reviewConfirm}" styleClass="checkbox" title="I have read and understand the above information"/>
                                <label for="thisPage:thisForm:confirmChbx" class="inlinelabel">I have read and understand the above information</label>
                                <apex:outputPanel rendered="{!ehandler.confirmHasError}" style="width: 100%;">
                                    <p class="error">{!ehandler.confirmError.fieldMessage}</p>
                                </apex:outputPanel>
                            </li>
                        </ol>   
                    </fieldset>
                    <p>Note: Please click the “Submit Proposal” button only once to avoid duplicate entries. Please note it may take up to a minute for the page to refresh. You will have the opportunity to print your submitted proposal.  After clicking ‘Submit Proposal’, you will no longer be able to review or make changes to this proposal.</p>
                    <ol class="buttons">
                        <li><apex:commandButton value="Submit Proposal" action="{!submitProposal}" styleClass="btn btn-primary btn-sm"/></li>
                        <li><apex:commandButton value="Previous" action="{!previous}" immediate="true" styleClass="btn btn-primary btn-sm"/></li>
                         <li><apex:commandButton value="Save and Exit" action="{!saveAndExit}" styleClass="btn btn-primary btn-sm" onClick="return confirm('{!JSENCODE(saveAlert)}');" /></li>
                        <li><apex:commandButton value="Cancel" action="{!cancel}"  immediate="true" styleClass="btn btn-primary btn-sm" onClick="return confirm('{!JSENCODE(cancelAlert)}');" /></li>
                    </ol>
                    <div class="spacer"></div>
                    <apex:actionFunction name="clearSession" action="{!cancel}" immediate="true" />
                </apex:form>
            </apex:outputPanel>
        </div>
        
        <script src="{!$Resource.IPTS_Resize}" type="text/javascript"/>
        <script src="{!$Resource.IPTS_Session}" type="text/javascript"/>
        <script type="text/javascript" >
            <apex:outputPanel layout="none" rendered="{!dupeExists}" >
                alert('{!JSENCODE(dupeAlert)}');
            </apex:outputPanel>
            <apex:outputPanel layout="none" rendered="{!needDocuments}" >
                alert('{!JSENCODE(docAlert)}');            
            </apex:outputPanel>
            <apex:outputPanel layout="none" rendered="{!!dupeExists && isSaveAndContinue}" >
                top.location.href = "{!cancelURL}";
            </apex:outputPanel>
        </script>
        
    </body>
    <DIV class="footer text-center">
      <SPAN class="tagline">We serve those who serve California.</SPAN><BR/>© 
        Copyright 2015 California Public Employees' Retirement System (CalPERS)
        <BR/><BR/>
    </DIV>    
</html> 
</apex:page>