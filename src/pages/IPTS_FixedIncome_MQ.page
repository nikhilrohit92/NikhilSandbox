<apex:page cache="false" name="IPTS_GlobalEquity_MQ" id="thisPage" standardStylesheets="false" controller="InvestmentSubmissionController" title="Investment Proposal::{!pageTitle}" showHeader="false" sidebar="false" applyBodyTag="false" applyHtmlTag="false">
<html lang="en">
<head><title>Fixed Income Minimum Qualifications</title>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_Styles, '/ipts/css/reset.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_Styles, '/ipts/css/style.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.IPTS_CustomStyles)}"/>
    <script src="{!$Resource.IPTS_Script}" type="text/javascript"/><noscript><h3>This page uses JavaScript. Please enable JavaScript in your browser to use all features.</h3></noscript>
    </head>
    <body>
        <div id="mainContent">
            <br/>
            <apex:image url="{!URLFOR($Resource.logo)}" width="201" height="46" alt="CalPERS Logo"  />
            <br/>
            <h1>Investment Proposal Submission</h1>

            <apex:outputPanel id="proposalPanel">
                
                <apex:outputPanel rendered="{!ehandler.hasErrors}">
                    <div id="error-message" class="error">
                        <h3>Please correct the highlighted form fields before continuing</h3>
                        <ol>
                            <apex:repeat value="{!ehandler.errors}" var="e">
                                <li>
                                    <a href="#{!e.fieldId}" onclick="document.getElementById('{!e.fieldId}').focus(); return false;">{!e.fieldLabel}</a> - {!e.pageMessage}
                                </li>
                            </apex:repeat>
                        </ol>
                    </div>
                </apex:outputPanel>

                <ul class="legend">
                    <li><em>Required</em><span class="required">*</span></li>
                </ul>
                
                <apex:form id="thisForm" styleClass="cmxform" target="_self">
                    <fieldset>
                        <legend>Minimum Qualifications</legend>
                        <p>All proposals must meet all of the following Minimum Qualifications to be give further consideration. Failure to satisfy any of these qualifications and requirements will result in a rejection of the proposal.</p>
                        <ol>
                            <li class="{!IF(ehandler.olog.registerWithSecError,'error', '')}">
                                <label for="thisPage:thisForm:RegisterWithSEC"><em>Is your Firm a SEC-Registered Investment Advisor or exempt from Registration?</em><span class="required">*</span></label> 
                                <apex:selectList id="RegisterWithSEC" value="{!proposal.Registered_with_SEC__c}" multiselect="false" size="1">
                                    <apex:selectOptions value="{!yesNoOptions}" />
                                </apex:selectList>
                                <apex:outputPanel rendered="{!ehandler.olog.registerWithSecError}" style="width: 100%;">
                                    <p class="error">{!selectRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.managedAccountsError,'error', '')}">
                                <label for="thisPage:thisForm:ManagedAccounts"><em>Does your Firm Offer Separately Managed Accounts?</em><span class="required">*</span></label> 
                                <apex:selectList id="ManagedAccounts" value="{!proposal.Separately_Managed_Accounts__c}" multiselect="false" size="1">
                                    <apex:selectOptions value="{!yesNoOptions}" />
                                </apex:selectList>
                                <apex:outputPanel rendered="{!ehandler.olog.managedAccountsError}" style="width: 100%;">
                                    <p class="error">{!selectRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.fullSecurityLevelTransparencyError,'error', '')}">
                                <label for="thisPage:thisForm:FullSecurityLevelTransparency"><em>Is your Firm Willing to Provide Full Security Level Transparency?</em><span class="required">*</span></label> 
                                <apex:selectList id="FullSecurityLevelTransparency" value="{!proposal.Full_Security_Level_Transparency__c}" multiselect="false" size="1">
                                    <apex:selectOptions value="{!yesNoOptions}" />
                                </apex:selectList>
                                <apex:outputPanel rendered="{!ehandler.olog.fullSecurityLevelTransparencyError}" style="width: 100%;">
                                    <p class="error">{!selectRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.ftseIndexError,'error', '')}">
                                <label for="thisPage:thisForm:FTSEIndex"><em>Is your Firm willing to Manage Against a Custom Index as determined by CalPERS?</em><span class="required">*</span></label> 
                                <apex:selectList id="FTSEIndex" value="{!proposal.Manage_Against_FTSE_Index__c}" multiselect="false" size="1">
                                    <apex:selectOptions value="{!yesNoOptions}" />
                                </apex:selectList>
                                <apex:outputPanel rendered="{!ehandler.olog.ftseIndexError}" style="width: 100%;">
                                    <p class="error">{!selectRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <li class="{!IF(ehandler.olog.regulatorySanctionsError,'error', '')}">
                                <label for="thisPage:thisForm:RegulatorySanctions"><em>Does your Firm have any Regulatory Sanctions directly related to the team members or Strategy(s) submitted for consideration?</em><span class="required">*</span></label>
                                <apex:actionRegion > 
                                    <apex:selectList id="RegulatorySanctions" value="{!proposal.Regulatory_Sanctions__c}" multiselect="false" size="1">
                                        <apex:selectOptions value="{!yesNoOptions}" />
                                        <apex:actionSupport event="onchange" action="{!setRegulatorySanctions}" rerender="sanctionsPanel" status="sanctionsPanelStatus"/>
                                    </apex:selectList>
                                </apex:actionRegion>
                                <apex:outputPanel rendered="{!ehandler.olog.regulatorySanctionsError}" style="width: 100%;">
                                    <p class="error">{!selectRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <apex:outputPanel id="sanctionsPanel" style="width: 100%;">
                                <apex:outputPanel style="width: 100%;" rendered="{!regulatoryExplanationRequired}">
                                    <li class="{!IF(ehandler.olog.regulatorySanctionsExplanationError || ehandler.olog.regulatorySanctionsExplanationSizeError,'error', '')}">
                                        <label for="thisPage:thisForm:RegulatorySanctionsExplain"><em>Regulatory Sanctions Explanation - 1000 character limit</em><span class="required">*</span></label>
                                        <apex:inputTextArea id="RegulatorySanctionsExplain" value="{!proposal.Regulatory_Sanctions_Explanation__c}" style="width: 550px; height: 170px;" required="false" styleClass="text"/>
                                        <apex:outputPanel rendered="{!ehandler.olog.regulatorySanctionsExplanationError}" style="width: 100%;">
                                            <p class="error">{!inputRequiredMessage}</p>
                                        </apex:outputPanel>
                                        <apex:outputPanel rendered="{!ehandler.olog.regulatorySanctionsExplanationSizeError}" style="width: 100%;">
                                            <p class="error">Exceeded 1000 character limit.</p>
                                        </apex:outputPanel>
                                    </li>
                                </apex:outputPanel>
                            </apex:outputPanel>
                            <li class="{!IF(ehandler.olog.convictedOfFelonyError,'error', '')}">
                                <label for="thisPage:thisForm:ConvictedOfFelony"><em>Have any Members of the Firm’s key Personnel been convicted of a Felony?</em><span class="required">*</span></label>
                                <apex:actionRegion >  
                                    <apex:selectList id="ConvictedOfFelony" value="{!proposal.Personnel_Convicted_of_Felony__c}" multiselect="false" size="1">
                                        <apex:selectOptions value="{!yesNoOptions}" />
                                        <apex:actionSupport event="onchange" action="{!setFelonyConviction}" rerender="FelonyExplanationPanel" status="FelonyExplanationPanelStatus"/>
                                    </apex:selectList>
                                </apex:actionRegion>
                                <apex:outputPanel rendered="{!ehandler.olog.convictedOfFelonyError}" style="width: 100%;">
                                    <p class="error">{!selectRequiredMessage}</p>
                                </apex:outputPanel>
                            </li>
                            <apex:outputPanel id="FelonyExplanationPanel" style="width: 100%;">
                                <apex:outputPanel style="width: 100%;" rendered="{!felonyConvictionExplanationRequired}">
                                    <li class="{!IF(ehandler.olog.convictedOfFelonyExplanationError || ehandler.olog.convictedOfFelonyExplanationSizeError,'error', '')}">
                                        <label for="thisPage:thisForm:FelonyExplanation"><em>Conviction Explanation - 32000 character limit</em><span class="required">*</span></label>
                                        <apex:inputTextArea id="FelonyExplanation" value="{!proposal.Felony_Conviction_Explanation__c}" style="width: 550px; height: 170px;" required="false" styleClass="text"/>
                                        <apex:outputPanel rendered="{!ehandler.olog.convictedOfFelonyExplanationError}" style="width: 100%;">
                                            <p class="error">{!inputRequiredMessage}</p>
                                        </apex:outputPanel>
                                        <apex:outputPanel rendered="{!ehandler.olog.convictedOfFelonyExplanationSizeError}" style="width: 100%;">
                                            <p class="error">Exceeded 32000 character limit.</p>
                                        </apex:outputPanel>
                                    </li>
                                </apex:outputPanel>
                            </apex:outputPanel>
                        </ol>
                    </fieldset>
                    <ol class="buttons">
                        <li> 
                            <apex:commandButton value="Next" styleClass="btn btn-primary btn-sm" action="{!validateGlobalEquityMinimumQualifications}" />
                        </li>
                        <li>
                            <apex:commandLink value="Previous" action="{!previous}" immediate="true"/>
                        </li>
                        <li>
                            <apex:commandLink value="Cancel" action="{!cancel}" target="_top" immediate="true" onClick="return confirm('{!JSENCODE(cancelAlert)}');" />
                        </li>
                    </ol>
                    <div class="spacer"></div>
                    <div class="spacer"></div>
                    <div class="spacer"></div>
                    <apex:actionFunction name="clearSession" action="{!cancel}" immediate="true" />
                </apex:form>
            </apex:outputPanel>
        </div>
        
        <!-- 
        <script src="{!$Resource.IPTS_Resize}" type="text/javascript"/>
         -->
        <script type="text/javascript">
            var f1 = ('{!regulatoryExplanationRequired}' == 'false') ? 195 : 0;
            var f2 = ('{!felonyConvictionExplanationRequired}' == 'false') ? 195 : 0;
            if (document.all) {
                    h = document.body.scrollHeight + f1 + f2 + 75;
            } else {
                    h = document.body.offsetHeight + f1 + f2 + 75;
            }
            document.getElementById("grandChild").src = "https://www.calpers.ca.gov/jasper/investments/ipts/step3.jsp?height=" + h;
        </script>
        <script src="{!$Resource.IPTS_Session}" type="text/javascript"/>
    </body>
    <DIV class="footer text-center">
      <SPAN class="tagline">We serve those who serve California.</SPAN><BR/>© 
        Copyright 2015 California Public Employees' Retirement System (CalPERS)
        <BR/><BR/>
    </DIV>
 </html> 
    
</apex:page>